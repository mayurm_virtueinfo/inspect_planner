
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:inspect_planner/src/app_router.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/add_note_dialog_bloc.dart';
import 'package:inspect_planner/src/bloc/alert_bloc.dart';
import 'package:inspect_planner/src/bloc/attachment_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/cancel_sync_bloc.dart';
import 'package:inspect_planner/src/bloc/checklist_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/customer_bloc.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/last_sync_date_time_bloc.dart';
import 'package:inspect_planner/src/bloc/life_cycle_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/newsposts_bloc.dart';
import 'package:inspect_planner/src/bloc/next_task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/note_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/prev_task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/refresh_home_screen_bloc.dart';
import 'package:inspect_planner/src/bloc/refreshing/refreshing_bloc.dart';
import 'package:inspect_planner/src/bloc/show_checklist_tab.dart';
import 'package:inspect_planner/src/bloc/show_hide_sync_progress_bloc.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_list_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/bloc/task_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/pages/splash_screen.dart';
import 'package:inspect_planner/src/service/life_cycle_mgr.dart';
import 'package:inspect_planner/src/service/navigation_service.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => LifeCycleBloc());
  locator.registerLazySingleton(() => LoadingBloc());
  locator.registerLazySingleton(() => UserModelBloc());
  locator.registerLazySingleton(() => HomeTabBloc());
  locator.registerLazySingleton(() => DetailTabBloc());
  locator.registerLazySingleton(() => NewspostsBloc());
  locator.registerLazySingleton(() => TaskListBloc());
  locator.registerLazySingleton(() => TaskModelBloc());
  locator.registerLazySingleton(() => TaskIndexBloc());
  locator.registerLazySingleton(() => CancelSyncBloc());
  locator.registerLazySingleton(() => ShowHideChecklistTabBloc());
  locator.registerLazySingleton(() => TaskSyncCountBloc());
  locator.registerLazySingleton(() => AttachmentSyncCountBloc());
  locator.registerLazySingleton(() => ChecklistSyncCount());
  locator.registerLazySingleton(() => NoteSyncCountBloc());
  locator.registerLazySingleton(() => ShowHideSyncProgressBloc());
  locator.registerLazySingleton(() => LastSyncDateTimeBloc());
  locator.registerLazySingleton(() => IPKeys());
  locator.registerLazySingleton(() => CustomerBloc());
  locator.registerLazySingleton(() => RefreshingBloc());
  locator.registerLazySingleton(() => PrevTaskIndexBloc());
  locator.registerLazySingleton(() => NextTaskIndexBloc());
  locator.registerLazySingleton(() => RefreshHomeScreenBloc());
  locator.registerLazySingleton(() => AlertBloc());
  locator.registerLazySingleton(() => AddNoteDialogBloc());
}

GetIt locator = GetIt.instance;
UserModel appUserModel;
// List<NewspostsModel> appListNewspostsModel;

/*
* Local notification part and push notification part ----- Start
* */
/*
* Local notification part and push notification part ----- End
* */
Database appDatabase;
TaskModel appTaskModel ;
bool showCheckListTab=false;
int appTaskIndex=0;
String appLastSyncDateTime='';
List<dynamic> appTaskList= [];
bool appCancelSyncProcess=false;
CustomerModel appCustomer;
int appCurrentTab=1;

/// Run first apps open
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/LICENSE.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  setupLocator();
  /* Background push notification message handler */
  /* Initialize Local notification */
  locator<UserModelBloc>().userModelStream.listen((event) {
    appUserModel = event;
  });
  /*locator<NewspostsBloc>().newspostsStrem.listen((event) {
    // appListNewspostsModel = event;
  });*/
  locator<TaskModelBloc>().taskModelStream.listen((event) {
    appTaskModel = event;
  });
  locator<HomeTabBloc>().homeTabStream.listen((event) {
    appCurrentTab = event;
  });
  locator<ShowHideChecklistTabBloc>().showHideChecklistTabStream.listen((event) {
    showCheckListTab = event;
  });
  locator<TaskIndexBloc>().taskIndexStream.listen((event) {
    appTaskIndex = event;
  });
  locator<TaskListBloc>().taskListStream.listen((event) {
    appTaskList = event;
  });
  locator<LastSyncDateTimeBloc>().lastSyncDateTimeStream.listen((event) {
    appLastSyncDateTime = event;
  });

  locator<CancelSyncBloc>().cancelSyncStream.listen((event) {
    appCancelSyncProcess = event;
    if(appCancelSyncProcess){
      DialogMgr.hideSyncDialog();
    }
  });
  locator<CustomerBloc>().customerStream.listen((event) {
    appCustomer = event;
  });


  // if (await Permission.storage.request().isGranted) {
    // Either the permission was already granted before or the user just granted it.



  final dabasesPath = await getDatabasesPath();
  // var dabasesPath = await ExtStorage.getExternalStorageDirectory();
  final path = join(dabasesPath, "admin_inspectieplanner.db");
  debugPrint("database Path : ${path}");
  // Delete the database
  // await deleteDatabase(path);
// open the database
  debugPrint("creating tables -- start");
  appDatabase = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
    debugPrint("in---database");
    // When creating the db, create the table
    Batch batch = db.batch();
    batch.execute(AppTablesQuery.CREATE_TABLE_TASK);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_TASK}");
    batch.execute(AppTablesQuery.CREATE_TABLE_TASK_TYPE);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_TASK_TYPE}");
    batch.execute(AppTablesQuery.CREATE_TABLE_FUNCTIONS);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_FUNCTIONS}");
    batch.execute(AppTablesQuery.CREATE_TABLE_LOCATION);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_LOCATION}");
    batch.execute(AppTablesQuery.CREATE_TABLE_TASK_FREQUENCY);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_TASK_FREQUENCY}");
    batch.execute(AppTablesQuery.CREATE_TABLE_CHECKLISTITEM);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_CHECKLISTITEM}");
    batch.execute(AppTablesQuery.CREATE_TABLE_NOTE);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_NOTE}");
    batch.execute(AppTablesQuery.CREATE_TABLE_ATTACHMENT);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_ATTACHMENT}");
    batch.execute(AppTablesQuery.CREATE_TABLE_USER);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_USER}");
    batch.execute(AppTablesQuery.CREATE_TABLE_USERS_GROUPS);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_USERS_GROUPS}");
    batch.execute(AppTablesQuery.CREATE_TABLE_NEWSPOSTS);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_NEWSPOSTS}");
    batch.execute(AppTablesQuery.CREATE_TABLE_MESSAGE_VIEW_LOG);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_MESSAGE_VIEW_LOG}");
    batch.execute(AppTablesQuery.CREATE_TABLE_CUSTOMER);
    debugPrint("created ${AppTablesQuery.CREATE_TABLE_CUSTOMER}");
    List<dynamic> res = await batch.commit();
    debugPrint("Batch res : ${res.toString()}");
  });
  debugPrint("created tables -- start");


  runApp(App());
  // }else{
  //   debugPrint("Accept permission");
  // }
  /*runZonedGuarded(() {
  }, (error, stackTrace) {
    print('runZonedGuarded: Caught error in my root zone.');
  });*/
}

/// Set orienttation
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /// To set orientation always portrait
    SystemChrome.setPreferredOrientations([
      // DeviceOrientation.portraitUp,
      // DeviceOrientation.portraitDown,
      // DeviceOrientation.landscapeLeft,
      // DeviceOrientation.landscapeRight,
    ]);

    ///Set color status bar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return LifeCycleMgr(
      child: new MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate
        ],
        supportedLocales: [
          const Locale('en'),
          const Locale('nl')
        ],
        navigatorKey: locator<NavigationService>().navigatorKey,
        theme: ThemeData(
          fontFamily: 'HelveticaNeue',
          brightness: Brightness.light,
          backgroundColor: Colors.green,
          primaryColorLight: Colors.white,
          primaryColorBrightness: Brightness.light,
          primaryColor: Colors.white,
        ),
        debugShowCheckedModeBanner: false,

        /// Routes
        initialRoute: SplashScreen.routeName,
        onGenerateRoute: AppRouter.generateRoute,
      ),
    );
  }
}

/*

\"([^\"]*)\"

'([^']*)'

(["'])(?:(?=(\\?))\2.)*?\1

*/


/*
Follow step:
1. flutter build ios --release
2. pod deintegrate
3. pod install
4. flutter build ios --release*/
