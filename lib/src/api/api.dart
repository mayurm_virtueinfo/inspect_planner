class API{
  // static const String BASE = "http://18.118.153.253";
  // static const String BASE = "https://ip.dev.digidock.nl";
  // http://test.inspectieplanner.nl
  static const String BASE_LIVE = "https://app.inspectieplanner.nl"; //LIVE
  static const String BASE_TEST = "https://test.inspectieplanner.nl"; // TEST
  static const String BASE = BASE_TEST; // TEST
  // static const String BASE = BASE_LIVE; // BASE
  static const String POST_LOGIN = BASE+"/api/login";
  static const String POST_CHECK_2FA = BASE+"/api/check-2fa";
  static const String POST_SYNC_WEB_TO_APP = BASE+"/api/sync_web_to_app";
  static const String POST_ADD_TASK = BASE+"/api/add-task";
  static const String POST_SYNC_ADD_TASK = BASE+"/api/sync_add_task";
  static const String POST_SYNC_READ_MESSAGE = BASE+"/api/sync_read_message";
  static const String POST_SYNC_ADD_NOTE = BASE+"/api/sync_add_note";
  static const String POST_SYNC_DELETE_NOTE = BASE+"/api/sync_remove_note";
  static const String POST_ADD_NOTE = BASE+"/api/add-note";
  static const String POST_ADD_READ_MESSAGE = BASE+"/api/add-read-message";
  static const String POST_SYNC_UPDATE_CHECKLISTITEMS = BASE+"/api/sync_update_checklistitems";
  static const String POST_SYNC_ADD_CHECKLISTITEMS = BASE+"/api/add-checklist";
  static const String POST_UPDATE_NOTE = BASE+"/api/edit-note";
  static const String PUT_UPDATE_CHECKLIST_ANSWER = BASE+"/api/update-checklist-answer";
  static const String PUT_COMPLETE_TASK = BASE+"/api/complete-task";
  static const String POST_DELETE_NOTE = BASE+"/api/delete-note?id={id}";
  static const String POST_DELETE_ATTACHMENT = BASE+"/api/delete-attachment?id={id}";
  static const String POST_ADD_ATTACHMENT = BASE+"/api/add-attachment";
  static const String GET_SUPPORT_TEXT=BASE+"/api/support-text";
  static const String GET_LOGOUT=BASE+"/api/user-logout";
  static const String GET_TABLE_TASK=BASE+"/api/all-tasks";
  static const String GET_ORGANIZATION_CLIENT=BASE+"/api/organisation-client";
  static const String GET_ORGANIZATION_CLIENT_LOGIN=BASE+"/api/organisation-client-login?client_id={client_id}";
}
