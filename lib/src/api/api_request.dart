import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiRequest {
  static String API_LOG_TAG = 'API_App';

  static Future<Map<String, dynamic>> postLogin(
      {String username, String password}) async {
    String apiStr = API.POST_LOGIN;
    Map<String, String> params = Map();
    params['username'] = username;
    params['password'] = password;


//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - postLogin : ${apiStr}");
    debugPrint("$API_LOG_TAG - params postLogin  : ${params.toString()}");
    final response = await client.post(Uri.parse(apiStr),
        headers: <String, String>{
          "Content-Type": "application/x-www-form-urlencoded",
          "Content-type": "application/json"
        }, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result postLogin: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postCheck2FA(
      {String userId, String oneTimePassword}) async {

    String apiStr = API.POST_CHECK_2FA;
    Map<String, String> params = Map();
    params['user_id'] = userId;
    params['one_time_password'] = oneTimePassword;


//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - postCheck2FA : ${apiStr}");
    debugPrint("$API_LOG_TAG - params postCheck2FA  : ${params.toString()}");
    final response = await client.post(Uri.parse(apiStr),
        headers: <String, String>{
          "Content-Type": "application/x-www-form-urlencoded",
          "Content-type": "application/json"
        }, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result postCheck2FA: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }
//
  static Future<Map<String, dynamic>> postSyncWebToApp() async {
    String apiStr = API.POST_SYNC_WEB_TO_APP;
    Map<String, String> params = Map();
    // params['username'] = username;

//    FormData
    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json';
    var client = http.Client();
    debugPrint("$API_LOG_TAG - postSyncWebToApp : ${apiStr}");
    debugPrint(
        "$API_LOG_TAG - params postSyncWebToApp  : ${params.toString()}");
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result postSyncWebToApp: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> posAddTask(
      {Map<String, dynamic> mapAddTask}) async {
    String apiStr = API.POST_ADD_TASK;
    Map<String, dynamic> params = Map();
    await Future.forEach(mapAddTask.entries, (MapEntry entry) {
      params[entry.key] = entry.value ?? '';
    });

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';
    // "Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"

//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - posAddTask : ${apiStr}");
    debugPrint("$API_LOG_TAG - params posAddTask  : ${params.toString()}");
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result posAddTask: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postSyncAddTask(
      {Map<String, dynamic> mapAddTask}) async {
    String apiStr = API.POST_SYNC_ADD_TASK;
    Map<String, dynamic> params = Map();
    List<dynamic> listTask = mapAddTask['sync_add_task'];
    List<dynamic> listParamsTask = [];
    await Future.forEach(listTask, (element) async {
      Map<String, dynamic> newParams = Map();
      await Future.forEach(element.entries, (MapEntry entry) {
        newParams[entry.key] = entry.value ?? '';
      });
      debugPrint("$API_LOG_TAG - params listParamsTask  : ${newParams.toString()}");
      listParamsTask.add(newParams);
    });
    params['sync_add_task'] = listParamsTask;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';


//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - posSyncAddTask : ${apiStr}");
    debugPrint("$API_LOG_TAG - params posSyncAddTask  : ${params.toString()}");
    // return Future.value(params);
    final response = await client.post(Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result posSyncAddTask: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    // return Future.value(null);
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postSyncReadMessage(
      {Map<String, dynamic> mapReadMessage}) async {
    String apiStr = API.POST_SYNC_READ_MESSAGE;
    Map<String, dynamic> params = Map();
    List<dynamic> listTask = mapReadMessage['sync_read_message'];
    List<dynamic> listParamsTask = [];
    await Future.forEach(listTask, (element) async {
      Map<String, dynamic> newParams = Map();
      await Future.forEach(element.entries, (MapEntry entry) {
        newParams[entry.key] = entry.value ?? '';
      });
      listParamsTask.add(newParams);
    });
    params['sync_read_message'] = listParamsTask;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';


//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - postSyncReadMessage : ${apiStr}");
    debugPrint(
        "$API_LOG_TAG - params postSyncReadMessage  : ${params.toString()}");
    // return Future.value(params);
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result postSyncReadMessage: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postSyncAddNote(
      {Map<String, dynamic> mapAddNote}) async {
    String apiStr = API.POST_SYNC_ADD_NOTE;
    Map<String, dynamic> params = Map();
    List<dynamic> listNote = mapAddNote['sync_add_note'];
    List<dynamic> listParamsNote = [];
    await Future.forEach(listNote, (element) async {
      Map<String, dynamic> newParams = Map();
      await Future.forEach(element.entries, (MapEntry entry) {
        if (entry.key == 'removed' || entry.key == 'synced') {

        }
        else {
          newParams[entry.key] = entry.value ?? '';
        }
      });
      listParamsNote.add(newParams);
    });
    params['sync_add_note'] = listParamsNote;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - postSyncAddNote : ${apiStr}");
    debugPrint("$API_LOG_TAG - params postSyncAddNote  : ${params.toString()}");
    // return Future.value(params);
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result postSyncAddNote: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postSyncDeleteNote(
      {Map<String, dynamic> mapDeleteNote}) async {
    String apiStr = API.POST_SYNC_DELETE_NOTE;
    Map<String, dynamic> params = Map();
    List<dynamic> listDeleteNote = mapDeleteNote['sync_remove_note'];
    List<dynamic> listParamsDeleteNote = [];
    await Future.forEach(listDeleteNote, (element) async {
      Map<String, dynamic> newParams = Map();
      await Future.forEach(element.entries, (MapEntry entry) {
        if (entry.key == 'id') {
          newParams[entry.key] = entry.value ?? '';
        }
      });
      listParamsDeleteNote.add(newParams);
    });
    params['sync_remove_note'] = listParamsDeleteNote;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - postSyncDeleteNote : ${apiStr}");
    debugPrint(
        "$API_LOG_TAG - params postSyncDeleteNote  : ${params.toString()}");
    // return Future.value(params);
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result postSyncDeleteNote: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> posAddAttachment(
      {Map<String, dynamic> mapAddAttachment}) async {
    String apiStr = API.POST_ADD_ATTACHMENT;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';
    /*headers['Accept'] = 'application/json';
    headers['Content-type'] = 'application/json';
    headers['Content-type'] = 'application/x-www-form-urlencoded';*/

//    FormData

    var request = http.MultipartRequest('POST', Uri.parse(apiStr));
    request.headers.addAll(headers);

    await Future.forEach(mapAddAttachment.entries, (MapEntry entry) async {
      if (entry.key == 'image_name') {
        request.files.add(
            await http.MultipartFile.fromPath('image_name', entry.value));
      } else {
        request.fields[entry.key] = '${entry.value ?? ''}';
      }
    });

    debugPrint("$API_LOG_TAG - posAddAttachment : ${apiStr}");
    debugPrint("$API_LOG_TAG - params posAddAttachment  : ${request.fields
        .toString()}");
    debugPrint(
        "$API_LOG_TAG - files posAddAttachment  : ${request.files.toString()}");
    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    debugPrint("$API_LOG_TAG - result posAddAttachment: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> getSupportText({String keyword}) async {
    var client = http.Client();
    String apiStr = API.GET_SUPPORT_TEXT;
    debugPrint("$API_LOG_TAG - getSupportText : ${apiStr}");
    final response = await client.get(Uri.parse(apiStr));
    debugPrint("$API_LOG_TAG - result getSupportText : ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Future.value(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(AppStrings.errGettingSupportText);
    }
  }

  static Future<Map<String, dynamic>> logout() async {
    var client = http.Client();
    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Accept'] = 'application/json';
    headers['Content-type'] = 'application/json';
    String strAPI = API.GET_LOGOUT;
    print("API : logout : ${strAPI}");
    print("API : headers : ${headers.toString()}");
    try {
      final response = await client.get(Uri.parse(strAPI), headers: headers);
      final body = json.decode(response.body);
      debugPrint("$API_LOG_TAG - result logout : ${response.body}");
      debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
      return Future.value(body);
    } catch (e) {
      throw Exception(e.response);
    }
  }

  static Future<Map<String, dynamic>> getTaskTable() async {
    var client = http.Client();

    String page = '10';
    String limit = '3';


    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Accept'] = 'application/json';
    headers['Content-type'] = 'application/json';
    String strAPI = API.GET_TABLE_TASK; //+'?page=$page&limit=$limit';
    print("API : postTaskTable : ${strAPI}");
    print("API : headers : ${headers.toString()}");
    try {
      final response = await client.get(Uri.parse(strAPI), headers: headers);
      Map<String, dynamic> body = json.decode(response.body);
      bool status = body['status'];
      debugPrint(
          "$API_LOG_TAG - result postTaskTable : due to long response we are not logging it-1 : ");
      debugPrint("$API_LOG_TAG - statusCode : ${response.statusCode}");
      if(status){
        return Future.value(body);
      }else{
        throw Exception(body);
      }

    } catch (e) {
      throw Exception(e.response);
    }
  }

  static Future<Map<String, dynamic>> getOrganizationClientList(
      {String accessToken}) async {
    var client = http.Client();

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer $accessToken';
    headers['Accept'] = 'application/json';
    headers['Content-type'] = 'application/json';
    String strAPI = API.GET_ORGANIZATION_CLIENT;
    print("API : getOrganizationClientList : ${strAPI}");
    print("API : headers : ${headers.toString()}");
    try {
      final response = await client.get(Uri.parse(strAPI), headers: headers);
      final body = json.decode(response.body);
      debugPrint(
          "$API_LOG_TAG - result postTaskTable : due to long response we are not logging it-2 ");
      debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
      return Future.value(body);
    } catch (e) {
      throw Exception(e.response);
    }
  }

  static Future<Map<String, dynamic>> getClientLogin(
      {CustomerModel customerModel}) async {
    var client = http.Client();

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Accept'] = 'application/json';
    headers['Content-type'] = 'application/json';
    String strAPI = API.GET_ORGANIZATION_CLIENT_LOGIN;
    strAPI = strAPI.replaceAll('{client_id}', '${customerModel.id}');
    print("API : getClientLogin : ${strAPI}");
    print("API : headers : ${headers.toString()}");
    try {
      final response = await client.get(Uri.parse(strAPI), headers: headers);
      final body = json.decode(response.body);
      debugPrint(
          "$API_LOG_TAG - result postTaskTable : due to long response we are not logging it-3 ");
      debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
      return Future.value(body);
    } catch (e) {
      throw Exception(e.response);
    }
  }

  static Future<Map<String, dynamic>> posAddNote(
      {Map<String, dynamic> mapAddNote}) async {
    String apiStr = API.POST_ADD_NOTE;
    Map<String, dynamic> params = Map();
    await Future.forEach(mapAddNote.entries, (MapEntry entry) {
      params[entry.key] = entry.value ?? '';
    });

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';
    // "Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"

//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - posAddNote : ${apiStr}");
    debugPrint("$API_LOG_TAG - params posAddNote  : ${params.toString()}");
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result posAddNote: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postAddReadMessage(
      {Map<String, dynamic> mapAddReadMessage}) async {
    String apiStr = API.POST_ADD_READ_MESSAGE;
    Map<String, dynamic> params = Map();
    await Future.forEach(mapAddReadMessage.entries, (MapEntry entry) {
      params[entry.key] = entry.value ?? '';
    });

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';
    // "Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"

//    FormData

    var client = http.Client();
    debugPrint("$API_LOG_TAG - posAddReadMessage : ${apiStr}");
    debugPrint(
        "$API_LOG_TAG - params posAddReadMessage  : ${params.toString()}");
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result posAddReadMessage: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> putUpdateNote(
      {String note, int assignedToUserId, int noteId}) async {
    String apiStr = API.POST_UPDATE_NOTE;
    Map<String, dynamic> params = Map();
    params['note'] = note;
    params['assignedto_user_id'] = assignedToUserId;
    params['id'] = noteId;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - updateNote : ${apiStr}");
    debugPrint("$API_LOG_TAG - params updateNote  : ${params.toString()}");
    final response = await client.put(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result updateNote: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> putUpdateChecklistAnswer(
      {int id, String result}) async {
    String apiStr = API.PUT_UPDATE_CHECKLIST_ANSWER;
    Map<String, dynamic> params = Map();
    params['id'] = id;
    params['result'] = result;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - putUpdateChecklistAnswer : ${apiStr}");
    debugPrint("$API_LOG_TAG - params updatputUpdateChecklistAnswer : ${params
        .toString()}");
    final response = await client.put(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint(
        "$API_LOG_TAG - result putUpdateChecklistAnswer: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> putCompleteTask(
      {int id, int status_id, String finish_date, int force_completed=0}) async {
    String apiStr = API.PUT_COMPLETE_TASK;
    Map<String, dynamic> params = Map();
    params['id'] = id;
    params['status_id'] = status_id;
    params['finish_date'] = finish_date;
    params['force_completed'] = force_completed;

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - putCompleteputCompleteTask ${apiStr}");
    debugPrint("$API_LOG_TAG - params putCompleteTask : ${params.toString()}");
    final response = await client.put(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result putCompleteTask: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> deleteDeleteNote({int id}) async {
    String apiStr = API.POST_DELETE_NOTE;
    apiStr = apiStr.replaceAll('{id}', '$id');

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';
    // "Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"

    var client = http.Client();
    debugPrint("$API_LOG_TAG - deleteDeleteNote : ${apiStr}");
    final response = await client.delete(Uri.parse(apiStr), headers: headers);
    debugPrint("$API_LOG_TAG - result deleteDeleteNote: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> deleteDeleteAttachment({int id}) async {
    String apiStr = API.POST_DELETE_ATTACHMENT;
    apiStr = apiStr.replaceAll('{id}', '$id');

    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';
    // "Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"

    var client = http.Client();
    debugPrint("$API_LOG_TAG - deleteDeleteAttachment : ${apiStr}");
    final response = await client.delete(Uri.parse(apiStr), headers: headers);
    debugPrint(
        "$API_LOG_TAG - result deleteDeleteAttachment: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postSyncChecklistItems(
      {Map<String, dynamic> mapChecklistItems}) async {
    String apiStr = API.POST_SYNC_UPDATE_CHECKLISTITEMS;
    Map<String, dynamic> params = mapChecklistItems;


    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - posSyncChecklistItems : ${apiStr}");
    debugPrint(
        "$API_LOG_TAG - params posSyncChecklistItems  : ${params.toString()}");
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint("$API_LOG_TAG - result posSyncChecklistItems: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }

  static Future<Map<String, dynamic>> postSyncAddChecklistItems(
      {Map<String, dynamic> mapChecklistItems}) async {
    String apiStr = API.POST_SYNC_ADD_CHECKLISTITEMS;
    Map<String, dynamic> params = mapChecklistItems;


    Map<String, String> headers = Map();
    headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
    headers['Content-type'] = 'application/json; charset=UTF-8';

    var client = http.Client();
    debugPrint("$API_LOG_TAG - postSyncAddChecklistItems : ${apiStr}");
    debugPrint("$API_LOG_TAG - params postSyncAddChecklistItems  : ${params
        .toString()}");
    final response = await client.post(
        Uri.parse(apiStr), headers: headers, body: jsonEncode(params));
    debugPrint(
        "$API_LOG_TAG - result postSyncAddChecklistItems: ${response.body}");
    debugPrint("$API_LOG_TAG - status : ${response.statusCode}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);

      return Future.value(map);
    } else if (response.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      return Future.value(null);
    }
  }
}
