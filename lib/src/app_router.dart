import 'dart:convert';

import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/bloc/customer_bloc.dart';
import 'package:inspect_planner/src/bloc/last_sync_date_time_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/pages/home/detail_screen.dart';
import 'package:inspect_planner/src/pages/home/file_viewer_screen.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/pages/home/home_screen_organization.dart';
import 'package:inspect_planner/src/pages/home/image_viewer_screen.dart';
import 'package:inspect_planner/src/pages/login/login_screen.dart';
import 'package:inspect_planner/src/pages/no_routes_found.dart';
import 'package:inspect_planner/src/pages/splash_screen.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:inspect_planner/src/utils/app_preferences.dart';
import 'package:inspect_planner/src/utils/app_user_role.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/source.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    try {
      final args = settings.arguments;
      switch (settings.name) {
        case SplashScreen.routeName:
        // Map<String,dynamic> mapArg = Map();
          return MaterialPageRoute(builder: (_) => SplashScreen(/*mapArg: mapArg,*/));
        case LoginScreen.routeName:
          return MaterialPageRoute(builder: (_) => LoginScreen());
        case HomeScreen.routeName:
          return MaterialPageRoute(builder: (_) => HomeScreen());
        case DetailScreen.routeName:
          return MaterialPageRoute(builder: (_) => DetailScreen());
        case HomeScreenOrganication.routeName:
          return MaterialPageRoute(builder: (_) => HomeScreenOrganication());
        case ImageViewerScreen.routeName:
          List<dynamic> lstArgs = args;
          Source source = lstArgs[0];
          String url = lstArgs[1];
          return MaterialPageRoute(builder: (_) => ImageViewerScreen(source: source,url: url,));
        case FileViewerScreen.routeName:
          List<dynamic> lstArgs = args;
          Source source = lstArgs[0];
          String url = lstArgs[1];
          return MaterialPageRoute(builder: (_) => FileViewerScreen(source: source,url: url,));


      }
      return MaterialPageRoute(builder: (_) => NoRoutesFound());
    }catch(e){
      debugPrint("AppRouter.dart : line number 110 : ${e.toString()}");
      return MaterialPageRoute(builder: (_) => NoRoutesFound());
    }
  }

  static void navigateToLoginScreen(BuildContext context){
    Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
  }

  static Future<void> navigatorPage(BuildContext context) async{
    debugPrint("-----entry1");
    /*AppRouter.navigateToLoginScreen(context);

    return;*/
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString(AppPreferences.pref_user);
    debugPrint("userString : $userString");
    if(userString == null){
      AppRouter.navigateToLoginScreen(context);
      return;
    }
    Map<String,dynamic> userDetail=json.decode(userString);
    UserModel userModel = UserModel.fromMap(map: userDetail);
    debugPrint("-----entry1");
    if(userModel.expires_at!=null) {
      debugPrint("-----entry2");
      DateTime currentTime = DateTime.now();
      DateTime expiredTime = DateFormat('yyyy-MM-dd HH:mm:ss').parse(
          userModel.expires_at);
      debugPrint('currentTime : $currentTime');
      debugPrint('expiredTime : $expiredTime');
      if (currentTime.isAfter(expiredTime)/* || Utility.isSameDate(currentTime, expiredTime)*/) {
        debugPrint("-----entry3");
        await Utility.logoutData();
        AppRouter.navigateToLoginScreen(context);
        return;
      }
    }

    String lastSync = prefs.getString(AppPreferences.pref_last_sync_date_time);
    if(lastSync != null){
      locator<LastSyncDateTimeBloc>().lastSyncDateTimeEventSink.add(lastSync);
    }
    locator<UserModelBloc>().userModelEventSink.add(userModel);
    if(userModel.role == AppUserRole.ROLE_ORGANISATION){
      Navigator.of(context).pushReplacementNamed(HomeScreenOrganication.routeName);
    }else {
      String customerString = prefs.getString(AppPreferences.pref_customer);
      if(customerString != null){
        Map<String,dynamic> customerDetail=json.decode(customerString);
        CustomerModel customerModel = CustomerModel.fromMap(map: customerDetail);
        locator<CustomerBloc>().customerEventSink.add(customerModel);
      }


      Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
    }

  }


}
