import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/bloc/attachment_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/cancel_sync_bloc.dart';
import 'package:inspect_planner/src/bloc/checklist_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/note_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/refreshing/refreshing_bloc.dart';
import 'package:inspect_planner/src/bloc/refreshing/refreshing_bloc_event.dart';
import 'package:inspect_planner/src/bloc/show_hide_sync_progress_bloc.dart';
import 'package:inspect_planner/src/bloc/task_sync_count_bloc.dart';
import 'package:inspect_planner/src/models/message_view_log_model.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:sqflite/sqflite.dart';

const int syncErrorValue = -1;

class AppSync {
  static Future<int> addTask(
      {BuildContext context, Map<String, dynamic> mapNewTask}) async {
    int maxTaskId = mapNewTask['id'];

    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddTask =
        await ApiRequest.posAddTask(mapAddTask: mapNewTask);
    debugPrint("AddTask api Result : $resultAddTask");
    if (resultAddTask['status'] == true) {
      final mData = resultAddTask['data'];
      int oldTaskId = -1;
      if (mData['old_task_id'] is String) {
        oldTaskId = int.parse(mData['old_task_id']);
      } else {
        oldTaskId = mData['old_task_id'];
      }

      int newTaskId = mData['id'];
      if (maxTaskId == oldTaskId) {
        String sqlUpdateTask =
            "update ${AppTables.TASK} set id = ?, synced = ? where id = ?";
        List<dynamic> sqlUpdateValues = [newTaskId, 1, oldTaskId];
        int updateResult =
            await appDatabase.rawUpdate(sqlUpdateTask, sqlUpdateValues);
        maxTaskId = newTaskId;
        if (updateResult == 0) {
          MyToast.showToast(
              "Error syncing add task in local task table", context);
          // delete record from local database if update is not success
          return Future.value(syncErrorValue);
        }
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddTask['message']}', context);
      return Future.value(syncErrorValue);
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(maxTaskId);
  }

  static Future<Map<String, dynamic>> syncAddTask(
      {BuildContext context,
      Map<String, dynamic> mapNewTask,
      List<int> taskSyncIds}) async {
    List<int> mSyncedOldIds = [];
    List<int> mSyncedNewIds = [];

    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddTask = await ApiRequest.postSyncAddTask(mapAddTask: mapNewTask);
    if (resultAddTask['status'] == true) {
      Map<String, dynamic> mData = resultAddTask['data'];
      await Future.forEach(mData.entries, (MapEntry entry) async {
        String key = entry.key;
        Map<String, dynamic> value = entry.value;
        int intKey = int.parse(key);
        if (taskSyncIds.contains(intKey)) {
          int oldTaskId = value['old_task_id'];
          int id = value['id'];

          Batch batchUpdateTaskId = appDatabase.batch();
          String sqlUpdateTask =
              "update ${AppTables.TASK} set id = ?, synced = ? where id = ?";
          List<dynamic> sqlUpdateValues = [id, 1, oldTaskId];
          batchUpdateTaskId.rawUpdate(sqlUpdateTask, sqlUpdateValues);
          /*int updateResult = await appDatabase.rawUpdate(sqlUpdateTask,sqlUpdateValues);
            if(updateResult == 0){
              MyToast.showToast("Error syncing add task in local task table", context);
              // delete record from local database if update is not success
              return Future.value(syncErrorValue);
            }*/

          if (oldTaskId != id) {
            // update old task_id in task attachment
            String sqlUpdateTaskAttachment = "update ${AppTables.ATTACHMENT} set task_id = ? where task_id = ?";
            List<dynamic> sqlUpdateTaskAttachmentValues = [id, oldTaskId];
            batchUpdateTaskId.rawUpdate(sqlUpdateTaskAttachment, sqlUpdateTaskAttachmentValues);

            // update old task_id in note
            String sqlUpdateNote = "update ${AppTables.NOTE} set task_id = ? where task_id = ?";
            List<dynamic> sqlUpdateNoteValues = [id, oldTaskId];
            batchUpdateTaskId.rawUpdate(sqlUpdateNote, sqlUpdateNoteValues);

            // update old task_id in checklist
            String sqlUpdateChecklist = "update ${AppTables.CHECKLISTITEM} set task_id = ? where task_id = ?";
            List<dynamic> sqlUpdateChecklistValues = [id, oldTaskId];
            batchUpdateTaskId.rawUpdate(sqlUpdateChecklist, sqlUpdateChecklistValues);


            List updateResonse = await batchUpdateTaskId.commit(continueOnError: true);
            Utility.printJson("batch --> updateTaskResponse : $updateResonse", updateResonse);
          } else {
            List updateResonse = await batchUpdateTaskId.commit(continueOnError: true);
            Utility.printJson(
                "batch --> updateTaskResponse : $updateResonse", updateResonse);
          }
          mSyncedOldIds.add(oldTaskId);
          mSyncedNewIds.add(id);
        }
      });

      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

      Map<String, dynamic> mapSyncStatus = Map();
      mapSyncStatus['status'] = true;
      mapSyncStatus['oldIds'] = mSyncedOldIds;
      mapSyncStatus['newIds'] = mSyncedNewIds;
      return Future.value(mapSyncStatus);
    }
    // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

    Map<String, dynamic> mapSyncStatus = Map();
    mapSyncStatus['status'] = false;
    return Future.value(mapSyncStatus);
  }
  static Future<Map<String, dynamic>> syncReadMessage({BuildContext context,Map<String, dynamic> mapNewTask,List<int> taskSyncIds}) async {
    List<int> mSyncedOldIds = [];
    List<int> mSyncedNewIds = [];

    Map<String, dynamic> resultAddTask = await ApiRequest.postSyncReadMessage(mapReadMessage: mapNewTask);
    if (resultAddTask['status'] == true) {
      List<dynamic> mData = resultAddTask['data'];
      Batch batchUpdateTaskId = appDatabase.batch();
      await Future.forEach(mData, (element) async {
        Map<String, dynamic> value = element;
        int intKey = value['old_read_id'];
        if (taskSyncIds.contains(intKey)) {
          int oldTaskId = -1;
          if (value['old_read_id'] is String) {
            oldTaskId = int.parse(value['old_read_id']);
          } else {
            oldTaskId = value['old_read_id'];
          }
          int id = value['id'];

          String sqlUpdateTask = "update ${AppTables.MESSAGE_VIEW_LOG} set id = ?, synced = ? where id = ?";
          List<dynamic> sqlUpdateValues = [id, 1, oldTaskId];
          batchUpdateTaskId.rawUpdate(sqlUpdateTask, sqlUpdateValues);


          mSyncedOldIds.add(oldTaskId);
          mSyncedNewIds.add(id);
        }
      });
      List updateResonse = await batchUpdateTaskId.commit(continueOnError: true);
      Utility.printJson("batch --> updateMessageViewResponse : $updateResonse", updateResonse);

      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

      Map<String, dynamic> mapSyncStatus = Map();
      mapSyncStatus['status'] = true;
      mapSyncStatus['oldIds'] = mSyncedOldIds;
      mapSyncStatus['newIds'] = mSyncedNewIds;
      return Future.value(mapSyncStatus);
    }
    // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

    Map<String, dynamic> mapSyncStatus = Map();
    mapSyncStatus['status'] = false;
    return Future.value(mapSyncStatus);
  }
  static Future<Map<String, dynamic>> syncAddNote(
      {BuildContext context,
      Map<String, dynamic> mapNewNote,
      List<int> noteSyncIds}) async {

    List<int> mSyncedOldIds = [];
    List<int> mSyncedNewIds = [];

    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddNote = await ApiRequest.postSyncAddNote(mapAddNote: mapNewNote);

    if (resultAddNote['status'] == true) {
      Map<String, dynamic> mData = resultAddNote['data'];

      Batch batchUpdateNoteId = appDatabase.batch();

      await Future.forEach(mData.entries, (MapEntry entry) async {
        String key = entry.key;
        Map<String, dynamic> value = entry.value;
        int intKey = int.parse(key);
        if (noteSyncIds.contains(intKey)) {
          int oldTaskId = value['old_note_id'];
          int id = value['id'];

          String sqlUpdateTask =
              "update ${AppTables.NOTE} set id = ?, synced = ? where id = ?";
          List<dynamic> sqlUpdateValues = [id, 1, oldTaskId];
          batchUpdateNoteId.rawUpdate(sqlUpdateTask, sqlUpdateValues);
          mSyncedOldIds.add(oldTaskId);
          mSyncedNewIds.add(id);
        }
      });

      List updateNoteResonse =
          await batchUpdateNoteId.commit(continueOnError: true);
      Utility.printJson("batch --> updateNoteResonse ", updateNoteResonse);

      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

      Map<String, dynamic> mapSyncStatus = Map();
      mapSyncStatus['status'] = true;
      mapSyncStatus['oldIds'] = mSyncedOldIds;
      mapSyncStatus['newIds'] = mSyncedNewIds;
      return Future.value(mapSyncStatus);
    }
    // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

    Map<String, dynamic> mapSyncStatus = Map();
    mapSyncStatus['status'] = false;
    return Future.value(mapSyncStatus);
    /*
     {
        "status": true,
        "data": {
            "9024": {
                "old_note_id": 9024,
                "id": 9024
            },
            "90251212": {
                "old_note_id": 90251212,
                "id": 9039
            }
        },
        "message": "Note Sync"
    }
    */
  }

  static Future<Map<String, dynamic>> syncDeleteNote(
      {BuildContext context,
      Map<String, dynamic> mapDeleteNote,
      List<int> deleteNoteSyncIds}) async {
    List<int> mSyncedDeletedIds = [];

    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultDeleteNote =
        await ApiRequest.postSyncDeleteNote(mapDeleteNote: mapDeleteNote);
    if (resultDeleteNote['status'] == true) {
      List<dynamic> mData = resultDeleteNote['data'];

      Batch batchUpdateNoteId = appDatabase.batch();

      await Future.forEach(mData, (element) async {
        if (deleteNoteSyncIds.contains(element)) {
          String sqlDeleteNote = "delete from ${AppTables.NOTE} where id = ?";
          List<dynamic> sqlDeleteNoteValues = [element];
          batchUpdateNoteId.rawUpdate(sqlDeleteNote, sqlDeleteNoteValues);
          mSyncedDeletedIds.add(element);
        }
      });

      List updateNoteResonse =
          await batchUpdateNoteId.commit(continueOnError: true);
      Utility.printJson("batch --> deleteNoteResonse ", updateNoteResonse);

      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

      Map<String, dynamic> mapSyncStatus = Map();
      mapSyncStatus['status'] = true;
      mapSyncStatus['deletedIds'] = mSyncedDeletedIds;
      return Future.value(mapSyncStatus);
    }
    // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

    Map<String, dynamic> mapSyncStatus = Map();
    mapSyncStatus['status'] = false;
    return Future.value(mapSyncStatus);
    /*
     {
        "status": true,
        "data": [
            9024,
            90251212
        ],
        "message": "Note Remove Sync"
    }
    */
  }

  static Future<int> addAttachment(
      {BuildContext context, Map<String, dynamic> mapNewAtchmnt}) async {
    int maxAtchmntId = mapNewAtchmnt['id'];
    debugPrint("Network available");
    // I am connected to network.
    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddAttachment =
        await ApiRequest.posAddAttachment(mapAddAttachment: mapNewAtchmnt);
    debugPrint("Add Attachment api Result : $resultAddAttachment");
    if (resultAddAttachment['status'] == true) {
      final mData = resultAddAttachment['data'];
      int newAtchmntId = mData['id'];
      String imageNme = mData['image_name'];
      int oldAtchmntId = -1;
      if (mData['old_attachment_id'] is String) {
        oldAtchmntId = int.parse(mData['old_attachment_id']);
      } else {
        oldAtchmntId = mData['old_attachment_id'];
      }

      if (maxAtchmntId == oldAtchmntId) {
        String sqlUpdateTask =
            "update ${AppTables.ATTACHMENT} set id = ?, synced = ?, image_name = ? where id = ?";
        List<dynamic> sqlUpdateValues = [
          newAtchmntId,
          1,
          imageNme,
          oldAtchmntId
        ];
        int updateResult =
            await appDatabase.rawUpdate(sqlUpdateTask, sqlUpdateValues);
        maxAtchmntId = newAtchmntId;
        if (updateResult == 0) {
          MyToast.showToast(
              "Error syncing add task in local task table", context);
          // delete record if get any error in update
          return Future.value(syncErrorValue);
        }
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddAttachment['message']}', context);
      return Future.value(syncErrorValue);
    }
    // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(maxAtchmntId);
  }

  static Future<int> deleteAttachment(
      {BuildContext context, Map<String, dynamic> mapNewAtchmnt}) async {
    int maxAtchmntId = mapNewAtchmnt['id'];
    debugPrint("Network available");
    // I am connected to network.
    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddAttachment =
        await ApiRequest.deleteDeleteAttachment(id: maxAtchmntId);
    debugPrint("deleteAttachment api Result : $resultAddAttachment");
    if (resultAddAttachment['status'] == true) {
      final mData = resultAddAttachment['data'];
      int newAtchmntId = mData['id'];
      int oldAtchmntId = -1;
      if (mData['id'] is String) {
        oldAtchmntId = int.parse(mData['id']);
      } else {
        oldAtchmntId = mData['id'];
      }

      if (maxAtchmntId == oldAtchmntId) {
        String sqlUpdateTask =
            "update ${AppTables.ATTACHMENT} set synced = ?, removed = ? where id = ?";
        List<dynamic> sqlUpdateValues = [1, mData['removed'], oldAtchmntId];
        debugPrint('sqlUpdateAttachment : $sqlUpdateTask');
        debugPrint('sqlUpdateValues : $sqlUpdateValues');
        int updateResult =
            await appDatabase.rawUpdate(sqlUpdateTask, sqlUpdateValues);
        maxAtchmntId = newAtchmntId;
        if (updateResult == 0) {
          String errMsg = "Error syncing deleteAttachment local table";
          debugPrint(errMsg);
          MyToast.showToast(errMsg, context);
          // delete record if get any error in update
          return Future.value(syncErrorValue);
        }
      }
    } else {
      String errMsg =
          'Error in deleteAttachment api : ${resultAddAttachment['message']}';
      debugPrint(errMsg);
      MyToast.showToast(errMsg, context);
      return Future.value(syncErrorValue);
    }
    // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(maxAtchmntId);
  }

  static Future<int> updateChecklistAnswer(
      {BuildContext context, int id, String result}) async {
    debugPrint("Network available");
    int updateChecklistId = id;
    // I am connected to network.
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddNote =
        await ApiRequest.putUpdateChecklistAnswer(id: id, result: result);
    debugPrint("updateNote api Result : $resultAddNote");
    if (resultAddNote['status'] == true) {
      final mData = resultAddNote['data'];
      int newUpdateChecklistId = mData['id'];

      if (updateChecklistId == newUpdateChecklistId) {
        String sqlChkLstUpdate =
            "update ${AppTables.CHECKLISTITEM} set synced = ? where id = ?";
        List<dynamic> sqlValues = [1, id];
        debugPrint('sql ---> query : $sqlChkLstUpdate');
        debugPrint('sql ---> values : $sqlValues');
        int updateResult =
            await appDatabase.rawUpdate(sqlChkLstUpdate, sqlValues);
        debugPrint("update Result : $updateResult");
        if (updateResult == 0) {
          debugPrint("Error updating checklist answer");
          MyToast.showToast('Error updating checklist answer', context);
          return Future.value(syncErrorValue);
        }
      } else {
        return Future.value(syncErrorValue);
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddNote['message']}', context);
      return Future.value(syncErrorValue);
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(updateChecklistId);
  }

  static Future<int> updateCompleteTask(
      {BuildContext context, int id, int status_id, String finish_date, int force_completed=0}) async {
    debugPrint("Network available");
    int updateCompleteTaskId = id;
    // I am connected to network.
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddNote = await ApiRequest.putCompleteTask(
        id: id, status_id: status_id, finish_date: finish_date,force_completed: force_completed);
    debugPrint("updateCompleteTask api Result : $resultAddNote");
    if (resultAddNote['status'] == true) {
      final mData = resultAddNote['data'];
      int newTaskCompleteId = mData['id'];
      int force_completed = mData['force_completed'];

      if (updateCompleteTaskId == newTaskCompleteId) {
        String sqlTaskComplete =
            "update ${AppTables.TASK} set synced = ? where id = ?";
        List<dynamic> sqlValues = [1, newTaskCompleteId];
        int updateResult =
            await appDatabase.rawUpdate(sqlTaskComplete, sqlValues);
        if (updateResult == 0) {
          debugPrint("Error completing task");
          MyToast.showToast('Error completing task', context);
          return Future.value(syncErrorValue);
        }
      } else {
        return Future.value(syncErrorValue);
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddNote['message']}', context);
      return Future.value(syncErrorValue);
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(updateCompleteTaskId);
  }

  static Future<int> addNote(
      {BuildContext context, Map<String, dynamic> mapAddNote}) async {
    int maxAddNoteId = mapAddNote['id'];
    debugPrint("Network available");
    // I am connected to network.
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddNote =
        await ApiRequest.posAddNote(mapAddNote: mapAddNote);
    debugPrint("addNote api Result : $resultAddNote");
    if (resultAddNote['status'] == true) {
      final mData = resultAddNote['data'];
      int newAddNoteId = mData['id'];
      int oldAAddNoteId = -1;
      if (mData['old_note_id'] is String) {
        oldAAddNoteId = int.parse(mData['old_note_id']);
      } else {
        oldAAddNoteId = mData['old_note_id'];
      }

      if (maxAddNoteId == oldAAddNoteId) {
        String sqlUpdateTask =
            "update ${AppTables.NOTE} set id = ?, synced = ? where id = ?";
        List<dynamic> sqlUpdateValues = [newAddNoteId, 1, oldAAddNoteId];
        int updateResult =
            await appDatabase.rawUpdate(sqlUpdateTask, sqlUpdateValues);
        maxAddNoteId = newAddNoteId;
        if (updateResult == 0) {
          MyToast.showToast(
              "Error syncing add task in local task table", context);
          // delete record if get any error in update
          return Future.value(syncErrorValue);
        }
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddNote['message']}', context);
      return Future.value(syncErrorValue);
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(maxAddNoteId);
  }

  static Future<int> addReadMessage(
      {BuildContext context, Map<String, dynamic> mapReadMsg}) async {
    int maxAddNoteId = mapReadMsg['id'];
    debugPrint("Network available");
    // I am connected to network.
    Map<String, dynamic> resultAddReadMsg =
        await ApiRequest.postAddReadMessage(mapAddReadMessage: mapReadMsg);
    debugPrint("resultAddReadMsg api Result : $resultAddReadMsg");
    debugPrint('----1');
    if (resultAddReadMsg['status'] == true) {
      debugPrint('----2');
      final mData = resultAddReadMsg['data'];
      debugPrint('----3');
      int newAddNoteId = mData['id'];
      int oldAAddNoteId = -1;
      if (mData['old_read_id'] is String) {
        oldAAddNoteId = int.parse(mData['old_read_id']);
      } else {
        oldAAddNoteId = mData['old_read_id'];
      }
      debugPrint('----4 $maxAddNoteId - $oldAAddNoteId');
      if (maxAddNoteId == oldAAddNoteId) {
        debugPrint('----5');
        Map<String, dynamic> mapNewReadMsg = Map();
        mapNewReadMsg['user_id'] = mData['user_id'];
        mapNewReadMsg['message_id'] = mData['message_id'];
        mapNewReadMsg['date'] = mData['date'];
        mapNewReadMsg['created_at'] = mData['created_at'];
        mapNewReadMsg['updated_at'] = mData['updated_at'];
        mapNewReadMsg['id'] = mData['id'];

        MessageViewLogModel mViewModel = MessageViewLogModel.fromMap(map: mapNewReadMsg);
        mViewModel.synced = 1;
        String sql = AppTablesQuery.INSERT_TABLE_MESSAGE_VIEW_LOG + '(?,?,?,?,?,?,?)';
        int insertResult = await appDatabase.rawInsert(sql, mViewModel.getDataList(mViewModel));
        if (insertResult == 0) {
          MyToast.showToast(
              "Error syncing add task in local task table", context);
          // delete record if get any error in update
          return Future.value(syncErrorValue);
        }
        return Future.value(insertResult);
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddReadMsg['message']}', context);
      return Future.value(syncErrorValue);
    }
    return Future.value(maxAddNoteId);
  }

  static Future<int> updateNote(
      {BuildContext context,
      String note,
      int assignToUserId,
      int noteId}) async {
    debugPrint("Network available");
    int updateNoteId = -1;
    // I am connected to network.
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddNote = await ApiRequest.putUpdateNote(
        assignedToUserId: assignToUserId, note: note, noteId: noteId);
    debugPrint("updateNote api Result : $resultAddNote");
    if (resultAddNote['status'] == true) {
      final mData = resultAddNote['data'];
      int newUpdateNoteId = mData['id'];
      updateNoteId = newUpdateNoteId;
      int oldUpdateNoteId = -1;
      if (mData['old_note_id'] is String) {
        oldUpdateNoteId = int.parse(mData['old_note_id']);
      } else {
        oldUpdateNoteId = mData['old_note_id'];
      }

      if (newUpdateNoteId == oldUpdateNoteId) {
        String sqlTaskComplete = "update ${AppTables.NOTE} "
            "set id = ?, note = ?, assignedto_user_id = ?, synced = ? where id = ?";
        List<dynamic> sqlValues = [
          newUpdateNoteId,
          note,
          assignToUserId,
          1,
          oldUpdateNoteId
        ];
        int updateResult =
            await appDatabase.rawUpdate(sqlTaskComplete, sqlValues);
        debugPrint("Note update result : $updateResult");
        if (updateResult == 0) {
          MyToast.showToast(
              "Error syncing add task in local task table", context);
          return Future.value(syncErrorValue);
        }
      } else {
        return Future.value(syncErrorValue);
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddNote['message']}', context);
      return Future.value(syncErrorValue);
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(updateNoteId);
  }

  static Future<int> deleteNote({BuildContext context, int id}) async {
    int oldDeleteNoteId = id;
    int newDeleteNoteId = -1;
    debugPrint("Network available");
    // I am connected to network.
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultAddNote =
        await ApiRequest.deleteDeleteNote(id: id);
    debugPrint("addNote api Result : $resultAddNote");
    if (resultAddNote['status'] == true) {
      final mData = resultAddNote['data'];

      if (mData['note_id'] is String) {
        newDeleteNoteId = int.parse(mData['note_id']);
      } else {
        newDeleteNoteId = mData['note_id'];
      }

      if (oldDeleteNoteId == newDeleteNoteId) {
        String sqlDeleteNote = "delete from ${AppTables.NOTE} where id = ?";
        List<dynamic> sqlDeleteNoteValues = [newDeleteNoteId];
        int deleteResult =
            await appDatabase.rawDelete(sqlDeleteNote, sqlDeleteNoteValues);
        if (deleteResult == 0) {
          MyToast.showToast("Error delete from local and web", context);
          // delete record if get any error in update
          return Future.value(syncErrorValue);
        }
      }
    } else {
      MyToast.showToast(
          'Error in add task api : ${resultAddNote['message']}', context);
      return Future.value(syncErrorValue);
    }
    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    return Future.value(newDeleteNoteId);
  }

  static Future<Map<String, dynamic>> funSyncAddTask(
      {BuildContext mContext}) async {
    Map<String, dynamic> mapSyncAddTask = Map();
    String sqlAddTask = "select * from ${AppTables.TASK} where synced = ?";
    List<dynamic> valuesAddTask = [0];
    List<Map<String, Object>> listAddTask = await appDatabase.rawQuery(sqlAddTask, valuesAddTask);
    if (listAddTask.length > 0) {
      mapSyncAddTask['sync_add_task'] = listAddTask;
      List<int> taskSyncIds = [];
      listAddTask.forEach((element) {
        taskSyncIds.add(element['id']);
      });
      Utility.printJson("Sync AddTask Sync Ids ", taskSyncIds);
      Utility.printJson("Sync AddTask Request", mapSyncAddTask);
      Map<String, dynamic> mapSyncAddTaskResult = await AppSync.syncAddTask(
          context: mContext,
          mapNewTask: mapSyncAddTask,
          taskSyncIds: taskSyncIds);
      Utility.taskSyncCount = listAddTask.length;
      return Future.value(mapSyncAddTaskResult);
      Utility.printJson('mapSyncAddTaskResult', mapSyncAddTaskResult);
    }
    return Future.value({});
  }

  static Future<Map<String, dynamic>> funSyncAddNote(
      {BuildContext mContext}) async {
    String sqlAddNote =
        "select * from ${AppTables.NOTE} where synced = ? and removed = ?";
    List<dynamic> valuesAddNote = [0, 0];
    List<Map<String, Object>> listAddNote = await appDatabase.rawQuery(sqlAddNote, valuesAddNote);
    Utility.printJson("Task Note", listAddNote);
    Map<String, dynamic> mapSyncAddNote = Map();
    if (listAddNote.length > 0) {
      mapSyncAddNote['sync_add_note'] = listAddNote;
      List<int> noteSyncIds = [];
      listAddNote.forEach((element) {
        noteSyncIds.add(element['id']);
      });
      Utility.printJson("Sync AddNote Sync Ids ", noteSyncIds);
      Utility.printJson("Sync AddNote Request", mapSyncAddNote);
      Map<String, dynamic> mapSyncAddNoteResult = await AppSync.syncAddNote(
          context: mContext,
          mapNewNote: mapSyncAddNote,
          noteSyncIds: noteSyncIds);
      Utility.noteSyncCount += listAddNote.length;
      return Future.value(mapSyncAddNoteResult);
      Utility.printJson('mapSyncAddNoteResult', mapSyncAddNoteResult);
    }
    return Future.value({});
  }

  static Future<Map<String, dynamic>> funSyncDeleteNote(
      {BuildContext mContext}) async {
    String sqlDeleteNote =
        "select * from ${AppTables.NOTE} where synced = ? and removed = ?";
    List<dynamic> valuesDeleteNote = [0, 1];
    List<Map<String, Object>> listDeleteNote =
        await appDatabase.rawQuery(sqlDeleteNote, valuesDeleteNote);
    Utility.printJson("Delete Note", listDeleteNote);
    Map<String, dynamic> mapSyncDeleteNote = Map();
    if (listDeleteNote.length > 0) {
      mapSyncDeleteNote['sync_remove_note'] = listDeleteNote;
      List<int> deleteNoteSyncIds = [];
      listDeleteNote.forEach((element) {
        deleteNoteSyncIds.add(element['id']);
      });
      Utility.printJson("Sync DeleteNote Sync Ids ", deleteNoteSyncIds);
      Utility.printJson("Sync DeleteNote Request", mapSyncDeleteNote);
      Map<String, dynamic> mapSyncDeleteNoteResult =
          await AppSync.syncDeleteNote(
              context: mContext,
              mapDeleteNote: mapSyncDeleteNote,
              deleteNoteSyncIds: deleteNoteSyncIds);
      Utility.noteSyncCount += listDeleteNote.length;
      Utility.printJson('mapSyncDeleteNoteResult', mapSyncDeleteNoteResult);
    }
  }

  static Future<void> funSyncAddAttachment({BuildContext mContext}) async {
    String sqlAttachment =
        "select * from ${AppTables.ATTACHMENT} where synced = ? and removed = ?";
    List<dynamic> valuesAttachment = [0, 0];
    List<Map<String, Object>> listAttachment =
        await appDatabase.rawQuery(sqlAttachment, valuesAttachment);
    Utility.printJson("Add Attachment", listAttachment);
    if (listAttachment.length > 0) {
      await Future.forEach(listAttachment, (element) async {
        int maxAtchmntId = await AppSync.addAttachment(
            context: mContext, mapNewAtchmnt: element);
        if (maxAtchmntId == syncErrorValue) {
          Utility.printJson("Attachment add not synced", element);
        }
      });
      Utility.attachmentSyncCount += listAttachment.length;
    }
  }

  static Future<void> funSyncDeleteAttachment({BuildContext mContext}) async {
    String sqlDeleteAttachment =
        "select * from ${AppTables.ATTACHMENT} where synced = ? and removed = ?";
    List<dynamic> valuesDeleteAttachment = [0, 1];
    List<Map<String, Object>> listDeleteAttachment =
        await appDatabase.rawQuery(sqlDeleteAttachment, valuesDeleteAttachment);
    Utility.printJson("Delete Task Attachment", listDeleteAttachment);
    if (listDeleteAttachment.length > 0) {
      await Future.forEach(listDeleteAttachment, (element) async {
        int maxAtchmntId = element['id'];
        Map<String, dynamic> resultAddAttachment =
            await ApiRequest.deleteDeleteAttachment(id: element['id']);
        if (resultAddAttachment['status'] == true) {
          dynamic mData = resultAddAttachment['data'];
          if (mData.length == 0) {
            dynamic resultNewAtchmnt =
                await ApiRequest.posAddAttachment(mapAddAttachment: element);

            debugPrint("Add Attachment api Result : $resultNewAtchmnt");
            if (resultNewAtchmnt['status'] == true) {
              final mData = resultNewAtchmnt['data'];
              int newAtchmntId = mData['id'];
              String imageNme = mData['image_name'];
              int oldAtchmntId = -1;
              if (mData['old_attachment_id'] is String) {
                oldAtchmntId = int.parse(mData['old_attachment_id']);
              } else {
                oldAtchmntId = mData['old_attachment_id'];
              }

              if (maxAtchmntId == oldAtchmntId) {
                String sqlUpdateTask =
                    "update ${AppTables.ATTACHMENT} set id = ?, synced = ?, image_name = ? where id = ?";
                List<dynamic> sqlUpdateValues = [
                  newAtchmntId,
                  1,
                  imageNme,
                  oldAtchmntId
                ];
                int updateResult =
                    await appDatabase.rawUpdate(sqlUpdateTask, sqlUpdateValues);
                maxAtchmntId = newAtchmntId;
                if (updateResult == 0) {
                  String errMsg = "Error syncing add task in local task table";
                  debugPrint("errMsg funSyncDeleteAttachment : $errMsg");
                  MyToast.showToast(errMsg, mContext);
                  // delete record if get any error in update
                  // return Future.value(syncErrorValue);
                }

                if (resultNewAtchmnt['status'] == true) {
                  final mNewData = resultNewAtchmnt['data'];
                  if (mNewData.length != 0) {
                    int maxAtchmntId = await AppSync.deleteAttachment(
                        context: mContext, mapNewAtchmnt: mNewData);
                    if (maxAtchmntId == syncErrorValue) {
                      String errMsg = "Attachment delete not synced";
                      debugPrint("AppSync.deleteAttachment : $errMsg");
                      Utility.printJson(errMsg, element);
                    }
                  }
                }
              }
            } else {
              String errMsg =
                  'Error in add task api : ${resultAddAttachment['message']}';
              debugPrint("$errMsg");
              MyToast.showToast(errMsg, mContext);

              // return Future.value(syncErrorValue);
            }
          } else {
            int maxAtchmntId = element['id'];
            int newAtchmntId = mData['id'];
            int oldAtchmntId = -1;
            if (mData['id'] is String) {
              oldAtchmntId = int.parse(mData['id']);
            } else {
              oldAtchmntId = mData['id'];
            }
            if (maxAtchmntId == oldAtchmntId) {
              String sqlUpdateTask =
                  "update ${AppTables.ATTACHMENT} set synced = ?, removed = ? where id = ?";
              List<dynamic> sqlUpdateValues = [
                1,
                mData['removed'],
                oldAtchmntId
              ];
              int updateResult =
                  await appDatabase.rawUpdate(sqlUpdateTask, sqlUpdateValues);
              maxAtchmntId = newAtchmntId;
              if (updateResult == 0) {
                String errMsg = "Error syncing deleteAttachment local table";
                debugPrint(errMsg);
                MyToast.showToast(errMsg, mContext);
                // delete record if get any error in update
                return Future.value(syncErrorValue);
              }
            }
          }
        }
      });
      Utility.attachmentSyncCount += listDeleteAttachment.length;
    }
  }

  static Future<void> syncToWeb({BuildContext mContext,bool showSyncDialog,bool showRefreshing}) async {
    debugPrint("--------------------syncToWeb");
    Utility.isSyncingToWeb = true;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      // I am connected to a mobile network.
      MyToast.showToast(AppStrings.itIsNotPossibleToSync, mContext);
      Utility.isSyncingToWeb=false;
      return null;
    }

    Utility.clearSyncCount();
    if(showRefreshing != null && showRefreshing){
      locator<RefreshingBloc>().loadingEventSink.add(RefreshingEvent());
    }
    if(showSyncDialog != null && showSyncDialog) {
      DialogMgr.showSyncDialog(showCancelButton: true);
    }

    // sync add task
    debugPrint("--------------------sync-add-task");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync-add-task-abort");
      Utility.isSyncingToWeb=false;
      return;
    }
    Map<String, dynamic> mapSyncAddTaskResult = await AppSync.funSyncAddTask(mContext: mContext);
    Utility.printJson('mapSyncAddTaskResult', mapSyncAddTaskResult);
    // if(mapSyncAddTaskResult == null){
    //   return;
    // }
    // return;

    // sync add task & checklist attachment
    debugPrint("--------------------sync-add-attachment");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync-add-attachment-abort");
      Utility.isSyncingToWeb=false;
      return;
    }
    await AppSync.funSyncAddAttachment(mContext: mContext);

    // sync delete task & checklist attachment
    debugPrint("--------------------sync-delete-attachment");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync-delete-attachment-abort");
      Utility.isSyncingToWeb=false;
      return;
    }
    await AppSync.funSyncDeleteAttachment(mContext: mContext);

    // sync add note
    debugPrint("--------------------sync-add-note");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync-add-note-abort");
      Utility.isSyncingToWeb=false;
      return;
    }
    Map<String, dynamic> mapSyncAddNoteResult = await AppSync.funSyncAddNote(mContext: mContext);
    Utility.printJson('mapSyncAddNoteResult', mapSyncAddNoteResult);

    // sync delete note
    debugPrint("--------------------sync-delete-note");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync-delete-note-abort");
      Utility.isSyncingToWeb=false;
      return;
    }
    Map<String, dynamic> mapSyncDeleteNoteResult = await AppSync.funSyncDeleteNote(mContext: mContext);
    Utility.printJson('mapSyncDeleteNoteResult', mapSyncDeleteNoteResult);

    // sync checklistitem
    debugPrint("--------------------sync-checklist");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync-checklist-abort");
      Utility.isSyncingToWeb=false;
      return;
    }
    String sqlChecklist = "select * from ${AppTables.CHECKLISTITEM} where synced = ?";
    List<dynamic> sqlChecklistValues = [0];
    List<Map<String, Object>> listChecklistItems = await appDatabase.rawQuery(sqlChecklist, sqlChecklistValues);
    List<int> chklstIds = [];
    if (listChecklistItems.length > 0) {
      Utility.printJson("Sync Checklist", listChecklistItems);
      Map<String, dynamic> mapRequest = Map();
      List<Map<String, dynamic>> listData = [];
      listChecklistItems.forEach((element) {
        Map<String, dynamic> mapData = Map();
        mapData['id'] = element['id'];
        mapData['task_id'] = element['task_id'];
        mapData['question'] = element['question'];
        mapData['type'] = element['type'];
        mapData['result'] = element['result'];
        mapData['removed'] = element['removed'];
        mapData['description'] = element['description'];
        mapData['order_number'] = element['order_number'];
        mapData['unique_hash'] = element['unique_hash'];
        mapData['clone_checklist_id'] = element['clone_checklist_id'];
        mapData['synced'] = element['synced'];
        mapData['digit_range'] = element['digit_range'];
        mapData['right_answer'] = element['right_answer'];
        mapData['wrong_answer'] = element['wrong_answer'];

        chklstIds.add(element['id']);
        listData.add(mapData);
      });
      mapRequest['sync_update_checklistitems'] = listData;
      Utility.printJson("Sync Checklist - request", mapRequest);
      Map<String, dynamic> syncChklstResult = await AppSync.funSyncChecklistAnswer(context: mContext, mapRequest: mapRequest, chklstIds: chklstIds);
      Utility.checklistSyncCount = listChecklistItems.length;
      Utility.printJson("syncChklstResult", syncChklstResult);
    }

    // sync view message log
    debugPrint("--------------------sync view message log");
    if (appCancelSyncProcess) {
      locator<CancelSyncBloc>().cancelSyncEventSink.add(false);
      debugPrint("--------------------sync view message log");
      Utility.isSyncingToWeb=false;
      return;
    }
    String sqlVLog = "select * from ${AppTables.MESSAGE_VIEW_LOG} where user_id = ? and synced = ?";
    List<dynamic> sqlVLogValue = [appUserModel.id,0];
    List<Map<String, Object>> listMVLog = await appDatabase.rawQuery(sqlVLog, sqlVLogValue);
    Utility.printJson('Newspost sync', listMVLog);

    bool isSyncedMsgRead=false;
    Map<String, dynamic> mapSyncAddReadMsg = Map();
    if(listMVLog.length > 0){
      isSyncedMsgRead = true;
      mapSyncAddReadMsg['sync_read_message'] = listMVLog;
      List<int> readSyncIds = [];
      listMVLog.forEach((element) {
        readSyncIds.add(element['id']);
      });
      Utility.printJson("Sync MsgRead Sync Ids ", readSyncIds);
      Utility.printJson("Sync MsgRead Request", mapSyncAddReadMsg);
      Map<String, dynamic> mapSyncAddTaskResult = await AppSync.syncReadMessage(
          context: mContext,
          mapNewTask: mapSyncAddReadMsg,
          taskSyncIds: readSyncIds);
      // Utility.taskSyncCount = listAddTask.length;
      // return Future.value(mapSyncAddTaskResult);
    }



    Utility.addLastSyncTime();

    if (Utility.taskSyncCount == 0 &&
        Utility.attachmentSyncCount == 0 &&
        Utility.checklistSyncCount == 0 &&
        Utility.noteSyncCount == 0 && !isSyncedMsgRead) {
      if(showSyncDialog != null && showSyncDialog) {
        DialogMgr.hideSyncDialog();
      }
      locator<RefreshingBloc>().loadingEventSink.add(RefreshedEvent());
      Utility.isSyncingToWeb=false;
      return;
    }


    locator<TaskSyncCountBloc>()
        .taskSyncCountEventSink
        .add(Utility.taskSyncCount);
    locator<AttachmentSyncCountBloc>()
        .attachmentSyncCountEventSink
        .add(Utility.attachmentSyncCount);
    locator<ChecklistSyncCount>()
        .checklistSyncCountEventSink
        .add(Utility.checklistSyncCount);
    locator<NoteSyncCountBloc>()
        .noteSyncCountEventSink
        .add(Utility.noteSyncCount);
    locator<ShowHideSyncProgressBloc>()
        .showHideSyncProgressEventSink
        .add(false);


    if(isSyncedMsgRead){
      if(showSyncDialog != null && showSyncDialog) {
        DialogMgr.hideSyncDialog();
      }
      locator<RefreshingBloc>().loadingEventSink.add(RefreshedEvent());
    }
    locator<RefreshingBloc>().loadingEventSink.add(RefreshedEvent());
    Utility.isSyncingToWeb = false;
  }

  static Future<Map<String, dynamic>> funSyncChecklistAnswer(
      {BuildContext context,
      Map<String, dynamic> mapRequest,
      List<dynamic> chklstIds}) async {

    List<int> mSyncedOldIds = [];
    List<int> mSyncedNewIds = [];
    // I am connected to network.
    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultSyncedChecklist = await ApiRequest.postSyncChecklistItems(mapChecklistItems: mapRequest);

    if (resultSyncedChecklist['status'] == true) {

      Map<String, dynamic> mData = resultSyncedChecklist['data'];

      Batch batchUpdateChecklist = appDatabase.batch();

      await Future.forEach(mData.entries, (MapEntry entry) {

        String key = entry.key;
        Map<String, dynamic> value = entry.value;
        int intKey = int.parse(key);

        if (chklstIds.contains(intKey)) {

          int oldChecklistId = value['old_checklist_id'];
          int id = value['id'];
          int taskId = value['task_id'];

          String sqlChkLstUpdate = "update ${AppTables.CHECKLISTITEM} set id = ?, task_id = ?, synced = ? where id = ?";
          List<dynamic> sqlValues = [id,taskId,1, oldChecklistId];
          debugPrint('sql ---> query : $sqlChkLstUpdate');
          debugPrint('sql ---> values : $sqlValues');
          // int updateResult = await appDatabase.rawUpdate(sqlChkLstUpdate,sqlValues);
          batchUpdateChecklist.rawUpdate(sqlChkLstUpdate, sqlValues);

          mSyncedOldIds.add(oldChecklistId);
          mSyncedNewIds.add(id);

        }
      });

      List<dynamic> updateResult = await batchUpdateChecklist.commit(continueOnError: true);
      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      debugPrint("------chklst-sync-debug-----end");


      Map<String, dynamic> mapSyncStatus = Map();
      mapSyncStatus['status'] = true;
      mapSyncStatus['oldIds'] = mSyncedOldIds;
      mapSyncStatus['newIds'] = mSyncedNewIds;

      return Future.value(mapSyncStatus);
    } else {
      String errMsg = "Error in sync checklist";
      MyToast.showToast(errMsg, context);
      debugPrint(errMsg);
      debugPrint("------chklst-sync-debug-----end");
      return Future.value({});
    }
  }
  static Future<Map<String, dynamic>> funSyncAddChecklistAnswer(
      {BuildContext context,
        Map<String, dynamic> mapRequest,
        List<dynamic> chklstIds}) async {
    debugPrint("Network available");
    // I am connected to network.
    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    Map<String, dynamic> resultSyncedChecklist = await ApiRequest.postSyncAddChecklistItems(mapChecklistItems: mapRequest);
    debugPrint("result postSyncAddChecklistItems api Result : $resultSyncedChecklist");
    Batch batchUpdateChecklist = appDatabase.batch();
    List<int> mSyncedNewIds = [];
    List<int> mSyncedOldIds = [];
    if (resultSyncedChecklist['status'] == true) {

      Map<String, dynamic> mData = resultSyncedChecklist['data'];
      await Future.forEach(mData.entries, (MapEntry entry) async {
        String key = entry.key;
        Map<String, dynamic> value = entry.value;
        int intKey = int.parse(key);
        if (chklstIds.contains(intKey)) {
          int oldTaskId = value['old_checklist_id'];
          int id = value['id'];

          Batch batchUpdateTaskId = appDatabase.batch();
          String sqlUpdateTask =
              "update ${AppTables.CHECKLISTITEM} set id = ?, synced = ? where id = ?";
          List<dynamic> sqlUpdateValues = [id, 1, oldTaskId];
          batchUpdateTaskId.rawUpdate(sqlUpdateTask, sqlUpdateValues);
          await batchUpdateTaskId.commit(continueOnError: true);
          mSyncedOldIds.add(oldTaskId);
          mSyncedNewIds.add(id);
        }
      });

      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

      Map<String, dynamic> mapSyncStatus = Map();
      mapSyncStatus['status'] = true;
      mapSyncStatus['oldIds'] = mSyncedOldIds;
      mapSyncStatus['newIds'] = mSyncedNewIds;
      return Future.value(mapSyncStatus);

      // Map<String,dynamic> mData = resultSyncedChecklist['data'];
      // await Future.forEach(mData, (element) {
      //   int id = element['id'];
      //   if (chklstIds.contains(id)) {
      //     String sqlChkLstUpdate =
      //         "update ${AppTables.CHECKLISTITEM} set synced = ? where id = ?";
      //     List<dynamic> sqlValues = [1, id];
      //     debugPrint('sql ---> query : $sqlChkLstUpdate');
      //     debugPrint('sql ---> values : $sqlValues');
      //     // int updateResult = await appDatabase.rawUpdate(sqlChkLstUpdate,sqlValues);
      //     batchUpdateChecklist.rawUpdate(sqlChkLstUpdate, sqlValues);
      //   }
      // });
      //
      // List<dynamic> updateResult =
      // await batchUpdateChecklist.commit(continueOnError: true);
      // return Future.value(updateResult);
    } else {
      String errMsg = "Error in sync checklist";
      MyToast.showToast(errMsg, context);
      debugPrint(errMsg);
      return Future.value(null);
    }
  }
}
