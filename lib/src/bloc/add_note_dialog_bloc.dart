import 'dart:async';

import 'package:inspect_planner/src/models/alert_model.dart';
import 'package:inspect_planner/src/models/dialog_add_note_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class AddNoteDialogBloc {


  final addNoteDialogController = StreamController<DialogAddNoteModel>.broadcast();
  StreamSink<DialogAddNoteModel> get addNoteDialogSink => addNoteDialogController.sink;
  // For state, exposing only a stream which outputs data
  Stream<DialogAddNoteModel> get addNoteDialogStream => addNoteDialogController.stream;

  final addNoteDialogEventController = StreamController<DialogAddNoteModel>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<DialogAddNoteModel> get addNoteDialogEventSink => addNoteDialogEventController.sink;

  AddNoteDialogBloc() {
    // Whenever there is a new event, we want to map it to a new state
    addNoteDialogEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(DialogAddNoteModel event) {
    addNoteDialogSink.add(event);
  }

  void dispose() {
    addNoteDialogController.close();
    addNoteDialogEventController.close();
  }
}
