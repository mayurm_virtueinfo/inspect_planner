import 'dart:async';

import 'package:inspect_planner/src/models/alert_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class AlertBloc {


  final alertController = StreamController<AlertModel>.broadcast();
  StreamSink<AlertModel> get alertSink => alertController.sink;
  // For state, exposing only a stream which outputs data
  Stream<AlertModel> get alertStream => alertController.stream;

  final alertEventController = StreamController<AlertModel>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<AlertModel> get alertEventSink => alertEventController.sink;

  AlertBloc() {
    // Whenever there is a new event, we want to map it to a new state
    alertEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(AlertModel event) {
    alertSink.add(event);
  }

  void dispose() {
    alertController.close();
    alertEventController.close();
  }
}
