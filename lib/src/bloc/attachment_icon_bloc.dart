import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class AttachmentIconBloc {


  final attachmentIconController = StreamController<int>.broadcast();
  StreamSink<int> get attachmentIconSink => attachmentIconController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get attachmentIconStream => attachmentIconController.stream;

  final attachmentIconEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get attachmentIconEventSink => attachmentIconEventController.sink;

  AttachmentIconBloc() {
    // Whenever there is a new event, we want to map it to a new state
    attachmentIconEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    attachmentIconSink.add(event);
  }

  void dispose() {
    attachmentIconController.close();
    attachmentIconEventController.close();
  }
}
