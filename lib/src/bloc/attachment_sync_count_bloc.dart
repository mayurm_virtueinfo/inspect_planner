import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class AttachmentSyncCountBloc {


  final attachmentSyncCountController = StreamController<int>.broadcast();
  StreamSink<int> get attachmentSyncCountSink => attachmentSyncCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get attachmentSyncCountStream => attachmentSyncCountController.stream;

  final attachmentSyncCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get attachmentSyncCountEventSink => attachmentSyncCountEventController.sink;

  AttachmentSyncCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    attachmentSyncCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    attachmentSyncCountSink.add(event);
  }

  void dispose() {
    attachmentSyncCountController.close();
    attachmentSyncCountEventController.close();
  }
}
