import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class CancelSyncBloc {


  final cancelSyncController = StreamController<bool>.broadcast();
  StreamSink<bool> get cancelSyncSink => cancelSyncController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get cancelSyncStream => cancelSyncController.stream;

  final cancelSyncEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get cancelSyncEventSink => cancelSyncEventController.sink;

  CancelSyncBloc() {
    // Whenever there is a new event, we want to map it to a new state
    cancelSyncEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    cancelSyncSink.add(event);
  }

  void dispose() {
    cancelSyncController.close();
    cancelSyncEventController.close();
  }
}
