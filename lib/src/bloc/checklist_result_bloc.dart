import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class ChecklistResultBloc {


  final checklistResultController = StreamController<String>.broadcast();
  StreamSink<String> get checklistResultSink => checklistResultController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get checklistResultStream => checklistResultController.stream;

  final checklistResultEventController = StreamController<String>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<String> get checklistResultEventSink => checklistResultEventController.sink;

  ChecklistResultBloc() {
    // Whenever there is a new event, we want to map it to a new state
    checklistResultEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(String event) {
    checklistResultSink.add(event);
  }

  void dispose() {
    checklistResultController.close();
    checklistResultEventController.close();
  }
}
