import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class ChecklistSyncCount {


  final checklistSyncCountController = StreamController<int>.broadcast();
  StreamSink<int> get checklistSyncCountSink => checklistSyncCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get checklistSyncCountStream => checklistSyncCountController.stream;

  final checklistSyncCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get checklistSyncCountEventSink => checklistSyncCountEventController.sink;

  ChecklistSyncCount() {
    // Whenever there is a new event, we want to map it to a new state
    checklistSyncCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    checklistSyncCountSink.add(event);
  }

  void dispose() {
    checklistSyncCountController.close();
    checklistSyncCountEventController.close();
  }
}
