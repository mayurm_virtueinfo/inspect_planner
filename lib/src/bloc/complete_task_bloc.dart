import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class CompleteTaskBloc {


  final completeTaskController = StreamController<bool>.broadcast();
  StreamSink<bool> get completeTaskSink => completeTaskController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get completeTaskStream => completeTaskController.stream;

  final completeTaskEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get completeTaskEventSink => completeTaskEventController.sink;

  CompleteTaskBloc() {
    // Whenever there is a new event, we want to map it to a new state
    completeTaskEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    completeTaskSink.add(event);
  }

  void dispose() {
    completeTaskController.close();
    completeTaskEventController.close();
  }
}
