import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class CompleteTaskCountBloc {


  final completeTaskCountController = StreamController<int>.broadcast();
  StreamSink<int> get completeTaskCountSink => completeTaskCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get completeTaskCountStream => completeTaskCountController.stream;

  final completeTaskCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get completeTaskCountEventSink => completeTaskCountEventController.sink;

  CompleteTaskCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    completeTaskCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    completeTaskCountSink.add(event);
  }

  void dispose() {
    completeTaskCountController.close();
    completeTaskCountEventController.close();
  }
}
