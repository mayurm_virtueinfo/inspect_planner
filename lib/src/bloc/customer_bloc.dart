import 'dart:async';

import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class CustomerBloc {


  final customerBloc = StreamController<CustomerModel>.broadcast();
  StreamSink<CustomerModel> get customerSink => customerBloc.sink;
  // For state, exposing only a stream which outputs data
  Stream<CustomerModel> get customerStream => customerBloc.stream;

  final customerEventController = StreamController<CustomerModel>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<CustomerModel> get customerEventSink => customerEventController.sink;

  CustomerBloc() {
    // Whenever there is a new event, we want to map it to a new state
    customerEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(CustomerModel event) {
    customerSink.add(event);
  }

  void dispose() {
    customerBloc.close();
    customerEventController.close();
  }
}
