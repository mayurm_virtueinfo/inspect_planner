import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class DetailTabBloc {


  final detailTabController = StreamController<int>.broadcast();
  StreamSink<int> get detailTabSink => detailTabController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get detailTabStrem => detailTabController.stream;

  final detailTabEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get detailTabEventSink => detailTabEventController.sink;

  DetailTabBloc() {
    // Whenever there is a new event, we want to map it to a new state
    detailTabEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    detailTabSink.add(event);
  }

  void dispose() {
    detailTabController.close();
    detailTabEventController.close();
  }
}
