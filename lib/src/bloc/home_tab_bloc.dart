import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class HomeTabBloc {


  final homeTabController = StreamController<int>.broadcast();
  StreamSink<int> get homeTabSink => homeTabController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get homeTabStream => homeTabController.stream;

  final homeTabEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get homeTabEventSink => homeTabEventController.sink;

  HomeTabBloc() {
    // Whenever there is a new event, we want to map it to a new state
    homeTabEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    homeTabSink.add(event);
  }

  void dispose() {
    homeTabController.close();
    homeTabEventController.close();
  }
}
