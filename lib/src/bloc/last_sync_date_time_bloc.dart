import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class LastSyncDateTimeBloc {


  final lastSyncDateTimeController = StreamController<String>.broadcast();
  StreamSink<String> get lastSyncDateTimeSink => lastSyncDateTimeController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get lastSyncDateTimeStream => lastSyncDateTimeController.stream;

  final lastSyncDateTimeEventController = StreamController<String>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<String> get lastSyncDateTimeEventSink => lastSyncDateTimeEventController.sink;

  LastSyncDateTimeBloc() {
    // Whenever there is a new event, we want to map it to a new state
    lastSyncDateTimeEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(String event) {
    lastSyncDateTimeSink.add(event);
  }

  void dispose() {
    lastSyncDateTimeController.close();
    lastSyncDateTimeEventController.close();
  }
}
