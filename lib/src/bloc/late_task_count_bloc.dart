import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class LateTaskCountBloc {


  final lateTaskCountController = StreamController<int>.broadcast();
  StreamSink<int> get lateTaskCountSink => lateTaskCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get lateTaskCountStream => lateTaskCountController.stream;

  final lateTaskCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get lateTaskCountEventSink => lateTaskCountEventController.sink;

  LateTaskCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    lateTaskCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    lateTaskCountSink.add(event);
  }

  void dispose() {
    lateTaskCountController.close();
    lateTaskCountEventController.close();
  }
}
