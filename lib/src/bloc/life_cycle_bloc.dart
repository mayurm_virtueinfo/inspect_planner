import 'dart:async';



class LifeCycleBloc {


  final lifeCycleController = StreamController<String>.broadcast();
  StreamSink<String> get lifeCycleSink => lifeCycleController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get lifeCycleStream => lifeCycleController.stream;

  final lifeCycleEventController = StreamController<String>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<String> get lifeCycleEventSink => lifeCycleEventController.sink;

  LifeCycleBloc() {
    // Whenever there is a new event, we want to map it to a new state
    lifeCycleEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(String event) {
    lifeCycleSink.add(event);
  }

  void dispose() {
    lifeCycleController.close();
    lifeCycleEventController.close();
  }
}
