import 'dart:async';

import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';




class LoadMoreBloc {

  final loadMoreController = StreamController<bool>.broadcast();
  StreamSink<bool> get _inUploadingWraprz => loadMoreController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get loadMoreStream => loadMoreController.stream;

  final loadMoreEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get loadMoreEventSink => loadMoreEventController.sink;

  LoadMoreBloc() {
    // Whenever there is a new event, we want to map it to a new state
    loadMoreEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool loading) {
    _inUploadingWraprz.add(loading);
  }

  void dispose() {
    loadMoreController.close();
    loadMoreEventController.close();
  }
}
