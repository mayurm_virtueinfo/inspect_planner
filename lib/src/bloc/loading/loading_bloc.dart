import 'dart:async';

import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';




class LoadingBloc {
  bool isLoading = true;

  final loadingController = StreamController<bool>.broadcast();
  StreamSink<bool> get _inUploadingWraprz => loadingController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get loadingStream => loadingController.stream;

  final loadingEventController = StreamController<LoadingBlocEvent>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<LoadingBlocEvent> get loadingEventSink => loadingEventController.sink;

  LoadingBloc() {
    // Whenever there is a new event, we want to map it to a new state
    loadingEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(LoadingBlocEvent event) {
    if (event is LoadingEvent)
      isLoading=true;
    else
      isLoading=false;

    _inUploadingWraprz.add(isLoading);
  }

  void dispose() {
    loadingController.close();
    loadingEventController.close();
  }
}
