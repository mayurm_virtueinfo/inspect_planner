import 'dart:async';

import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class NewspostsBloc {


  final newspostsController = StreamController<List<NewspostsModel>>.broadcast();
  StreamSink<List<NewspostsModel>> get newspostsSink => newspostsController.sink;
  // For state, exposing only a stream which outputs data
  Stream<List<NewspostsModel>> get newspostsStrem => newspostsController.stream;

  final newspostsEventController = StreamController<List<NewspostsModel>>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<List<NewspostsModel>> get newspostsEventSink => newspostsEventController.sink;

  NewspostsBloc() {
    // Whenever there is a new event, we want to map it to a new state
    newspostsEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(List<NewspostsModel> event) {
    newspostsSink.add(event);
  }

  void dispose() {
    newspostsController.close();
    newspostsEventController.close();
  }
}
