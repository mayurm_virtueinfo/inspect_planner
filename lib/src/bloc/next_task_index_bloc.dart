import 'dart:async';

import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class NextTaskIndexBloc {


  final nextTaskIndexController = StreamController<int>.broadcast();
  StreamSink<int> get nextTaskIndexSink => nextTaskIndexController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get nextTaskIndexStream => nextTaskIndexController.stream;

  final nextIndexEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get nextTaskIndexEventSink => nextIndexEventController.sink;

  NextTaskIndexBloc() {
    // Whenever there is a new event, we want to map it to a new state
    nextIndexEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    nextTaskIndexSink.add(event);
  }

  void dispose() {
    nextTaskIndexController.close();
    nextIndexEventController.close();
  }
}
