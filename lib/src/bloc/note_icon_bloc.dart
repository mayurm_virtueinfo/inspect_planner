import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class NoteIconBloc {


  final noteIconController = StreamController<int>.broadcast();
  StreamSink<int> get noteIconSink => noteIconController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get noteIconStream => noteIconController.stream;

  final noteIconEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get noteIconEventSink => noteIconEventController.sink;

  NoteIconBloc() {
    // Whenever there is a new event, we want to map it to a new state
    noteIconEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    noteIconSink.add(event);
  }

  void dispose() {
    noteIconController.close();
    noteIconEventController.close();
  }
}
