import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class NoteSyncCountBloc {


  final noteSyncCountController = StreamController<int>.broadcast();
  StreamSink<int> get noteSyncCountSink => noteSyncCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get noteSyncCountStream => noteSyncCountController.stream;

  final noteSyncCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get noteSyncCountEventSink => noteSyncCountEventController.sink;

  NoteSyncCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    noteSyncCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    noteSyncCountSink.add(event);
  }

  void dispose() {
    noteSyncCountController.close();
    noteSyncCountEventController.close();
  }
}
