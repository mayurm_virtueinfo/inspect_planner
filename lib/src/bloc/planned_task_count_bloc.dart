import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class PlannedTaskCountBloc {


  final plannedTaskCountController = StreamController<int>.broadcast();
  StreamSink<int> get plannedTaskCountSink => plannedTaskCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get plannedTaskCountStream => plannedTaskCountController.stream;

  final plannedTaskCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get plannedTaskCountEventSink => plannedTaskCountEventController.sink;

  PlannedTaskCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    plannedTaskCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    plannedTaskCountSink.add(event);
  }

  void dispose() {
    plannedTaskCountController.close();
    plannedTaskCountEventController.close();
  }
}
