import 'dart:async';

import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class PrevTaskIndexBloc {


  final prevTaskIndexController = StreamController<int>.broadcast();
  StreamSink<int> get prevTaskIndexSink => prevTaskIndexController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get prevTaskIndexStream => prevTaskIndexController.stream;

  final prevIndexEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get prevTaskIndexEventSink => prevIndexEventController.sink;

  PrevTaskIndexBloc() {
    // Whenever there is a new event, we want to map it to a new state
    prevIndexEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    prevTaskIndexSink.add(event);
  }

  void dispose() {
    prevTaskIndexController.close();
    prevIndexEventController.close();
  }
}
