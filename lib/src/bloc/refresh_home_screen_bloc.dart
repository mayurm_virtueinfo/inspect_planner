import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class RefreshHomeScreenBloc {


  final refreshHomeController = StreamController<bool>.broadcast();
  StreamSink<bool> get refreshHomeSink => refreshHomeController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get refreshHomeStream => refreshHomeController.stream;

  final refreshHomeEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get refreshHomeEventSink => refreshHomeEventController.sink;

  RefreshHomeScreenBloc() {
    // Whenever there is a new event, we want to map it to a new state
    refreshHomeEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    refreshHomeSink.add(event);
  }

  void dispose() {
    refreshHomeController.close();
    refreshHomeEventController.close();
  }
}
