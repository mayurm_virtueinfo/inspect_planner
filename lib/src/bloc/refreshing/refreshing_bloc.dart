import 'dart:async';

import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/refreshing/refreshing_bloc_event.dart';




class RefreshingBloc {
  bool isLoading = true;

  final loadingController = StreamController<bool>.broadcast();
  StreamSink<bool> get _inUploadingWraprz => loadingController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get loadingStream => loadingController.stream;

  final loadingEventController = StreamController<RefreshingBlocEvent>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<RefreshingBlocEvent> get loadingEventSink => loadingEventController.sink;

  RefreshingBloc() {
    // Whenever there is a new event, we want to map it to a new state
    loadingEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(RefreshingBlocEvent event) {
    if (event is RefreshingEvent)
      isLoading=true;
    else
      isLoading=false;

    _inUploadingWraprz.add(isLoading);
  }

  void dispose() {
    loadingController.close();
    loadingEventController.close();
  }
}
