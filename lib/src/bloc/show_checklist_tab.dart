import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class ShowHideChecklistTabBloc {


  final showHideChecklistTabController = StreamController<bool>.broadcast();
  StreamSink<bool> get showHideChecklistTabSink => showHideChecklistTabController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get showHideChecklistTabStream => showHideChecklistTabController.stream;

  final showHideChecklistTabEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get showHideChecklistTabEventSink => showHideChecklistTabEventController.sink;

  ShowHideChecklistTabBloc() {
    // Whenever there is a new event, we want to map it to a new state
    showHideChecklistTabEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    showHideChecklistTabSink.add(event);
  }

  void dispose() {
    showHideChecklistTabController.close();
    showHideChecklistTabEventController.close();
  }
}
