import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class ShowHideFilterClearBloc {

  final showHideFilterClearControler = StreamController<int>.broadcast();
  StreamSink<int> get showHideFilterClearSink => showHideFilterClearControler.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get showHideFilterClearStream => showHideFilterClearControler.stream;

  final showHideFilterClearEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get showHideFilterClearEventSink => showHideFilterClearEventController.sink;

  ShowHideFilterClearBloc() {
    // Whenever there is a new event, we want to map it to a new state
    showHideFilterClearEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    showHideFilterClearSink.add(event);
  }

  void dispose() {
    showHideFilterClearControler.close();
    showHideFilterClearEventController.close();
  }
}
