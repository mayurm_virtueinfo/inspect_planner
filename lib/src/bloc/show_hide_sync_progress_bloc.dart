import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class ShowHideSyncProgressBloc {

  final showHideSyncProgressController = StreamController<bool>.broadcast();
  StreamSink<bool> get showHideSyncProgressSink => showHideSyncProgressController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get showHideSyncProgressStream => showHideSyncProgressController.stream;

  final showHideSyncProgressEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get showHideSyncProgressEventSink => showHideSyncProgressEventController.sink;

  ShowHideSyncProgressBloc() {
    // Whenever there is a new event, we want to map it to a new state
    showHideSyncProgressEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    showHideSyncProgressSink.add(event);
  }

  void dispose() {
    showHideSyncProgressController.close();
    showHideSyncProgressEventController.close();
  }
}
