import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class ShowSearchBloc {


  final showSearchController = StreamController<bool>.broadcast();
  StreamSink<bool> get showSearchSink => showSearchController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get showSearchStream => showSearchController.stream;

  final showSearchEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get showSearchEventSink => showSearchEventController.sink;

  ShowSearchBloc() {
    // Whenever there is a new event, we want to map it to a new state
    showSearchEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    showSearchSink.add(event);
  }

  void dispose() {
    showSearchController.close();
    showSearchEventController.close();
  }
}
