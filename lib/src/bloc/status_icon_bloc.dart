import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class StatusIconBloc {


  final statusIconController = StreamController<String>.broadcast();
  StreamSink<String> get statusIconSink => statusIconController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get statusIconStream => statusIconController.stream;

  final statusIconEventController = StreamController<String>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<String> get statusIconEventSink => statusIconEventController.sink;

  StatusIconBloc() {
    // Whenever there is a new event, we want to map it to a new state
    statusIconEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(String event) {
    statusIconSink.add(event);
  }

  void dispose() {
    statusIconController.close();
    statusIconEventController.close();
  }
}
