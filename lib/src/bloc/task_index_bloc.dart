import 'dart:async';

import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class TaskIndexBloc {


  final taskIndexController = StreamController<int>.broadcast();
  StreamSink<int> get taskIndexSink => taskIndexController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get taskIndexStream => taskIndexController.stream;

  final taskIndexEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get taskIndexEventSink => taskIndexEventController.sink;

  TaskIndexBloc() {
    // Whenever there is a new event, we want to map it to a new state
    taskIndexEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    taskIndexSink.add(event);
  }

  void dispose() {
    taskIndexController.close();
    taskIndexEventController.close();
  }
}
