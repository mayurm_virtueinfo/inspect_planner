import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class TaskListBloc {


  final taskListController = StreamController<List<dynamic>>.broadcast();
  StreamSink<List<dynamic>> get taskListSink => taskListController.sink;
  // For state, exposing only a stream which outputs data
  Stream<List<dynamic>> get taskListStream => taskListController.stream;

  final taskListEventController = StreamController<List<dynamic>>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<List<dynamic>> get taskListEventSink => taskListEventController.sink;

  TaskListBloc() {
    // Whenever there is a new event, we want to map it to a new state
    taskListEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(List<dynamic> event) {
    taskListSink.add(event);
  }

  void dispose() {
    taskListController.close();
    taskListEventController.close();
  }
}
