import 'dart:async';

import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';



class TaskModelBloc {


  final taskModelController = StreamController<TaskModel>.broadcast();
  StreamSink<TaskModel> get taskModelSink => taskModelController.sink;
  // For state, exposing only a stream which outputs data
  Stream<TaskModel> get taskModelStream => taskModelController.stream;

  final taskModelEventController = StreamController<TaskModel>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<TaskModel> get taskModelEventSink => taskModelEventController.sink;

  TaskModelBloc() {
    // Whenever there is a new event, we want to map it to a new state
    taskModelEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(TaskModel event) {
    taskModelSink.add(event);
  }

  void dispose() {
    taskModelController.close();
    taskModelEventController.close();
  }
}
