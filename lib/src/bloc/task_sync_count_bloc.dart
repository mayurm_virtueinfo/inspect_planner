import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class TaskSyncCountBloc {


  final taskSyncCountController = StreamController<int>.broadcast();
  StreamSink<int> get taskSyncCountSink => taskSyncCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get taskSyncCountStream => taskSyncCountController.stream;

  final taskSyncCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get taskSyncCountEventSink => taskSyncCountEventController.sink;

  TaskSyncCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    taskSyncCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    taskSyncCountSink.add(event);
  }

  void dispose() {
    taskSyncCountController.close();
    taskSyncCountEventController.close();
  }
}
