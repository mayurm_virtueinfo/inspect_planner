import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class TodayTaskCountBloc {


  final todayTaskCountController = StreamController<int>.broadcast();
  StreamSink<int> get todayTaskCountSink => todayTaskCountController.sink;
  // For state, exposing only a stream which outputs data
  Stream<int> get todayTaskCountStream => todayTaskCountController.stream;

  final todayTaskCountEventController = StreamController<int>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<int> get todayTaskCountEventSink => todayTaskCountEventController.sink;

  TodayTaskCountBloc() {
    // Whenever there is a new event, we want to map it to a new state
    todayTaskCountEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(int event) {
    todayTaskCountSink.add(event);
  }

  void dispose() {
    todayTaskCountController.close();
    todayTaskCountEventController.close();
  }
}
