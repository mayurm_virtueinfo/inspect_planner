import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/utils/sort_type.dart';



class UpDownSortBloc {


  final upDownSortController = StreamController<SortType>.broadcast();
  StreamSink<SortType> get upDownSortSink => upDownSortController.sink;
  // For state, exposing only a stream which outputs data
  Stream<SortType> get upDownSortStream => upDownSortController.stream;

  final upDownSortEventController = StreamController<SortType>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<SortType> get upDownSortEventSink => upDownSortEventController.sink;

  UpDownSortBloc() {
    // Whenever there is a new event, we want to map it to a new state
    upDownSortEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(SortType event) {
    upDownSortSink.add(event);
  }

  void dispose() {
    upDownSortController.close();
    upDownSortEventController.close();
  }
}
