import 'dart:async';

import 'package:inspect_planner/src/models/user_model.dart';



class UserModelBloc {


  final userModelController = StreamController<UserModel>.broadcast();
  StreamSink<UserModel> get userModelSink => userModelController.sink;
  // For state, exposing only a stream which outputs data
  Stream<UserModel> get userModelStream => userModelController.stream;

  final userModelEventController = StreamController<UserModel>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<UserModel> get userModelEventSink => userModelEventController.sink;

  UserModelBloc() {
    // Whenever there is a new event, we want to map it to a new state
    userModelEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(UserModel event) {
    userModelSink.add(event);
  }

  void dispose() {
    userModelController.close();
    userModelEventController.close();
  }
}
