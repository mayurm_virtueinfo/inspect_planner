import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/add_note_dialog_bloc.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';


class DialogAddNote extends StatefulWidget {
  TaskModel taskModel;
  bool isFromDialogValidation;
  DialogAddNote({this.taskModel,this.isFromDialogValidation=false,Key key}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogAddNote> {
  // Task type
  List<DropdownMenuItem<UserTableModel>> _ddmUser;
  UserTableModel selectedUser;
  List<UserTableModel> listUserTableModel = [];
  TextEditingController noteController = TextEditingController();
  bool isNoteValid=true;
  String noteError = '';
  TextStyle loginScreenErrorTextStyle = TextStyle(color: Colors.red, fontSize: 10);
  @override
  void initState() {
    // TODO: implement initState
    getUser();

    super.initState();
  }

  void getUser()async{
    String sql = 'select * from ${AppTables.USER} where customer_id = ? and removed = ?';
    List<dynamic> sqlValues = [appUserModel.customer_id,0];
    List<Map<String,dynamic>> listUser = await appDatabase.rawQuery(sql,sqlValues);
    UserTableModel utModel = UserTableModel(id: -1,name: AppStrings.commentNotLinked);
    List<UserTableModel> listUserTableModel= [];
    listUserTableModel.add(utModel);
    listUser.forEach((element) {
      UserTableModel userTableModel = UserTableModel.fromMap(map: element);
      listUserTableModel.add(userTableModel);
    });
    _ddmUser = buildDropDownMenuItemsTaskType(listUserTableModel);
    selectedUser = _ddmUser[0].value;
    setState((){});
  }
  List<DropdownMenuItem<UserTableModel>> buildDropDownMenuItemsTaskType(List listItems) {
    List<DropdownMenuItem<UserTableModel>> items = [];
    for (UserTableModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  @override
  Widget build(BuildContext context) {
    double staticWidth = 2.5;
    double portraitHeight = 0.45;
    double landscapeHeight = 0.6;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*staticWidth;
    double dialogHeight = (MediaQuery.of(context).size.height);
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: OrientationBuilder(
          builder: (context, orientation) {
            return Container(
              width:dialogWidth,
              height: orientation == Orientation.landscape ? dialogHeight*landscapeHeight:dialogHeight*portraitHeight,
              child: Stack(
                children: [
                  SingleChildScrollView(

                    child: Container(
                      height: orientation == Orientation.landscape ? dialogHeight*landscapeHeight:dialogHeight*portraitHeight,
                      width: dialogWidth,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Container(
                                child: Column(
                                  children: [
                                    DialogTitleWidget(
                                      title: AppStrings.notitie_toevoegen,
                                      hideCloseIcon: widget.isFromDialogValidation?true:false,
                                    ),
                                    SizedBox(height: 20,),
                                    Container(
                                      padding: EdgeInsets.only(left: 30,right: 30),
                                      // width: (dialogWidth/5)*3.5,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(AppStrings.notitie,style: TextStyle(
                                              color: MyColors.colorConvert('#797979'),
                                              fontSize: 18
                                          ),),
                                          Container(
                                            width: (dialogWidth/5)*staticWidth,
                                            padding: EdgeInsets.only(left: 10, right: 10),
                                            decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                border: Border.all(width: 1.0, color: MyColors.colorConvert('#5BB0EC')),
                                                borderRadius: BorderRadius.circular(10),
                                                color: Colors.white /*MyColors.dropdownBackgroudColor*/
                                            ),
                                            child: TextField(
                                              controller: noteController,
                                              keyboardType: TextInputType.multiline,
                                              minLines: 5,
                                              maxLines: 6,
                                              decoration: new InputDecoration(
                                                isDense: true,
                                                contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                                labelText: "",
                                                filled: true,

                                                fillColor: Colors.white,
                                                border: textFieldOutlineInputBorder,
                                                focusedBorder: textFieldOutlineInputBorder,
                                                disabledBorder: textFieldOutlineInputBorder,
                                                enabledBorder: textFieldOutlineInputBorder,
                                                errorBorder: textFieldOutlineInputBorder,
                                                focusedErrorBorder: textFieldOutlineInputBorder,

                                                //fillColor: Colors.green
                                              ),
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(left: 30,right: 30,),
                                      // width: (dialogWidth/5)*3.5,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Text('',style: TextStyle(
                                              color: MyColors.colorConvert('#797979'),
                                              fontSize: 18
                                          ),),
                                          Container(
                                            width: (dialogWidth/5)*staticWidth,
                                            padding: EdgeInsets.only(left: 10, right: 10),
                                            height: 50,
                                            child: isNoteValid
                                                ? Container()
                                                : Column(
                                              children: [

                                                Row(
                                                  // mainAxisAlignment: MainAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      AppStrings.enterNote,
                                                      style: loginScreenErrorTextStyle.copyWith(
                                                          fontSize: 18
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 20,),
                                    Container(
                                      padding: EdgeInsets.only(left: 30,right: 30,),
                                      // width: (dialogWidth/5)*3.5,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Text(AppStrings.linkedToUser,style: TextStyle(
                                              color: MyColors.colorConvert('#797979'),
                                              fontSize: 18
                                          ),),
                                          Container(
                                            width: (dialogWidth/5)*staticWidth,
                                            padding: EdgeInsets.only(left: 10, right: 10),
                                            decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                border: Border.all(width: 1.0, color: MyColors.colorConvert('#5BB0EC')),
                                                borderRadius: BorderRadius.circular(10),
                                                color: Colors.white /*MyColors.dropdownBackgroudColor*/
                                            ),
                                            height: 50,
                                            child: DropdownButtonHideUnderline(

                                              child: DropdownButton(
                                                  isExpanded: true,
                                                  icon: Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: MyColors.colorConvert('#5BB0EC'),
                                                    size: 25,
                                                  ),
                                                  value: selectedUser,
                                                  items: _ddmUser,
                                                  onChanged: (value) async {
                                                    setState((){
                                                      selectedUser = value;
                                                      FocusScope.of(context).requestFocus(FocusNode());
                                                    });
                                                  }),
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 20,),
                                  ],
                                ),
                              )
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                                color: MyColors.colorConvert('#F4F4F4')
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [

                                Container(
                                  width: 150,
                                  height: 50,
                                  child: ElevatedButton(

                                    style: ElevatedButton.styleFrom(
                                      padding: EdgeInsets.all(0),
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      primary: Colors.transparent,
                                    ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                      if (states.contains(MaterialState.pressed)) {
                                        return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                      }
                                      return null;
                                    })),
                                    onPressed: () async {
                                      if(widget.isFromDialogValidation){
                                        locator<AddNoteDialogBloc>().addNoteDialogEventSink.add(null);
                                      }else{
                                        Navigator.of(context).pop();
                                      }

                                    },
                                    child: Text(AppStrings.annuleren, style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black
                                    )),
                                  ),
                                ),

                                Container(
                                  width: 150,
                                  height: 50,
                                  child: ElevatedButton(

                                    style: ElevatedButton.styleFrom(
                                      padding: EdgeInsets.all(0),
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      primary: MyColors.colorConvert('#266E9F'),
                                    ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                      if (states.contains(MaterialState.pressed)) {
                                        return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                      }
                                      return null;
                                    })),
                                    onPressed: () async {
                                      //onPressOps
                                      if(noteController.text == ''){
                                        setState(() {
                                          isNoteValid = false;
                                        });
                                        return;
                                      }
                                      setState(() {
                                        isNoteValid = true;
                                      });
                                      debugPrint('noteController : ${noteController.text}');


                                      int maxId = new DateTime.now().millisecondsSinceEpoch;
                                      debugPrint("new note id : ${maxId}");
                                      Map<String,dynamic> mapAddNote = Map();
                                      mapAddNote['id']=maxId;
                                      mapAddNote['user_id']=appUserModel.id;
                                      mapAddNote['task_id']=widget.taskModel.id;
                                      mapAddNote['note']=noteController.text;
                                      mapAddNote['create_date']=DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
                                      mapAddNote['assignedto_user_id']=selectedUser.id==-1?null:selectedUser.id;
                                      debugPrint("map Note add : $mapAddNote");
                                      NoteModel nNoteModel = NoteModel.fromMap(map: mapAddNote);
                                      nNoteModel.synced = 0;
                                      nNoteModel.removed=0;
                                      debugPrint("add note : ${mapAddNote.toString()}");
                                      String sqlNoteLstItmMdl = AppTablesQuery.INSERT_TABLE_NOTE + '(?,?,?,?,?,?,?,?)';
                                      int insertResult = await appDatabase.rawInsert(sqlNoteLstItmMdl, nNoteModel.getDataList(nNoteModel));
                                      if(insertResult == 0){
                                        debugPrint("Error inserting note");
                                        MyToast.showToast(AppStrings.err_error_inserting_task, context);
                                        return;
                                      }
                                      var connectivityResult = await (Connectivity().checkConnectivity());
                                      if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1) {
                                        //network availabe - sync add task
                                        maxId = await AppSync.addNote(context: context,mapAddNote: mapAddNote);
                                        if(maxId == syncErrorValue){
                                          return;
                                        }
                                        debugPrint("AddTask sync success");
                                      }

                                      if(widget.isFromDialogValidation){
                                        locator<AddNoteDialogBloc>().addNoteDialogEventSink.add(null);
                                      }else{
                                        Navigator.of(context).pop();
                                      }


                                      debugPrint("Note add result : $insertResult");
                                      locator<DetailTabBloc>().detailTabEventSink.add(2);
                                      locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
                                    },
                                    child: Text(widget.isFromDialogValidation?AppStrings.opslaan:AppStrings.bewaar, style: TextStyle(
                                        fontSize: 17
                                    )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          //end
                        ],
                      ),
                    ),
                  ),
                  LoadingWidget()
                ],
              ),
            );
          },
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}