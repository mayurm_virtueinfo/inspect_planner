import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/models/attachment_model.dart';
import 'package:inspect_planner/src/models/task_frequency_list_model.dart';
import 'package:inspect_planner/src/models/task_frequency_model.dart';
import 'package:inspect_planner/src/models/task_frequency_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:path/path.dart' as p;
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/source.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


class DialogAddTask extends StatefulWidget {
  DialogAddTask({Key key,}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogAddTask> {

  Color switchEnablColor = MyColors.colorConvert('#EBEDF3');
  Color switchDisableColor = MyColors.colorConvert('#EBEDF3');
  bool isNotValidTitle = false;
  bool isNotValidEveryWeek = false;
  bool isNotValidEveryMonth = false;
  bool isNotValidTypeTask = false;
  bool isNotValidFrequency = false;
  String inValidFrequencyErrMsg = "";
  bool isNotValidFunction = false;
  bool isNotValidLocation = false;
  bool isNotValidDescription = false;
  bool isNotValidAttachmnt = false;

  Map<String,dynamic> mapSelectedFile=Map();
  TextEditingController titleController = TextEditingController();
  TextEditingController everyWeekController = TextEditingController();
  TextEditingController everyMonthController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  DateTime selectedStartDate = DateTime.now();

  // Frequency
  List<DropdownMenuItem<TaskFrequencyModel>> _ddmFrequency;
  TaskFrequencyModel selectedFrequency;
  List<TaskFrequencyModel> listDDMFrequency = [];


  // Task type
  List<DropdownMenuItem<TaskTypeModel>> _ddmTaskType;
  TaskTypeModel selectedTaskType;
  List<TaskTypeModel> listDDMTaskType = [];

  // Function
  List<DropdownMenuItem<FunctionsModel>> _ddmFunction;
  FunctionsModel selectedFunction;
  List<FunctionsModel> listDDMFunction = [];

  // location
  List<DropdownMenuItem<LocationModel>> _ddmLocation;
  LocationModel selectedLocation;
  List<LocationModel> listDDMLocation = [];




  // List<Map<String,dynamic>> listFiles=[];
  final String mKey ='key';
  // key = to view display -> view/image/other
  final String mPath ='path';
  // path = file path -> url/filepath
  final String mType = 'type';
  // type = file extension -> '.jpg/.png/...'
  final String mSource = 'source';
  // source = from web or local file -> Source.WEB, Source.LOCAL
  final String mData = 'data';
  // data = data of local or web database
  bool isCheckedMorning = false;
  bool isCheckedAfternoon = false;
  bool isCheckedEvening = false;
  bool isCheckedMonday = false;
  bool isCheckedTuesday = false;
  bool isCheckedWednesday = false;
  bool isCheckedThursday = false;
  bool isCheckedFriday = false;
  bool isCheckedSaturday = false;
  bool isCheckedSunday = false;
  @override
  void initState() {
    // TODO: implement initState
    getTaskType();
    getFunction();
    getLocation();
    getFrequency();
    // initAttachments();
    super.initState();
  }
  void initAttachments(){
    Map<String,dynamic> mapFile = Map();
    mapFile['key']='view';
    // listFiles.add(mapFile);

    /*widget.listAttachment.forEach((element) {
      String imageName = element['image_name'];
      Map<String,dynamic> mapFile = Map();
      if(imageName.startsWith('http')){
        mapFile[mSource]=Source.WEB;
      }else{
        mapFile[mSource]=Source.LOCAL;
      }

      File file = File(imageName);
      debugPrint("file : ${file.path}");
      String extension = p.context.extension(file.path);
      if(isImage(file)){
        mapFile[mKey]='image';
      }else{
        mapFile[mKey]='other';
      }

      mapFile[mPath]=imageName;
      mapFile[mType]=extension;
      mapFile[mData]=element;


      listFiles.add(mapFile);
    });*/

    setState(() {});
  }
  void getTaskType()async{
    String sql = 'select * from ${AppTables.TASK_TYPE} order by name asc';
    List<Map<String,dynamic>> listTaskType = await appDatabase.rawQuery(sql);
    TaskTypeListModel taskTypeListModel = TaskTypeListModel();
    TaskTypeModel mTTM = TaskTypeModel(id: -1,name: AppStrings.select);
    List<TaskTypeModel> lDDMTT= await taskTypeListModel.getTaskList(listTaskType);
    listDDMTaskType.add(mTTM);
    listDDMTaskType.addAll(lDDMTT);
    _ddmTaskType = buildDropDownMenuItemsTaskType(listDDMTaskType);

    selectedTaskType = _ddmTaskType[0].value;
    setState((){});
  }
  void getFunction()async{
    String sql = 'select * from ${AppTables.FUNCTIONS} order by name asc';
    List<Map<String,dynamic>> listTaskType = await appDatabase.rawQuery(sql);
    FunctionsListModel functionListModel = FunctionsListModel();
    FunctionsModel mFM = FunctionsModel(id: -1,name: AppStrings.select);
    List<FunctionsModel> lDDMFunction= await functionListModel.getTaskList(listTaskType);
    listDDMFunction.add(mFM);
    listDDMFunction.addAll(lDDMFunction);
    _ddmFunction = buildDropDownMenuItemsFunction(listDDMFunction);

    selectedFunction = _ddmFunction[0].value;
    setState((){});
  }
  void getFrequency()async{
    String sql = 'select * from ${AppTables.TASK_FREQUENCY} order by sort_order asc';
    List<Map<String,dynamic>> listFrequency = await appDatabase.rawQuery(sql);
    TaskFrequencyListModel frequencyListModel = TaskFrequencyListModel();
    List<TaskFrequencyModel> lDDMFrequency= await frequencyListModel.getTaskList(listFrequency);
    listDDMFrequency.addAll(lDDMFrequency);
    _ddmFrequency = buildDropDownMenuItemsFrequency(listDDMFrequency);

    selectedFrequency = _ddmFrequency[0].value;
    setState((){});
  }
  void getLocation()async{
    String sql = 'select * from ${AppTables.LOCATION} order by name asc';
    List<Map<String,dynamic>> listTaskType = await appDatabase.rawQuery(sql);
    LocationListModel locationListModel = LocationListModel();
    LocationModel mLM=LocationModel(id: -1,name: AppStrings.select);
    List<LocationModel>  lLM=await locationListModel.getTaskList(listTaskType);
    listDDMLocation.add(mLM);
    listDDMLocation.addAll(lLM);
    _ddmLocation = buildDropDownMenuItemsLocation(listDDMLocation);

      selectedLocation = _ddmLocation[0].value;
    setState((){});
  }
  List<DropdownMenuItem<TaskTypeModel>> buildDropDownMenuItemsTaskType(List listItems) {
    List<DropdownMenuItem<TaskTypeModel>> items = [];
    for (TaskTypeModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  List<DropdownMenuItem<FunctionsModel>> buildDropDownMenuItemsFunction(List listItems) {
    List<DropdownMenuItem<FunctionsModel>> items = [];
    for (FunctionsModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  List<DropdownMenuItem<TaskFrequencyModel>> buildDropDownMenuItemsFrequency(List listItems) {
    List<DropdownMenuItem<TaskFrequencyModel>> items = [];
    for (TaskFrequencyModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  List<DropdownMenuItem<LocationModel>> buildDropDownMenuItemsLocation(List listItems) {
    List<DropdownMenuItem<LocationModel>> items = [];
    for (LocationModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/10)*9;
    double dialogHeight = (MediaQuery.of(context).size.height/10)*9;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth ,
          height: dialogHeight,
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    DialogTitleWidget(title: AppStrings.addTask,),
                    Padding(
                      padding: const EdgeInsets.only(left: 50,right: 50,top: 20,bottom: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 5,
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Task type
                                  SizedBox(height: 20,),
                                  Container(
                                    // width: (dialogWidth/5)*3.5,
                                    child: Text(AppStrings.title,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    height: loginBoxHeight / 8,
                                    child: TextField(
                                      controller: titleController,
                                      decoration: new InputDecoration(
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                        labelText: "",
                                        filled: true,
                                        fillColor: MyColors.colorConvert('#f4f4f4'),
                                        border: textFieldOutlineInputBorder,
                                        focusedBorder: textFieldOutlineInputBorder,
                                        disabledBorder: textFieldOutlineInputBorder,
                                        enabledBorder: textFieldOutlineInputBorder,
                                        errorBorder: textFieldOutlineInputBorder,
                                        focusedErrorBorder: textFieldOutlineInputBorder,

                                        //fillColor: Colors.green
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                  ),
                                  isNotValidTitle?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(AppStrings.pleaseEnterTitle,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),
                                  SizedBox(height: 10,),
                                  Container(
                                    // width: (dialogWidth/5)*3.5,
                                    child: Text(AppStrings.taskType,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    width: double.infinity,
                                    child: Container(
                                      // width: (dialogWidth/5)*3,
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                                          borderRadius: borderRadius,
                                          color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                                      ),
                                      height: 50,
                                      child: DropdownButtonHideUnderline(

                                        child: DropdownButton(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: MyColors.colorConvert('#d8d8d8'),
                                              size: 25,
                                            ),
                                            value: selectedTaskType,
                                            items: _ddmTaskType,
                                            onChanged: (value) async {
                                              FocusScope.of(context).requestFocus(FocusNode());
                                              setState((){
                                                selectedTaskType = value;
                                              });
                                            }),
                                      ),
                                    ),
                                  ),
                                  isNotValidTypeTask?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(AppStrings.pleseSelectTaskType,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),
                                  // Function
                                  SizedBox(height: 20,),
                                  Container(
                                    child: Text(AppStrings.taskFunction,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    width: double.infinity,
                                    child: Container(
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                                          borderRadius: borderRadius,
                                          color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                                      ),
                                      height: 50,
                                      child: DropdownButtonHideUnderline(

                                        child: DropdownButton(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: MyColors.colorConvert('#d8d8d8'),
                                              size: 25,
                                            ),
                                            value: selectedFunction,
                                            items: _ddmFunction,
                                            onChanged: (value) async {
                                              FocusScope.of(context).requestFocus(FocusNode());
                                              setState((){
                                                selectedFunction = value;
                                              });
                                            }),
                                      ),
                                    ),
                                  ),
                                  isNotValidFunction?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(AppStrings.pleaseSelectFunction,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),
                                  // Location
                                  SizedBox(height: 20,),
                                  Container(
                                    child: Text(AppStrings.taskLocation,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    width: double.infinity,
                                    child: Container(
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                                          borderRadius: borderRadius,
                                          color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                                      ),
                                      height: 50,
                                      child: DropdownButtonHideUnderline(

                                        child: DropdownButton(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: MyColors.colorConvert('#d8d8d8'),
                                              size: 25,
                                            ),
                                            value: selectedLocation,
                                            items: _ddmLocation,
                                            onChanged: (value) async {
                                              FocusScope.of(context).requestFocus(FocusNode());
                                              setState((){
                                                selectedLocation = value;
                                              });
                                            }),
                                      ),
                                    ),
                                  ),
                                  isNotValidLocation?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(AppStrings.pleaseSelectLocation,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),
                                  // Start date
                                  SizedBox(height: 20,),
                                  Container(
                                    child: Text(AppStrings.deadlineDate,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    child: GestureDetector(
                                      onTap: () {
                                        /*appUserModel.is_manager = 0;
                                        appUserModel.role = 'ROLE_USER';*/
                                        int isManager = appUserModel.is_manager;
                                        debugPrint("isManager : $isManager");
                                        debugPrint("role : ${appUserModel.role}");
                                        if (appUserModel.role == 'ROLE_USER' && appUserModel.is_manager==0){
                                          return;
                                        }
                                        selectStartDate(context);
                                      },
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        padding: EdgeInsets.only(left: 10, right: 10),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                                            borderRadius: borderRadius,
                                            color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                                        ),
                                        height: 50,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              DateFormat('dd-MM-yyyy').format(selectedStartDate),
                                              style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
                                            ),
                                            Icon(
                                              Icons.keyboard_arrow_down,
                                              color: MyColors.colorConvert('#d8d8d8'),
                                              size: 25,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 20,),
                                  Container(
                                    width: (dialogWidth/5)*3.5,
                                    height: 50,
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        padding: EdgeInsets.all(0),
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                        primary: MyColors.colorConvert('#646464'),
                                      ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                        if (states.contains(MaterialState.pressed)) {
                                          return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                        }
                                        return null;
                                      })),
                                      onPressed: () async {
                                        FocusScope.of(context).requestFocus(FocusNode());
                                        if(titleController.text == ''){
                                          isNotValidTitle = true;
                                        }
                                        else{
                                          isNotValidTitle = false;
                                        }

                                        if(selectedTaskType.id == -1){
                                          isNotValidTypeTask = true;
                                        }else{
                                          isNotValidTypeTask = false;
                                        }

                                        if(selectedFunction.id == -1){
                                          isNotValidFunction = true;
                                        }else{
                                          isNotValidFunction = false;
                                        }

                                        // if(selectedFrequency.id == 1){
                                        //   isNotValidFrequency = true;
                                        //   inValidFrequencyErrMsg=AppStrings.pleseSelectFrequency;
                                        // }else if(selectedFrequency.id == 2){
                                        //   debugPrint("---2");
                                        //   if(!isCheckedMorning && !isCheckedAfternoon && !isCheckedEvening){
                                        //     debugPrint("---2-1");
                                        //     isNotValidFrequency = true;
                                        //     inValidFrequencyErrMsg=AppStrings.pleaseSelectDayparts;
                                        //   }else if(!isCheckedMonday && !isCheckedTuesday && !isCheckedWednesday && !isCheckedThursday && !isCheckedFriday && !isCheckedSaturday && !isCheckedSunday){
                                        //     debugPrint("---2-2");
                                        //     isNotValidFrequency = true;
                                        //     inValidFrequencyErrMsg=AppStrings.pleaseSelectDaysOfWeek;
                                        //   }else {
                                        //     debugPrint("---2-3");
                                        //     isNotValidLocation = false;
                                        //   }
                                        // }else if(selectedFrequency.id == 3){
                                        //   if(everyWeekController.text.toString() == ""){
                                        //     isNotValidFrequency = true;
                                        //     inValidFrequencyErrMsg=AppStrings.pleaseEnterNumberOfWeeks;
                                        //   }else{
                                        //     isNotValidFrequency = false;
                                        //   }
                                        // }else if(selectedFrequency.id == 4){
                                        //   if(everyMonthController.text.toString() == ""){
                                        //     isNotValidFrequency = true;
                                        //     inValidFrequencyErrMsg=AppStrings.pleaseEnterNumberOfMonth;
                                        //   }else{
                                        //     isNotValidFrequency = false;
                                        //   }
                                        // }

                                        if(selectedLocation.id == -1){
                                          isNotValidLocation = true;
                                        }else{
                                          isNotValidLocation = false;
                                        }

                                        if(descriptionController.text==''){
                                          isNotValidDescription = true;
                                        }else{
                                          isNotValidDescription = false;
                                        }

                                        /*if(mapSelectedFile.length == 0){
                                          isNotValidAttachmnt = true;
                                        }else{
                                          isNotValidAttachmnt = false;
                                        }*/

                                        if(isNotValidTitle == false && isNotValidTypeTask == false && isNotValidFunction == false && isNotValidLocation == false && isNotValidDescription == false /*&& isNotValidAttachmnt == false*/) {

                                          int maxTaskId = new DateTime.now().millisecondsSinceEpoch;
                                          debugPrint("new task id : $maxTaskId");

                                          Map<String,dynamic> mapNewTask = Map();
                                          mapNewTask['id'] = maxTaskId??'';
                                          mapNewTask['customer_id'] = appUserModel.customer_id??'';
                                          mapNewTask['location_id'] = selectedLocation.id??'';
                                          mapNewTask['group_id'] = selectedFunction.id??'';
                                          mapNewTask['frequency_id'] = selectedFrequency.id??'';
                                          mapNewTask['user_id'] = appUserModel.id??'';
                                          mapNewTask['clone_task_id'] = null;
                                          mapNewTask['title'] = titleController.text;
                                          mapNewTask['description'] = descriptionController.text;
                                          mapNewTask['create_date'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
                                          mapNewTask['removed'] = 0;
                                          mapNewTask['stamitem'] = 0;
                                          mapNewTask['customer_stamitem'] = 0;
                                          mapNewTask['start_date'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(selectedStartDate);
                                          mapNewTask['tempate_id'] = null;
                                          mapNewTask['type_id'] = selectedTaskType.id;

                                          mapNewTask['last_edit_user_id'] = null;
                                          mapNewTask['status_id'] = 1;
                                          mapNewTask['createdby_user_id'] = appUserModel.id;
                                          mapNewTask['createdby_user_name'] = appUserModel.name;
                                          mapNewTask['finish_date'] = null;
                                          mapNewTask['deadline_notification_count'] = 0;
                                          mapNewTask['frequency_step_number'] = null;
                                          mapNewTask['frequency_daily_part_of_day'] = null;
                                          mapNewTask['part_of_day_this_task'] = null;
                                          mapNewTask['active_daypart'] = 0;
                                          mapNewTask['currunt_active_daypart'] = 0;


                                          int dayPart1 = 0;
                                          int dayPart2 = 0;
                                          int dayPart3 = 0;
                                          int totalDayPart = 0;

                                          int dayOfWeek1 = 0;
                                          int dayOfWeek2 = 0;
                                          int dayOfWeek3 = 0;
                                          int dayOfWeek4 = 0;
                                          int dayOfWeek5 = 0;
                                          int dayOfWeek6 = 0;
                                          int dayOfWeek7 = 0;
                                          int totalDayOfWeek = 0;

                                          if(selectedFrequency.id == 2){
                                            if(isCheckedMorning){
                                              dayPart1 = 1;
                                            }
                                            if(isCheckedAfternoon){
                                              dayPart2 = 2;
                                            }
                                            if(isCheckedEvening){
                                              dayPart3 = 4;
                                            }
                                            if(isCheckedMonday){
                                              dayOfWeek1=1;
                                            }
                                            if(isCheckedTuesday){
                                              dayOfWeek2=2;
                                            }
                                            if(isCheckedWednesday){
                                              dayOfWeek3=4;
                                            }
                                            if(isCheckedThursday){
                                              dayOfWeek4=8;
                                            }
                                            if(isCheckedFriday){
                                              dayOfWeek5=16;
                                            }
                                            if(isCheckedSaturday){
                                              dayOfWeek6=32;
                                            }
                                            if(isCheckedSunday){
                                              dayOfWeek7=64;
                                            }
                                            totalDayOfWeek = dayOfWeek1+dayOfWeek2+dayOfWeek3+dayOfWeek4+dayOfWeek5+dayOfWeek6+dayOfWeek7;
                                            totalDayPart = dayPart1+dayPart2+dayPart3;

                                            if (totalDayOfWeek == 0) {
                                              totalDayOfWeek = 127;
                                            }

                                            if (totalDayPart == 0) {
                                              // totalDayPart = 7;
                                              dayPart1 = 1;
                                            }

                                            int active_daypart = 0;
                                            int currunt_active_daypart = 0;

                                            if (dayPart1 != 0) {
                                              active_daypart = 1;
                                              currunt_active_daypart = 1;
                                            }
                                            if (active_daypart == 0) {
                                              if (dayPart2 != 0) {
                                                active_daypart = 2;
                                                currunt_active_daypart = 2;
                                              }
                                            }
                                            if (active_daypart == 0) {
                                              if (dayPart3 != 0) {
                                                active_daypart = 4;
                                                currunt_active_daypart = 4;
                                              }
                                            }

                                            mapNewTask['frequency_step_number'] = totalDayOfWeek;
                                            mapNewTask['frequency_daily_part_of_day'] = totalDayPart;
                                            mapNewTask['part_of_day_this_task'] = totalDayPart == 7 ? 1 : totalDayPart;
                                            mapNewTask['active_daypart'] = active_daypart;
                                            mapNewTask['currunt_active_daypart'] = currunt_active_daypart;

                                          }
                                          if(selectedFrequency.id == 3){
                                            //week
                                            if(everyWeekController.text.toString() == ""){
                                              mapNewTask['frequency_step_number']=1;
                                            }else{
                                              mapNewTask['frequency_step_number'] = int.parse(everyWeekController.text.toString());
                                            }
                                          }else if(selectedFrequency.id == 4){
                                            //month
                                            if(everyMonthController.text.toString() == ""){
                                              mapNewTask['frequency_step_number']=1;
                                            }else{
                                              mapNewTask['frequency_step_number'] = int.parse(everyMonthController.text.toString());
                                            }
                                          }

                                          mapNewTask['days_active'] = 0;

                                          String sqlTT = 'select * from ${AppTables.TASK_TYPE} where id = ?';
                                          List<dynamic> sqlTTValues = [selectedTaskType.id];
                                          List<Map<String,dynamic>> lstTT = await appDatabase.rawQuery(sqlTT,sqlTTValues);
                                          if(lstTT.length>0){
                                            Map<String,dynamic> mapTT = lstTT[0];
                                            TaskTypeModel ttModel = TaskTypeModel.fromMap(map: mapTT);
                                            mapNewTask['tasktype_name'] = ttModel.name;
                                          }

                                          mapNewTask['frequency_name']  = selectedFrequency.name;

                                          String sqlL = 'select * from ${AppTables.LOCATION} where id = ?';
                                          List<dynamic> sqlLValues = [selectedLocation.id];
                                          List<Map<String,dynamic>> lstL = await appDatabase.rawQuery(sqlL,sqlLValues);
                                          if(lstL.length>0){
                                            Map<String,dynamic> mapL = lstL[0];
                                            LocationModel lModel = LocationModel.fromMap(map: mapL);
                                            mapNewTask['location_name']  = lModel.name;
                                          }

                                          String sqlFN = 'select * from ${AppTables.FUNCTIONS} where id = ?';
                                          List<dynamic> sqlFNValues = [selectedFunction.id];
                                          List<Map<String,dynamic>> lstFN = await appDatabase.rawQuery(sqlFN,sqlFNValues);
                                          if(lstFN.first != null){
                                            Map<String,dynamic> mapL = lstFN.first;
                                            TaskFrequencyModel fnModel = TaskFrequencyModel.fromMap(map: mapL);
                                            mapNewTask['function_name']  = fnModel.name;
                                          }

                                          debugPrint("add task request : ${Utility.getPrettyJSONString(mapNewTask)}");

                                          TaskModel taskModel = TaskModel.fromMap(map: mapNewTask);
                                          taskModel.synced = 0;
                                          debugPrint("---Task---oldID : ${taskModel.id}");
                                          String sql = AppTablesQuery.INSERT_TABLE_TASK + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
                                          int insertResult = await appDatabase.rawInsert(sql, taskModel.getDataList(taskModel));
                                          if(insertResult == 0){
                                            debugPrint("Error inserting task");
                                            MyToast.showToast('Error inserting task', context);
                                            return;
                                          }
                                          var connectivityResult = await (Connectivity().checkConnectivity());
                                          if (connectivityResult != ConnectivityResult.none) {
                                            //network availabe - sync add task
                                            maxTaskId = await AppSync.addTask(context: context,mapNewTask : mapNewTask);
                                            if(maxTaskId == syncErrorValue){
                                              return;
                                            }
                                            debugPrint("AddTask sync success");
                                          }
                                          debugPrint("task added : $insertResult");

                                          debugPrint("mapNewTask : $mapNewTask");

                                          if(mapSelectedFile.length > 0){
                                            Map<String,dynamic> mapTaskAttachment = Map();
                                            int maxAtchmntId = new DateTime.now().millisecondsSinceEpoch;

                                            // mapSelectedFile
                                            debugPrint("new attachment id : $maxAtchmntId");
                                            mapTaskAttachment['id'] = maxAtchmntId;
                                            mapTaskAttachment['attachments_id'] = appUserModel.id;
                                            mapTaskAttachment['createDate'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
                                            mapTaskAttachment['image_name'] = mapSelectedFile[mPath];
                                            mapTaskAttachment['removed'] = 0;
                                            mapTaskAttachment['Checklistitem_id'] = null;
                                            mapTaskAttachment['task_id'] = maxTaskId;
                                            // mapTaskAttachment['synced'] = 'synced';
                                            debugPrint("map attachment : $mapTaskAttachment");

                                            AttachmentModel attachmntModel = AttachmentModel.fromMap(map: mapTaskAttachment);
                                            attachmntModel.synced = 0;
                                            String sqlAtchmntLstItmMdl = AppTablesQuery.INSERT_TABLE_ATTACHMENT + '(?,?,?,?,?,?,?,?)';
                                            int insertAtchmntResult = await appDatabase.rawInsert(sqlAtchmntLstItmMdl, attachmntModel.getDataList(attachmntModel));
                                            if(insertAtchmntResult == 0){
                                              debugPrint("Error inserting attachment");
                                              MyToast.showToast(AppStrings.errInsertingAttachment, context);
                                              return;
                                            }
                                            connectivityResult = await (Connectivity().checkConnectivity());
                                            if (connectivityResult != ConnectivityResult.none ) {
                                              locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                              maxAtchmntId = await AppSync.addAttachment(context:context,mapNewAtchmnt:mapTaskAttachment);
                                              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                              if(maxAtchmntId == syncErrorValue){
                                                MyToast.showToast(AppStrings.errAddingTaskAttachment, context);
                                                return;
                                              }
                                            }
                                            debugPrint("insert atchmnt result : $insertAtchmntResult");
                                            debugPrint("attachment : $mapTaskAttachment");

                                          }
                                          debugPrint("---Task---newId : ${maxTaskId}");
                                          String sqlTmpTask = "select * from ${AppTables.TASK} where id = ?";
                                          List<dynamic> tmpTaskValues = [maxTaskId];
                                          List<Map<String,dynamic>> listTmpTask = await appDatabase.rawQuery(sqlTmpTask,tmpTaskValues);
                                          Map<String,dynamic> mapTmpTask = listTmpTask.first;
                                          int mCurrentTab = -1;
                                          if(mapTmpTask != null){
                                            TaskModel tmpTaskModel = TaskModel.fromMap(map: mapTmpTask);
                                            DateTime mStartDate = DateFormat('yyyy-MM-dd').parse(tmpTaskModel.start_date);
                                            int customerId = tmpTaskModel.customer_id;
                                            int removed = tmpTaskModel.removed;
                                            int statusId = tmpTaskModel.status_id;

                                            if(Utility.isSameDate(DateTime.now(), mStartDate) && customerId==appUserModel.customer_id && removed != 1 && statusId != 3){
                                              mCurrentTab = 0;
                                            }else if(mStartDate.isBefore(DateTime.now()) && customerId == appUserModel.customer_id && removed != 1 && statusId != 3){
                                              mCurrentTab = 1;
                                            }else if(mStartDate.isAfter(DateTime.now()) && customerId == appUserModel.customer_id && removed != 1 && statusId != 3){
                                              mCurrentTab = 2;
                                            }else if(customerId == appUserModel.customer_id && removed != 1 && statusId == 3){
                                              mCurrentTab = 3;
                                            }
                                          }
                                          Navigator.of(context).pop();
                                          if(mCurrentTab == -1) {
                                            locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
                                          }else{
                                            locator<HomeTabBloc>().homeTabEventSink.add(mCurrentTab);
                                          }
                                        }
                                        setState(() {});

                                      },
                                      child: Text(AppStrings.save, style: TextStyle(
                                          fontSize: 17
                                      )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(width: 30,),
                          Expanded(
                            flex: 6,
                            child: Container(

                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(height: 20,),
                                  Container(
                                    // width: (dialogWidth/5)*3.5,
                                    child: Text(AppStrings.frequency,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    width: double.infinity,
                                    child: Container(
                                      // width: (dialogWidth/5)*3,
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                                          borderRadius: borderRadius,
                                          color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                                      ),
                                      height: 50,
                                      child: DropdownButtonHideUnderline(

                                        child: DropdownButton(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: MyColors.colorConvert('#d8d8d8'),
                                              size: 25,
                                            ),
                                            value: selectedFrequency,
                                            items: _ddmFrequency,
                                            onChanged: (value) async {
                                              FocusScope.of(context).requestFocus(FocusNode());
                                              setState((){
                                                selectedFrequency = value;
                                              });
                                            }),
                                      ),
                                    ),
                                  ),
                                  isNotValidFrequency?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(inValidFrequencyErrMsg,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),
                                  SizedBox(height: 20,),
                                  // Elke dag
                                  (selectedFrequency != null && selectedFrequency.id == 2)?
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      // dayparts
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(height: 10,),
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(AppStrings.dayparts,style:TextStyle(
                                                    color:MyColors.colorConvert('#646464'),
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.bold
                                                )),
                                              ),
                                              SizedBox(height: 20,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.morning,style:TextStyle(
                                                          color:MyColors.colorConvert('#646464'),
                                                          fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedMorning = value;
                                                              });
                                                            },
                                                            value: isCheckedMorning,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.afternoon,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedAfternoon = value;
                                                              });
                                                            },
                                                            value: isCheckedAfternoon,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.evening,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedEvening = value;
                                                              });
                                                            },
                                                            value: isCheckedEvening,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      // days of week
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(height: 10,),
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(AppStrings.days_of_the_week,style:TextStyle(
                                                    color:MyColors.colorConvert('#646464'),
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.bold
                                                )),
                                              ),
                                              SizedBox(height: 20,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.monday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedMonday = value;
                                                              });
                                                            },
                                                            value: isCheckedMonday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.tuesday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedTuesday = value;
                                                              });
                                                            },
                                                            value: isCheckedTuesday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.wednesday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedWednesday = value;
                                                              });
                                                            },
                                                            value: isCheckedWednesday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.thursday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedThursday = value;
                                                              });
                                                            },
                                                            value: isCheckedThursday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.friday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedFriday = value;
                                                              });
                                                            },
                                                            value: isCheckedFriday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.saturday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedSaturday = value;
                                                              });
                                                            },
                                                            value: isCheckedSaturday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(right: 50),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text(AppStrings.sunday,style:TextStyle(
                                                        color:MyColors.colorConvert('#646464'),
                                                        fontSize: 17,
                                                      )),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      height: 30,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                                          color: MyColors.colorConvert('EBEDF3')
                                                      ),
                                                      child: Transform.scale(
                                                          scale: 1,
                                                          child: Switch(
                                                            onChanged: (value) {
                                                              setState(() {
                                                                isCheckedSunday = value;
                                                              });
                                                            },
                                                            value: isCheckedSunday,
                                                            activeColor: MyColors.colorConvert('5BB0EC'),
                                                            activeTrackColor: MyColors.colorConvert('EBEDF3'),
                                                            inactiveThumbColor: MyColors.colorConvert('#F9FAFB'),
                                                            inactiveTrackColor: MyColors.colorConvert('#EBEDF3'),
                                                          )
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )

                                    ],
                                  ):
                                  // Elke Week
                                  (selectedFrequency != null && selectedFrequency.id == 3)?Column(
                                    children: [
                                      SizedBox(height: 10,),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        // width: (dialogWidth/5)*3.5,
                                        child: Text(AppStrings.every_how_many_weeks,style:TextStyle(
                                            color:MyColors.colorConvert('#646464'),
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold
                                        )),
                                      ),
                                      SizedBox(height: 10,),
                                      Container(
                                        height: loginBoxHeight / 11,
                                        child: TextField(
                                          controller: everyWeekController,
                                          decoration: new InputDecoration(
                                            isDense: true,
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                            labelText: "",
                                            filled: true,
                                            fillColor: MyColors.colorConvert('#f4f4f4'),
                                            border: textFieldOutlineInputBorder,
                                            focusedBorder: textFieldOutlineInputBorder,
                                            disabledBorder: textFieldOutlineInputBorder,
                                            enabledBorder: textFieldOutlineInputBorder,
                                            errorBorder: textFieldOutlineInputBorder,
                                            focusedErrorBorder: textFieldOutlineInputBorder,

                                            //fillColor: Colors.green
                                          ),
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                        ),
                                      ),
                                      isNotValidEveryWeek?Column(
                                        children: [
                                          SizedBox(height: 5,),
                                          Text(AppStrings.pleaseEnterValidWeek,style: loginScreenErrorTextStyle,),
                                        ],
                                      ):Container(),
                                    ],
                                  ):
                                  (selectedFrequency != null && selectedFrequency.id == 4)?Container(
                                    child: Column(
                                      children: [
                                        SizedBox(height: 10,),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(AppStrings.every_how_many_months,style:TextStyle(
                                              color:MyColors.colorConvert('#646464'),
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold
                                          )),
                                        ),
                                        SizedBox(height: 10,),
                                        Container(
                                          height: loginBoxHeight / 11,
                                          child: TextField(
                                            controller: everyMonthController,
                                            decoration: new InputDecoration(
                                              isDense: true,
                                              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                              labelText: "",
                                              filled: true,
                                              fillColor: MyColors.colorConvert('#f4f4f4'),
                                              border: textFieldOutlineInputBorder,
                                              focusedBorder: textFieldOutlineInputBorder,
                                              disabledBorder: textFieldOutlineInputBorder,
                                              enabledBorder: textFieldOutlineInputBorder,
                                              errorBorder: textFieldOutlineInputBorder,
                                              focusedErrorBorder: textFieldOutlineInputBorder,

                                              //fillColor: Colors.green
                                            ),
                                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          ),
                                        ),
                                        isNotValidEveryMonth?Column(
                                          children: [
                                            SizedBox(height: 5,),
                                            Text(AppStrings.pleaseEnterValidWeek,style: loginScreenErrorTextStyle,),
                                          ],
                                        ):Container(),
                                      ],
                                    ),
                                  ):Container(),
                                  // Function
                                  SizedBox(height: 20,),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    // width: (dialogWidth/5)*3.5,
                                    child: Text(AppStrings.description,style:TextStyle(
                                        color:MyColors.colorConvert('#646464'),
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    )),
                                  ),
                                  SizedBox(height: 10,),
                                  Container(
                                    child: TextField(
                                      keyboardType: TextInputType.multiline,
                                      minLines: 8,
                                      maxLines: 8,
                                      controller: descriptionController,
                                      decoration: new InputDecoration(
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                        labelText: "",
                                        filled: true,
                                        fillColor: MyColors.colorConvert('#f4f4f4'),
                                        border: textFieldOutlineInputBorder,
                                        focusedBorder: textFieldOutlineInputBorder,
                                        disabledBorder: textFieldOutlineInputBorder,
                                        enabledBorder: textFieldOutlineInputBorder,
                                        errorBorder: textFieldOutlineInputBorder,
                                        focusedErrorBorder: textFieldOutlineInputBorder,

                                        //fillColor: Colors.green
                                      ),
                                    ),
                                  ),
                                  isNotValidDescription?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(AppStrings.pleaseEnterDescription,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),

                              SizedBox(height: 20,),
                                  Container(
                                    // padding: EdgeInsets.only(left: 20,right: 20),
                                    width: dialogWidth,
                                    child: Wrap(
                                      alignment: WrapAlignment.start,
                                      children: [
                                        addFileWidget(),
                                        mapSelectedFile.length==0?Container():mapSelectedFile[mKey]=='image'?imageFileWidget():otherFileWidget()
                                      ],
                                    ),
                                  ),
                                  isNotValidAttachmnt?Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Text(AppStrings.pleaseAttachFile,style: loginScreenErrorTextStyle,),
                                    ],
                                  ):Container(),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                    //end
                  ],
                ),
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
  selectStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      locale: Locale('nl',''),
      context: context,
      initialDate: selectedStartDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2050),
    );
    if (picked != null && picked != selectedStartDate) {
      setState(() {
        selectedStartDate = picked;
        String strStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(selectedStartDate);

      });
    }
  }
  BorderRadiusGeometry borderRadius = BorderRadius.all(Radius.circular(5));


  Widget addFileWidget(){
    return GestureDetector(
      onTap: () async{
        FilePickerResult result = await FilePicker.platform.pickFiles();

        if(result != null) {
          File file = File(result.files.single.path);
          debugPrint("file : ${file.path}");
          String extension = p.context.extension(file.path);
          Map<String,dynamic> mapFile = Map();
          if(isImage(file)){
            mapFile[mKey]='image';
          }else{
            mapFile[mKey]='other';
          }

          mapFile[mPath]=file.path;
          mapFile[mType]=extension;
          mapFile[mSource]=Source.LOCAL;

          int maxId = new DateTime.now().millisecondsSinceEpoch;
          await Future.delayed(Duration(milliseconds: 10),(){});
          int maxAttachmentId = new DateTime.now().millisecondsSinceEpoch;
          debugPrint("MAX ID : $maxId");
          debugPrint("MAX ATTACHMENT ID : $maxAttachmentId");

          Map<String,dynamic> mapAtchmnt = Map();
          mapAtchmnt['attachments_id'] = maxAttachmentId;
          mapAtchmnt['createDate'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
          mapAtchmnt['id'] = maxId;
          mapAtchmnt['image_name'] = file.path;
          mapAtchmnt['removed'] = 0;
          mapAtchmnt['task_id'] = null;

          mapFile[mData]=mapAtchmnt;
          mapSelectedFile = mapFile;
          setState(() {});

        } else {
          // User canceled the picker
        }
      },
      child: Container(
        margin: EdgeInsets.only(right: 10,bottom: 10),
        height: 130,
        width: 150,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: MyColors.colorConvert('#CCCCCC')),
            color: MyColors.colorConvert('#F4F4F4')
        ),
        child: Icon(Icons.add,color: MyColors.colorConvert('#C3C3C3'),size: 50,),
      ),
    );
  }

  Widget imageFileWidget(){
    String type = mapSelectedFile[mType];
    String path = mapSelectedFile[mPath];
    Source source = mapSelectedFile[mSource];
    dynamic data = mapSelectedFile[mData];
    return Container(
      margin: EdgeInsets.only(right: 10,bottom: 10),
      height: 130,
      width: 150,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: MyColors.colorConvert('#CCCCCC')),
          color: MyColors.colorConvert('#F4F4F4')
      ),
      child: Stack(
        children: [
          Container(
            height: 130,
            width: 150,
            child: ClipRRect(

                borderRadius: BorderRadius.circular(10),
                child: source == Source.LOCAL?Image.file(File(path),fit: BoxFit.cover,):Image.network(path,fit: BoxFit.cover,)
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 5,top: 5,right: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: (){
                    onClickDeleteImage(data:data);
                  },
                  child: Container(

                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: MyColors.colorConvert('#0b1419')
                      ),
                      child: Icon(Icons.clear,color: Colors.white,)
                  ),
                ),
                SizedBox(width: 5,),
                Flexible(
                  child: Container(

                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: MyColors.colorConvert('#0b1419')
                    ),
                    child: Text(basename(path),style: TextStyle(
                        fontSize: 12,
                        color: Colors.white
                    ),),
                  ),
                ),

              ],),
          )
        ],
      ),
    );
  }
  Widget otherFileWidget(){
    String type = mapSelectedFile[mType];
    String path = mapSelectedFile[mPath];
    Source source = mapSelectedFile[mSource];
    dynamic data = mapSelectedFile[mData];
    return Container(
      margin: EdgeInsets.only(right: 10,bottom: 10),
      height: 130,
      width: 150,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: MyColors.colorConvert('#CCCCCC')),
          color: MyColors.colorConvert('#38627E')
      ),
      child: Stack(
        children: [
          Align(
              alignment: Alignment.center,
              child: Icon(Icons.file_copy,color: Colors.white,size: 50,)
          ),
          Container(
            margin: EdgeInsets.only(left: 5,top: 5,right: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    onClickDeleteImage(data:data);
                  },
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: MyColors.colorConvert('#0b1419')
                      ),
                      child: Icon(Icons.clear,color: Colors.white,)
                  ),
                ),
                SizedBox(width: 5,),
                Flexible(
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: MyColors.colorConvert('#0b1419')
                    ),
                    child: Text(basename(path),style: TextStyle(
                        fontSize: 12,
                        color: Colors.white
                    ),),
                  ),
                ),
              ],),
          )
        ],
      ),
    );
  }
  bool isImage(File file){
    String extension = p.context.extension(file.path);
    Map<String,dynamic> mapFile = Map();
    if(extension.endsWith('png') ||
        extension.endsWith('PNG') ||
        extension.endsWith('jpg') ||
        extension.endsWith('JPG') ||
        extension.endsWith('JPEG') ||
        extension.endsWith('jpeg')){
      return true;
    }else{
      return false;
    }

  }
  void onClickDeleteImage({dynamic data})async{
    debugPrint("data : $data");
    mapSelectedFile=Map();
    setState(() {});
  }
  TextStyle loginScreenErrorTextStyle = TextStyle(color: Colors.red, fontSize: 10);
}