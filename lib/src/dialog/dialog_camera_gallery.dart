import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';


class DialogCameraGallery extends StatefulWidget {

  DialogCameraGallery();

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogCameraGallery> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  double borderRadius = 30 ;
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = 350;//(MediaQuery.of(context).size.width/2)*1.25;
    double dialogHeight = 235;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(
      backgroundColor: MyColors.colorConvert('#F9F9F9'),
        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
        elevation: 0,
        child: Container(
          width:dialogWidth ,
          height: dialogHeight,
          child: Stack(
            children: [
              Column(

                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20,),
                          Container(

                            padding: EdgeInsets.only(left: 30, right: 20),
                            // width: double.infinity,
                            // height: loginBoxHeight / 7,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(borderRadius), topRight: Radius.circular(borderRadius)),
                              color: MyColors.colorConvert('#F9F9F9'),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
                                Expanded(
                                  flex: 1,
                                  child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 32),
                                        child: Text('${AppStrings.foto_uploaded}', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.black)),
                                      )),
                                ),
                                IconButton(
                                  onPressed: () {
                                    Navigator.of(context).pop('cancel');
                                  },
                                  icon: Icon(Icons.clear, color: MyColors.colorConvert('#365e77'), size: 30),
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 30,right: 30),
                            child: Text(AppStrings.foto_upload_foto_take,style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                            ),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  )
,
                  Container(
                    // padding: EdgeInsets.all(20),
                    alignment: Alignment.bottomCenter,
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(borderRadius),bottomRight: Radius.circular(borderRadius)),
                        color: MyColors.colorConvert('#DCDCE0')
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 59,
                            child: ElevatedButton(

                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                elevation: 0,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(borderRadius))),
                                primary: MyColors.colorConvert('#F9F9F9'),
                              ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                if (states.contains(MaterialState.pressed)) {
                                  return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                }
                                return null;
                              })),
                              onPressed: () async {
                                Navigator.of(context).pop('camera');
                              },
                              child: Text(AppStrings.camera, style: TextStyle(
                                  fontSize: 22,
                                  color: MyColors.cameraButtonColor,
                                  fontWeight: FontWeight.bold
                              )),
                            ),
                          ),
                        ),
                        SizedBox(width: 1,),

                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 59,
                            child: ElevatedButton(

                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                elevation: 0,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomRight: Radius.circular(borderRadius))),
                                primary: MyColors.colorConvert('#F9F9F9'),
                              ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                if (states.contains(MaterialState.pressed)) {
                                  return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                }
                                return null;
                              })),
                              onPressed: () async {
                                Navigator.of(context).pop('library');
                              },
                              child: Text(AppStrings.library, style: TextStyle(
                                  fontSize: 22,
                                  color: MyColors.cameraButtonColor,
                              )),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //end
                ],
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}