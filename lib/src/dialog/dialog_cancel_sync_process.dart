import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';


class DialogCancelSyncProcess extends StatefulWidget {
  DialogCancelSyncProcess();

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogCancelSyncProcess> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;

    double dialogHeight = MediaQuery.of(context).size.height/4;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.red,
        child: Container(
          width: (screenWidth/5)*2.5,
          height: dialogHeight,
          child: Stack(
            children: [
              Column(

                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 30,right: 30),
                      child: Text(AppStrings.cancelProcess,style: TextStyle(
                          color: Colors.white,
                          fontSize: 16
                      )/*,textAlign: TextAlign.center,*/),
                    ),
                  )
,
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                        color: Colors.red
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [

                        Container(
                          width: 100,
                          height: 40,
                          child: ElevatedButton(

                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              elevation: 0,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              primary: Colors.transparent,
                            ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) {
                                return MyColors.loginButtonReveleColor.withOpacity(0.2);
                              }
                              return null;
                            })),
                            onPressed: () async {
                              Navigator.pop(context,'no');
                            },
                            child: Text(AppStrings.nee, style: TextStyle(
                                color: Colors.white,
                                fontSize: 16
                            )),
                          ),
                        ),
                        SizedBox(width: 20,),

                        Container(
                          width: 100,
                          height: 40,
                          child: ElevatedButton(

                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              elevation: 0,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              primary: MyColors.colorConvert('#266E9F'),
                            ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) {
                                return MyColors.loginButtonReveleColor.withOpacity(0.2);
                              }
                              return null;
                            })),
                            onPressed: () async {
                              Navigator.pop(context,'yes');
                            },
                            child: Text(AppStrings.ja, style: TextStyle(
                                color: Colors.white,
                                fontSize: 16
                            )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //end
                ],
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }

}