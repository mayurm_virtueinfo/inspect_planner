import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/dialog/dialog_newsposts_popup.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:intl/intl.dart';


class DialogComment extends StatefulWidget {
  DialogComment();

  @override
  _DialogCommentState createState() => _DialogCommentState();
}

class _DialogCommentState extends State<DialogComment> {

  @override
  void initState() {
    // TODO: implement initState
    initNoteData();
    super.initState();
  }
  List<Map<String,dynamic>> listSqlOpmerkingen = [];
  void initNoteData()async{
    debugPrint("initNoteDate-initialized");
    debugPrint("---data-userID :${appUserModel.id} ");
    debugPrint("---data-customerID :${appUserModel.customer_id} ");
    String sqlNote = "select * from ${AppTables.NOTE} where assignedto_user_id = ? order by id asc";
    List<dynamic> sqlValues = [appUserModel.id];
    listSqlOpmerkingen = await appDatabase.rawQuery(sqlNote,sqlValues);
    listSqlOpmerkingen = listSqlOpmerkingen.reversed.toList();
    debugPrint("Notes : ${listSqlOpmerkingen.toString()}");
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = MediaQuery.of(context).size.width/2;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: SingleChildScrollView(
          child: Container(
            width:dialogWidth ,
            // height: 100,
            child: Column(
              // mainAxisSize: MainAxisSize.min,
              children: [
                DialogTitleWidget(title: AppStrings.comments,),
                SizedBox(height: 20,),
                listSqlOpmerkingen.length>0?ListView.separated(
                  controller: ScrollController(),
                  shrinkWrap: true,
                  itemCount: listSqlOpmerkingen.length,
                  separatorBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(left: 20,right: 20,top: 10,bottom: 10),
                      height: 1,
                      color: MyColors.colorConvert('#B5B5C3'),
                    );
                  },
                  itemBuilder: (context, index) {
                    NoteModel mNoteModel = NoteModel.fromMap(map: listSqlOpmerkingen[index]);
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),

                      width: dialogWidth,
                      padding: EdgeInsets.only(left: 20,right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Icon(Icons.note,color: MyColors.colorConvert('#B5B5C3'),)
                          ),
                          SizedBox(width: 20,),
                          Expanded(
                            flex: 1,
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(appUserModel.name),
                                  SizedBox(height: 2,),
                                  Text(mNoteModel.note),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },):Container(),
                SizedBox(height: 20,),
              ],
            ),
          ),
        ),
      ),
    );
  }

}