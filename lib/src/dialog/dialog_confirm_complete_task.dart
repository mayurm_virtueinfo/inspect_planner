import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';


class DialogConfirmCompleteTask extends StatefulWidget {

  DialogConfirmCompleteTask();

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogConfirmCompleteTask> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  double borderRadius = 30 ;
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = 400;//(MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = 400;//MediaQuery.of(context).size.height/2.75;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
        elevation: 0,
        backgroundColor: MyColors.colorConvert('F9F9F9'),
        child: Container(
          width:dialogWidth ,
          height: dialogHeight,
          child: Stack(
            children: [
              Column(

                children: [
          SizedBox(height: 30,),
            Container(
              padding: EdgeInsets.only(left: 30, right: 20),
              // width: double.infinity,
              // height: loginBoxHeight / 7,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(borderRadius), topRight: Radius.circular(borderRadius)),
                color: MyColors.colorConvert('#F9F9F9'),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
                  Text(AppStrings.payAttension, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold,  color: MyColors.colorConvert('#040404'),)),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 30,right: 30),
                child: Text(AppStrings.completeChecklistBeforeComplete,style: TextStyle(
                  color: MyColors.colorConvert('#0D0D0D'),
                  fontSize: 20,
                ),textAlign: TextAlign.center,),
              ),
            ),
                  Divider(height: 2,color: MyColors.colorConvert('#DADADE'),),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop('yes');
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 70,
                      // margin: EdgeInsets.only(top: 15),
                      child: Text(AppStrings.forcedRound, style: TextStyle(
                          fontSize: 22,
                          color: MyColors.colorConvert('#FF4035')
                      )),
                    ),
                  ),

                  Divider(height: 2,color: MyColors.colorConvert('#DADADE'),),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop('no');
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 70,
                      child: Text(AppStrings.annuleer, style: TextStyle(
                          fontSize: 22,
                        color: MyColors.cameraButtonColor,
                        fontWeight: FontWeight.bold
                      )),
                    ),
                  ),
                  //end
                ],
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}