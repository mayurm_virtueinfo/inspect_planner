import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';


class DialogDeleteNote extends StatefulWidget {
  NoteModel noteModel;
  DialogDeleteNote({Key key,this.noteModel}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogDeleteNote> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = MediaQuery.of(context).size.height/3;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth ,
          height: dialogHeight,
          child: Stack(
            children: [
              Column(

                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          DialogTitleWidget(title: AppStrings.deleteNote,),
                          SizedBox(height: 20,),
                          Container(
                            padding: EdgeInsets.only(left: 30,right: 30),
                            child: Text(AppStrings.doYouReallyWantToDeleteNote,style: TextStyle(
                                color: MyColors.colorConvert('#777777'),
                                fontSize: 18
                            ),),
                          ),
                          SizedBox(height: 20,),
                        ],
                      ),
                    ),
                  )
,
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                        color: MyColors.colorConvert('#F4F4F4')
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [

                        Container(
                          width: 150,
                          height: 50,
                          child: ElevatedButton(

                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              elevation: 0,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              primary: Colors.transparent,
                            ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) {
                                return MyColors.loginButtonReveleColor.withOpacity(0.2);
                              }
                              return null;
                            })),
                            onPressed: () async {
                              Navigator.of(context).pop();
                            },
                            child: Text(AppStrings.annuleren, style: TextStyle(
                                fontSize: 17,
                                color: Colors.black
                            )),
                          ),
                        ),

                        Container(
                          width: 150,
                          height: 50,
                          child: ElevatedButton(

                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              elevation: 0,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              primary: MyColors.colorConvert('#266E9F'),
                            ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) {
                                return MyColors.loginButtonReveleColor.withOpacity(0.2);
                              }
                              return null;
                            })),
                            onPressed: () async {

                              debugPrint("note delete id : ${widget.noteModel.id}");
                              String sqlDeleteAtchmnt = 'update ${AppTables.NOTE} set synced = ?, removed = ? where id = ?';
                              List<dynamic> sqlValues = [0,1,widget.noteModel.id];
                              int deleteResult = await appDatabase.rawUpdate(sqlDeleteAtchmnt,sqlValues);
                              debugPrint("delete result : $deleteResult");
                              debugPrint("delete note 1");

                              String sqlNote1 = "select * from ${AppTables.NOTE} where task_id = ? and removed = ? ";
                              List<dynamic> sqlValues1 = [appTaskModel.id,0];
                              final listNote = await appDatabase.rawQuery(sqlNote1,sqlValues1);
                              debugPrint("Notes-current : ${listNote.toString()}");

                              if(deleteResult == 0){
                                debugPrint("delete note 2");
                                debugPrint("Error deleting note");
                                MyToast.showToast('Error deleting note', context);
                                return;
                              }
                              var connectivityResult = await (Connectivity().checkConnectivity());
                              if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1 && widget.noteModel.synced==1) {
                                debugPrint("delete note 3");
                                //network availabe - sync add task
                                int deleteNoteIdId = await AppSync.deleteNote(context: context,id: widget.noteModel.id);
                                if(deleteNoteIdId == syncErrorValue){
                                  return;
                                }
                                debugPrint("AddTask sync success");
                              }
                              else if(widget.noteModel.synced == 0){
                                debugPrint("delete note 4");
                                String sqlDeleteNote = "delete from ${AppTables.NOTE} where id = ?";
                                List<dynamic> sqlDeleteNoteValues = [widget.noteModel.id];
                                int deleteResult = await appDatabase.rawDelete(sqlDeleteNote,sqlDeleteNoteValues);
                                if(deleteResult == 0){
                                  MyToast.showToast(AppStrings.errDeleteFromLocal, context);
                                  // delete record if get any error in update
                                  return;
                                }
                              }

                              locator<DetailTabBloc>().detailTabEventSink.add(2);
                              locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
                              Navigator.of(context).pop();
                            },
                            child: Text(AppStrings.remove, style: TextStyle(
                                fontSize: 17
                            )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //end
                ],
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}