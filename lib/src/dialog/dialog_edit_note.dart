import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';


class DialogEditNote extends StatefulWidget {
  NoteModel noteModel;
  DialogEditNote({Key key,this.noteModel}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogEditNote> {
  // Task type
  List<DropdownMenuItem<UserTableModel>> _ddmUser;
  UserTableModel selectedUser;
  List<UserTableModel> listUserTableModel = [];
  TextEditingController noteController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    getUser();
    super.initState();
  }

  void getUser()async{
    noteController.text = widget.noteModel.note;

    String sql = 'select * from ${AppTables.USER} where customer_id = ? and removed = ?';
    List<dynamic> sqlValues = [appUserModel.customer_id,0];
    List<Map<String,dynamic>> listUser = await appDatabase.rawQuery(sql,sqlValues);
    UserTableModel utModel = UserTableModel(id: -1,name: AppStrings.commentNotLinked);
    List<UserTableModel> listUserTableModel= [];
    listUserTableModel.add(utModel);
    listUser.forEach((element) {
      UserTableModel userTableModel = UserTableModel.fromMap(map: element);
      listUserTableModel.add(userTableModel);
    });
    _ddmUser = buildDropDownMenuItemsTaskType(listUserTableModel);
    if(widget.noteModel.assignedto_user_id!=null) {
      int indexAssignedTo = listUserTableModel.indexWhere((element) => element.id == widget.noteModel.assignedto_user_id);
      selectedUser = _ddmUser[indexAssignedTo].value;
    }else{
      selectedUser = _ddmUser[0].value;
    }
    setState((){});
  }
  List<DropdownMenuItem<UserTableModel>> buildDropDownMenuItemsTaskType(List listItems) {
    List<DropdownMenuItem<UserTableModel>> items = [];
    for (UserTableModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = (MediaQuery.of(context).size.width/10)*4.5;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth ,
          height: dialogHeight,
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Container(
                  height: dialogHeight,
                  width: dialogWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              DialogTitleWidget(title: AppStrings.notitie_toevoegen,),
                              SizedBox(height: 20,),
                              Container(
                                padding: EdgeInsets.only(left: 30,right: 30),
                                // width: (dialogWidth/5)*3.5,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(AppStrings.notitie,style: TextStyle(
                                        color: MyColors.colorConvert('#797979'),
                                        fontSize: 18
                                    ),),
                                    Container(
                                      width: (dialogWidth/5)*3,
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(width: 1.0, color: MyColors.colorConvert('#5BB0EC')),
                                          borderRadius: BorderRadius.circular(10),
                                          color: Colors.white /*MyColors.dropdownBackgroudColor*/
                                      ),
                                      child: TextField(
                                        controller: noteController,
                                        keyboardType: TextInputType.multiline,
                                        minLines: 5,
                                        maxLines: 6,
                                        decoration: new InputDecoration(
                                          isDense: true,
                                          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                          labelText: "",
                                          filled: true,

                                          fillColor: Colors.white,
                                          border: textFieldOutlineInputBorder,
                                          focusedBorder: textFieldOutlineInputBorder,
                                          disabledBorder: textFieldOutlineInputBorder,
                                          enabledBorder: textFieldOutlineInputBorder,
                                          errorBorder: textFieldOutlineInputBorder,
                                          focusedErrorBorder: textFieldOutlineInputBorder,

                                          //fillColor: Colors.green
                                        ),
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              SizedBox(height: 20,),
                              Container(
                                padding: EdgeInsets.only(left: 30,right: 30,),
                                // width: (dialogWidth/5)*3.5,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(AppStrings.linkedToUser,style: TextStyle(
                                      color: MyColors.colorConvert('#797979'),
                                      fontSize: 18
                                    ),),
                                    Container(
                                      width: (dialogWidth/5)*3,
                                      padding: EdgeInsets.only(left: 10, right: 10),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        border: Border.all(width: 1.0, color: MyColors.colorConvert('#5BB0EC')),
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white /*MyColors.dropdownBackgroudColor*/
                                      ),
                                      height: 50,
                                      child: DropdownButtonHideUnderline(

                                        child: DropdownButton(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: MyColors.colorConvert('#5BB0EC'),
                                              size: 25,
                                            ),
                                            value: selectedUser,
                                            items: _ddmUser,
                                            onChanged: (value) async {
                                              setState((){
                                                selectedUser = value;
                                                FocusScope.of(context).requestFocus(FocusNode());
                                              });
                                            }),
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              SizedBox(height: 20,),

                              //end
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                            color: MyColors.colorConvert('#F4F4F4')
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [

                            Container(
                              width: 150,
                              height: 50,
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: Colors.transparent,
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {
                                  Navigator.of(context).pop();
                                },
                                child: Text(AppStrings.annuleren, style: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black
                                )),
                              ),
                            ),

                            Container(
                              width: 150,
                              height: 50,
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: MyColors.colorConvert('#266E9F'),
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {


                                  var connectivityResult = await (Connectivity().checkConnectivity());
                                  if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1 &&widget.noteModel.synced==1) {
                                    //network availabe - sync add task
                                    int updatedNoteId = await AppSync.updateNote(
                                        context: context,
                                        note: noteController.text,
                                        noteId: widget.noteModel.id,
                                        assignToUserId: selectedUser.id==-1?null:selectedUser.id
                                    );
                                    if(updatedNoteId == syncErrorValue){
                                      MyToast.showToast(AppStrings.errUpdatingNote, context);
                                      return;
                                    }
                                    debugPrint("Update note sync success");

                                  }else /*if(widget.noteModel.synced==0)*/{
                                    String sqlTaskComplete = "update ${AppTables
                                        .NOTE} set note = ?, assignedto_user_id = ?, synced = ? where id = ?";
                                    List<dynamic> sqlValues = [
                                      noteController.text,
                                      selectedUser.id==-1?null:selectedUser.id,
                                      0,
                                      widget.noteModel.id
                                    ];
                                    int updateResult = await appDatabase.rawUpdate(
                                        sqlTaskComplete, sqlValues);
                                    debugPrint("Note update result : $updateResult");
                                    if(updateResult == 0){
                                      debugPrint("Error updating note");
                                      MyToast.showToast(AppStrings.errUpdatingTask, context);
                                      return;
                                    }
                                  }
                                  locator<DetailTabBloc>().detailTabEventSink.add(2);
                                  locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
                                  Navigator.of(context).pop();
                                },
                                child: Text(AppStrings.bewaar, style: TextStyle(
                                    fontSize: 17
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}