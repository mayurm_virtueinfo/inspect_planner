import 'dart:async';
import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';


class DialogEnterOTP extends StatefulWidget {
  String userId = '';
  double loginBoxHeight;
  double loginBoxWidth;
  int qrShowStatus;
  String qrImage='';
  DialogEnterOTP({this.userId,this.loginBoxHeight,this.loginBoxWidth,this.qrImage,this.qrShowStatus,Key key}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogEnterOTP> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  TextStyle loginScreenTextStyle = TextStyle(color: MyColors.colorConvert('#9e9e9e'), fontSize: 14);
  TextStyle loginScreenErrorTextStyle = TextStyle(color: Colors.red, fontSize: 15,);
  TextEditingController otpController = TextEditingController();
  bool isOtpValid = true;
  String otpError = '';
  @override
  Widget build(BuildContext context) {
    // double qrCodeWidthLandscape = MediaQuery.of(context).size.width*0.275;
    // double qrCodeWidth = MediaQuery.of(context).size.width*0.275;
    // widget.qrShowStatus = 0;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = widget.loginBoxWidth*1.10;

    double verticalGap = 10;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: OrientationBuilder(
        builder: (context, orientation) {
          bool isPortrait = (orientation == Orientation.portrait);
          double qrCodeWidthLandscape = MediaQuery.of(context).size.width*0.19;
          double qrCodeWidthPortrait = MediaQuery.of(context).size.width*0.275;
          double qrCodeWidth = isPortrait?qrCodeWidthPortrait:qrCodeWidthLandscape;


          double dialogHeight = isPortrait ? widget.qrShowStatus == 1 ? widget.loginBoxHeight * 1.30 : widget.loginBoxHeight/1.3 :
          widget.qrShowStatus == 1 ? widget.loginBoxHeight * 1.25 : widget.loginBoxHeight/1.45;

          return Dialog(

            insetPadding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
            elevation: 0,
            backgroundColor: Colors.white,
            child: Container(
              width:dialogWidth ,
              height: dialogHeight,
              // height: 700,
              child: Stack(
                children: [
                  Column(

                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DialogTitleWidget(title: AppStrings.required2FA,),
                              SizedBox(height: 20,),
                              widget.qrShowStatus == 1 ? Container(
                                width: MediaQuery.of(context).size.width,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: qrCodeWidth,
                                    height: qrCodeWidth,
                                    child: Html(
                                      data: widget.qrImage,
                                    ),
                                  ),
                                ),
                              ): Container(),
                              widget.qrShowStatus == 1 ? Container(
                                width: dialogWidth,
                                child: Center(
                                  child: Container(
                                    // width: dialogWidth*0.6,
                                    child: Text('${AppStrings.scanQRCode}\n${AppStrings.enterOTP}',style: TextStyle(
                                        color: MyColors.colorConvert('#777777'),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold
                                    ),textAlign: TextAlign.center,),
                                  ),
                                ),
                              ) : Container(),
                              widget.qrShowStatus != 1 ? Container(
                                width: dialogWidth,
                                child: Center(
                                  child: Container(
                                    // width: dialogWidth*0.6,
                                    child: Text(AppStrings.enterOTP,style: TextStyle(
                                        color: MyColors.colorConvert('#777777'),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold
                                    ),textAlign: TextAlign.center,),
                                  ),
                                ),
                              ) : Container(),
                              // SizedBox(height: 20,),
                              // Container(
                              //   padding: EdgeInsets.only(left: 30,right: 30),
                              //   child: Text(AppStrings.enterOTP,style: TextStyle(
                              //       color: MyColors.colorConvert('#777777'),
                              //       fontSize: 18
                              //   ),),
                              // ),
                              SizedBox(height: 20,),
                              Container(
                                height: 55,
                                margin: EdgeInsets.only(left: 30,right: 30),
                                child: TextField(
                                  controller: otpController,
                                  decoration: new InputDecoration(
                                    isDense: true,
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                    labelText: "",
                                    filled: true,
                                    fillColor: MyColors.colorConvert('#f4f4f4'),
                                    border: textFieldOutlineInputBorder,
                                    focusedBorder: textFieldOutlineInputBorder,
                                    disabledBorder: textFieldOutlineInputBorder,
                                    enabledBorder: textFieldOutlineInputBorder,
                                    errorBorder: textFieldOutlineInputBorder,
                                    focusedErrorBorder: textFieldOutlineInputBorder,

                                    //fillColor: Colors.green
                                  ),
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                              isOtpValid
                                  ? Container()
                                  : Column(
                                children: [
                                  SizedBox(
                                    height: verticalGap / 2,
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: dialogWidth*0.8,
                                        margin:EdgeInsets.only(left: 30,),
                                        child: Text(
                                          otpError,
                                          style: loginScreenErrorTextStyle,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                      ,
                      Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                          // color: MyColors.colorConvert('#F4F4F4')
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [

                            Container(
                              width: 150,
                              height: 50,
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: Colors.transparent,
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {
                                  Navigator.of(context).pop({
                                    'action':'cancel'
                                  });
                                },
                                child: Text(AppStrings.cancelS, style: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black
                                )),
                              ),
                            ),

                            Container(
                              width: 150,
                              height: 50,
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: MyColors.colorConvert('#266E9F'),
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {

                                  String otpText = otpController.text;
                                  if (otpText.length == 0) {
                                    otpError = AppStrings.otpIsRequired;
                                    isOtpValid = false;
                                    setState(() {});
                                    return null;
                                  }

                                  var connectivityResult = await (Connectivity().checkConnectivity());
                                  if (connectivityResult == ConnectivityResult.none) {
                                    // I am not connected to an any network.
                                    MyToast.showToast(AppStrings.internetIsRequiredToLogin, context);
                                    return null;
                                  }

                                  FocusScope.of(context).requestFocus(FocusNode());
                                  locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                  try {
                                    debugPrint("trying---loing--");

                                    Map<String, dynamic> apiResult = await ApiRequest.postCheck2FA(userId: widget.userId, oneTimePassword: otpText);
                                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                    debugPrint("trying---otp-dialog-apiResult-- $apiResult");
                                    // return;
                                    if (apiResult == null) {
                                      otpError = AppStrings.errWhile2FA;
                                      isOtpValid = false;
                                      setState(() {
                                        MyToast.showToast(AppStrings.errWhile2FA, context);
                                      });
                                      return;
                                    } else {
                                      debugPrint("trying---otp-dialog-1--");
                                      bool status = apiResult['status'];
                                      if (status != null && status) {
                                        debugPrint("trying---otp-dialog-2--");
                                        try {
                                          Map<String, dynamic> userDetail = apiResult['user_detail'];
                                          if(userDetail.containsKey('twofa_verified')){
                                            int twoFA = userDetail['twofa'];
                                            int twoFAVerified = userDetail['twofa_verified'];
                                            debugPrint("twoFA : $twoFA");
                                            if(twoFAVerified == 1){
                                              debugPrint("trying---otp-dialog-3--");
                                              otpError = '';
                                              isOtpValid = true;
                                              setState(() {
                                                MyToast.showToast(AppStrings.otpVerified, context);
                                                Navigator.of(context).pop({
                                                  'action':'otp',
                                                  'verified':'1'
                                                });
                                              });

                                            }else{
                                              debugPrint("trying---otp-dialog-4--");
                                              otpError = AppStrings.otpVerificationFailed;
                                              isOtpValid = false;
                                              setState(() {
                                                MyToast.showToast(AppStrings.otpVerificationFailed, context);
                                              });
                                            }

                                          }

                                          debugPrint("trying---otp-dialog-5-userDetail-- $userDetail");
                                          // await Utility.processLogin(mContext: context, apiResult: apiResult);
                                        }catch (e) {
                                          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                          debugPrint("otp-dialog-process cancelled");
                                        }
                                      } else if (apiResult['message'] != null) {
                                        debugPrint('---otp-dialog-6');

                                        String msgInvalidCredential = apiResult['message'];
                                        otpError = msgInvalidCredential;
                                        isOtpValid = false;

                                        setState(() {
                                          MyToast.showToast(msgInvalidCredential, context);
                                        });
                                      }
                                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                    }
                                  } catch (e) {
                                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                  }
                                },
                                child: Text(AppStrings.login, style: TextStyle(
                                    fontSize: 17
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //end
                    ],
                  ),
                  LoadingWidget()
                ],
              ),
            ),
          );
        },
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}