import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:intl/intl.dart';


class DialogFilter extends StatefulWidget {
  Map<String,dynamic> mapFilter;
  DialogFilter({this.mapFilter,Key key}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogFilter> {
  DateTime selectedStartDate = DateTime.now();
  DateTime selectedEndDate = DateTime.now();
  // Task type
  List<DropdownMenuItem<TaskTypeModel>> _ddmTaskType;
  TaskTypeModel selectedTaskType;
  List<TaskTypeModel> listDDMTaskType = [];

  // Function
  List<DropdownMenuItem<FunctionsModel>> _ddmFunction;
  FunctionsModel selectedFunction;
  List<FunctionsModel> listDDMFunction = [];

  // location
  List<DropdownMenuItem<LocationModel>> _ddmLocation;
  LocationModel selectedLocation;
  List<LocationModel> listDDMLocation = [];
  Map<String,dynamic> mapTempFilter = Map();
  bool isChanges = false;
  @override
  void initState() {
    // TODO: implement initState
    mapTempFilter = widget.mapFilter;
    debugPrint("--dialog-filter-initState");
    getTaskType();
    getFunction();
    getLocation();
    getDate();

    super.initState();
  }
  bool valSDAvail=false;
  bool valEDAvail=false;
  void getDate(){
    String sSd = widget.mapFilter[HomeScreen.start_date_filter];
    String sEd = widget.mapFilter[HomeScreen.end_date];
    //2021-06-01 10:00:00
    DateFormat dtFormat = DateFormat('yyyy-MM-dd HH:mm:ss');

    if(sSd != null){
      selectedStartDate = dtFormat.parse(sSd);
      valSDAvail = true;
    }
    if(sEd != null){
      selectedEndDate = dtFormat.parse(sEd);
      valEDAvail = true;
    }
    setState((){});

  }
  void getTaskType()async{
    String sql = 'select * from ${AppTables.TASK_TYPE} order by name asc';
    List<Map<String,dynamic>> listTaskType = await appDatabase.rawQuery(sql);
    TaskTypeListModel taskTypeListModel = TaskTypeListModel();
    TaskTypeModel mTTM = TaskTypeModel(id: -1,name: 'Alles');
    List<TaskTypeModel> lDDMTT= await taskTypeListModel.getTaskList(listTaskType);
    listDDMTaskType.add(mTTM);
    listDDMTaskType.addAll(lDDMTT);
    _ddmTaskType = buildDropDownMenuItemsTaskType(listDDMTaskType);
    int typeId = widget.mapFilter[HomeScreen.type_id];
    if(typeId != null && typeId is int){
      int index = listDDMTaskType.indexWhere((element) => element.id == typeId);
      selectedTaskType = _ddmTaskType[index].value;
    }else {
      selectedTaskType = _ddmTaskType[0].value;
    }
    setState((){});
  }
  void getFunction()async{
    String sql = 'select * from ${AppTables.FUNCTIONS} order by name asc';
    List<Map<String,dynamic>> listTaskType = await appDatabase.rawQuery(sql);
    FunctionsListModel functionListModel = FunctionsListModel();
    FunctionsModel mFM = FunctionsModel(id: -1,name: AppStrings.alles);
    List<FunctionsModel> lDDMFunction= await functionListModel.getTaskList(listTaskType);
    listDDMFunction.add(mFM);
    listDDMFunction.addAll(lDDMFunction);
    _ddmFunction = buildDropDownMenuItemsFunction(listDDMFunction);
    int groupId = widget.mapFilter[HomeScreen.group_id];
    if(groupId != null && groupId is int){
      int index = listDDMFunction.indexWhere((element) => element.id == groupId);
      selectedFunction = _ddmFunction[index].value;
    }else {
      selectedFunction = _ddmFunction[0].value;
    }
    setState((){});
  }
  void getLocation()async{
    String sql = 'select * from ${AppTables.LOCATION} order by name';
    List<Map<String,dynamic>> listTaskType = await appDatabase.rawQuery(sql);
    LocationListModel locationListModel = LocationListModel();
    LocationModel mLM=LocationModel(id: -1,name: AppStrings.all);
    List<LocationModel>  lLM=await locationListModel.getTaskList(listTaskType);
    listDDMLocation.add(mLM);
    listDDMLocation.addAll(lLM);
    _ddmLocation = buildDropDownMenuItemsLocation(listDDMLocation);
    int locationId = widget.mapFilter[HomeScreen.location_id];
    if(locationId != null && locationId is int){
      int index = listDDMLocation.indexWhere((element) => element.id == locationId);
      selectedLocation = _ddmLocation[index].value;
    }else {
      selectedLocation = _ddmLocation[0].value;
    }
    setState((){});
  }
  List<DropdownMenuItem<TaskTypeModel>> buildDropDownMenuItemsTaskType(List listItems) {
    List<DropdownMenuItem<TaskTypeModel>> items = [];
    for (TaskTypeModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  List<DropdownMenuItem<FunctionsModel>> buildDropDownMenuItemsFunction(List listItems) {
    List<DropdownMenuItem<FunctionsModel>> items = [];
    for (FunctionsModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  List<DropdownMenuItem<LocationModel>> buildDropDownMenuItemsLocation(List listItems) {
    List<DropdownMenuItem<LocationModel>> items = [];
    for (LocationModel listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem.name,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth ,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 30, right: 20),
                  // width: double.infinity,
                  height: loginBoxHeight / 7,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                    color: MyColors.colorConvert('#5BB0EC'),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
                      Text(AppStrings.taskFilter, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
                      IconButton(
                        onPressed: () {
                          /*if(isChanges) {
                            checkForFilter();
                            Navigator.of(context).pop(mapTempFilter);
                            return;
                          }*/
                          Navigator.of(context).pop();
                        },
                        icon: Icon(Icons.clear, color: MyColors.colorConvert('#365e77'), size: 30),
                      )
                    ],
                  ),
                ),
                // Task type
                SizedBox(height: 20,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Text(AppStrings.taskType,style:TextStyle(
                    color:MyColors.colorConvert('#646464'),
                    fontSize: 17,
                    fontWeight: FontWeight.bold
                  )),
                ),
                SizedBox(height: 10,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: (dialogWidth/5)*3,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                          borderRadius: BorderRadius.circular(10),
                          color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                        ),
                        height: 50,
                        child: DropdownButtonHideUnderline(

                          child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: MyColors.colorConvert('#d8d8d8'),
                                size: 25,
                              ),
                              value: selectedTaskType,
                              items: _ddmTaskType,
                              onChanged: (value) async {
                                isChanges= true;
                                setState((){
                                  selectedTaskType = value;
                                  widget.mapFilter[HomeScreen.type_id]=selectedTaskType.id;
                                });
                              }),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState((){
                            isChanges=true;
                            _ddmTaskType = buildDropDownMenuItemsTaskType(listDDMTaskType);
                            selectedTaskType = _ddmTaskType[0].value;
                            widget.mapFilter.remove(HomeScreen.type_id);
                          });
                        },
                        icon: Icon(Icons.clear,color:MyColors.colorConvert('#365f7a')),
                      )
                    ],
                  ),
                ),

                // Function
                SizedBox(height: 20,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Text(AppStrings.function,style:TextStyle(
                      color:MyColors.colorConvert('#646464'),
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  )),
                ),
                SizedBox(height: 10,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: (dialogWidth/5)*3,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                            borderRadius: BorderRadius.circular(10),
                            color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                        ),
                        height: 50,
                        child: DropdownButtonHideUnderline(

                          child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: MyColors.colorConvert('#d8d8d8'),
                                size: 25,
                              ),
                              value: selectedFunction,
                              items: _ddmFunction,
                              onChanged: (value) async {
                                isChanges = true;
                                setState((){
                                  selectedFunction = value;
                                  widget.mapFilter[HomeScreen.group_id]=selectedFunction.id;
                                });
                              }),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState((){
                            isChanges=true;
                            _ddmFunction = buildDropDownMenuItemsFunction(listDDMFunction);
                            selectedFunction = _ddmFunction[0].value;
                            widget.mapFilter.remove(HomeScreen.group_id);
                          });
                        },
                        icon: Icon(Icons.clear,color:MyColors.colorConvert('#365f7a')),
                      )
                    ],
                  ),
                ),

                // Location
                SizedBox(height: 20,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Text(AppStrings.location,style:TextStyle(
                      color:MyColors.colorConvert('#646464'),
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  )),
                ),
                SizedBox(height: 10,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: (dialogWidth/5)*3,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                            borderRadius: BorderRadius.circular(10),
                            color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                        ),
                        height: 50,
                        child: DropdownButtonHideUnderline(

                          child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: MyColors.colorConvert('#d8d8d8'),
                                size: 25,
                              ),
                              value: selectedLocation,
                              items: _ddmLocation,
                              onChanged: (value) async {
                                isChanges = true;
                                setState((){
                                  selectedLocation = value;
                                  widget.mapFilter[HomeScreen.location_id]=selectedLocation.id;
                                });
                              }),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState((){
                            isChanges=true;
                            _ddmLocation = buildDropDownMenuItemsLocation(listDDMLocation);
                            selectedLocation = _ddmLocation[0].value;
                            widget.mapFilter.remove(HomeScreen.location_id);
                          });
                        },
                        icon: Icon(Icons.clear,color:MyColors.colorConvert('#365f7a')),
                      )
                    ],
                  ),
                ),

                // Start date
                SizedBox(height: 20,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Text(AppStrings.startDate,style:TextStyle(
                      color:MyColors.colorConvert('#646464'),
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  )),
                ),
                SizedBox(height: 10,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          selectStartDate(context);
                        },
                        child: Container(
                          width: (dialogWidth/5)*3,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                              borderRadius: BorderRadius.circular(10),
                              color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                          ),
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                !valSDAvail?'':DateFormat('dd-MM-yyyy').format(selectedStartDate),
                                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                                color: MyColors.colorConvert('#d8d8d8'),
                                size: 25,
                              )
                            ],
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState((){
                            isChanges=true;
                            valSDAvail = false;
                            selectedStartDate = DateTime.now();
                            widget.mapFilter.remove(HomeScreen.start_date_filter);
                          });
                        },
                        icon: Icon(Icons.clear,color:MyColors.colorConvert('#365f7a')),
                      )
                    ],
                  ),
                ),

                // End date
                SizedBox(height: 20,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Text(AppStrings.endDate,style:TextStyle(
                      color:MyColors.colorConvert('#646464'),
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  )),
                ),
                SizedBox(height: 10,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          selectEndDate(context);
                        },
                        child: Container(
                          width: (dialogWidth/5)*3,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                              borderRadius: BorderRadius.circular(10),
                              color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
                          ),
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                !valEDAvail?'':DateFormat('dd-MM-yyyy').format(selectedEndDate),
                                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                                color: MyColors.colorConvert('#d8d8d8'),
                                size: 25,
                              )
                            ],
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState((){
                            isChanges=true;
                            valEDAvail = false;
                            selectedEndDate = DateTime.now();
                            widget.mapFilter.remove(HomeScreen.end_date);
                          });
                        },
                        icon: Icon(Icons.clear,color:MyColors.colorConvert('#365f7a')),
                      )
                    ],
                  ),
                ),

                // filter button
                SizedBox(height: 20,),
                Container(
                  width: (dialogWidth/5)*3.5,
                  height: 50,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(0),
                      elevation: 0,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                      primary: MyColors.loginButtonColor,
                    ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed)) {
                        return MyColors.loginButtonReveleColor.withOpacity(0.2);
                      }
                      return null;
                    })),
                    onPressed: () async {
                      checkForFilter();
                      debugPrint('mapFilter : ${widget.mapFilter}');
                      Navigator.pop(context,widget.mapFilter);
                    },
                    child: Text(AppStrings.applyFilter, style: TextStyle(
                      fontSize: 17
                    )),
                  ),
                ),
                SizedBox(height: 20,),
                //end
              ],
            ),
          ),
        ),
      ),
    );
  }
  void checkForFilter(){
    if(selectedTaskType.id == -1){
      widget.mapFilter.removeWhere((key, value) => key==HomeScreen.type_id);
    }
    if(selectedFunction.id == -1){
      widget.mapFilter.removeWhere((key, value) => key == HomeScreen.group_id);
    }
    if(selectedLocation.id == -1){
      widget.mapFilter.removeWhere((key, value) => key == HomeScreen.location_id);
    }
  }

  selectStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      locale: Locale('nl',''),
      context: context,
      initialDate: selectedStartDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2050),
    );
    if (picked != null && picked != selectedStartDate) {
      setState(() {
        isChanges = true;
        valSDAvail = true;
        selectedStartDate = picked;
        String strStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(selectedStartDate);

        widget.mapFilter[HomeScreen.start_date_filter]=strStartDate;
      });
    }
  }

  selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      locale: Locale('nl',''),
      context: context,
      initialDate: selectedEndDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2050),
    );
    if (picked != null && picked != selectedEndDate) {
      setState(() {
        isChanges=true;
        valEDAvail = true;
        selectedEndDate = picked;
        selectedEndDate = selectedEndDate.add(Duration(hours: 11,minutes: 59,seconds: 59));

        debugPrint("Selected End Date : ${selectedEndDate.toString()}");
        String strEndDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(selectedEndDate);

        widget.mapFilter[HomeScreen.end_date]=strEndDate;
      });
    }
  }
}