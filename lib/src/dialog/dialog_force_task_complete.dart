import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

import 'dialog_add_note.dart';


class DialogForceTaskComplete extends StatefulWidget {
  DialogForceTaskComplete({Key key}) : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogForceTaskComplete> {
  // Task type
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = (MediaQuery.of(context).size.width/10)*3.6;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth,
          height: dialogHeight,
          child: Stack(
            children: [
              SingleChildScrollView(

                child: Container(
                  height: dialogHeight,
                  width: dialogWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Column(
                            children: [
                              DialogTitleWidget(title: AppStrings.forceTaskCompletionDialogTitle,),
                              SizedBox(height: 20,),
                              Container(
                                margin: EdgeInsets.only(left: 20,right: 20),
                                child: Text(AppStrings.forceTaskCompletionDialogMsg,style: TextStyle(
                                    color: MyColors.colorConvert('#797979'),
                                    fontSize: 18
                                ),),
                              ),
                              SizedBox(height: 20,),
                            ],
                          ),
                        )
                      ),
                      Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                            color: MyColors.colorConvert('#F4F4F4')
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              width: 150,
                              height: 50,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  border: Border.all(
                                    color: MyColors.colorConvert('#57a5e8'),
                                  ),
                                  color: Colors.white),
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: Colors.white,
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {
                                  debugPrint('Sluiten');
                                  Navigator.of(context).pop('no');
                                },
                                child: Text(AppStrings.annuleren, style: TextStyle(
                                    fontSize: 17,
                                  color:MyColors.colorConvert('#57a5e8')
                                )),
                              ),
                            ),
                            Container(
                              width: 160,
                              height: 50,
                              margin: EdgeInsets.only(left: 10),
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: MyColors.colorConvert('#266E9F'),
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {
                                  // debugPrint('Add Note');
                                  // Navigator.of(context).pop();
                                  // showDialog(
                                  //   context: context,
                                  //   builder: (context) => DialogAddNote(
                                  //     taskModel: appTaskModel,
                                  //     isFromDialogValidation:true
                                  //   ),
                                  // );
                                  Navigator.of(context).pop('yes');
                                },
                                child: Text(AppStrings.btnForceComplete, style: TextStyle(
                                    fontSize: 17
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //end
                    ],
                  ),
                ),
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}