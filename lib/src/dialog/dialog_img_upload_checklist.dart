import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_camera_gallery.dart';
import 'package:inspect_planner/src/dialog/dialog_confirm_delete_attachment.dart';
import 'package:inspect_planner/src/models/attachment_model.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/pages/home/image_viewer_screen.dart';
import 'package:inspect_planner/src/service/navigation_service.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/source.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path/path.dart' as p;
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:url_launcher/url_launcher.dart';


class DialogImgUploadChecklist extends StatefulWidget {
  CheckListItemModel checkListItemModel;
  DialogImgUploadChecklist({this.checkListItemModel});

  @override
  _DialogImgUploadChecklistState createState() => _DialogImgUploadChecklistState();
}

class _DialogImgUploadChecklistState extends State<DialogImgUploadChecklist> {
  List<Map<String,dynamic>> listFiles=[];
  final String mKey ='key';
  // key = to view display -> view/image/other
  final String mPath ='path';
  // path = file path -> url/filepath
  final String mType = 'type';
  // type = file extension -> '.jpg/.png/...'
  final String mSource = 'source';
  // source = from web or local file -> Source.WEB, Source.LOCAL
  final String mData = 'data';
  // data = data of local or web database
  @override
  void initState() {
    // TODO: implement initState
    initAttachments();
    super.initState();
  }
  @override
  void didUpdateWidget(covariant DialogImgUploadChecklist oldWidget) {
    // TODO: implement didUpdateWidget
    initAttachments();
    super.didUpdateWidget(oldWidget);
  }
  List<Map<String,dynamic>> listAttachments = [];
  void initAttachments()async{
    listFiles.clear();
    debugPrint("----refresh - initAttachments");
    listAttachments = await Utility.getCheckListAttachment(chkLstItmModel: widget.checkListItemModel);
    debugPrint("List Attachments : ${listAttachments.toString()}");
    Map<String,dynamic> mapFile = Map();
    mapFile['key']='view';
    listFiles.add(mapFile);

    await Future.forEach(listAttachments,(element) {
      String imageName = element['image_name'];
      Map<String,dynamic> mapFile = Map();
      if(imageName.startsWith('http')){
        mapFile[mSource]=Source.WEB;
      }else{
        mapFile[mSource]=Source.LOCAL;
      }

      File file = File(imageName);
      debugPrint("file : ${file.path}");
      String extension = p.context.extension(file.path);
      if(isImage(file)){
        mapFile[mKey]='image';
      }else{
        mapFile[mKey]='other';
      }

      mapFile[mPath]=imageName;
      mapFile[mType]=extension;
      mapFile[mData]=element;


      listFiles.add(mapFile);
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = (MediaQuery.of(context).size.height/3)*2;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth ,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                DialogTitleWidget(title: AppStrings.fotos,),
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.only(left: 20,right: 20),
                  width: dialogWidth,
                  child: Wrap(
                    alignment: WrapAlignment.start,
                    children: listFiles.map((e){
                      Map<String,dynamic> mapFile = e;
                      String key = mapFile[mKey];
                      String type = mapFile[mType];
                      String path = mapFile[mPath];
                      Source source = mapFile[mSource];
                      dynamic data = mapFile[mData];
                      return key=='view'?addFileWidget(context: context):key=='image'?
                      imageFileWidget(path: path,type: type,source: source,data: data,mContext:context):
                      otherFileWidget(type: type,path: path,source: source,data:data,mContext: context);
                    }).toList(growable: true),
                  ),
                ),
                // cancel button
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                      color: MyColors.colorConvert('#F4F4F4')
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: 150,
                        height: 50,
                        child: ElevatedButton(

                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(0),
                            elevation: 0,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            primary: MyColors.colorConvert('#5BB0EC'),
                          ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                            if (states.contains(MaterialState.pressed)) {
                              return MyColors.loginButtonReveleColor.withOpacity(0.2);
                            }
                            return null;
                          })),
                          onPressed: () async {
                            Navigator.of(context).pop();
                          },
                          child: Text(AppStrings.sluiten, style: TextStyle(
                              fontSize: 17
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
                //end
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget addFileWidget({BuildContext context}){
    return GestureDetector(
      onTap: () async{

        // onClickAddPhoto
        String dialogResult = await showDialog(context: context, builder: (context) => DialogCameraGallery(),);
        debugPrint("dialogResult : $dialogResult");
        String filePath;
        if(dialogResult == 'cancel'){
          return;
        }else if(dialogResult == 'camera'){
          final ImagePicker _picker = ImagePicker();
          // Capture a photo
          final XFile photo = await _picker.pickImage(source: ImageSource.camera);
          if(photo == null){
            return;
          }
          filePath = photo.path;
          debugPrint("Camera photo path : ${photo.path}");
        }else if(dialogResult == 'library'){
          /*FilePickerResult result = await FilePicker.platform.pickFiles();
          if( result == null){
            return;
          }
          filePath = result.files.single.path;*/
          final ImagePicker _picker = ImagePicker();
          final XFile photo = await _picker.pickImage(source: ImageSource.gallery);
          if(photo == null){
            return;
          }
          filePath = photo.path;
          debugPrint("Gellery photo path : ${filePath}");
        }
        debugPrint("taskid : ${appTaskModel.synced}");
        /*locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
        await Future.delayed(Duration(seconds: 2),(){
          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        });
        return;*/
        // FilePickerResult result = await FilePicker.platform.pickFiles();

        if(filePath != null) {
          File file = File(filePath);
          debugPrint("file : ${file.path}");
          debugPrint("file : ${file.path}");
          String extension = p.context.extension(file.path);
          Map<String,dynamic> mapFile = Map();
          if(isImage(file)){
            mapFile[mKey]='image';
          }else{
            mapFile[mKey]='other';
          }

          mapFile[mPath]=file.path;
          mapFile[mType]=extension;
          mapFile[mSource]=Source.LOCAL;

          int atchmntId = new DateTime.now().millisecondsSinceEpoch;
          await Future.delayed(Duration(milliseconds: 10),(){});
          debugPrint("MAX ID : $atchmntId");

          Map<String,dynamic> mapAtchmnt = Map();
          mapAtchmnt['attachments_id'] = appUserModel.id;
          mapAtchmnt['createDate'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
          mapAtchmnt['Checklistitem_id'] = widget.checkListItemModel.id;
          mapAtchmnt['id'] = atchmntId;
          mapAtchmnt['image_name'] = file.path;
          mapAtchmnt['removed'] = 0;
          mapAtchmnt['task_id'] = null;

          mapFile[mData]=mapAtchmnt;
          listFiles.add(mapFile);

          AttachmentModel attachmentModel = AttachmentModel.fromMap(map: mapAtchmnt);
          attachmentModel.synced=0;
          String sqlAtchmntLstItmMdl = AppTablesQuery.INSERT_TABLE_ATTACHMENT + '(?,?,?,?,?,?,?,?)';
          int insertResult = await appDatabase.rawInsert(sqlAtchmntLstItmMdl, attachmentModel.getDataList(attachmentModel));
          debugPrint("Image inserted : $insertResult");
          if(insertResult == 0){
            debugPrint("Error adding attachment");
            MyToast.showToast(AppStrings.errAddingAttachment, context);
            return;
          }
          var connectivityResult = await (Connectivity().checkConnectivity());
          if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1 && widget.checkListItemModel.synced == 1) {
            DialogMgr.showProgressDialog(context);
            atchmntId = await AppSync.addAttachment(context:context,mapNewAtchmnt:mapAtchmnt);
            Navigator.of(locator<IPKeys>().keyDialogProgress.currentContext,).pop();
            if(atchmntId == syncErrorValue){
              MyToast.showToast(AppStrings.errAddingChecklistAttachment, context);
              return;
            }
          }
          locator<DetailTabBloc>().detailTabEventSink.add(1);
          locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
          // insert data to database here

          //
          initAttachments();
          setState(() {});
          // final mimeType = lookupMimeType('/some/path/to/file/file.jpg');

        } else {
          // User canceled the picker
        }
      },
      child: Container(
        margin: EdgeInsets.only(right: 10,bottom: 10),
        height: 130,
        width: 150,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: MyColors.colorConvert('#CCCCCC')),
            color: MyColors.colorConvert('#F4F4F4')
        ),
        child: Icon(Icons.add,color: MyColors.colorConvert('#C3C3C3'),size: 50,),
      ),
    );
  }

  Widget imageFileWidget({String type,String path,Source source,dynamic data,BuildContext mContext}){
    return Container(
      margin: EdgeInsets.only(right: 10,bottom: 10),
      height: 130,
      width: 150,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: MyColors.colorConvert('#CCCCCC')),
          color: MyColors.colorConvert('#F4F4F4')
      ),
      child: Stack(
        children: [
          GestureDetector(
            onTap: () {
              debugPrint("onClick");
              debugPrint("path $path");
              Navigator.of(mContext).pushNamed(ImageViewerScreen.routeName,arguments: [source,path]);
            },
            child: Container(
              height: 130,
              width: 150,
              child: ClipRRect(

                  borderRadius: BorderRadius.circular(10),
                  child: source == Source.LOCAL?Image.file(File(path),fit: BoxFit.cover,):FancyShimmerImage(
                    imageUrl: path,
                    boxFit: BoxFit.cover,
                  )/*Image.network(path,fit: BoxFit.cover,)*/
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 5,top: 5,right: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              GestureDetector(
                onTap: (){

                  onClickDeleteImage(data:data,mContext: mContext);
                },
                child: Container(

                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: MyColors.colorConvert('#0b1419')
                    ),
                    child: Icon(Icons.clear,color: Colors.white,)
                ),
              ),
                SizedBox(width: 5,),
                Flexible(
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: MyColors.colorConvert('#0b1419')
                    ),
                    child: Text(AppStrings.bestand,style: TextStyle(
                        fontSize: 12,
                        color: Colors.white
                    ),),
                  ),
                ),
            ],),
          )
        ],
      ),
    );
  }
  Widget otherFileWidget({String type,String path,Source source,dynamic data,BuildContext mContext}){
    return GestureDetector(
      onTap: () async{
        debugPrint("onClick file");
        if(source == Source.WEB) {
          debugPrint("onClick Web : $path");
          await launch(path);
        }else{
          debugPrint("onClick OpenFilex.open : $path");
          await OpenFilex.open(path);
          // await launch(path);
          // Navigator.of(mContext).pushNamed(FileViewerScreen.routeName,arguments: [source,path]);

        }
      },
      child: Container(
        margin: EdgeInsets.only(right: 10,bottom: 10),
        height: 130,
        width: 150,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: MyColors.colorConvert('#CCCCCC')),
            color: MyColors.colorConvert('#38627E')
        ),
        child: Stack(
          children: [
            Align(
                alignment: Alignment.center,
                child: Icon(Icons.file_copy,color: Colors.white,size: 50,)
            ),
            Container(
              margin: EdgeInsets.only(left: 5,top: 5,right: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                GestureDetector(
                  onTap: () {
                    onClickDeleteImage(data:data,mContext: mContext);
                  },
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: MyColors.colorConvert('#0b1419')
                      ),
                      child: Icon(Icons.clear,color: Colors.white,)
                  ),
                ),
                SizedBox(width: 5,),
                Flexible(
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: MyColors.colorConvert('#0b1419')
                    ),
                    child: Text(AppStrings.bestand,style: TextStyle(
                      fontSize: 12,
                      color: Colors.white
                    ),),
                  ),
                ),
              ],),
            )
          ],
        ),
      ),
    );
  }
  bool isImage(File file){
    String extension = p.context.extension(file.path);
    Map<String,dynamic> mapFile = Map();
    if(extension.endsWith('png') ||
        extension.endsWith('PNG') ||
        extension.endsWith('jpg') ||
        extension.endsWith('JPG') ||
        extension.endsWith('JPEG') ||
        extension.endsWith('jpeg')){
      return true;
    }else{
      return false;
    }

  }
  void onClickDeleteImage({dynamic data,BuildContext mContext})async{
    String result = await showDialog(context: mContext, builder: (context) => DialogConfirmDeleteAttachment(),);
    if(result == 'no'){
      return;
    }
    AttachmentModel attachmentModel = AttachmentModel.fromMap(map: data);
    attachmentModel.synced = data['synced'];
    debugPrint("data : $data");
    String sqlDeleteAtchmnt = "update ${AppTables.ATTACHMENT} set removed = ?, synced = ? where id = ?";
    List<dynamic> sqlValues = [1,0,data['id']];
    int deleteResult = await appDatabase.rawUpdate(sqlDeleteAtchmnt,sqlValues);
    debugPrint("delete result : $deleteResult");
    if(deleteResult == 0){
      debugPrint("Error deleting attachment in checklist");
      MyToast.showToast(AppStrings.errDeletingAttachmentInChecklist, mContext);
      return;
    }
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1 && widget.checkListItemModel.synced == 1 && attachmentModel.synced==1) {
      DialogMgr.showProgressDialog(mContext);
      int atchmntId = await AppSync.deleteAttachment(context:mContext,mapNewAtchmnt:data);
      Navigator.of(locator<IPKeys>().keyDialogProgress.currentContext,).pop();
      if(atchmntId == syncErrorValue){
        MyToast.showToast(AppStrings.errAddingTaskAttachment, mContext);
        return;
      }
    }

    List<Map<String,dynamic>> newListFiles=[];
    listFiles.forEach((element) {
      Map<String,dynamic> eleData = element['data'];
      if(element[mKey]=='view'){
        newListFiles.add(element);
      }
      else if(eleData['id']!=data['id']){
        newListFiles.add(element);
      }
    });
    listFiles = newListFiles;


    locator<DetailTabBloc>().detailTabEventSink.add(1);
    locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
    initAttachments();
    setState(() {});
  }
}
