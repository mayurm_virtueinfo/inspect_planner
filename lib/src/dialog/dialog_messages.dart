import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/dialog/dialog_newsposts_popup.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:intl/intl.dart';


class DialogMessages extends StatefulWidget {
  DialogMessages();

  @override
  _DialogMessagesState createState() => _DialogMessagesState();
}

class _DialogMessagesState extends State<DialogMessages> {

  List<Map<String,dynamic>> listSqlBerichten = [];
  @override
  void initState() {
    // TODO: implement initState
    getNewsPosts();
    super.initState();
  }
  void getNewsPosts() async{
    String sql = "select * from ${AppTables.NEWSPOSTS} order by publication_date desc";
    listSqlBerichten = await appDatabase.rawQuery(sql);
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = MediaQuery.of(context).size.width/2;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: SingleChildScrollView(
          child: Container(
            width:dialogWidth ,
            // height: 100,
            child: Column(
              // mainAxisSize: MainAxisSize.min,
              children: [
                DialogTitleWidget(title: AppStrings.messages,),
                SizedBox(height: 20,),
                listSqlBerichten.length>0?ListView.separated(
                  controller: ScrollController(),
                  shrinkWrap: true,
                  itemCount: listSqlBerichten.length,
                  separatorBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(left: 20,right: 20,top: 10,bottom: 10),
                      height: 1,
                      color: MyColors.colorConvert('#B5B5C3'),
                    );
                  },
                  itemBuilder: (context, index) {
                    NewspostsModel mNewsPostModel = NewspostsModel.fromMap(map: listSqlBerichten[index]);
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),

                      width: dialogWidth,
                      padding: EdgeInsets.only(left: 20,right: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            child: Image.asset('assets/icon/icon-chat-grey.png',width: 20,)
                          ),
                          SizedBox(width: 20,),
                          Expanded(
                            flex: 1,
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(mNewsPostModel.title),
                                  SizedBox(height: 2,),
                                  Text('${DateFormat('dd-MM-yyyy').format(DateFormat('yyyy-MM-dd').parse(mNewsPostModel.publication_date))}'),
                                  SizedBox(height: 2,),
                                  Text(mNewsPostModel.content),
                                ],
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              showDialog(context: context, builder: (context) => DialogNewspostsPopup(newspostsModel: mNewsPostModel,),);
                            },
                            child: Container(
                              padding: EdgeInsets.only(right: 10),
                              child: Icon(Icons.search,color: MyColors.colorConvert('#B5B5C3'))
                            ),
                          )
                        ],
                      ),
                    );
                  },):Container()
              ],
            ),
          ),
        ),
      ),
    );
  }

}