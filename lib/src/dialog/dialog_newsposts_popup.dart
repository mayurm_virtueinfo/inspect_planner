import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/models/attachment_model.dart';
import 'package:inspect_planner/src/models/message_view_log_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/source.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:path/path.dart' as p;
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


class DialogNewspostsPopup extends StatefulWidget {
  NewspostsModel newspostsModel;
  int newsPostCount;
  int appListNewspostsModel;
  DialogNewspostsPopup({this.newspostsModel,this.newsPostCount,this.appListNewspostsModel});

  @override
  _DialogMessagePopupState createState() => _DialogMessagePopupState();
}

class _DialogMessagePopupState extends State<DialogNewspostsPopup> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void markAsReadMessage(BuildContext mContext)async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.none) {
      //network availabe - sync add task
      Map<String,dynamic> mapRequest = Map();
      mapRequest['message_id'] = widget.newspostsModel.id;
      mapRequest['id'] = DateTime.now().millisecondsSinceEpoch;
      int resultId = await AppSync.addReadMessage(context: mContext,mapReadMsg: mapRequest);
      if(resultId == syncErrorValue){
        return;
      }
      debugPrint("AddTask sync success");
    }
    else{
      Map<String, dynamic> mapNewReadMsg = Map();
      mapNewReadMsg['user_id'] = appUserModel.id;
      mapNewReadMsg['message_id'] = widget.newspostsModel.id;
      mapNewReadMsg['date'] = DateFormat('yyyy-MM-dd').format(DateTime.now());
      mapNewReadMsg['created_at'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
      mapNewReadMsg['updated_at'] = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
      mapNewReadMsg['id'] = DateTime.now().millisecondsSinceEpoch;

      MessageViewLogModel mViewModel = MessageViewLogModel.fromMap(map: mapNewReadMsg);
      mViewModel.synced = 0;

      debugPrint("local sync : ${mViewModel.toJSON()}");
      String sql = AppTablesQuery.INSERT_TABLE_MESSAGE_VIEW_LOG + '(?,?,?,?,?,?,?)';
      int insertResult = await appDatabase.rawInsert(sql, mViewModel.getDataList(mViewModel));
      if (insertResult == 0) {
        MyToast.showToast(AppStrings.errAddingMsgViewLogInLocal, mContext);
        // delete record if get any error in update
        return Future.value(syncErrorValue);
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/6)*3;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        child: Container(
          width:dialogWidth ,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 30, right: 20),
                  // width: double.infinity,
                  height: loginBoxHeight / 7,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                    color: MyColors.colorConvert('#5BB0EC'),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
                      Text(AppStrings.message, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
                      IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(Icons.clear, color: MyColors.colorConvert('#365e77'), size: 30),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.only(left: 30,right: 30),
                  width: dialogWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${widget.newspostsModel.title}',style:TextStyle(
                        color: MyColors.colorConvert('#4fa8f1'),
                        fontSize: 26
                      )),
                      SizedBox(height: 20,),
                      Text('${widget.newspostsModel.content}',style:TextStyle(
                          color: MyColors.colorConvert('#777777'),
                          fontSize: 16
                      ))
                    ],
                  ),
                ),
                // cancel button
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.only(left: 30,right: 30,top: 15,bottom: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                    color: MyColors.colorConvert('#F4F4F4')
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('${DateFormat('dd-MM-yyyy').format(DateFormat('yyyy-MM-dd').parse(widget.newspostsModel.publication_date))}',style: TextStyle(
                        fontSize: 16,
                        color: MyColors.colorConvert('#777777')
                      ),),
                      Container(
                        width: widget.newsPostCount==(widget.appListNewspostsModel-1)?80:160,
                        height: 50,
                        child: ElevatedButton(

                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(0),
                            elevation: 0,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            primary: MyColors.colorConvert('#5BB0EC'),
                          ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                            if (states.contains(MaterialState.pressed)) {
                              return MyColors.loginButtonReveleColor.withOpacity(0.2);
                            }
                            return null;
                          })),
                          onPressed: () async {
                            markAsReadMessage(context);
                            Navigator.of(context).pop('cancel');
                          },
                          child: Text(widget.newsPostCount==(widget.appListNewspostsModel-1)?AppStrings.close:AppStrings.next_message, style: TextStyle(
                            fontSize: 17
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
                //end
              ],
            ),
          ),
        ),
      ),
    );
  }

}
