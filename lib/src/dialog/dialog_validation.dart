import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/add_note_dialog_bloc.dart';
import 'package:inspect_planner/src/bloc/alert_bloc.dart';
import 'package:inspect_planner/src/models/dialog_add_note_model.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/widget/dialog_title_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';

import 'dialog_add_note.dart';


class DialogValidation extends StatelessWidget {
  String message;
  String digitRange;
  DialogValidation({this.message,this.digitRange,Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = (MediaQuery.of(context).size.width/10)*4;
    return WillPopScope(
      onWillPop: ()async{
        return false;
      },
      child: Dialog(

        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        elevation: 0,
        backgroundColor: Colors.white,
        child: Container(
          width:dialogWidth,
          height: dialogHeight,
          child: Stack(
            children: [
              SingleChildScrollView(

                child: Container(
                  height: dialogHeight,
                  width: dialogWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                          flex: 1,
                          child: Container(
                            child: Column(
                              children: [
                                DialogTitleWidget(title: AppStrings.melding,hideCloseIcon: true,),
                                SizedBox(height: 20,),
                                Text(message,style: TextStyle(
                                    color: MyColors.colorConvert('#797979'),
                                    fontSize: 20
                                ),),
                                SizedBox(height: 20,),
                              ],
                            ),
                          )
                      ),

                      Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                            color: MyColors.colorConvert('#F4F4F4')
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [



                            Container(
                              width: 150,
                              height: 50,
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: MyColors.colorConvert('#266E9F'),
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {
                                  debugPrint('Sluiten');
                                  // Navigator.of(context).pop('no');
                                  locator<AlertBloc>().alertEventSink.add(null);
                                },
                                child: Text(AppStrings.sluiten, style: TextStyle(
                                    fontSize: 17
                                )),
                              ),
                            ),
                            Container(
                              width: 160,
                              height: 50,
                              margin: EdgeInsets.only(left: 10),
                              child: ElevatedButton(

                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(0),
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  primary: MyColors.colorConvert('#266E9F'),
                                ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                  }
                                  return null;
                                })),
                                onPressed: () async {
                                  debugPrint('Add Note');
                                  // Navigator.of(context).pop();
                                  locator<AlertBloc>().alertEventSink.add(null);
                                  // FocusScope.of(context).requestFocus(FocusNode());
                                  // await Future.delayed(const Duration(milliseconds: 100));
                                  locator<AddNoteDialogBloc>().addNoteDialogEventSink.add(DialogAddNoteModel(
                                    show: true,
                                    taskModel: appTaskModel,
                                    isFromDialogValidation:true
                                  ));
                                  // showDialog(
                                  //   context: context,
                                  //   builder: (context) => DialogAddNote(
                                  //       taskModel: appTaskModel,
                                  //       isFromDialogValidation:true
                                  //   ),
                                  // );
                                },
                                child: Text(AppStrings.addNote, style: TextStyle(
                                    fontSize: 17
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //end
                    ],
                  ),
                ),
              ),
              LoadingWidget()
            ],
          ),
        ),
      ),
    );
  }
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
}
