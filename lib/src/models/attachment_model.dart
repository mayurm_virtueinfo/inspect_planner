class AttachmentModel {
  int id;
  int attachments_id;
  String createDate;
  String image_name;
  int removed;
  int Checklistitem_id;
  int task_id;
  int synced=0;

  AttachmentModel({
    this.id,
    this.attachments_id,
    this.createDate,
    this.image_name,
    this.removed,
    this.Checklistitem_id,
    this.task_id,
  });

  AttachmentModel.fromMap({Map<String, dynamic> map})
      :
        this.id = map['id']??null,
        this.attachments_id = map['attachments_id']??null,
        this.createDate = map['createDate']??null,
        this.image_name = map['image_name']??null,
        this.removed = map['removed']??null,
        this.Checklistitem_id = map['Checklistitem_id']??null,
        this.task_id = map['task_id'??null]
  ;

  Map<String, dynamic> toJSON() {
    return {
      'id':this.id,
      'attachments_id':this.attachments_id,
      'createDate':this.createDate,
      'image_name':this.image_name,
      'removed':this.removed,
      'Checklistitem_id':this.Checklistitem_id,
      'task_id':this.task_id,
    };
  }

  List<dynamic> getDataList(AttachmentModel taskModel) {
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.attachments_id);
    lstData.add(this.createDate);
    lstData.add(this.image_name);
    lstData.add(this.removed);
    lstData.add(this.Checklistitem_id);
    lstData.add(this.task_id);
    lstData.add(this.synced);

    return lstData;
  }


}
