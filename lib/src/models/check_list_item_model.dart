import 'package:inspect_planner/src/models/attachment_model.dart';

class CheckListItemModel {
  int id; //": 972691,
  int task_id; //": 106797,
  String question; //": "Is het onderhoud uitgevoerd?",
  int type; //": 1,
  String result; //": null,
  int removed; //": 0,
  String description; //": "",
  int order_number; //": null,
  String unique_hash; //": null,
  int clone_checklist_id; //": 5393
  int synced=0;
  String digit_range;
  String right_answer;
  String wrong_answer;
  String dropdown_answers;
  int is_multiple=0;
  List<AttachmentModel> checklist_attachment;

  CheckListItemModel({
    this.id,
    this.task_id,
    this.question,
    this.type,
    this.result,
    this.removed,
    this.description,
    this.order_number,
    this.unique_hash,
    this.clone_checklist_id,
    this.checklist_attachment,
    this.digit_range,
    this.right_answer,
    this.wrong_answer,
    this.dropdown_answers,
    this.is_multiple,
  });

  CheckListItemModel.fromMap({Map<String, dynamic> map})
      : this.id = map['id'],
        this.task_id = map['task_id'],
        this.question = map['question'],
        this.type = map['type'],
        this.result = map['result'],
        this.removed = map['removed'],
        this.description = map['description'],
        this.order_number = map['order_number'],
        this.unique_hash = map['unique_hash'],
        this.clone_checklist_id = map['clone_checklist_id'],
        this.digit_range = map['digit_range'],
        this.right_answer = map['right_answer'],
        this.wrong_answer = map['wrong_answer'],
        this.dropdown_answers = map['dropdown_answers'],
        this.is_multiple = map['is_multiple'],
        this.checklist_attachment = getCheckListAttachmentList(map['checklist_attachment']);


  static List<AttachmentModel> getCheckListAttachmentList(List<dynamic> mTaskList){
    List<AttachmentModel> lstCheckList = [];
    if(mTaskList!=null) {
      mTaskList.forEach((element) async {
        AttachmentModel taskModel = AttachmentModel.fromMap(map: element);
        lstCheckList.add(taskModel);
      });
    }
    return lstCheckList;
  }

  Map<String, dynamic> toJSON() {
    return {
      'id': this.id,
      'task_id': this.task_id,
      'question': this.question,
      'type': this.type,
      'result': this.result,
      'removed': this.removed,
      'description': this.description,
      'order_number': this.order_number,
      'unique_hash': this.unique_hash,
      'clone_checklist_id': this.clone_checklist_id,
      'digit_range': this.digit_range,
      'right_answer': this.right_answer,
      'wrong_answer': this.wrong_answer,
      'dropdown_answers' : this.dropdown_answers,
      'is_multiple' : this.is_multiple
    };
  }

  List<dynamic> getChkListDataList(CheckListItemModel taskModel) {
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.task_id);
    lstData.add(this.question);
    lstData.add(this.type);
    lstData.add(this.result);
    lstData.add(this.removed);
    lstData.add(this.description);
    lstData.add(this.order_number);
    lstData.add(this.unique_hash);
    lstData.add(this.clone_checklist_id);
    lstData.add(this.synced);
    lstData.add(this.digit_range);
    lstData.add(this.right_answer);
    lstData.add(this.wrong_answer);
    lstData.add(this.dropdown_answers);
    lstData.add(this.is_multiple);

    return lstData;
  }


}
