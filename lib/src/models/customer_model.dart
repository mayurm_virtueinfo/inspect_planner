class CustomerModel {
  int id;
  String name;
  String phone_number;
  int is_active;
  String create_date;
  int removed;
  int ipad_access;
  int dashboard_access;

  CustomerModel({
    this.id,
    this.name,
    this.phone_number,
    this.is_active,
    this.create_date,
    this.removed,
    this.ipad_access,
    this.dashboard_access,
  });

  CustomerModel.fromMap({Map<String, dynamic> map})
      :
        this.id=map['id'],
        this.name=map['name'],
        this.phone_number=map['phone_number'],
        this.is_active=map['is_active'],
        this.create_date=map['create_date'],
        this.removed=map['removed'],
        this.ipad_access=map['ipad_access'],
        this.dashboard_access=map['dashboard_access']
  ;

  Map<String, dynamic> toJSON() {
    return {
      'id':this.id,
      'name':this.name,
      'phone_number':this.phone_number,
      'is_active':this.is_active,
      'create_date':this.create_date,
      'removed':this.removed,
      'ipad_access':this.ipad_access,
      'dashboard_access':this.dashboard_access
    };
  }

  List<dynamic> getDataList(CustomerModel taskModel) {
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.name);
    lstData.add(this.phone_number);
    lstData.add(this.is_active);
    lstData.add(this.create_date);
    lstData.add(this.removed);
    lstData.add(this.ipad_access);
    lstData.add(this.dashboard_access);

    return lstData;
  }


}
