import 'package:inspect_planner/src/models/task_model.dart';

class DialogAddNoteModel {
  bool show;
  TaskModel taskModel;
  bool isFromDialogValidation;

  DialogAddNoteModel({
    this.show,
    this.taskModel,
    this.isFromDialogValidation
  });

}
