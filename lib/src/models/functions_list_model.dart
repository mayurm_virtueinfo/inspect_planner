import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';

class FunctionsListModel{
  List<FunctionsModel> listTask;


  FunctionsListModel({this.listTask});

  /*FunctionsListModel.froList({List<dynamic> mTaskList}):
    this.listTask = getTaskList(mTaskList)
  ;*/

  Future<List<FunctionsModel>> getTaskList(List<dynamic> mTaskList){
    List<FunctionsModel> lstTask = [];
    mTaskList.forEach((element) async{
      FunctionsModel taskModel = FunctionsModel.fromMap(map: element);
      lstTask.add(taskModel);
    });
    return Future.value(lstTask);
  }
}



























