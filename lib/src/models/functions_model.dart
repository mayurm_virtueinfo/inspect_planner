class FunctionsModel{
  int id;//": 119,
  int customer_id;//": 24,
  String name;//": "Technische dienst",
  int removed;//": 0,
  String create_date;//": "2015-10-13 06:47:48",
  int stamitem;//": 0,
  int clone_usergroup_id;//": 1,
  int is_main_usergroup;//": 0


  FunctionsModel({
    this.id,
    this.customer_id,
    this.name,
    this.removed,
    this.create_date,
    this.stamitem,
    this.clone_usergroup_id,
    this.is_main_usergroup
  });

  FunctionsModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id']??null,
    this.customer_id = map['customer_id']??null,
    this.name = map['name']??null,
    this.removed = map['removed']??null,
    this.create_date = map['create_date']??null,
    this.stamitem = map['stamitem']??null,
    this.clone_usergroup_id = map['clone_usergroup_id']??null,
    this.is_main_usergroup = map['is_main_usergroup']??null
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'customer_id':this.customer_id,
      'name':this.name,
      'removed':this.removed,
      'create_date':this.create_date,
      'stamitem':this.stamitem,
      'clone_usergroup_id':this.clone_usergroup_id,
      'is_main_usergroup':this.is_main_usergroup
    };
  }

  List<dynamic> getDataList(FunctionsModel taskModel){
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.customer_id);
    lstData.add(this.name);
    lstData.add(this.removed);
    lstData.add(this.create_date);
    lstData.add(this.stamitem);
    lstData.add(this.clone_usergroup_id);
    lstData.add(this.is_main_usergroup);

    return lstData;

  }

}