import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';

class LocationListModel{
  List<LocationModel> listTask;


  LocationListModel({this.listTask});

  /*LocationListModel.froList({List<dynamic> mTaskList}):
    this.listTask = getTaskList(mTaskList)
  ;*/

  Future<List<LocationModel>> getTaskList(List<dynamic> mTaskList){
    List<LocationModel> lstTask = [];
    mTaskList.forEach((element) async{
      LocationModel taskModel = LocationModel.fromMap(map: element);
      lstTask.add(taskModel);
    });
    return Future.value(lstTask);
  }
}



























