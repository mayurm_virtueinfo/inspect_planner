class LocationModel{
  int id;//": 257,
  int customer_id;//": 24,
  int parent_id;//": null,
  String name;//": "Zwembad Helder Water",
  int removed;//": 0,
  String create_date;//": "2015-10-13 06:47:48",
  int stamitem;//": 0,
  int clone_location_id;//": null


  LocationModel({
    this.id,
    this.customer_id,
    this.parent_id,
    this.name,
    this.removed,
    this.create_date,
    this.stamitem,
    this.clone_location_id
  });

  LocationModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id']??null,
    this.customer_id = map['customer_id']??null,
    this.parent_id = map['parent_id']??null,
    this.name = map['name']??null,
    this.removed = map['removed']??null,
    this.create_date = map['create_date']??null,
    this.stamitem = map['stamitem']??null,
    this.clone_location_id = map['clone_location_id']??null
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'customer_id':this.customer_id,
      'parent_id':this.parent_id,
      'name':this.name,
      'removed':this.removed,
      'create_date':this.create_date,
      'stamitem':this.stamitem,
      'clone_location_id':this.clone_location_id
    };
  }

  List<dynamic> getDataList(LocationModel taskModel){
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.customer_id);
    lstData.add(this.parent_id);
    lstData.add(this.name);
    lstData.add(this.removed);
    lstData.add(this.create_date);
    lstData.add(this.stamitem);
    lstData.add(this.clone_location_id);
    return lstData;

  }
}