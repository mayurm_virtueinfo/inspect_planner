class MessageViewLogModel {
  int id;
  int user_id;
  int message_id;
  String date;
  String created_at;
  String updated_at;
  int synced=0;

  MessageViewLogModel({
    this.id,
    this.user_id,
    this.message_id,
    this.date,
    this.created_at,
    this.updated_at,
  });

  MessageViewLogModel.fromMap({Map<String, dynamic> map})
      :
      this.id = map['id'],
      this.user_id = map['user_id'],
      this.message_id = map['message_id'],
      this.date = map['date'],
      this.created_at = map['created_at'],
      this.updated_at = map['updated_at']
  ;

  Map<String, dynamic> toJSON() {
    return {
      'id':this.id,
      'user_id':this.user_id,
      'message_id':this.message_id,
      'date':this.date,
      'created_at':this.created_at,
      'updated_at':this.updated_at,
    };
  }

  List<dynamic> getDataList(MessageViewLogModel taskModel) {
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.user_id);
    lstData.add(this.message_id);
    lstData.add(this.date);
    lstData.add(this.created_at);
    lstData.add(this.updated_at);
    lstData.add(this.synced);

    return lstData;
  }


}
