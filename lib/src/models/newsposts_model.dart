class NewspostsModel{
  int id;
  String title;
  String publication_date;
  String due_date;
  String status;
  String created_at;
  String updated_at;
  int removed;
  String content;


  NewspostsModel({
    this.id,
    this.title,
    this.publication_date,
    this.due_date,
    this.status,
    this.created_at,
    this.updated_at,
    this.removed,
    this.content,
  });

  NewspostsModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id'],
    this.title = map['title'],
    this.publication_date = map['publication_date'],
    this.due_date = map['due_date'],
    this.status = map['status'],
    this.created_at = map['created_at'],
    this.updated_at = map['updated_at'],
    this.removed = map['removed'] is String?int.parse(map['removed']):map['removed'],
    this.content = map['content'];


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'title':this.title,
      'publication_date':this.publication_date,
      'due_date':this.due_date,
      'status':this.status,
      'created_at':this.created_at,
      'updated_at':this.updated_at,
      'removed':this.removed,
      'content':this.content,
    };
  }

  List<dynamic> getDataList(NewspostsModel taskModel){

    List<dynamic> lstData = [];
    lstData.add(id);
    lstData.add(title);
    lstData.add(publication_date);
    lstData.add(due_date);
    lstData.add(status);
    lstData.add(created_at);
    lstData.add(updated_at);
    lstData.add(removed);
    lstData.add(content);

    return lstData;

  }
}