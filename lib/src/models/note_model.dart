class NoteModel {
  int id;
  int user_id;
  int task_id;
  String note;
  String create_date;
  int assignedto_user_id;
  int synced=0;
  int removed=0;

  NoteModel({
    this.id,
    this.user_id,
    this.task_id,
    this.note,
    this.create_date,
    this.assignedto_user_id,
  });

  NoteModel.fromMap({Map<String, dynamic> map})
      :
      this.id = map['id']??null,
      this.user_id = map['user_id']??null,
      this.task_id = map['task_id']??null,
      this.note = map['note']??null,
      this.create_date = map['create_date']??null,
      this.assignedto_user_id = map['assignedto_user_id']??null
  ;

  Map<String, dynamic> toJSON() {
    return {
      'id':this.id,
      'user_id':this.user_id,
      'task_id':this.task_id,
      'note':this.note,
      'create_date':this.create_date,
      'assignedto_user_id':this.assignedto_user_id,
    };
  }

  List<dynamic> getDataList(NoteModel taskModel) {
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

    lstData.add(this.id);
    lstData.add(this.user_id);
    lstData.add(this.task_id);
    lstData.add(this.note);
    lstData.add(this.create_date);
    lstData.add(this.assignedto_user_id);
    lstData.add(this.synced);
    lstData.add(this.removed);

    return lstData;
  }


}
