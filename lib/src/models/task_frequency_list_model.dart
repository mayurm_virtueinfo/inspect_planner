import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/task_frequency_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/task_frequency_model.dart';

class TaskFrequencyListModel{
  List<TaskFrequencyModel> listTask;


  TaskFrequencyListModel({this.listTask});

  Future<List<TaskFrequencyModel>> getTaskList(List<dynamic> mTaskList){
    List<TaskFrequencyModel> lstTask = [];
    mTaskList.forEach((element) async{
      TaskFrequencyModel taskModel = TaskFrequencyModel.fromMap(map: element);
      lstTask.add(taskModel);
    });
    return Future.value(lstTask);
  }
}



























