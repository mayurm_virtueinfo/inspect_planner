class TaskFrequencyModel{
  int id;//": 119,
  String name;//": "Technische dienst",
  String date_modify;//": "Technische dienst",
  int sort_order;//": "Technische dienst",

  TaskFrequencyModel({
    this.id,
    this.name,
    this.date_modify,
    this.sort_order
  });

  TaskFrequencyModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id']??null,
    this.name = map['name']??null,
    this.date_modify = map['date_modify']??null,
    this.sort_order = map['sort_order']??null
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'name':this.name,
      'date_modify':this.date_modify,
      'sort_order':this.sort_order
    };
  }

  List<dynamic> getDataList(TaskFrequencyModel taskModel){
    List<dynamic> lstData = [];

    lstData.add(taskModel.id);
    lstData.add(taskModel.name);
    lstData.add(taskModel.date_modify);
    lstData.add(taskModel.sort_order);
    return lstData;

  }
}