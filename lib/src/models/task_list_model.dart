import 'package:flutter/cupertino.dart';
import 'package:inspect_planner/src/models/task_model.dart';

class TaskListModel{
  List<TaskModel> listTask;


  TaskListModel({this.listTask});

  /*TaskListModel.froList({List<dynamic> mTaskList}):
    this.listTask = getTaskList(mTaskList)
  ;*/

  Future<List<TaskModel>> getTaskList(List<dynamic> mTaskList){
    List<TaskModel> lstTask = [];
    mTaskList.forEach((element) async{

      TaskModel taskModel = TaskModel.fromMap(map: element);
      lstTask.add(taskModel);
    });
    return Future.value(lstTask);
  }
}



























