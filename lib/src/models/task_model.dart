import 'package:flutter/cupertino.dart';
import 'package:inspect_planner/src/models/attachment_model.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/utils/app_log.dart';

class TaskModel{
  int id;
  int customer_id;
  int location_id;
  int group_id;
  int user_id;
  int clone_task_id; //null
  String title; //"Test taak Edward (ma, di, wo, do, vrij, zo)"
  String description; //"Test taak Edward (ma, di, wo, do, vrij, zo)"
  String create_date; //"2021-06-08 11:17:14"
  int removed; //0
  int stamitem; //0
  int customer_stamitem; //1
  String start_date; //start_date
  int tempate_id; //null
  int type_id; //5 /// for task type
  int frequency_id; //2
  int last_edit_user_id; //null
  int status_id; //1
  int createdby_user_id; //469
  String finish_date; //null
  int deadline_notification_count; //0
  int frequency_daily_part_of_day; //0
  int part_of_day_this_task; //null
  int frequency_step_number; //95
  int days_active; //0
  int active_daypart; //0
  int currunt_active_daypart; //0
  String tasktype_name; //"Contract"
  String frequency_name; //"Elke dag"
  String location_name; //"Fitness ruimte
  List<CheckListItemModel> checklistitem;
  List<NoteModel> task_note;
  List<AttachmentModel> task_attachment;

  String function_name;
  String createdby_user_name;
  int force_completed=0;

  int synced=0;

  TaskModel({
    this.id,
    this.customer_id,
    this.location_id,
    this.group_id,
    this.user_id,
    this.clone_task_id,
    this.title,
    this.description,
    this.create_date,
    this.removed,
    this.stamitem,
    this.customer_stamitem,
    this.start_date,
    this.tempate_id,
    this.type_id,
    this.frequency_id,
    this.last_edit_user_id,
    this.status_id,
    this.createdby_user_id,
    this.finish_date,
    this.deadline_notification_count,
    this.frequency_daily_part_of_day,
    this.part_of_day_this_task,
    this.frequency_step_number,
    this.days_active,
    this.active_daypart,
    this.currunt_active_daypart,
    this.tasktype_name,
    this.frequency_name,
    this.location_name,
    this.checklistitem,
    this.task_note,
    this.task_attachment,
    this.function_name,
    this.createdby_user_name,
    this.force_completed
  });

  TaskModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id']??null,
    this.customer_id = map['customer_id']??null,
    this.location_id = map['location_id']??null,
    this.group_id = map['group_id']??null,
    this.user_id = map['user_id']??null,
    this.clone_task_id = map['clone_task_id']??null,
    this.title = map['title']??null,
    this.description = map['description']??null,
    this.create_date = map['create_date']??null,
    this.removed = map['removed']??null,
    this.stamitem = map['stamitem']??null,
    this.customer_stamitem = map['customer_stamitem']??null,
    this.start_date = map['start_date']??null,
    this.tempate_id = map['tempate_id']??null,
    this.type_id = map['type_id']??null,
    this.frequency_id = map['frequency_id']??null,
    this.last_edit_user_id = map['last_edit_user_id']??null,
    this.status_id = map['status_id']??null,
    this.createdby_user_id = map['createdby_user_id']??null,
    this.finish_date = map['finish_date']??null,
    this.deadline_notification_count = map['deadline_notification_count']??null,
    this.frequency_daily_part_of_day = map['frequency_daily_part_of_day']??null,
    this.part_of_day_this_task = map['part_of_day_this_task']??null,
    this.frequency_step_number = map['frequency_step_number']??null,
    this.days_active = map['days_active']??null,
    this.active_daypart = map['active_daypart']??null,
    this.currunt_active_daypart = map['currunt_active_daypart']??null,
    this.tasktype_name = map['tasktype_name']??null,
    this.frequency_name = map['frequency_name']??null,
    this.location_name = map['location_name']??null,
    this.checklistitem = getCheckListItemList(map['checklistitem'])??null,
    this.task_note = getNoteList(map['task_note'])??null,
    this.task_attachment = getAttechmentList(map['task_attachment'])??null,
    this.function_name = map['function_name']??null,
    this.createdby_user_name = map['createdby_user_name']??null,
    this.force_completed = map['force_completed']??0
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'customer_id':this.customer_id,
      'location_id':this.location_id,
      'group_id':this.group_id,
      'user_id':this.user_id,
      'clone_task_id':this.clone_task_id,
      'title':this.title,
      'description':this.description,
      'create_date':this.create_date,
      'removed':this.removed,
      'stamitem':this.stamitem,
      'customer_stamitem':this.customer_stamitem,
      'start_date':this.start_date,
      'tempate_id':this.tempate_id,
      'type_id':this.type_id,
      'frequency_id':this.frequency_id,
      'last_edit_user_id':this.last_edit_user_id,
      'status_id':this.status_id,
      'createdby_user_id':this.createdby_user_id,
      'finish_date':this.finish_date,
      'deadline_notification_count':this.deadline_notification_count,
      'frequency_daily_part_of_day':this.frequency_daily_part_of_day,
      'part_of_day_this_task':this.part_of_day_this_task,
      'frequency_step_number':this.frequency_step_number,
      'days_active':this.days_active,
      'active_daypart':this.active_daypart,
      'currunt_active_daypart':this.currunt_active_daypart,
      'tasktype_name':this.tasktype_name,
      'frequency_name':this.frequency_name,
      'location_name':this.location_name,
      'function_name':this.function_name,
      'createdby_user_name':this.createdby_user_name,
      'force_completed':this.force_completed
    };
  }
  static List<CheckListItemModel> getCheckListItemList(List<dynamic> mTaskList){
    List<CheckListItemModel> lstCheckList = [];
    if(mTaskList!=null) {
      mTaskList.forEach((element) async {
        // AppLog.debugApp('checklist element : $element');
        CheckListItemModel taskModel = CheckListItemModel.fromMap(map: element);
        taskModel.synced = element['synced'];
        lstCheckList.add(taskModel);
      });
    }
    return lstCheckList;
  }
  static List<NoteModel> getNoteList(List<dynamic> mTaskList){
    List<NoteModel> lstCheckList = [];
    if(mTaskList!=null) {
      mTaskList.forEach((element) async {
        NoteModel taskModel = NoteModel.fromMap(map: element);
        lstCheckList.add(taskModel);
      });
    }
    return lstCheckList;
  }
  static List<AttachmentModel> getAttechmentList(List<dynamic> mTaskList){
    List<AttachmentModel> lstCheckList = [];
    if(mTaskList!=null) {
      mTaskList.forEach((element) async {
        AttachmentModel taskModel = AttachmentModel.fromMap(map: element);
        lstCheckList.add(taskModel);
      });
    }
    return lstCheckList;
  }
  List<dynamic> getDataList(TaskModel taskModel){
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];

      lstData.add(taskModel.id);
      lstData.add(taskModel.customer_id);
      lstData.add(taskModel.location_id);
      lstData.add(taskModel.group_id);
      lstData.add(taskModel.user_id);
      lstData.add(taskModel.clone_task_id);
      lstData.add(taskModel.title);
      lstData.add(taskModel.description);
      lstData.add(taskModel.create_date);
      lstData.add(taskModel.removed);
      lstData.add(taskModel.stamitem);
      lstData.add(taskModel.customer_stamitem);
      lstData.add(taskModel.start_date);
      lstData.add(taskModel.tempate_id);
      lstData.add(taskModel.type_id);
      lstData.add(taskModel.frequency_id);
      lstData.add(taskModel.last_edit_user_id);
      lstData.add(taskModel.status_id);
      lstData.add(taskModel.createdby_user_id);
      lstData.add(taskModel.finish_date);
      lstData.add(taskModel.deadline_notification_count);
      lstData.add(taskModel.frequency_daily_part_of_day);
      lstData.add(taskModel.part_of_day_this_task);
      lstData.add(taskModel.frequency_step_number);
      lstData.add(taskModel.days_active);
      lstData.add(taskModel.active_daypart);
      lstData.add(taskModel.currunt_active_daypart);
      lstData.add(taskModel.tasktype_name);
      lstData.add(taskModel.frequency_name);
      lstData.add(taskModel.location_name);
      lstData.add(taskModel.function_name);
      lstData.add(taskModel.createdby_user_name);
      lstData.add(taskModel.synced);
      lstData.add(taskModel.force_completed);

      return lstData;

  }

}



























