import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';

class TaskTypeListModel{
  List<TaskTypeModel> listTask;


  TaskTypeListModel({this.listTask});

  /*TaskTypeListModel.froList({List<dynamic> mTaskList}):
    this.listTask = getTaskList(mTaskList)
  ;*/

  Future<List<TaskTypeModel>> getTaskList(List<dynamic> mTaskList){
    List<TaskTypeModel> lstTask = [];
    mTaskList.forEach((element) async{
      TaskTypeModel taskModel = TaskTypeModel.fromMap(map: element);
      lstTask.add(taskModel);
    });
    return Future.value(lstTask);
  }
}



























