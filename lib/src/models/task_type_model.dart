class TaskTypeModel{
  int id;//": 119,
  String name;//": "Technische dienst",

  TaskTypeModel({
    this.id,
    this.name,
  });

  TaskTypeModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id']??null,
    this.name = map['name']??null
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'name':this.name
    };
  }

  List<dynamic> getDataList(TaskTypeModel taskModel){
    List<dynamic> lstData = [];

    lstData.add(taskModel.id);
    lstData.add(taskModel.name);
    return lstData;

  }
}