class UserModel{
  int id;//": 2,
  String name;//": "aaaaa",
  String username;//": "aaaaa",
  String role;//": "gsgs@a.com",
  String accessToken;
  int customer_id;
  int is_manager;
  String expires_at;


  UserModel({
    this.id,
    this.name,
    this.username,
    this.role,
    this.accessToken,
    this.customer_id,
    this.is_manager,
    this.expires_at
  });

  UserModel.fromMap({Map<String,dynamic> map}):
    this.id = map['id']??null,
    this.name = map['name']??null,
    this.username = map['username']??null,
    this.role = map['role']??null,
    this.accessToken = map['access_token']??null,
    this.customer_id = map['customer_id']??null,
    this.is_manager = map['is_manager']??null,
    this.expires_at = map['expires_at']??null
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'name':this.name,
      'username':this.username,
      'role':this.role,
      'access_token':this.accessToken,
      'customer_id':this.customer_id,
      'is_manager':this.is_manager,
      'expires_at':this.expires_at
    };
  }
}