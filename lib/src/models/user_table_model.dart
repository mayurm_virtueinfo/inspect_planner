class UserTableModel{
  int id;
  int customer_id;
  String username;
  String password;
  String name;
  String phone_number;
  String role;
  int is_active;
  int removed;
  String create_date;
  String apiHash;
  String apiHash_endDate;
  String passwordResetHash;
  int is_manager;
  int skip_update_email;


  UserTableModel({
    this.id,
    this.customer_id,
    this.username,
    this.password,
    this.name,
    this.phone_number,
    this.role,
    this.is_active,
    this.removed,
    this.create_date,
    this.apiHash,
    this.apiHash_endDate,
    this.passwordResetHash,
    this.is_manager,
    this.skip_update_email,
  });

  UserTableModel.fromMap({Map<String,dynamic> map}):
      this.id = map['id']??null,
      this.customer_id = map['customer_id']??null,
      this.username = map['username']??null,
      this.password = map['password']??null,
      this.name = map['name']??null,
      this.phone_number = map['phone_number']??'',
      this.role = map['role']??null,
      this.is_active = map['is_active']??null,
      this.removed = map['removed']??null,
      this.create_date = map['create_date']??null,
      this.apiHash = map['apiHash']??null,
      this.apiHash_endDate = map['apiHash_endDate']??null,
      this.passwordResetHash = map['passwordResetHash']??null,
      this.is_manager = map['is_manager']??null,
      this.skip_update_email = map['skip_update_email']??null
  ;


  Map<String,dynamic> toJSON(){
    return {
      'id':this.id,
      'customer_id':this.customer_id,
      'username':this.username,
      'password':this.password,
      'name':this.name,
      'phone_number':this.phone_number,
      'role':this.role,
      'is_active':this.is_active,
      'removed':this.removed,
      'create_date':this.create_date,
      'apiHash':this.apiHash,
      'apiHash_endDate':this.apiHash_endDate,
      'passwordResetHash':this.passwordResetHash,
      'is_manager':this.is_manager,
      'skip_update_email':this.skip_update_email,
    };
  }

  List<dynamic> getDataList(UserTableModel taskModel){
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/

    List<dynamic> lstData = [];
    lstData.add(this.id);
    lstData.add(this.customer_id);
    lstData.add(this.username);
    lstData.add(this.password);
    lstData.add(this.name);
    lstData.add(this.phone_number);
    lstData.add(this.role);
    lstData.add(this.is_active);
    lstData.add(this.removed);
    lstData.add(this.create_date);
    lstData.add(this.apiHash);
    lstData.add(this.apiHash_endDate);
    lstData.add(this.passwordResetHash);
    lstData.add(this.is_manager);
    lstData.add(this.skip_update_email);
    return lstData;

  }
}