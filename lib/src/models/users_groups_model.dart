class UsersGroupsModel {
  int user_id;
  int usergroup_id;
  int id;
  int is_main_usergroup;


  UsersGroupsModel({
    this.user_id,
    this.usergroup_id,
    this.id,
    this.is_main_usergroup
  });

  UsersGroupsModel.fromMap({Map<String, dynamic> map})
      :
        this.user_id=map['user_id']??null,
        this.usergroup_id=map['usergroup_id']??null,
        this.id=map['id']??null,
        this.is_main_usergroup=map['is_main_usergroup']??null
  ;

  Map<String, dynamic> toJSON() {
    return {
      'user_id':this.user_id,
      'usergroup_id':this.usergroup_id,
      'id':this.id,
      'is_main_usergroup':this.is_main_usergroup,
    };
  }

  List<dynamic> getDataList(UsersGroupsModel taskModel) {
    /*Map<String,dynamic> mapTask = taskModel.toJSON();
    mapTask.map*/
    List<dynamic> lstData = [];
    lstData.add(this.user_id);
    lstData.add(this.usergroup_id);
    lstData.add(this.id);
    lstData.add(this.is_main_usergroup);
    return lstData;
  }


}
