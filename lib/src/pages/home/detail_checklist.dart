import 'dart:convert';

// import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/add_note_dialog_bloc.dart';
import 'package:inspect_planner/src/bloc/alert_bloc.dart';
import 'package:inspect_planner/src/bloc/complete_task_bloc.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_add_note.dart';
import 'package:inspect_planner/src/dialog/dialog_confirm_complete_task.dart';
import 'package:inspect_planner/src/dialog/dialog_force_task_complete.dart';
import 'package:inspect_planner/src/dialog/dialog_validation.dart';
import 'package:inspect_planner/src/models/alert_model.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/dialog_add_note_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/const.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/item_checklist.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class DetailChecklist extends StatefulWidget {

  DetailChecklist();

  @override
  _DetailChecklistState createState() => _DetailChecklistState();
}

class _DetailChecklistState extends State<DetailChecklist> {
  int currentTab = 1;
  @override
  void initState() {
    // TODO: implement initState
    initChecklistItem();
    locator<DetailTabBloc>().detailTabStrem.listen((event) {
      currentTab = event;
      // checkForCompleteTaskButton();
    });
    super.initState();
  }
  List<Map<String,dynamic>> currentChecklist= [];
  bool isAllTaskCompleted=false;
  CompleteTaskBloc blocCompleteTask = CompleteTaskBloc();
  void initChecklistItem()async{
    String sql = "select * from ${AppTables.CHECKLISTITEM} where task_id = ${appTaskModel.id} and removed = 0 order by order_number asc";
    debugPrint('sql detal checklist : ${sql}');
    currentChecklist= await appDatabase.rawQuery(sql);
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(currentChecklist);
    debugPrint('-------------detail-checlist-----------------');
    debugPrint(prettyprint);
    debugPrint('------------------------------------');
    // checkForCompleteTaskButton();
    setState(() {});
  }



  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, right: 20,top: 20),
                padding: EdgeInsets.only(left: 10, right: 10),
                width: MediaQuery.of(context).size.width,
                height: AppBar().preferredSize.height,
                color: MyColors.taskListHeaderBackColor,
                child: Row(
                  children: [
                    //Deadline
                    Expanded(flex: 2, child: Text(AppStrings.id, style: styleHeader.copyWith(fontWeight: FontWeight.bold))),
                    Expanded(flex: 8, child: Text(AppStrings.questing, style: styleHeader)),
                    Expanded(flex: 5, child: Text(AppStrings.answer, style: styleHeader)),
                    Expanded(flex: 2, child: Text(AppStrings.photos, style: styleHeader)),
                    Expanded(flex: 2, child: Text(AppStrings.log, style: styleHeader)),
                    Expanded(flex: 2, child: Text(AppStrings.status, style: styleHeader)),
                  ],
                ),
              ),

              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(left: 20, right: 20,bottom: 20,top: 20),
                  width: MediaQuery.of(context).size.width,
                  child: ListView.separated(
                    shrinkWrap: true,
                    controller: ScrollController(),
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: 15,
                      );
                    },
                    itemCount: currentChecklist.length,
                    itemBuilder: (context, index) {
                      Map<String,dynamic> mapCurrentCheckListItem = currentChecklist[index];
                      CheckListItemModel checkListItemModel = CheckListItemModel.fromMap(map: mapCurrentCheckListItem);
                      checkListItemModel.synced = mapCurrentCheckListItem['synced'];

                      // return GestureDetector(
                      //   onTap: () {
                      //     debugPrint('onPress Checklist');
                      //     /*Navigator.of(context).pushNamed(DetailScreen.routeName,arguments: {
                      //       'taskModel':taskModel,
                      //       'tasklist':listTask,
                      //       'index':index
                      //     });*/
                      //   },
                      //   child: ItemCheckList(
                      //     taskModel: appTaskModel,
                      //     checkListItemModel: checkListItemModel,
                      //     index: index,
                      //   ),
                      // );
                      return ItemCheckList(
                        taskModel: appTaskModel,
                        checkListItemModel: checkListItemModel,
                        index: index,
                        key: UniqueKey(),
                      );
                    },
                  ),
                ),
              ),
              Row(children: [
                Expanded(
                  child: Container(
                  ),
                ),
                appTaskModel.status_id == 3 ? Container():GestureDetector(
                  onTap: () async{
                    //onPressComplete
                    // debugPrint("onPressTaskComplete : $isAllTaskCompleted");
                    bool isAnswerDone = await Utility.isAllAnswerCompleted();
                    // getCheckListItems();

                    debugPrint("isAllAnswerCompleted : $isAnswerDone");

                    if(isAnswerDone){
                      debugPrint("All Done");
                      Utility.completeTask(mAppTaskModel: appTaskModel, context: context,currentChecklist: currentChecklist,currentTab: currentTab);
                    }else {
                      String result = await showDialog(context: context, builder: (context) => DialogConfirmCompleteTask(),);
                      if (result == 'no') {
                        return;
                      }
                      if(result == 'yes'){
                        Utility.completeTask(mAppTaskModel: appTaskModel, context: context,currentChecklist: currentChecklist,currentTab: currentTab,forceComplete: true);
                      }

                    }


                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      margin: EdgeInsets.only(right: 20,bottom: 5),
                      height: 40,
                      width: 160,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          border: Border.all(
                            color: MyColors.colorConvert('#57a5e8'),
                          ),
                          color: Colors.white),
                      child: Center(
                        child: Text(
                          AppStrings.completeTask,
                          style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                ),
              ],),
              // appTaskModel.status_id == 3 ? Container():Row(children: [
              //   Expanded(
              //     child: Container(
              //       decoration: BoxDecoration(
              //           borderRadius: BorderRadius.all(Radius.circular(5)),
              //           border: Border.all(
              //             color: MyColors.colorConvert('#57a5e8'),
              //           ),
              //           color: Colors.white),
              //       child: Row(
              //         children: [
              //           Expanded(
              //             child: Align(
              //               alignment: Alignment.centerRight,
              //               child: Container(
              //                 margin: EdgeInsets.only(right: 20,bottom: 20,top: 20,left: 20),
              //
              //                 child: Center(
              //                   child: Text(
              //                     AppStrings.forceTaskCompletionMsg,
              //                     style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
              //                   ),
              //                 ),
              //               ),
              //             ),
              //           ),
              //           GestureDetector(
              //             onTap: () async{
              //               debugPrint("onPressForceComplete button");
              //
              //                 String result = await showDialog(context: context, builder: (context) => DialogForceTaskComplete(),);
              //                 debugPrint('dialog Result : $result');
              //                 if (result == 'no') {
              //                   return;
              //                 }
              //                 if(result == 'yes'){
              //                   // return;
              //                   Utility.completeTask(mAppTaskModel: appTaskModel, context: context,currentChecklist: currentChecklist,currentTab: currentTab,forceComplete: true);
              //                 }
              //
              //               return;
              //
              //             },
              //             child: Align(
              //               alignment: Alignment.centerRight,
              //               child: Container(
              //                 margin: EdgeInsets.only(right: 20,bottom: 20,top: 20),
              //                 height: 40,
              //                 width: 200,
              //                 decoration: BoxDecoration(
              //                     borderRadius: BorderRadius.all(Radius.circular(5)),
              //                     border: Border.all(
              //                       color: MyColors.colorConvert('#57a5e8'),
              //                     ),
              //                     color: MyColors.colorConvert('#57a5e8')),
              //                 child: Center(
              //                   child: Text(
              //                     AppStrings.forceTaskCompletion,
              //                     style: TextStyle(color: Colors.white, fontSize: 16),
              //                   ),
              //                 ),
              //               ),
              //             ),
              //           ),
              //         ],
              //       ),
              //     ),
              //   ),
              // ],)
              /*StreamBuilder<bool>(
                initialData: isAllTaskCompleted,
                stream: blocCompleteTask.completeTaskStream,
                builder: (context, snapshot) {
                  if(snapshot.hasData){
                    if(snapshot.data){
                      return GestureDetector(
                        onTap: () async{
                          String result = await showDialog(context: context, builder: (context) => DialogConfirmCompleteTask(),);
                          if(result == 'no'){
                            return;
                          }
                          // update task set status_id = 3, finish_date = '$todayDate' where task_id = 'taskID'
                          try {
                            DateTime dtFinishDate = DateTime.now();
                            String strFinishDate = DateFormat('yyyy-MM-dd HH:mm:hh').format(
                                dtFinishDate);
                            String sqlTaskComplete = "update ${AppTables
                                .TASK} set status_id = ?, finish_date = ?, synced = ? where id = ?";
                            List<dynamic> sqlValues = [
                              3,
                              strFinishDate,
                              0,
                              appTaskModel.id
                            ];
                            int updateResult = await appDatabase.rawUpdate(sqlTaskComplete, sqlValues);
                            if(updateResult == 0){
                              debugPrint("Error completing answer");
                              MyToast.showToast(AppStrings.errCompletingAnswer,context);
                              return;
                            }
                            var connectivityResult = await (Connectivity().checkConnectivity());
                            if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1 ) {
                              int updateCompleteTaskId = await AppSync.updateCompleteTask(context:context,id: appTaskModel.id,status_id:3,finish_date: strFinishDate );
                              if(updateCompleteTaskId == syncErrorValue){
                                MyToast.showToast(AppStrings.errSyncingCompleteTask, context);
                                return;
                              }
                            }
                            if (updateResult != 0) {
                              String sqlQ = "select * from ${AppTables.TASK} where id = ?";
                              List<dynamic> sqlV = [appTaskModel.id];
                              List<Map<String, Object>> listNewData= await appDatabase.rawQuery(sqlQ, sqlV);
                              if(listNewData.length>0){
                                Map<String,dynamic> mapNewData = listNewData[0];
                                TaskModel taskM = TaskModel.fromMap(map: mapNewData);
                                debugPrint("-------testing--appindex : $appTaskIndex");
                                int newAppIndex = appTaskIndex - 1;
                                locator<TaskIndexBloc>().taskIndexEventSink.add(newAppIndex);
                                locator<TaskModelBloc>().taskModelEventSink.add(taskM);
                                locator<DetailTabBloc>().detailTabEventSink.add(1);
                                locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
                              }
                              debugPrint("update task result : $updateResult");

                            } else {
                              MyToast.showToast(AppStrings.noTaskUpdated, context);
                            }
                          }catch(e){
                            debugPrint("Error completing task : ${e.toString()}");
                            MyToast.showToast(AppStrings.errCompletingTask, context);
                          }
                        },
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            margin: EdgeInsets.only(right: 20,bottom: 20),
                            height: 40,
                            width: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                border: Border.all(
                                  color: MyColors.colorConvert('#57a5e8'),
                                ),
                                color: Colors.white),
                            child: Center(
                              child: Text(
                                AppStrings.completeTask,
                                style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                      );

                    }else{
                      return Container();
                    }
                  }
                  return Container();
                }
              )*/
            ],
          ),
        ),
        StreamBuilder<AlertModel>(
                initialData: null,
                stream: locator<AlertBloc>().alertStream,
                builder: (context, snapshot) {
                  if(snapshot.hasData){
                    if(snapshot.data != null && snapshot.data.show){
                      return Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height*0.8,
                          color: Colors.grey.withOpacity(0.5),
                          child: DialogValidation(message: snapshot.data.message,)
                      );

                    }else{
                      return Container();
                    }
                  }
                  return Container();
                }
              ),
        StreamBuilder<DialogAddNoteModel>(
            initialData: null,
            stream: locator<AddNoteDialogBloc>().addNoteDialogStream,
            builder: (context, snapshot) {
              if(snapshot.hasData){
                if(snapshot.data != null && snapshot.data.show){
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height*0.8,
                      color: Colors.grey.withOpacity(0.5),
                      child: DialogAddNote(
                          taskModel: appTaskModel,
                          isFromDialogValidation:true
                      )
                  );

                }else{
                  return Container();
                }
              }
              return Container();
            }
        )

      ],
    );
  }
  @override
  void didUpdateWidget(DetailChecklist oldWidget) {

    super.didUpdateWidget(oldWidget);
    initChecklistItem();
  }
  TextStyle styleHeader = TextStyle(fontSize: 16, color: MyColors.colorConvert('#e0e2e3'));
}
