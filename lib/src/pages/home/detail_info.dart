import 'dart:convert';
import 'dart:math' as math;

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/complete_task_bloc.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/refresh_home_screen_bloc.dart';
import 'package:inspect_planner/src/bloc/show_checklist_tab.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_confirm_complete_task.dart';
import 'package:inspect_planner/src/dialog/dialog_img_upload_task.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class DetailInfo extends StatefulWidget {
  DetailInfo();

  @override
  _DetailInfoState createState() => _DetailInfoState();
}

class _DetailInfoState extends State<DetailInfo> {
  AttachmentIconBloc blocAttachmentIcon = AttachmentIconBloc();
  int currentTab = 0;
  @override
  void initState() {
    // TODO: implement initState

    initInfoData();
    super.initState();
  }

  String stringStatus = '';
  String stringFinishData = '';
  String stringLastEditedUserId = '';
  Image stringStatusIcon;

  String stringStatusBackground = '';
  Color iconColor = Colors.white;
  double iconSize = 35;
  String frequencyName = '';
  String strStartDate = '';
  void initInfoData() async {
    initChecklistItem();

    /*List<Map<String,dynamic>> lstData = await appDatabase.rawQuery("select * from ${AppTables.TASK} where customer_id = ? and removed != ? and status_id != ? and start_date < ? and frequency_id = ?",[24,1,3,"2021-07-07",2]);
    lstData.forEach((element) {
      debugPrint("Title ${element['title']}");
    });*/
    try {
      strStartDate = appTaskModel.start_date == null ? '' : DateFormat('dd-MM-yyyy').format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(appTaskModel.start_date)) ?? '';
    }catch(e){
      strStartDate = "err : ${appTaskModel.start_date}";
    }
    debugPrint("----debug :");
    getLocationNameString();
    debugPrint("----debug :getLocationNameString");
    getNextInspectionDate();
    debugPrint("----debug :getNextInspectionDate");

    getStatusString();
    debugPrint("----debug :getStatusString");
    getStatusIcon();
    debugPrint("----debug :getStatusIcon");
    frequencyName = Utility.getFrequencyName(taskModel: appTaskModel);
    debugPrint("----debug :getFrequencyName");
    int qCount = await Utility.getAttachmentCount(taskModel: appTaskModel);
    blocAttachmentIcon.attachmentIconEventSink.add(qCount);

    //last edited user id
    if (appTaskModel.last_edit_user_id != null) {
      List<Map<String, dynamic>> lstResult = await appDatabase.rawQuery("select * from ${AppTables.USER} where id = ?", [appTaskModel.last_edit_user_id]);
      if (lstResult.first != null) {
        UserTableModel userTableModel = UserTableModel.fromMap(map: lstResult.first);
        stringLastEditedUserId = userTableModel.name;
      }
    }
    setState(() {});
  }
  List<Map<String,dynamic>> currentChecklist= [];
  bool isAllTaskCompleted=false;
  CompleteTaskBloc blocCompleteTask = CompleteTaskBloc();
  void checkForCompleteTaskButton(){
    blocCompleteTask.completeTaskStream.listen((event) {
      isAllTaskCompleted = event;
    });
    if(appTaskModel.status_id==3){
      blocCompleteTask.completeTaskEventSink.add(false);
      return;
    }
    bool flag=false;
    currentChecklist.forEach((element) {
      CheckListItemModel checkListItemModel = CheckListItemModel.fromMap(map: element);
      checkListItemModel.synced = element['synced'];
      if(checkListItemModel.type <4 && checkListItemModel.result==null){
        // not answered
        flag=true;
        return;
      }
    });
    if(currentChecklist.length==0){
      isAllTaskCompleted = false;
    }else {
      if (flag == true) {
        isAllTaskCompleted = false;
      } else {
        isAllTaskCompleted = true;
      }
    }
    blocCompleteTask.completeTaskEventSink.add(isAllTaskCompleted);
    // setState(() {});
  }
  void initChecklistItem()async{
    String sql = "select * from ${AppTables.CHECKLISTITEM} where task_id = ${appTaskModel.id} and removed = 0 order by order_number asc";
    debugPrint('sql detal checklist : ${sql}');
    currentChecklist= await appDatabase.rawQuery(sql);
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(currentChecklist);
    debugPrint('------------detail-info-checlist----------------');
    debugPrint(prettyprint);
    debugPrint('------------------------------------');
    checkForCompleteTaskButton();
    setState(() {});
  }

  String stringLocationName = '';

  void getLocationNameString() async {
    debugPrint("location id : ${appTaskModel.location_id}");
    List<Map<String, dynamic>> lstResult = await appDatabase.rawQuery("select * from ${AppTables.LOCATION} where id = ? ", [appTaskModel.location_id]);
    if (lstResult.first != null) {
      LocationModel locationModel = LocationModel.fromMap(map: lstResult.first);
      String newN = locationModel.name;
      stringLocationName = newN;
      debugPrint("stringLocationName 1 : ${newN}");
      if (locationModel.parent_id != null) {
        List<Map<String, dynamic>> lstResultNew = await appDatabase.rawQuery("select * from ${AppTables.LOCATION} where id = ? ", [locationModel.parent_id]);
        if (lstResultNew.first != null) {
          LocationModel locationModelNew = LocationModel.fromMap(map: lstResultNew.first);
          String newT = '${locationModelNew.name}';
          stringLocationName = '$newN ($newT)';
          debugPrint("stringLocationName 2 : ${newT}");
          debugPrint("stringLocationName 3 : ${stringLocationName}");
          setState(() {});
        }
      } else {
        stringLocationName = locationModel.name;
        setState(() {});
      }
    } else {
      stringLocationName = appTaskModel.location_name;
      setState(() {});
    }
  }

  void getStatusString() async {
    if (appTaskModel.status_id == 3) {
      stringStatus = AppStrings.rounded;
      DateTime dtFinishDate = DateFormat('yyyy-MM-dd HH:mm:ss').parse(appTaskModel.finish_date);
      stringFinishData = "${DateFormat('dd-MM-yyyy').format(dtFinishDate)} at ${DateFormat('HH:mm').format(dtFinishDate)}";
      setState(() {});
    } else {
      String start_date = appTaskModel.start_date;
      DateTime dtStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').parse(start_date);
      DateTime dtToday = DateTime.now();
      debugPrint("start_date : ${dtStartDate.toString()}");
      debugPrint("today : ${dtToday.toString()}");
      if (dtStartDate.isAfter(dtToday) || Utility.isSameDate(dtStartDate, dtToday)) {
        debugPrint("compate : true");
        List<Map<String, dynamic>> listStatus = await appDatabase.rawQuery("select count(*) from ${AppTables.CHECKLISTITEM} where task_id = ${appTaskModel.id} and result is not null");
        int qCount = Sqflite.firstIntValue(listStatus);
        // debugPrint("called item task listStatus : ${qCount}");
        if (qCount == 1) {
          stringStatus = AppStrings.partlyImplemented;
        } else {
          stringStatus = AppStrings.planned;
        }
        setState(() {});
        // Returns true if [this] occurs after [other].
      } else {
        debugPrint("compate : false");
        stringStatus = AppStrings.tooLate;
        setState(() {});
      }
    }
    /*
    @if($datas->status_id == 3)
    Afgerond
    @else
      <?php
      $start_date = $datas->start_date;
      $today = date("Y-m-d H:i:s");
      ?>
      @if($start_date >= $today)
        <?php
        $icon_value = getFilledChecklistItemsCount($datas->id);
        ?>
        @if($icon_value == 1)
          Deels uitgevoerd
        @else
          Gepland
        @endif
      @else
        Te laat
      @endif
    @endif
    */
  }

  String nextInspectionDate = '';

  void getNextInspectionDate() {
    debugPrint("-----Next-Inspection-Starts-----");
    int taskFrequencyId = appTaskModel.frequency_id;
    String strStartDate = appTaskModel.start_date;
    debugPrint("test---start_date : $strStartDate");
    debugPrint("test---frequency_id : $taskFrequencyId");
    debugPrint("test---frequency_step_number : ${appTaskModel.frequency_step_number}");
    DateTime dtStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').parse(strStartDate);
    if (taskFrequencyId == 1) {
      nextInspectionDate = '';
    }
    else if (taskFrequencyId == 2) {
      int part_of_day_this_task = appTaskModel.frequency_daily_part_of_day;
      int active_daypart = appTaskModel.active_daypart;
      String day_part = '';
      String new_expire_date = '';
      if (part_of_day_this_task == 1) {
        day_part = AppStrings.morning;
      }

      if (part_of_day_this_task == 2) {
        day_part = AppStrings.afternoon;
      }

      if (part_of_day_this_task == 4) {
        day_part = AppStrings.evening;
      }

      if (part_of_day_this_task == 3) {
        if (active_daypart == 3) {
          day_part = AppStrings.morning; //"Ochtend";
        } else {
          new_expire_date = appTaskModel.start_date;
          day_part = AppStrings.afternoon; //"Middag";
        }
      }

      if (part_of_day_this_task == 5) {
        if (active_daypart == 5) {
          day_part = AppStrings.morning;
        } else {
          new_expire_date = appTaskModel.start_date;
          day_part = AppStrings.evening;
        }
      }

      if (part_of_day_this_task == 6) {
        if (active_daypart == 6) {
          day_part = AppStrings.afternoon;
        } else {
          new_expire_date = appTaskModel.start_date;
          day_part = AppStrings.evening;
        }
      }

      if (part_of_day_this_task == 7) {
        if (active_daypart == 1) {
          new_expire_date = appTaskModel.start_date;
          day_part = AppStrings.afternoon;
        }
        if (active_daypart == 3) {
          new_expire_date = appTaskModel.start_date;
          day_part = AppStrings.evening;
        }
        if (active_daypart == 7) {
          day_part = AppStrings.morning;
        }
      }
      debugPrint(" day part: $day_part");
      if (new_expire_date.length != 0)
      {
        debugPrint("new expire date if: $new_expire_date");
        nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
      }
      else {
        debugPrint("new expire date else: $new_expire_date");
        if (appTaskModel.frequency_step_number == 127 || appTaskModel.frequency_step_number == 0) {
          // DateTime dtToday = DateTime.now();
          dtStartDate = dtStartDate.add(Duration(days: 1));
          nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
        } else {
          int mFrequencyStemNumber = appTaskModel.frequency_step_number;
          Map<String, dynamic> mapData = Utility.getFrequencyStepNumber(frequency_step_number: mFrequencyStemNumber);
          int nFrequencyStepNumber = mapData['frequency'];
          List<int> dayArray = mapData['array'];
          debugPrint('dayArray A-Z : ${dayArray} ${dayArray.length}');
          dayArray = dayArray.reversed.toList();
          debugPrint('dayArray Z-A : ${dayArray} ${dayArray.length}');
          dayArray.removeLast();
          debugPrint('dayArray Removed Last : ${dayArray} ${dayArray.length}');
          int mWeekday = dtStartDate.weekday;
          debugPrint('day of week : ${mWeekday}');
          debugPrint('task_id : ${appTaskModel.id}');
          String day = '';

          if (!dayArray.asMap().containsValue(mWeekday)) {
            debugPrint("--test--if");
            debugPrint("---debug--1");
            debugPrint("---debug--1-mWeekday : ${mWeekday}");
            debugPrint("---debug--1-dayArray : ${dayArray}");
            int day_of_week = int.parse('${Utility.GetTaskNextDeadline(mWeekday, dayArray)}');
            debugPrint("---debug--2");
            if (day_of_week == 1) {
              day = AppStrings.monday;
              debugPrint("--test--if-1");
            }
            if (day_of_week == 2) {
              debugPrint("--test--if-2");
              day = AppStrings.tuesday;
            }
            if (day_of_week == 3) {
              debugPrint("--test--if-3");
              day = AppStrings.wednesday;
            }
            if (day_of_week == 4) {
              debugPrint("--test--if-4");
              day = AppStrings.thursday;
            }
            if (day_of_week == 5) {
              debugPrint("--test--if-5");
              day = AppStrings.friday;
            }
            if (day_of_week == 6) {
              debugPrint("--test--if-6");
              day = AppStrings.saturday;
            }
            if (day_of_week == 7) {
              debugPrint("--test--if-7");
              day = AppStrings.sunday;
            }
            int tmpD = 0;
            debugPrint("--test--if-7.1 : $day_of_week");
            if (dtStartDate.weekday > day_of_week) {
              debugPrint("--test--if-8");
              tmpD = (7 - dtStartDate.weekday) + day_of_week;
            } else {
              debugPrint("--test--if-9");
              tmpD = (day_of_week - dtStartDate.weekday);
            }

            DateTime nextWeekdayDate = dtStartDate.add(Duration(days: tmpD));
            debugPrint('nextWeekdayDate : ${nextWeekdayDate}');
            nextInspectionDate = DateFormat('dd-MM-yyyy').format(nextWeekdayDate);
            debugPrint("--test--if-10 $nextInspectionDate");
          }
          else {
            debugPrint("--test--else");
            dayArray.asMap().forEach((key, value) {
              debugPrint('key : ${key}');
              debugPrint('value : ${value}');
              int nextKey = key + 1;
              if (mWeekday == value) {
                if (dayArray.asMap().containsKey(nextKey)) {
                  var dayOfWeek = 0;
                  if (dayArray[nextKey] == 1) {
                    day = AppStrings.monday;
                    dayOfWeek = 1;
                  }
                  if (dayArray[nextKey] == 2) {
                    day = AppStrings.tuesday;
                    dayOfWeek = 2;
                  }
                  if (dayArray[nextKey] == 3) {
                    day = AppStrings.wednesday;
                    dayOfWeek = 3;
                  }
                  if (dayArray[nextKey] == 4) {
                    day = AppStrings.thursday;
                    dayOfWeek = 4;
                  }
                  if (dayArray[nextKey] == 5) {
                    day = AppStrings.friday;
                    dayOfWeek = 5;
                  }
                  if (dayArray[nextKey] == 6) {
                    day = AppStrings.saturday;
                    dayOfWeek = 6;
                  }
                  if (dayArray[nextKey] == 7) {
                    day = AppStrings.sunday;
                    dayOfWeek = 7;
                  }
                  debugPrint("----startDate if :${dtStartDate}");
                  debugPrint("----startDate if day :$day");
                  debugPrint("${dtStartDate.weekday}");
                  int tmpD = 0;

                  if (dtStartDate.weekday > dayOfWeek) {
                    tmpD = (7 - dtStartDate.weekday) + dayOfWeek;
                  } else {
                    tmpD = (dayOfWeek - dtStartDate.weekday);
                  }

                  DateTime nextWeekdayDate = dtStartDate.add(Duration(days: tmpD));
                  debugPrint('nextWeekdayDate : ${nextWeekdayDate}');
                  nextInspectionDate = DateFormat('dd-MM-yyyy').format(nextWeekdayDate);
                } else {
                  var dayOfWeek = 0;
                  if (dayArray[0] == 1) {
                    day = AppStrings.monday;
                    dayOfWeek = 1;
                  }
                  if (dayArray[0] == 2) {
                    day = AppStrings.tuesday;
                    dayOfWeek = 2;
                  }
                  if (dayArray[0] == 3) {
                    day = AppStrings.wednesday;
                    dayOfWeek = 3;
                  }
                  if (dayArray[0] == 4) {
                    day = AppStrings.thursday;
                    dayOfWeek = 4;
                  }
                  if (dayArray[0] == 5) {
                    day = AppStrings.friday;
                    dayOfWeek = 5;
                  }
                  if (dayArray[0] == 6) {
                    day = AppStrings.saturday;
                    dayOfWeek = 6;
                  }
                  if (dayArray[0] == 7) {
                    day = AppStrings.sunday;
                    dayOfWeek = 7;
                  }
                  debugPrint("----startDate else :${dtStartDate}");
                  debugPrint("----startDate else day :$day");
                  debugPrint("${dtStartDate.weekday}");
                  int tmpD = 0;
                  if (dtStartDate.weekday > dayOfWeek) {
                    tmpD = (7 - dtStartDate.weekday) + dayOfWeek;
                  } else {
                    tmpD = (dayOfWeek - dtStartDate.weekday);
                  }

                  DateTime nextWeekdayDate = dtStartDate.add(Duration(days: tmpD));
                  debugPrint('nextWeekdayDate : ${nextWeekdayDate}');
                  nextInspectionDate = DateFormat('dd-MM-yyyy').format(nextWeekdayDate);
                }
              }
            });
          }
        }
      }
      if (day_part.length != 0) {
        nextInspectionDate = '$nextInspectionDate ( $day_part ) ';
      }
    }
    else if (taskFrequencyId == 3) {
      int stepNumber = appTaskModel.frequency_step_number;
      if (stepNumber == 0) {
        dtStartDate = dtStartDate.add(Duration(days: 7));
      } else {
        dtStartDate = dtStartDate.add(Duration(days: stepNumber * 7));
      }
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    }
    else if (taskFrequencyId == 4) {
      int stepNumber = appTaskModel.frequency_step_number;
      if (stepNumber == null) {
        stepNumber = 0;
      }
      if (stepNumber == 0) {
        dtStartDate = dtStartDate.add(Duration(days: 30));
      } else {
        dtStartDate = dtStartDate.add(Duration(days: stepNumber * 30));
      }
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    }
    else if (taskFrequencyId == 5) {
      dtStartDate = dtStartDate.add(Duration(days: 90));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 6) {
      dtStartDate = dtStartDate.add(Duration(days: 365));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 7) {
      dtStartDate = dtStartDate.add(Duration(days: 14));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 8) {
      dtStartDate = dtStartDate.add(Duration(days: 180));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 9) {
      dtStartDate = dtStartDate.add(Duration(days: 365 * 2));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 10) {
      dtStartDate = dtStartDate.add(Duration(days: 365 * 3));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 11) {
      dtStartDate = dtStartDate.add(Duration(days: 365 * 4));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    } else if (taskFrequencyId == 12) {
      dtStartDate = dtStartDate.add(Duration(days: 365 * 5));
      nextInspectionDate = DateFormat('dd-MM-yyyy').format(dtStartDate);
    }
    debugPrint("-----Next-Inspection-Ends-----");

    setState(() {});
  }

  String getNextInspeDayString(TaskModel mTaskModel) {
    int part_of_day_this_task = mTaskModel.frequency_daily_part_of_day;
    int active_daypart = mTaskModel.active_daypart;
    String day_part = '';
    String new_expire_date = '';
    if (part_of_day_this_task == 1) {
      day_part = AppStrings.morning;
    }

    if (part_of_day_this_task == 2) {
      day_part = AppStrings.afternoon;
    }

    if (part_of_day_this_task == 4) {
      day_part = AppStrings.evening;
    }

    if (part_of_day_this_task == 3) {
      if (active_daypart == 3) {
        day_part = AppStrings.morning; //"Ochtend";
      } else {
        new_expire_date = mTaskModel.start_date;
        day_part = AppStrings.afternoon; //"Middag";
      }
    }

    if (part_of_day_this_task == 5) {
      if (active_daypart == 5) {
        day_part = AppStrings.morning;
      } else {
        new_expire_date = mTaskModel.start_date;
        day_part = AppStrings.evening;
      }
    }

    if (part_of_day_this_task == 6) {
      if (active_daypart == 6) {
        day_part = AppStrings.afternoon;
      } else {
        new_expire_date = mTaskModel.start_date;
        day_part = AppStrings.evening;
      }
    }

    if (part_of_day_this_task == 7) {
      if (active_daypart == 1) {
        new_expire_date = mTaskModel.start_date;
        day_part = AppStrings.afternoon;
      }
      if (active_daypart == 3) {
        new_expire_date = mTaskModel.start_date;
        day_part = AppStrings.evening;
      }
      if (active_daypart == 7) {
        day_part = AppStrings.morning;
      }
    }
  }

  void getStatusIcon() async {
    /*
    @if($datas->status_id == 3)
      <a class="task-detail_action_icon_style" style="background-color: #A3E061;">
      <i class="icon-4x white fas fa-check"></i>
      </a>
    @else
      <?php
      $start_date = $datas->start_date;
      $today = date("Y-m-d H:i:s");
      ?>
      @if($start_date >= $today)
        <?php
        $icon_value = getFilledChecklistItemsCount($datas->id);
        ?>
        @if($icon_value == 1)
          <a class="task-detail_action_icon_style" style="background-color: #F18A00 ;min-height: 100px;">
          <i class="icon-4x white flaticon2-refresh-button"></i>
          </a>
        @else
          <a class="task-detail_action_icon_style" style="background-color: #38627E;min-height: 100px;">
          <i class="icon-4x white fas fa-calendar-alt"></i>
          </a>
        @endif
      @else
        <?php
        $icon_value = getFilledChecklistItemsCount($datas->id);
        ?>
        @if($icon_value == 1)
          <a class="task-detail_action_icon_style" style="background-color: #CC3017 ;min-height: 100px;">
          <i class="icon-4x white flaticon2-refresh-button"></i>
          </a>
        @else
          <a class="task-detail_action_icon_style" style="background-color: #CC3017 ;min-height: 100px;">
          <i class="icon-4x white fas fa-calendar-alt"></i>
          </a>

        @endif
      @endif
    @endif
    */

    if (appTaskModel.status_id == 3) {
      stringStatusBackground = '#31A3D4';
      stringStatusIcon = Image.asset('assets/status/check-white.png') /*'fa-check'*/;
    } else {
      String start_date = appTaskModel.start_date;
      DateTime dtStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').parse(start_date);
      DateTime dtToday = DateTime.now();
      debugPrint("start_date : ${dtStartDate.toString()}");
      debugPrint("today : ${dtToday.toString()}");
      if (dtStartDate.isAfter(dtToday) || Utility.isSameDate(dtStartDate, dtToday)) {
        debugPrint("compate : true");
        List<Map<String, dynamic>> listStatus = await appDatabase.rawQuery("select count(*) from ${AppTables.CHECKLISTITEM} where task_id = ${appTaskModel.id} and result is not null");
        int qCount = Sqflite.firstIntValue(listStatus);
        // debugPrint("called item task listStatus : ${qCount}");
        if (qCount != 0) {
          stringStatusBackground = '#F18A00';
          // stringStatusIcon = 'refresh';
          stringStatusIcon = Image.asset('assets/status/reload-white.png');
        } else {
          stringStatusBackground = '#38627E';
          // stringStatusIcon = 'calendar';
          stringStatusIcon = Image.asset('assets/status/calendar-white.png');
        }
        setState(() {});
        // Returns true if [this] occurs after [other].
      } else {
        debugPrint("compate : false");
        List<Map<String, dynamic>> listStatus = await appDatabase.rawQuery("select count(*) from ${AppTables.CHECKLISTITEM} where task_id = ${appTaskModel.id} and result is not null");
        int qCount = Sqflite.firstIntValue(listStatus);
        // debugPrint("called item task listStatus : ${qCount}");
        if (qCount != 0) {
          stringStatusBackground = '#CC3017';
          // stringStatusIcon = 'refresh';
          stringStatusIcon = Image.asset('assets/status/reload-white.png');
        } else {
          stringStatusBackground = '#CC3017';
          // stringStatusIcon = 'calendar';
          stringStatusIcon = Image.asset('assets/status/calendar-white.png');
        }
        setState(() {});
      }
    }
  }

  @override
  void didUpdateWidget(DetailInfo oldWidget) {
    initInfoData();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    double decreseHeight = AppBar().preferredSize.height + (AppBar().preferredSize.height + (AppBar().preferredSize.height / 4)) + (40 * 2);
    return OrientationBuilder(
      builder: (context, orientation) {
        return Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height - decreseHeight,
                      // maxWidth: (MediaQuery.of(context).size.width / 2) - 30,
                    ),
                    margin: EdgeInsets.only(left: 20, top: 20, bottom: 20),
                    // color: Colors.white,
                    child: Column(
                      children: [
                        Container(
                          width: (MediaQuery.of(context).size.width / 2) - 30,
                          // height: 150,
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), border: Border.all(color: MyColors.colorConvert('#e6e6e6')), color: Colors.white),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.all(20),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppStrings.status,
                                        style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 20),
                                      ),
                                      Text(
                                        '$stringStatus',
                                        style: TextStyle(color: MyColors.colorConvert('#8f8f8f'), fontSize: 16),
                                      ),
                                      (appTaskModel.status_id == 3 && stringFinishData != '')
                                          ? Text(
                                        '$stringFinishData',
                                        style: TextStyle(color: MyColors.colorConvert('#8f8f8f'), fontSize: 16),
                                      )
                                          : Container(),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      /*(appTaskModel.status_id == 3)
                                      ? Container()
                                      : GestureDetector(
                                          onTap: () {
                                            locator<DetailTabBloc>()
                                                .detailTabEventSink
                                                .add(1);
                                          },
                                          child: Container(
                                            height: 40,
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                                border: Border.all(
                                                  color: MyColors.colorConvert(
                                                      '#57a5e8'),
                                                ),
                                                color: Colors.white),
                                            child: Center(
                                              child: Text(
                                                'Naar checklist',
                                                style: TextStyle(
                                                    color:
                                                        MyColors.colorConvert(
                                                            '#57a5e8'),
                                                    fontSize: 16),
                                              ),
                                            ),
                                          ),
                                        ),*/
                                      //check vertical and horizontal case whenever change the code
                                      orientation == Orientation.landscape?
                                        Container(
                                        child: Row(
                                          children: [
                                            appTaskModel.status_id == 3 ? Container():Expanded(
                                              flex: 1,
                                              child: GestureDetector(
                                                onTap: () async {
                                                  // Utility.completeTask(mAppTaskModel: appTaskModel, context: context,currentChecklist: currentChecklist,currentTab: currentTab);
                                                  funCompleteTask(context:context);
                                                  return;

                                                },
                                                child: Align(
                                                  alignment: Alignment.centerRight,
                                                  child: Container(
                                                    height: 40,
                                                    // width: double.infinity,
                                                    decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.all(Radius.circular(5)),
                                                        border: Border.all(
                                                          color: MyColors.colorConvert('#57a5e8'),
                                                        ),
                                                        color: Colors.white),
                                                    child: Center(
                                                      child: Text(
                                                        AppStrings.completeTask,
                                                        style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: 10,),
                                            StreamBuilder<bool>(
                                                initialData: showCheckListTab,
                                                stream: locator<ShowHideChecklistTabBloc>().showHideChecklistTabStream,
                                                builder: (context, snapshot) {
                                                  if (snapshot.hasData && snapshot.data && appTaskModel.status_id != 3) {
                                                    return Expanded(
                                                      flex: 1,
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          locator<DetailTabBloc>().detailTabEventSink.add(1);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          // width: double.infinity,
                                                          decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.all(Radius.circular(5)),
                                                              border: Border.all(
                                                                color: MyColors.colorConvert('#57a5e8'),
                                                              ),
                                                              color: Colors.white),
                                                          child: Center(
                                                            child: Text(
                                                              AppStrings.toChecklist,
                                                              style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  } else {
                                                    return Container();
                                                  }
                                                }),
                                          ],
                                        ),
                                      ):
                                        Container(
                                        child: Column(
                                          children: [
                                            appTaskModel.status_id == 3 ? Container():GestureDetector(
                                              onTap: () async {
                                                funCompleteTask(context:context);
                                                return;
                                                Utility.completeTask(mAppTaskModel: appTaskModel, context: context,currentChecklist: currentChecklist,currentTab: currentTab);
                                                return;

                                              },
                                              child: Align(
                                                alignment: Alignment.centerRight,
                                                child: Container(
                                                  height: 40,
                                                  // width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(5)),
                                                      border: Border.all(
                                                        color: MyColors.colorConvert('#57a5e8'),
                                                      ),
                                                      color: Colors.white),
                                                  child: Center(
                                                    child: Text(
                                                      AppStrings.completeTask,
                                                      style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 10,),
                                            StreamBuilder<bool>(
                                                initialData: showCheckListTab,
                                                stream: locator<ShowHideChecklistTabBloc>().showHideChecklistTabStream,
                                                builder: (context, snapshot) {
                                                  if (snapshot.hasData && snapshot.data && appTaskModel.status_id != 3) {
                                                    return GestureDetector(
                                                      onTap: () {
                                                        locator<DetailTabBloc>().detailTabEventSink.add(1);
                                                      },
                                                      child: Container(
                                                        height: 40,
                                                        // width: double.infinity,
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.all(Radius.circular(5)),
                                                            border: Border.all(
                                                              color: MyColors.colorConvert('#57a5e8'),
                                                            ),
                                                            color: Colors.white),
                                                        child: Center(
                                                          child: Text(
                                                            AppStrings.toChecklist,
                                                            style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 16),
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  } else {
                                                    return Container();
                                                  }
                                                }),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 20),
                                width: 110,
                                height: 110,
                                decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), border: Border.all(color: MyColors.colorConvert('#e6e6e6')), color: MyColors.colorConvert(stringStatusBackground)),
                                child: Center(
                                  child: Container(height: 50, width: 50, child: stringStatusIcon ?? Container()),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          width: (MediaQuery.of(context).size.width / 2) - 30,
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), border: Border.all(color: MyColors.colorConvert('#e6e6e6')), color: Colors.white),
                          child: Column(
                            children: [
                              Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      '${appTaskModel.title}${Utility.getTitle(appTaskModel)}',
                                      style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 20),
                                    ),
                                  ),
                                  StreamBuilder<int>(
                                      initialData: 0,
                                      stream: blocAttachmentIcon.attachmentIconStream,
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          return Padding(
                                            padding: const EdgeInsets.only(left: 10),
                                            child: /*Icon(Icons.attach_file_outlined, size: 25, color: MyColors.loginButtonColor,)*/
                                            GestureDetector(
                                              onTap: () async {
                                                Map<String, dynamic> result = await showDialog(
                                                  context: context,
                                                  builder: (_) => DialogImgUploadTask(),
                                                );
                                              },
                                              child: Stack(
                                                alignment: Alignment.topRight,
                                                children: [
                                                  Transform.rotate(
                                                      angle: 45 * math.pi / 180,
                                                      child: Icon(
                                                        Icons.attach_file_outlined,
                                                        size: 40,
                                                        color: MyColors.colorConvert('#57a5e8'),
                                                      )),
                                                  Container(
                                                    height: 25,
                                                    width: 25,
                                                    padding: EdgeInsets.all(2),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100)), border: Border.all(width: 1, color: Colors.white), color: MyColors.colorConvert('#57a5e8')),
                                                    child: Text(
                                                      '${snapshot.data}',
                                                      style: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        }
                                        return Container();
                                      })
                                ],
                              ),
                              InfoWidget(label: AppStrings.verantwoordelijk, value: "${appTaskModel.function_name??''}"),
                              InfoWidget(label: AppStrings.type, value: "${appTaskModel.tasktype_name}"),
                              InfoWidget(label: AppStrings.location, value: "${stringLocationName}"),
                              InfoWidget(label: AppStrings.deadline, value: "${strStartDate}${Utility.getTitle(appTaskModel)}"),
                              InfoWidget(label: AppStrings.frequency, value: "$frequencyName ${Utility.getFrequencyString(appTaskModel)}"),
                              nextInspectionDate == '' ? Container() : InfoWidget(label: AppStrings.nextInspectionDate, value: "$nextInspectionDate"),
                              appTaskModel.createdby_user_id != null ? InfoWidget(label: AppStrings.madeBy, value: "${appTaskModel.createdby_user_name}") : Container(),
                              (appTaskModel.last_edit_user_id != null && stringLastEditedUserId != '') ? InfoWidget(label: AppStrings.lastUpdatedBy, value: "$stringLastEditedUserId") : Container()
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                width: 20,
                height: double.infinity,
              ),
              Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height - decreseHeight,
                      maxWidth: (MediaQuery.of(context).size.width / 2) - 30,
                    ),
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), border: Border.all(color: MyColors.colorConvert('#e6e6e6')), color: Colors.white),
                    margin: EdgeInsets.only(right: 20, top: 20, bottom: 20),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppStrings.description,
                            style: TextStyle(color: MyColors.colorConvert('#57a5e8'), fontSize: 20),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            '${appTaskModel.description}',
                            style: TextStyle(color: MyColors.colorConvert('#8f8f8f'), fontSize: 16),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void funCompleteTask({BuildContext context}) async {
    debugPrint("funCompleteTask : entered");
    if(showCheckListTab) {
      debugPrint("funCompleteTask : if true");
      debugPrint("funCompleteTask : isAllTaskCompleted : $isAllTaskCompleted");
      bool isAnswerDone = await Utility.isAllAnswerCompleted();
      // return;
      if (isAnswerDone) {
        debugPrint("funCompleteTask : if true-true");
        locator<RefreshHomeScreenBloc>().refreshHomeEventSink.add(true);
        Utility.completeTask(mAppTaskModel: appTaskModel, context: context, currentChecklist: currentChecklist, currentTab: currentTab);
      } else {
        debugPrint("funCompleteTask : if true-else");
        String result = await showDialog(context: context, builder: (context) => DialogConfirmCompleteTask(),);
        if (result == 'no') {
          debugPrint("funCompleteTask : if true-else-no");
          return;
        }
        if (result == 'yes') {
          debugPrint("funCompleteTask : if true-else-yes");
          locator<RefreshHomeScreenBloc>().refreshHomeEventSink.add(true);
          Utility.completeTask(mAppTaskModel: appTaskModel, context: context, currentChecklist: currentChecklist, currentTab: currentTab,forceComplete: true);
        }
      }
    }else{
      debugPrint("funCompleteTask : else");
      debugPrint("funCompleteTask : direct complete ");
      // return;
      locator<RefreshHomeScreenBloc>().refreshHomeEventSink.add(true);
      Utility.completeTask(mAppTaskModel: appTaskModel, context: context, currentChecklist: currentChecklist, currentTab: currentTab);
    }
  }
  Widget InfoWidget({String label, String value}) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Text(
                '$label',
                style: TextStyle(color: MyColors.colorConvert('#737373'), fontSize: 16),
              ),
            ),
            Expanded(
              flex: 3,
              child: Text(
                '$value',
                style: TextStyle(color: MyColors.colorConvert('#969696'), fontSize: 16),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          color: MyColors.colorConvert('#969696'),
          height: 1,
        )
      ],
    );
  }
}
