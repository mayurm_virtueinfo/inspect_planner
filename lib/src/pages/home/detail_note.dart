import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/widget/item_note.dart';

class DetailNote extends StatefulWidget {

  DetailNote();

  @override
  _DetailNoteState createState() => _DetailNoteState();
}

class _DetailNoteState extends State<DetailNote> {

  @override
  void initState() {
    // TODO: implement initState
    initNoteData();
    super.initState();
  }
  @override
  void didUpdateWidget(covariant DetailNote oldWidget) {
    // TODO: implement didUpdateWidget

    initNoteData();
    super.didUpdateWidget(oldWidget);
  }
  List<Map<String,dynamic>> listNote = [];
  void initNoteData()async{
    debugPrint("initNoteDate-initialized");
    debugPrint("---data-userID :${appUserModel.id} ");
    debugPrint("---data-customerID :${appUserModel.customer_id} ");
    String sqlNote = "select * from ${AppTables.NOTE} where task_id = ? and removed = ? ";
    List<dynamic> sqlValues = [appTaskModel.id,0];
    listNote = await appDatabase.rawQuery(sqlNote,sqlValues);
    listNote = listNote.reversed.toList();
    debugPrint("Notes : ${listNote.toString()}");
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return listNote.length == 0? Container(
      alignment: Alignment.center,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/icon_empty.png'),
            SizedBox(
              height: 20,
            ),
            Text(AppStrings.noMsgNotes,
              style: TextStyle(color: MyColors.colorConvert('#cccccc'), fontSize: 20),
            )
          ],
        ),
      ),
    ): Container(
      margin: EdgeInsets.only(left: 20, right: 20,bottom: 20,top: 20),
      width: MediaQuery.of(context).size.width,
      child: ListView.separated(
        shrinkWrap: true,
        controller: ScrollController(),
        separatorBuilder: (context, index) {
          return SizedBox(
            height: 15,
          );
        },
        itemCount: listNote.length,
        itemBuilder: (context, index) {
          Map<String,dynamic> mapNote = listNote[index];
          NoteModel noteModel = NoteModel.fromMap(map: mapNote);
          noteModel.synced=mapNote['synced'];
          noteModel.removed=mapNote['removed'];

          int noteCount = (listNote.length-index);
          return ItemNote(
            taskModel: appTaskModel,
            noteModel: noteModel,
            index: noteCount,
          );
        },
      ),
    );
  }
}
