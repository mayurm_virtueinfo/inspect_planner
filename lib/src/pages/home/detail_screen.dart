import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/late_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/next_task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/prev_task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/refresh_home_screen_bloc.dart';
import 'package:inspect_planner/src/bloc/show_checklist_tab.dart';
import 'package:inspect_planner/src/bloc/show_search_bloc.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_list_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_add_note.dart';
import 'package:inspect_planner/src/dialog/dialog_add_task.dart';
import 'package:inspect_planner/src/dialog/dialog_filter.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/task_list_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/pages/home/detail_checklist.dart';
import 'package:inspect_planner/src/pages/home/detail_info.dart';
import 'package:inspect_planner/src/pages/home/detail_note.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/item_task.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../../app_router.dart';

class DetailScreen extends StatefulWidget {
  static const String routeName = '/DetailScreen';

  DetailScreen();

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  int currentTab = 1;
  double tabMenuRadius = 5;

  // int mCurrentIndex = 0;
  bool shouldRefreshHome=false;
  @override
  void initState() {
    // TODO: implement initState

    locator<RefreshHomeScreenBloc>().refreshHomeStream.listen((event) {
      shouldRefreshHome = event;
    });

    locator<DetailTabBloc>().detailTabStrem.listen((event) async {
      currentTab = event;
    });
    locator<DetailTabBloc>().detailTabEventSink.add(0);

    Utility.checkForChecklistTab(taskModel: appTaskModel);

    Utility.checkNextPrevTask();
    super.initState();
  }



  @override
  void didUpdateWidget(covariant DetailScreen oldWidget) {
    // TODO: implement didUpdateWidget
    Utility.checkForChecklistTab(taskModel: appTaskModel);
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    double verticalGap = 10;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 7) * 3;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    debugPrint("devicePixelRatio : $devicePixelRatio");
    double centerTabsWidth = (((MediaQuery.of(context).size.width / 5) * 3) / 5) * 4;
    double tabWidth = centerTabsWidth / 4;
    double tabHeight = AppBar().preferredSize.height + (AppBar().preferredSize.height / 4);
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        backgroundColor: MyColors.appBackgroundColor,
        // resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          leadingWidth: MediaQuery.of(context).size.width / 3,
          backgroundColor: MyColors.colorConvert('#5BB0EC'),
          centerTitle: true,
          title: Text(
            '${appTaskModel.title}${Utility.getTitle(appTaskModel)}',
            style: TextStyle(color: MyColors.appBarTitleColor, fontSize: 18),
          ),
          leading: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 10,
                ),
                IconButton(
                  onPressed: () {
                    if(shouldRefreshHome) {
                      Navigator.of(context).pop("refresh");
                    }else{
                      Navigator.of(context).pop("back");
                    }
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: MyColors.appBarTitleColor,
                  ),
                )
              ],
            ),
          ),
          actions: [],
        ),
        body: OrientationBuilder(
          builder: (context, orientation) {
            return Container(
              child: Stack(
                children: [
                  SafeArea(
                    child: Column(
                      children: [
                        // Tab section
                        Container(
                          color: MyColors.tabbarColor,
                          child: Column(
                            children: [
                              Container(
                                padding: (orientation == Orientation.portrait) ? EdgeInsets.only(top: 10, bottom: 10) : EdgeInsets.zero,
                                width: MediaQuery.of(context).size.width,
                                height: (orientation == Orientation.portrait) ? tabHeight * 1.25 : tabHeight,
                                color: MyColors.tabbarColor,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: (orientation == Orientation.portrait)
                                          ? Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                // previous task
                                                previousTaskWidget(),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                // next task
                                                nextTaskWidget()
                                              ],
                                            )
                                          : Row(
                                              children: [
                                                // previous task
                                                previousTaskWidget(),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                // next task
                                                nextTaskWidget()
                                              ],
                                            ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: StreamBuilder<int>(
                                          // initialData: 1,
                                          stream: locator<DetailTabBloc>().detailTabStrem,
                                          builder: (context, snapshot) {
                                            // debugPrint(
                                            //     "cTab : ${snapshot.data}");
                                            if (snapshot.hasData) {
                                              int cTab = snapshot.data;
                                              return Container(
                                                // width: centerTabsWidth,
                                                padding: EdgeInsets.only(top: 10 + (AppBar().preferredSize.height / 8), bottom: 10 + (AppBar().preferredSize.height / 8)),
                                                alignment: Alignment.center,
                                                // color: Colors.green,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      /* decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            border: Border.all(color: Colors.white)
                                          ),*/
                                                      child: Row(
                                                        children: [
                                                          GestureDetector(
                                                            onTap: () {
                                                              locator<DetailTabBloc>().detailTabEventSink.add(0);
                                                            },
                                                            child: Container(
                                                              alignment: Alignment.center,
                                                              width: tabWidth,
                                                              decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.only(topLeft: Radius.circular(tabMenuRadius), bottomLeft: Radius.circular(tabMenuRadius)),
                                                                border: Border.all(color: Colors.white),
                                                                color: cTab == 0 ? Colors.white : MyColors.tabbarColor,
                                                              ),
                                                              child: Text(AppStrings.info, style: TextStyle(color: cTab == 0 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                            ),
                                                          ),
                                                          StreamBuilder<bool>(
                                                              initialData: showCheckListTab,
                                                              stream: locator<ShowHideChecklistTabBloc>().showHideChecklistTabStream,
                                                              builder: (context, snapshot) {
                                                                if (snapshot.hasData) {
                                                                  if (snapshot.data) {
                                                                    return GestureDetector(
                                                                      onTap: () {
                                                                        locator<DetailTabBloc>().detailTabEventSink.add(1);
                                                                      },
                                                                      child: Container(
                                                                        alignment: Alignment.center,
                                                                        width: tabWidth,
                                                                        decoration: BoxDecoration(
                                                                          // borderRadius: BorderRadius.only(topLeft: Radius.circular(tabMenuRadius),bottomLeft: Radius.circular(tabMenuRadius)),
                                                                          border: Border(
                                                                            top: BorderSide(color: Colors.white),
                                                                            bottom: BorderSide(color: Colors.white),
                                                                          ),
                                                                          color: cTab == 1 ? Colors.white : MyColors.tabbarColor,
                                                                        ),
                                                                        child: Row(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          children: [
                                                                            Text(AppStrings.checklist, style: TextStyle(color: cTab == 1 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    );
                                                                  }
                                                                  return Container();
                                                                } else {
                                                                  return Container();
                                                                }
                                                              }),
                                                          GestureDetector(
                                                            onTap: () {
                                                              locator<DetailTabBloc>().detailTabEventSink.add(2);
                                                            },
                                                            child: Container(
                                                              alignment: Alignment.center,
                                                              width: tabWidth,
                                                              decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.only(topRight: Radius.circular(tabMenuRadius), bottomRight: Radius.circular(tabMenuRadius)),
                                                                border: Border.all(color: Colors.white),
                                                                color: cTab == 2 ? Colors.white : MyColors.tabbarColor,
                                                              ),
                                                              child: Text(AppStrings.note, style: TextStyle(color: cTab == 2 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }
                                            return Container();
                                          }),
                                    ),
                                    Container(
                                      child: (orientation == Orientation.portrait)
                                          ? Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: [
                                                // Add note widget
                                                addNoteWidget(context: context),
                                                SizedBox(
                                                  height: 5,
                                                ),

                                                // Add task widget
                                                addTaskWidget(context: context)
                                              ],
                                            )
                                          : Row(
                                              children: [
                                                // Add note widget
                                                addNoteWidget(context: context),
                                                // Add task widget
                                                addTaskWidget(context: context)
                                              ],
                                            ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        // Body section
                        Expanded(
                            flex: 1,
                            child: Container(
                              color: MyColors.colorConvert('#f4f4f4'),
                              child: StreamBuilder<int>(
                                  stream: locator<DetailTabBloc>().detailTabStrem,
                                  builder: (context, snapshot) {
                                    int data = snapshot.data;
                                    if (snapshot.hasData) {
                                      return data == 0
                                          ? DetailInfo()
                                          : data == 1
                                              ? DetailChecklist()
                                              : DetailNote();
                                    }
                                    return Container();
                                  }),
                            ))
                      ],
                    ),
                  ),
                  LoadingWidget(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  Widget previousTaskWidget() {
    return StreamBuilder<int>(
        initialData: 1,
        stream: locator<PrevTaskIndexBloc>().prevTaskIndexStream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data == 0) {
            return emptyPreviousView();
          }

          if (snapshot.hasData && snapshot.data == 1) {
            return GestureDetector(
              onTap: () async {
                int mCurrentIndex = snapshot.data - 1;
                debugPrint("----current index : $mCurrentIndex");
                debugPrint("appTaskModel prev : ${appTaskModel.toJSON()}");

                String queryPrevTask = 'select * from ${AppTables.TASK} where removed = ? and clone_task_id = ? and id < ? order by id desc limit 1';
                List<dynamic> qValuesPrevTask = [0, appTaskModel.clone_task_id, appTaskModel.id];
                debugPrint("queryPrevTask : $queryPrevTask");
                debugPrint("query values qValuesPrevTask : $qValuesPrevTask");
                List<Map> listPrevTask = await appDatabase.rawQuery(queryPrevTask, qValuesPrevTask);
                debugPrint('appTaskModel prev : list : ${listPrevTask}');
                if (listPrevTask.length > 0) {
                  Map<String, dynamic> currentTask = listPrevTask[0];
                  TaskModel currentTaskModel = TaskModel.fromMap(map: currentTask);
                  currentTaskModel.synced = currentTask['synced'];
                  await Utility.checkForChecklistTab(taskModel: currentTaskModel);
                  locator<TaskModelBloc>().taskModelEventSink.add(currentTaskModel);
                  locator<DetailTabBloc>().detailTabEventSink.add(currentTab);
                }
                Utility.checkNextPrevTask();
                setState(() {});
                return;
                if (mCurrentIndex < 0) {
                  // mCurrentIndex = 0;
                  locator<TaskIndexBloc>().taskIndexEventSink.add(mCurrentIndex);
                  return;
                }

                locator<TaskIndexBloc>().taskIndexEventSink.add(mCurrentIndex);
                Map<String, dynamic> currentTask = appTaskList[mCurrentIndex];
                TaskModel currentTaskModel = TaskModel.fromMap(map: currentTask);
                currentTaskModel.synced = currentTask['synced'];
                await Utility.checkForChecklistTab(taskModel: currentTaskModel);
                locator<TaskModelBloc>().taskModelEventSink.add(currentTaskModel);
                locator<DetailTabBloc>().detailTabEventSink.add(currentTab);
                setState(() {});
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Container(alignment: Alignment.center, width: 25, height: 25, padding: EdgeInsets.only(left: 5), decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), border: Border.all(width: 1, color: Colors.white)), child: Icon(Icons.arrow_back_ios, color: MyColors.tabbarTextColor, size: 15)),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    AppStrings.previousTask,
                    style: TextStyle(fontSize: 16, color: MyColors.tabbarTextColor),
                  )
                ],
              ),
            );
          }
          return emptyPreviousView();
        });
  }

  Widget emptyPreviousView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 10,
        ),
        Container(alignment: Alignment.center, width: 25, height: 25, padding: EdgeInsets.only(left: 5), decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), border: Border.all(width: 1, color: Colors.transparent)), child: Icon(Icons.arrow_back_ios, color: Colors.transparent, size: 15)),
        SizedBox(
          width: 5,
        ),
        Text(
          AppStrings.previousTask,
          style: TextStyle(fontSize: 16, color: Colors.transparent),
        )
      ],
    );
  }

  Widget nextTaskWidget() {
    return StreamBuilder<int>(
        initialData: 1,
        stream: locator<NextTaskIndexBloc>().nextTaskIndexStream,
        builder: (context, snapshot) {
          debugPrint("-------testing--nexttask : $appTaskIndex");
          if (snapshot.hasData && snapshot.data == 0) {
            return emptyNextView();
          }
          if (snapshot.hasData && snapshot.data == 1) {
            return GestureDetector(
              onTap: () async {
                int mCurrentIndex = 0;
                /*if(snapshot.data==(appTaskList.length-1)){
                mCurrentIndex = (appTaskList.length-1);
                locator<TaskIndexBloc>().taskIndexEventSink.add(mCurrentIndex);
                return;
              }*/
                debugPrint("current index : $mCurrentIndex");
                debugPrint("appTaskModel next : ${appTaskModel.toJSON()}");

                String queryNextTask = 'select * from ${AppTables.TASK} where customer_id = ? and removed = ? and clone_task_id = ? and id > ? order by id asc limit 1';
                List<dynamic> qValuesNextTask = [appUserModel.customer_id, 0, appTaskModel.clone_task_id, appTaskModel.id];
                debugPrint("queryNextTask : $queryNextTask");
                debugPrint("query values qValuesNextTask : $qValuesNextTask");
                List<Map> listNextTask = await appDatabase.rawQuery(queryNextTask, qValuesNextTask);
                debugPrint('appTaskModel next : list : ${listNextTask}');
                if (listNextTask.length > 0) {
                  Map<String, dynamic> currentTask = listNextTask[0];
                  TaskModel currentTaskModel = TaskModel.fromMap(map: currentTask);
                  currentTaskModel.synced = currentTask['synced'];
                  await Utility.checkForChecklistTab(taskModel: currentTaskModel);
                  locator<TaskModelBloc>().taskModelEventSink.add(currentTaskModel);
                  locator<DetailTabBloc>().detailTabEventSink.add(currentTab);
                }
                Utility.checkNextPrevTask();
                setState(() {});
                return;
                mCurrentIndex = snapshot.data + 1;
                if (mCurrentIndex >= appTaskList.length) {
                  // mCurrentIndex = 0;
                  locator<TaskIndexBloc>().taskIndexEventSink.add(mCurrentIndex);
                  return;
                }
                locator<TaskIndexBloc>().taskIndexEventSink.add(mCurrentIndex);

                Map<String, dynamic> currentTask = appTaskList[mCurrentIndex];
                TaskModel currentTaskModel = TaskModel.fromMap(map: currentTask);
                currentTaskModel.synced = currentTask['synced'];
                await Utility.checkForChecklistTab(taskModel: currentTaskModel);
                locator<TaskModelBloc>().taskModelEventSink.add(currentTaskModel);
                locator<DetailTabBloc>().detailTabEventSink.add(currentTab);
                setState(() {});
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    AppStrings.nextTask,
                    style: TextStyle(fontSize: 16, color: MyColors.tabbarTextColor),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      alignment: Alignment.center,
                      width: 25,
                      height: 25,
                      // padding: EdgeInsets.only(right: 3),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), border: Border.all(width: 1, color: Colors.white)),
                      child: Icon(Icons.arrow_forward_ios, color: MyColors.tabbarTextColor, size: 15)),
                ],
              ),
            );
          }
          return emptyNextView();
        });
  }

  Widget emptyNextView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 10,
        ),
        Text(
          AppStrings.nextTask,
          style: TextStyle(fontSize: 16, color: Colors.transparent),
        ),
        SizedBox(
          width: 5,
        ),
        Container(
            alignment: Alignment.center,
            width: 25,
            height: 25,
            // padding: EdgeInsets.only(right: 3),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), border: Border.all(width: 1, color: Colors.transparent)),
            child: Icon(Icons.arrow_forward_ios, color: Colors.transparent, size: 15)),
      ],
    );
  }

  Widget addNoteWidget({BuildContext context}) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) => DialogAddNote(
            taskModel: appTaskModel,
          ),
        );
      },
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              AppStrings.addNote,
              style: TextStyle(fontSize: 16, color: MyColors.tabbarTextColor),
            ),
            SizedBox(
              width: 5,
            ),
            Icon(
              Icons.add_circle_outline_outlined,
              color: MyColors.tabbarTextColor,
              size: 30,
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget addTaskWidget({BuildContext context}) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) => DialogAddTask(),
        );
      },
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              AppStrings.addTask,
              style: TextStyle(fontSize: 16, color: MyColors.tabbarTextColor),
            ),
            SizedBox(
              width: 5,
            ),
            Icon(
              Icons.add_circle_outline_outlined,
              color: MyColors.tabbarTextColor,
              size: 30,
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}
