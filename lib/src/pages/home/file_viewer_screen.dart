import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_router.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/source.dart';

class FileViewerScreen extends StatefulWidget {
  static const String routeName = "/FileViewerScreen";
  Source source;
  String url;
  FileViewerScreen({this.source,this.url});
  // Map<String,dynamic> mapArg;
  // FileViewerScreen({this.mapArg});
  @override
  _FileViewerScreenState createState() => _FileViewerScreenState();
}

// Component UI
class _FileViewerScreenState extends State<FileViewerScreen> {
  // SharedPreferences prefs;

  /// Declare startTime to InitState
  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Code Create UI Splash Screen
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leadingWidth: MediaQuery.of(context).size.width / 3,
        backgroundColor: MyColors.colorConvert('#5BB0EC'),
        centerTitle: true,
        leading: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 10,
              ),
              IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: MyColors.appBarTitleColor,
                ),
              )
            ],
          ),
        ),
        actions: [],
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child:
        Container()
      ),
    );
  }
}
