import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/complete_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/customer_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/last_sync_date_time_bloc.dart';
import 'package:inspect_planner/src/bloc/late_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/load_more_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/planned_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/refresh_home_screen_bloc.dart';
import 'package:inspect_planner/src/bloc/refreshing/refreshing_bloc.dart';
import 'package:inspect_planner/src/bloc/refreshing/refreshing_bloc_event.dart';
import 'package:inspect_planner/src/bloc/show_checklist_tab.dart';
import 'package:inspect_planner/src/bloc/show_hide_filter_clear_bloc.dart';
import 'package:inspect_planner/src/bloc/show_search_bloc.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_list_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/bloc/today_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/up_down_sort_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_add_task.dart';
import 'package:inspect_planner/src/dialog/dialog_comment.dart';
import 'package:inspect_planner/src/dialog/dialog_confirm_complete_task.dart';
import 'package:inspect_planner/src/dialog/dialog_filter.dart';
import 'package:inspect_planner/src/dialog/dialog_messages.dart';
import 'package:inspect_planner/src/dialog/dialog_newsposts_popup.dart';
import 'package:inspect_planner/src/dialog/dialog_user.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/task_list_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/pages/home/detail_screen.dart';
import 'package:inspect_planner/src/service/navigation_service.dart';
import 'package:inspect_planner/src/utils/app_preferences.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/sort_type.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/item_task.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../../app_router.dart';

class HomeScreen extends StatefulWidget {
  static int currentTab = 1;
  static const String type_id = 'type_id';
  static const String group_id = 'group_id';
  static const String location_id = 'location_id';
  static const String start_date_filter = 'start_date_filter';
  static const String end_date = 'end_date';

  static const String routeName = '/HomeScreen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String noMsgToday = AppStrings.noMsgToday;
  String noMsgPast = AppStrings.noMsgPast;
  String noMsgFuture = AppStrings.noMsgFuture;
  String noMsgCompleted = AppStrings.noMsgCompleted;
  bool showSearch = false;



  TextEditingController searchController = TextEditingController();

  // TaskListBloc blocTaskList = TaskListBloc();
  double tabMenuRadius = 5;
  ShowSearchBloc blocShowSearch = ShowSearchBloc();
  TodayTaskCountBloc blocTodayTaskCount = TodayTaskCountBloc();
  LateTaskCountBloc blocLateTaskCount = LateTaskCountBloc();
  PlannedTaskCountBloc blocPlannedTaskCount = PlannedTaskCountBloc();
  CompleteTaskCountBloc blocCompleteTaskCount = CompleteTaskCountBloc();
  LoadMoreBloc blocLoadMore = LoadMoreBloc();

  String ORDER_BY = 'start_date' /*'id'*/;
  String ORDER_BY_START_DATE = 'start_date' /*'id'*/;
  String ORDER_BY_FINISH_DATE = 'finish_date' /*'id'*/;

  String ASC = 'asc';
  String DESC = 'desc';
  String defaultSortOrder = 'desc';
  Map<String, dynamic> mapFilter = Map();
  int countTooLate = 0;
  int countToday = 0;
  int countPlanned = 0;
  int countComplete = 0;
  ShowHideFilterClearBloc blocShowHideFilterClear = ShowHideFilterClearBloc();
  List<dynamic> mTaskList = [];
  List<int> lstUsrGId = [];
  //----loadmore
  int perPage  = 15;
  int present = 0;
  bool isLastPage=false;
  void _loadMore() async {

    String message = '';

    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
        message = "reach the bottom";



        if((present + perPage)> originalItems.length) {
          if(isLastPage){
            return;
          }
          blocLoadMore.loadMoreEventSink.add(true);
          Timer(Duration(milliseconds: 400), () {
            blocLoadMore.loadMoreEventSink.add(false);
            isLastPage=true;
            // debugPrint("----present 1 : $present");
            // debugPrint("----present 1 : original : ${originalItems.length}");
            // debugPrint("----present 1 : appTaskList : ${List.from(appTaskList).length}");
            // Utility.printJson("----present 1 : appTaskList data : ", List.from(originalItems));
            appTaskList.addAll(originalItems.getRange(present, (originalItems.length)));
            //
            locator<TaskListBloc>().taskListEventSink.add(appTaskList);
            present = appTaskList.length;
          });
          // isLastPage=true;
          // locator<TaskListBloc>().taskListEventSink.add(originalItems);
          // present = appTaskList.length;
        } else {

          blocLoadMore.loadMoreEventSink.add(true);
          Timer(Duration(milliseconds: 400), () {
            blocLoadMore.loadMoreEventSink.add(false);
            List<String> mLog = [];
            originalItems.forEach((element) {
              mLog.add(element['title']);
            });
            // debugPrint("----loadmore-test : 2 : $mLog");

            // debugPrint("----present 2 : $present");
            // debugPrint("----present 2 : original : ${originalItems.length}");
            // debugPrint("----present 2 : appTaskList : ${List.from(appTaskList).length}");

            appTaskList.addAll(originalItems.getRange(present, present + perPage));
            locator<TaskListBloc>().taskListEventSink.add(appTaskList);
            present = present + perPage;
          });

        }
        // debugPrint('Message : $message');




    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
        message = "reach the top";
    }
    // debugPrint("Message : $message ${_controller.offset} : ${_controller.position.maxScrollExtent} : ${_controller.position.outOfRange}");

    // debugPrint("----load-more : maxScrollExtent : ${scrollInfo.metrics.maxScrollExtent}");
    // debugPrint("----load-more : maxScrollExtent : ${scrollInfo.p}");
    // debugPrint("----load-more : extentAfter : ${_controller.position.extentAfter}");
    // if(_controller.position.extentBefore < 300){
    //   debugPrint('loadMore');
    // }
    // if (_hasNextPage == true &&
    //     _isFirstLoadRunning == false &&
    //     _isLoadMoreRunning == false &&
    //     _controller.position.extentAfter < 300) {
    //   setState(() {
    //     _isLoadMoreRunning = true; // Display a progress indicator at the bottom
    //   });
    //   _page += 1; // Increase _page by 1
    //   try {
    //     final res =
    //     await http.get(Uri.parse("$_baseUrl?_page=$_page&_limit=$_limit"));
    //
    //     final List fetchedPosts = json.decode(res.body);
    //     if (fetchedPosts.length > 0) {
    //       setState(() {
    //         _posts.addAll(fetchedPosts);
    //       });
    //     } else {
    //       // This means there is no more data
    //       // and therefore, we will not send another GET request
    //       setState(() {
    //         _hasNextPage = false;
    //       });
    //     }
    //   } catch (err) {
    //     print('Something went wrong!');
    //   }
    //
    //   setState(() {
    //     _isLoadMoreRunning = false;
    //   });
    // }
  }
// The controller for the ListView
  ScrollController _controller;
  List<Map<String,dynamic>> originalItems = [];
  void initializeHomeData() async {

    debugPrint("shouldRefreshHome : --- continuing");
    // TODO: implement initState
    blocUpDownSort.upDownSortStream.listen((event) {
      sortType = event;
    });
    locator<TaskListBloc>().taskListStream.listen((event) {
      mTaskList = event;
    });
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) async{
      // Got a new connectivity status!
      debugPrint("----syncToWeb---listen : ${result.toString()}");
      if (result != ConnectivityResult.none) {
        if(Utility.isSyncingToWeb){
          return;
        }
        AppSync.syncToWeb(mContext: locator<NavigationService>().navigatorKey.currentContext, showSyncDialog: true, showRefreshing: false);
      }
    });

    searchController.addListener(searchListener);

    blocShowSearch.showSearchStream.listen((event) {
      showSearch = event;
    });

    blocTodayTaskCount.todayTaskCountStream.listen((event) {
      countToday = event;
    });
    blocLateTaskCount.lateTaskCountStream.listen((event) {
      countTooLate = event;
    });
    blocPlannedTaskCount.plannedTaskCountStream.listen((event) {
      countPlanned = event;
    });
    blocCompleteTaskCount.completeTaskCountStream.listen((event) {
      countComplete = event;
    });

    locator<HomeTabBloc>().homeTabStream.listen((event) async {
      // if(!shouldRefreshHome){
      //   debugPrint("shouldRefreshHome : ${shouldRefreshHome} -- if");
      //   return;
      // }else{
      //   debugPrint("shouldRefreshHome : ${shouldRefreshHome} -- else");
      // }
      // debugPrint("shouldRefreshHome : continuing in listener");
      // remove search things when tab change
      ORDER_BY = ORDER_BY_START_DATE;
      lstQuery.removeWhere((element) => element[FIELD] == 'title');
      searchController.text = '';
      blocShowSearch.showSearchEventSink.add(false);
      // remove filter when tab change
      mapFilter = Map();
      // Display counter in tab
      getTotalTodayTask();
      getTotalTooLateTask();
      // getTotalPlannedTask();
      // getTotalComplete();

      lstQuery.clear();
      mapFilter = Map();
      blocShowHideFilterClear.showHideFilterClearEventSink.add(mapFilter.length);
      debugPrint('tab event occured : $event');
      HomeScreen.currentTab = event;
      if (event == 0) {
        blocUpDownSort.upDownSortEventSink.add(SortType.UP_START_TIME);

        // Vandaag -> Today
        DateTime dtNow = DateTime.now();

        String strStartDate = DateFormat('yyyy-MM-dd').format(dtNow);
        debugPrint('Today : today date : ${strStartDate}');

        // where
        // `task`.customer_id` = ? and
        // `task`.`removed` != ? and
        // `task`.`status_id` != ? and
        // `task`.`start_date` like ? order by `task`.`id` desc
        // Values : 0 => 24, 1 => 1, 2 => 3, 3 => "%2021-06-24%"

        Map<String, dynamic> qb1 = Map();
        qb1[FIELD] = 'customer_id';
        qb1[OPERATOR] = '=';
        qb1[Q_MARK] = appUserModel.customer_id;
        lstQuery.add(qb1);

        Map<String, dynamic> qb2 = Map();
        qb2[FIELD] = 'removed';
        qb2[OPERATOR] = '!=';
        qb2[Q_MARK] = 1;
        lstQuery.add(qb2);

        Map<String, dynamic> qb3 = Map();
        qb3[FIELD] = 'status_id';
        qb3[OPERATOR] = '!=';
        qb3[Q_MARK] = 3;
        lstQuery.add(qb3);

        // Map<String, dynamic> qb4 = Map();
        // qb4[FIELD] = 'start_date';
        // qb4[OPERATOR] = 'like';
        // qb4[Q_MARK] = '%$strStartDate%';
        // lstQuery.add(qb4);

      }
      else if (event == 1) {
        blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_START_TIME);
        debugPrint("Test---1");
        // Te laat -> Past
        DateTime dtNow = DateTime.now();
        DateTime dtTime = DateTime(dtNow.year, dtNow.month, dtNow.day, 0, 0, 0, 0, 0);
        String strStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(dtTime);
        debugPrint('Past : today date : ${strStartDate}');

        // where
        // `task`.`customer_id` = ? and
        // `task`.`removed` != ? and
        // `task`.`status_id` != ? and
        // `task`.`start_date` < ? order by `task`.`id` desc
        // Values : 0 => 24, 1 => 1, 2 => 3, 3 => "2021-06-23 16:57:50"

        Map<String, dynamic> qb1 = Map();
        qb1[FIELD] = 'customer_id';
        qb1[OPERATOR] = '=';
        qb1[Q_MARK] = appUserModel.customer_id;
        lstQuery.add(qb1);

        Map<String, dynamic> qb2 = Map();
        qb2[FIELD] = 'removed';
        qb2[OPERATOR] = '!=';
        qb2[Q_MARK] = 1;
        lstQuery.add(qb2);

        Map<String, dynamic> qb3 = Map();
        qb3[FIELD] = 'status_id';
        qb3[OPERATOR] = '!=';
        qb3[Q_MARK] = 3;
        lstQuery.add(qb3);

        /*Map<String, dynamic> qb4 = Map();
        qb4[FIELD] = 'start_date';
        qb4[OPERATOR] = '<'*/ /*'<='*/ /*;
        qb4[Q_MARK] = '$strStartDate';
        lstQuery.add(qb4);*/

      }
      else if (event == 2) {
        blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_START_TIME);
        // Gepland -> Plan
        DateTime dtNow = DateTime.now();
        DateTime dtTime = DateTime(dtNow.year, dtNow.month, dtNow.day, 23, 59, 59);
        String strStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(dtTime);
        debugPrint('Future : today date : ${strStartDate}');

        // where
        // `task`.`customer_id` = ? and
        // `task`.`removed` != ? and
        // `task`.`status_id` != ? and
        // `task`.`start_date` > ? order by `task`.`id` desc
        // Values : 0 => 24, 1 => 1, 2 => 3, 3 => "2021-06-24 16:58:58"

        Map<String, dynamic> qb1 = Map();
        qb1[FIELD] = 'customer_id';
        qb1[OPERATOR] = '=';
        qb1[Q_MARK] = appUserModel.customer_id;
        lstQuery.add(qb1);

        Map<String, dynamic> qb2 = Map();
        qb2[FIELD] = 'removed';
        qb2[OPERATOR] = '!=';
        qb2[Q_MARK] = 1;
        lstQuery.add(qb2);

        Map<String, dynamic> qb3 = Map();
        qb3[FIELD] = 'status_id';
        qb3[OPERATOR] = '!=';
        qb3[Q_MARK] = 3;
        lstQuery.add(qb3);

        Map<String, dynamic> qb4 = Map();
        qb4[FIELD] = 'start_date';
        qb4[OPERATOR] = '>';
        qb4[Q_MARK] = '$strStartDate';

        lstQuery.add(qb4);
      }
      else if (event == 3) {
        ORDER_BY = ORDER_BY_FINISH_DATE;
        blocUpDownSort.upDownSortEventSink.add(SortType.UP_START_TIME);
        // Afgerond -> Complete
        // where
        // `task`.`customer_id` = ? and
        // `task`.`removed` != ? and
        // `task`.`status_id` = ? order by `task`.`id` desc
        // Values : 0 => 24, 1 => 1, 2 => 3,

        Map<String, dynamic> qb1 = Map();
        qb1[FIELD] = 'customer_id';
        qb1[OPERATOR] = '=';
        qb1[Q_MARK] = appUserModel.customer_id;
        lstQuery.add(qb1);

        Map<String, dynamic> qb2 = Map();
        qb2[FIELD] = 'removed';
        qb2[OPERATOR] = '!=';
        qb2[Q_MARK] = 1;
        lstQuery.add(qb2);

        Map<String, dynamic> qb3 = Map();
        qb3[FIELD] = 'status_id';
        qb3[OPERATOR] = '=';
        qb3[Q_MARK] = 3;
        lstQuery.add(qb3);
      }

      /////
      String queryUsrG = 'select * from ${AppTables.USERS_GROUPS} where user_id = ? and is_main_usergroup = ?';
      List<dynamic> qValuesUsrG = [appUserModel.id, 1];
      debugPrint("queryUsrG : $queryUsrG");
      debugPrint("query values UsrG : $qValuesUsrG");
      List<Map> listUsrG = await appDatabase.rawQuery(queryUsrG, qValuesUsrG);
      lstUsrGId = [];
      listUsrG.forEach((element) {
        lstUsrGId.add(element['usergroup_id']);
      });
      // debugPrint("query result UsrG : $lstUsrGId");

      /////


      String query = 'select * from ${AppTables.TASK} where ${getWQuery()} order by $ORDER_BY $defaultSortOrder';//limit-20
      List<dynamic> qValues = getWValues();
      debugPrint("query task: $query");
      debugPrint("query values : $qValues");
      List<Map> list = await appDatabase.rawQuery(query, qValues);
      List<Map<String, dynamic>> newTodayList = [];
      List<dynamic> finalList = [];
      if (event == 0) {
        //days_active today
        list.forEach((element) {
          String strEle = element['start_date'];
          if (strEle != null) {
            int strDysActive = element['days_active'];
            DateFormat dateFormat = DateFormat("yyyy-MM-dd");
            DateTime dateTime = dateFormat.parse(strEle);
            DateTime sDT = dateTime.subtract(Duration(days: 1));
            sDT = sDT.add(Duration(hours: 23, minutes: 59, seconds: 59));
            DateTime dtToday = DateTime.now();

            DateTime activeDT = dateTime.add(Duration(days: strDysActive, hours: 23, minutes: 59, seconds: 59));
            // debugPrint("start date-start-date: ${dateTime.toString()}  --- ${strDysActive}");
            // debugPrint("start date-active: ${activeDT.toString()}");
            // debugPrint("start date-sDT: ${sDT.toString()}");
            // debugPrint("start date-current: ${dtToday.toString()}");
            // debugPrint("-------------------------------------------------");
            if (dtToday.isAfter(sDT) && dtToday.isBefore(activeDT)) {
              newTodayList.add(element);
            }
          }
        });
        finalList = newTodayList;
        // locator<TaskListBloc>().taskListEventSink.add(newTodayList);
      }
      else if (event == 1) {
        //days_active tolate
        list.forEach((element) {
          String strEle = element['start_date'];
          if (strEle != null) {
            int strDysActive = element['days_active'];
            DateFormat dateFormat = DateFormat("yyyy-MM-dd");
            DateTime dateTime = dateFormat.parse(strEle);
            DateTime sDT = dateTime.subtract(Duration(days: 1));
            sDT = sDT.add(Duration(hours: 23, minutes: 59, seconds: 59));

            DateTime dtNow = DateTime.now();
            DateTime dtToday = DateTime(dtNow.year, dtNow.month, dtNow.day, 0, 0, 0, 0, 0);

            DateTime activeDT = dateTime.add(Duration(days: strDysActive, hours: 23, minutes: 59, seconds: 59));
            // debugPrint("start date-start-date: ${dateTime.toString()}  --- ${strDysActive}");
            // debugPrint("start date-active: ${activeDT.toString()}");
            // debugPrint("start date-sDT: ${sDT.toString()}");
            // debugPrint("start date-current: ${dtToday.toString()}");
            // debugPrint("-------------------------------------------------");
            if (dtToday.isAfter(sDT) && dtToday.isAfter(activeDT)) {
              newTodayList.add(element);
            }
          }
        });
        finalList = newTodayList;
        // locator<TaskListBloc>().taskListEventSink.add(newTodayList);
      }
      else {
        finalList = list;
        // locator<TaskListBloc>().taskListEventSink.add(list);
      }
      isLastPage=false;
      perPage  = 15;
      present = 0;
      originalItems = List.from(finalList);
      if(originalItems.length <= perPage){
        present = originalItems.length;
      }else {
        present = present + perPage;
      }
      List<String> mLog = [];
      originalItems.forEach((element) {
        mLog.add(element['title']);
      });
      // debugPrint("----loadmore-test : 1 : onLoad : $mLog");
      // debugPrint("originalItems onLoad : ${originalItems}");


      if (sortType == SortType.UP_START_TIME) {
        onClickUpStartTime(finalList,originalItems);
      } else if (sortType == SortType.DOWN_START_TIME) {
        onClickDownStartTime(finalList,originalItems);
      }
    });
    getNewsPost();
  }

  @override
  void initState() {
    _controller = new ScrollController()..addListener(_loadMore);
    initializeHomeData();
    locator<HomeTabBloc>().homeTabEventSink.add(1);
    super.initState();
  }

  List<NewspostsModel> appListNewspostsModel = [];
  int newsPostCount = 0;

  void getNewsPost() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool newsposts = prefs.getBool(AppPreferences.newsposts);
    if (newsposts == null) {
      /*if(appListNewspostsModel==null){
        return;
      }*/
      String sqlMsgVLog = "select * from ${AppTables.MESSAGE_VIEW_LOG}";
      List<Map<String, dynamic>> lstValues = await appDatabase.rawQuery(sqlMsgVLog);
      List<int> vLogIds = [];
      lstValues.forEach((element) {
        vLogIds.add(element['message_id']);
      });
      String pubDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
      String dueDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
      String strVLogIds = vLogIds.join(",");
      //
      String sqlPopupMsg = "select * from ${AppTables.NEWSPOSTS} where publication_date <= '$pubDate' and due_date >= '$dueDate' and status = '1' and id NOT IN ($strVLogIds) ";
      debugPrint("sql PopupMsg : $sqlPopupMsg");
      debugPrint("sqlPopupMsg : $sqlPopupMsg");
      List<Map<String, dynamic>> lstResult = await appDatabase.rawQuery(sqlPopupMsg);
      lstResult.forEach((element) {
        debugPrint("popupResult: title : ${element['title']}");
        NewspostsModel mpMode = NewspostsModel.fromMap(map: element);
        appListNewspostsModel.add(mpMode);
      });

      Future.delayed(Duration(seconds: 1), () {
        checktoDisplayNewsposts();
      });
      await prefs.setBool(AppPreferences.newsposts, true);
    } else {
      debugPrint("newsposts 4 $newsposts");
    }
  }

  void checktoDisplayNewsposts() async {
    if (newsPostCount < appListNewspostsModel.length) {
      NewspostsModel newspostsModel = appListNewspostsModel[newsPostCount];
      String result = await showDialog(
        context: locator<NavigationService>().navigatorKey.currentContext,
        builder: (context) => DialogNewspostsPopup(newsPostCount: newsPostCount, appListNewspostsModel: appListNewspostsModel.length, newspostsModel: newspostsModel),
        // barrierColor: Colors.grey.withOpacity(0.25)
      );
      debugPrint("Result : $result");
      if (result == 'cancel') {
        newsPostCount++;
        checktoDisplayNewsposts();
      }
    }
  }

  void getTotalTooLateTask() async {
    DateTime dtNow = DateTime.now();
    DateTime dtTime = DateTime(dtNow.year, dtNow.month, dtNow.day, 0, 0, 0, 0, 0);
    String dtTodayStr = DateFormat('yyyy-MM-dd HH:mm:ss').format(dtTime);
    debugPrint('getTotalTooLateTask : today date : ${dtTodayStr}');

    // where
    // `task`.`customer_id` = ? and
    // `task`.`removed` != ? and
    // `task`.`status_id` != ? and
    // `task`.`start_date` < ? order by `task`.`id` desc
    // Values : 0 => 24, 1 => 1, 2 => 3, 3 => "2021-06-23 16:57:50"
    String sql = "select * from ${AppTables.TASK} where customer_id = ? and removed != ? and status_id != ?";
    List<dynamic> qValues = [appUserModel.customer_id, 1, 3];

    List<Map> list = await appDatabase.rawQuery(sql, qValues);
    List<Map> newTodayList = [];
    list.forEach((element) {
      String strEle = element['start_date'];
      if (strEle != null) {
        int strDysActive = element['days_active'];
        DateFormat dateFormat = DateFormat("yyyy-MM-dd");
        DateTime dateTime = dateFormat.parse(strEle);
        DateTime sDT = dateTime.subtract(Duration(days: 1));
        sDT = sDT.add(Duration(hours: 23, minutes: 59, seconds: 59));

        DateTime dtNow = DateTime.now();
        DateTime dtToday = DateTime(dtNow.year, dtNow.month, dtNow.day, 0, 0, 0, 0, 0);

        DateTime activeDT = dateTime.add(Duration(days: strDysActive, hours: 23, minutes: 59, seconds: 59));
        // debugPrint("start date-start-date: ${dateTime.toString()}  --- ${strDysActive}");
        // debugPrint("start date-active: ${activeDT.toString()}");
        // debugPrint("start date-sDT: ${sDT.toString()}");
        // debugPrint("start date-current: ${dtToday.toString()}");
        // debugPrint("-------------------------------------------------");
        if (dtToday.isAfter(sDT) && dtToday.isAfter(activeDT)) {
          newTodayList.add(element);
        }
      }
    });
    blocLateTaskCount.lateTaskCountEventSink.add(newTodayList.length);
  }

  void getTotalTodayTask() async {
    DateTime dtNow = DateTime.now();
    String strStartDate = DateFormat('yyyy-MM-dd').format(dtNow);
    debugPrint('Today : today date : ${strStartDate}');

    // where
    // `task`.`customer_id` = ? and
    // `task`.`removed` != ? and
    // `task`.`status_id` != ? and
    // `task`.`start_date` < ? order by `task`.`id` desc
    // Values : 0 => 24, 1 => 1, 2 => 3, 3 => "2021-06-23 16:57:50"
    String sql = "select * from ${AppTables.TASK} where customer_id = ? and removed != ? and status_id != ?";
    List<dynamic> qValues = [appUserModel.customer_id, 1, 3];
    List<Map> list = await appDatabase.rawQuery(sql, qValues);
    List<Map> newTodayList = [];
    list.forEach((element) {
      String strEle = element['start_date'];
      if (strEle != null) {
        int strDysActive = element['days_active'];
        DateFormat dateFormat = DateFormat("yyyy-MM-dd");
        DateTime dateTime = dateFormat.parse(strEle);
        DateTime sDT = dateTime.subtract(Duration(days: 1));
        sDT = sDT.add(Duration(hours: 23, minutes: 59, seconds: 59));
        DateTime dtToday = DateTime.now();

        DateTime activeDT = dateTime.add(Duration(days: strDysActive, hours: 23, minutes: 59, seconds: 59));
        // debugPrint("start date-start-date: ${dateTime.toString()}  --- ${strDysActive}");
        // debugPrint("start date-active: ${activeDT.toString()}");
        // debugPrint("start date-sDT: ${sDT.toString()}");
        // debugPrint("start date-current: ${dtToday.toString()}");
        // debugPrint("-------------------------------------------------");
        if (dtToday.isAfter(sDT) && dtToday.isBefore(activeDT)) {
          newTodayList.add(element);
        }
      }
    });
    debugPrint("getTotalTodayTask : ${newTodayList.length}");
    blocTodayTaskCount.todayTaskCountEventSink.add(newTodayList.length);
    // }
  }

  void getTotalPlannedTask() async {
    DateTime dtNow = DateTime.now();
    DateTime dtTime = DateTime(dtNow.year, dtNow.month, dtNow.day, 23, 59, 59);
    String strStartDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(dtTime);
    debugPrint('Future : today date : ${strStartDate}');

    // where
    // `task`.`customer_id` = ? and
    // `task`.`removed` != ? and
    // `task`.`status_id` != ? and
    // `task`.`start_date` < ? order by `task`.`id` desc
    // Values : 0 => 24, 1 => 1, 2 => 3, 3 => "2021-06-23 16:57:50"
    String sql = "select count(*) from ${AppTables.TASK} where customer_id = ? and removed != ? and status_id != ? and start_date > ?";
    List<dynamic> qValues = [appUserModel.customer_id, 1, 3, '%$strStartDate%'];
    List<Map> list = await appDatabase.rawQuery(sql, qValues);
    int qCount = Sqflite.firstIntValue(list);
    debugPrint("getTotalTodayTask : $qCount");
    blocPlannedTaskCount.plannedTaskCountEventSink.add(qCount);
    // }
  }

  void getTotalComplete() async {
    String sql = "select count(*) from ${AppTables.TASK} where customer_id = ? and removed != ? and status_id = ?";
    List<dynamic> qValues = [appUserModel.customer_id, 1, 3];
    List<Map> list = await appDatabase.rawQuery(sql, qValues);
    int qCount = Sqflite.firstIntValue(list);
    debugPrint("getTotalTodayTask : $qCount");
    blocCompleteTaskCount.completeTaskCountEventSink.add(qCount);
    // }
  }

  void searchListener() {
    print("controller change : ${searchController.text}");
    stopTime();
    startTime();
  }

  Timer mainTimer;

  startTime() async {
    mainTimer = Timer(Duration(seconds: 1), requestTimeOut);
    return mainTimer;
  }

  stopTime() {
    if (mainTimer != null) {
      mainTimer.cancel();
    }
  }

  void requestTimeOut() {
    // if (searchController.text.length > 0) {
    //   debugPrint("requested");
    searchTaskList();
    // }
  }

  List<Map<String, dynamic>> lstQuery = [];
  String FIELD = 'field';
  String OPERATOR = 'operator';
  dynamic Q_MARK = 'q_mark';

  String getWQuery() {
    List<String> lstString = [];
    lstQuery.forEach((element) {
      String field = element[FIELD];
      String operator = element[OPERATOR];
      dynamic q_mark = element[Q_MARK];
      lstString.add('$field $operator ?');
    });
    return lstString.join(' and ');
  }

  List<dynamic> getWValues() {
    List<dynamic> lstQValues = [];
    lstQuery.forEach((element) {
      dynamic q_mark = element[Q_MARK];
      lstQValues.add(q_mark);
    });
    return lstQValues;
  }

  void searchTaskList() async {
    try {
      // blocLateTaskCount.lateTaskCountEventSink.a dd(0);
      lstQuery.removeWhere((element) => element[FIELD] == 'title');
      String mQuery = "";
      if (searchController.text != '') {
        // Map<String, dynamic> mapSearch = Map();
        // mapSearch[FIELD] = 'title';
        // mapSearch[OPERATOR] = 'like';
        // mapSearch[Q_MARK] = '%${searchController.text}%';
        // lstQuery.add(mapSearch);

        mQuery = 'select * from ${AppTables.TASK} where (${getWQuery()}) and (title like \'%${searchController.text}%\' or description like \'%${searchController.text}%\') order by $ORDER_BY desc';
      } else {
        mQuery = 'select * from ${AppTables.TASK} where ${getWQuery()} order by $ORDER_BY desc';
      }

      List<dynamic> qValues = getWValues();
      // qValues.add('%${searchController.text}%');
      debugPrint("query : $mQuery");
      debugPrint("query values : $qValues");
      List<Map> list = await appDatabase.rawQuery(mQuery, qValues);
      locator<TaskListBloc>().taskListEventSink.add(list);
    } catch (e) {
      debugPrint('searchTaskList query error :${e.toString()}');
    }
  }

  CancelableOperation cancellableOperation;
  StreamSubscription subscription;
  RefreshingBloc mRefreshingBloc = new RefreshingBloc();

  void pullToRefreshData(BuildContext context,int mCurrentTab)async{

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      // I am connected to a mobile network.
      // MyToast.showToast(AppStrings.itIsNotPossibleToLogout, context);
      locator<HomeTabBloc>().homeTabEventSink.add(mCurrentTab);
      return null;
    }
    debugPrint("----syncToWeb---RefreshIndicator");
    await AppSync.syncToWeb(mContext: locator<NavigationService>().navigatorKey.currentContext, showSyncDialog: false, showRefreshing: true);
    // locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    mRefreshingBloc.loadingEventSink.add(RefreshingEvent());
    try {
      await Utility.clearDatabase();

      Map<String, dynamic> apiResult = await ApiRequest.postSyncWebToApp();
      if (apiResult == null) {
        MyToast.showToast(AppStrings.errWhileLogin, context);
        // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        return;
      } else {
        bool status = apiResult['status'];
        if (status != null && status) {
          await Utility.processSyncWebToApp(mContext: context, apiResult: apiResult);
          initializeHomeData();
          // locator<HomeTabBloc>().homeTabEventSink.add(1)
          locator<HomeTabBloc>().homeTabEventSink.add(mCurrentTab);
          mRefreshingBloc.loadingEventSink.add(RefreshedEvent());
        } else if (apiResult['message'] != null) {
          setState(() {});
        }
        // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      }
    } catch (e) {
      // locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    }

  }
  @override
  Widget build(BuildContext context) {
    double tabCountHeight = 16;
    double verticalGap = 10;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 7) * 3;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    debugPrint("devicePixelRatio : $devicePixelRatio");
    double centerTabsWidth = (((MediaQuery.of(context).size.width / 5) * 3) / 5) * 4;
    double tabWidth = centerTabsWidth / 4;
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        // backgroundColor: MyColors.appBackgroundColor,
        backgroundColor: MyColors.colorConvert('#5BB0EC'),
        resizeToAvoidBottomInset: true,
        /*appBar: AppBar(

          systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: MyColors.colorConvert('#5BB0EC')),
        ),*/
        /*appBar: AppBar(
          backgroundColor: MyColors.colorConvert('#5BB0EC'),
        ),*/
        body: RefreshIndicator(

          onRefresh: () async {
            pullToRefreshData(context,appCurrentTab);
          },
          child: OrientationBuilder(
            builder: (context, orientation) {
              return Container(
                child: Stack(
                  children: [
                    SafeArea(
                      child: Column(
                        children: [
                          //Header
                          Container(
                            height: orientation == Orientation.portrait ? 90 : 60,
                            color: MyColors.colorConvert('#5BB0EC'),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      padding: EdgeInsets.only(top: 7),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Icon(
                                            Icons.account_circle_outlined,
                                            color: MyColors.appBarTitleColor,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 5),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                orientation == Orientation.landscape
                                                    ? Row(
                                                        children: [
                                                          StreamBuilder<UserModel>(
                                                              initialData: appUserModel,
                                                              stream: locator<UserModelBloc>().userModelStream,
                                                              builder: (context, snapshot) {
                                                                if (snapshot.hasData) {
                                                                  return Text(
                                                                    '${AppStrings.welcome} ${snapshot.data.name}',
                                                                    style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                                                                  );
                                                                }
                                                                return Container();
                                                              }),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          StreamBuilder<CustomerModel>(
                                                              initialData: appCustomer,
                                                              stream: locator<CustomerBloc>().customerStream,
                                                              builder: (context, snapshot) {
                                                                if (snapshot.hasData && snapshot.data != null) {
                                                                  return Text(
                                                                    '(${snapshot.data.name})',
                                                                    style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                                                                  );
                                                                }
                                                                return Container();
                                                              }),
                                                        ],
                                                      )
                                                    : Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          StreamBuilder<UserModel>(
                                                              initialData: appUserModel,
                                                              stream: locator<UserModelBloc>().userModelStream,
                                                              builder: (context, snapshot) {
                                                                if (snapshot.hasData) {
                                                                  return Text(
                                                                    '${AppStrings.welcome} ${snapshot.data.name}',
                                                                    style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                                                                  );
                                                                }
                                                                return Container();
                                                              }),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          StreamBuilder<CustomerModel>(
                                                              initialData: appCustomer,
                                                              stream: locator<CustomerBloc>().customerStream,
                                                              builder: (context, snapshot) {
                                                                if (snapshot.hasData && snapshot.data != null) {
                                                                  return Text(
                                                                    '(${snapshot.data.name})',
                                                                    style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                                                                  );
                                                                }
                                                                return Container();
                                                              }),
                                                        ],
                                                      ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                StreamBuilder<String>(
                                                    initialData: appLastSyncDateTime,
                                                    stream: locator<LastSyncDateTimeBloc>().lastSyncDateTimeStream,
                                                    builder: (context, snapshot) {
                                                      if (snapshot.hasData && snapshot.data != null && snapshot.data != '') {
                                                        return Flexible(
                                                          child: Text(
                                                            '(${AppStrings.lastSync} ${snapshot.data})',
                                                            style: TextStyle(fontSize: 14, color: MyColors.appBarTitleColor),
                                                          ),
                                                        );
                                                      }
                                                      return Container();
                                                    }),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      AppStrings.taskOverview,
                                      style: TextStyle(color: MyColors.appBarTitleColor, fontSize: 18),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Container(
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        // IconButton(
                                        //   onPressed: () {
                                        //     showDialog(context: context, builder: (context) => DialogComment(),);
                                        //   },
                                        //   icon: Icon(Icons.note,color: Colors.white,),
                                        // ),
                                        GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder: (context) => DialogUser(),
                                              );
                                            },
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10),
                                                child: Container(
                                                    color: MyColors.colorConvert("#6fb8eb"),
                                                    padding: EdgeInsets.all(5),
                                                    child: Image.asset(
                                                      'assets/icon/icon-noti-2.png',
                                                      height: 30,
                                                      width: 30,
                                                    )))),
                                        // IconButton(
                                        //   onPressed: () {
                                        //     showDialog(context: context, builder: (context) => DialogMessages(),);
                                        //   },
                                        //   icon: Icon(Icons.message,color: Colors.white,),
                                        // ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        GestureDetector(
                                          onTap: () async {
                                            try {
                                              var connectivityResult = await (Connectivity().checkConnectivity());
                                              if (connectivityResult == ConnectivityResult.none) {
                                                // I am connected to a mobile network.
                                                MyToast.showToast(AppStrings.itIsNotPossibleToLogout, context);
                                                return null;
                                              }
                                              locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                              Map<String, dynamic> apiResult = await ApiRequest.logout();
                                              // check status
                                              bool status = apiResult['success'];
                                              // message
                                              String message = apiResult['message'];
                                              // status condition
                                              if (status) {
                                                await Utility.logoutData();
                                                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                AppRouter.navigatorPage(context);
                                              } else {
                                                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                MyToast.showToast(message, context);
                                              }
                                            } catch (e) {
                                              MyToast.showToast(e.toString(), context);
                                            }
                                          },
                                          child: Container(
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  AppStrings.logout,
                                                  style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Icon(
                                                  Icons.logout,
                                                  color: MyColors.appBarTitleColor,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          //Tab
                          Container(
                            color: MyColors.tabbarColor,
                            child: Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: AppBar().preferredSize.height + (AppBar().preferredSize.height / 4),
                                  color: MyColors.tabbarColor,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () async {
                                          Map<String, dynamic> result = await showDialog(
                                            context: context,
                                            builder: (_) => DialogFilter(
                                              mapFilter: mapFilter,
                                            ),
                                          );
                                          debugPrint("Dialog result : ${result}");

                                          if (result == null) {
                                            return;
                                          }
                                          if (result.length == 0) {
                                            String query = 'select * from ${AppTables.TASK} where ${getWQuery()} order by $ORDER_BY desc';
                                            List<dynamic> qValues = getWValues();
                                            debugPrint("strGetQry : $query");
                                            debugPrint("lstQValues : $qValues");
                                            List<Map> list = await appDatabase.rawQuery(query, qValues);
                                            locator<TaskListBloc>().taskListEventSink.add(list);
                                            blocShowHideFilterClear.showHideFilterClearEventSink.add(result.length);
                                            return;
                                          }
                                          String strGetQry = getWQuery();
                                          List<dynamic> lstQValues = getWValues();
                                          blocShowHideFilterClear.showHideFilterClearEventSink.add(result.length);
                                          mapFilter = result;
                                          if (mapFilter.containsKey(HomeScreen.type_id)) {
                                            strGetQry = strGetQry + ' and ${HomeScreen.type_id} = ?';
                                            lstQValues.add(mapFilter[HomeScreen.type_id]);
                                          }
                                          if (mapFilter.containsKey(HomeScreen.group_id)) {
                                            strGetQry = strGetQry + ' and ${HomeScreen.group_id} = ?';
                                            lstQValues.add(mapFilter[HomeScreen.group_id]);
                                          }
                                          if (mapFilter.containsKey(HomeScreen.location_id)) {
                                            strGetQry = strGetQry + ' and ${HomeScreen.location_id} = ?';
                                            lstQValues.add(mapFilter[HomeScreen.location_id]);
                                          }

                                          if (mapFilter.containsKey(HomeScreen.start_date_filter) && !mapFilter.containsKey(HomeScreen.end_date)) {
                                            strGetQry = strGetQry + ' and start_date >= ?';
                                            lstQValues.add(mapFilter[HomeScreen.start_date_filter]);
                                          } else if (!mapFilter.containsKey(HomeScreen.start_date_filter) && mapFilter.containsKey(HomeScreen.end_date)) {
                                            strGetQry = strGetQry + ' and start_date <= ?';
                                            lstQValues.add(mapFilter[HomeScreen.end_date]);
                                          } else if (mapFilter.containsKey(HomeScreen.start_date_filter) && mapFilter.containsKey(HomeScreen.end_date)) {
                                            strGetQry = strGetQry + ' and start_date between ? and ?';
                                            lstQValues.add(mapFilter[HomeScreen.start_date_filter]);
                                            lstQValues.add(mapFilter[HomeScreen.end_date]);

                                            /*strGetQry = strGetQry +' and start_date >= ? and start_date <= ?';
                                        lstQValues.add(mapFilter[HomeScreen.start_date_filter]);
                                        lstQValues.add(mapFilter[HomeScreen.end_date]);*/

                                          }
                                          debugPrint("strGetQry : $strGetQry");
                                          debugPrint("lstQValues : $lstQValues");

                                          String query1 = 'select * from ${AppTables.TASK} where $strGetQry order by $ORDER_BY desc';
                                          List<dynamic> qValues1 = lstQValues;
                                          List<Map> list = await appDatabase.rawQuery(query1, qValues1);
                                          locator<TaskListBloc>().taskListEventSink.add(list);

                                          return;
                                        },
                                        child: Container(
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Icon(Icons.filter_alt_outlined, color: MyColors.tabbarTextColor, size: 30),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                AppStrings.taskFilter,
                                                style: TextStyle(fontSize: 16, color: MyColors.tabbarTextColor),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              StreamBuilder<int>(
                                                  stream: blocShowHideFilterClear.showHideFilterClearStream,
                                                  builder: (context, snapshot) {
                                                    if (snapshot.hasData && snapshot.data > 0) {
                                                      return GestureDetector(
                                                          onTap: () async {
                                                            // clear filter
                                                            blocUpDownSort.upDownSortEventSink.add(SortType.UP_START_TIME);

                                                            String query = 'select * from ${AppTables.TASK} where ${getWQuery()} order by $ORDER_BY $ASC';
                                                            List<dynamic> qValues = getWValues();
                                                            debugPrint("strGetQry : $query");
                                                            debugPrint("lstQValues : $qValues");
                                                            List<Map> list = await appDatabase.rawQuery(query, qValues);
                                                            locator<TaskListBloc>().taskListEventSink.add(list);
                                                            mapFilter = Map();
                                                            blocShowHideFilterClear.showHideFilterClearEventSink.add(mapFilter.length);
                                                          },
                                                          child: Icon(Icons.clear, color: MyColors.tabbarTextColor, size: 30));
                                                    }
                                                    return Container();
                                                  }),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: StreamBuilder<int>(
                                            // initialData: 1,
                                            stream: locator<HomeTabBloc>().homeTabStream,
                                            builder: (context, snapshot) {
                                              // debugPrint("cTab : ${snapshot.data}");
                                              if (snapshot.hasData) {
                                                int cTab = snapshot.data;
                                                return Container(
                                                  // width: centerTabsWidth,
                                                  padding: EdgeInsets.only(top: 10 + (AppBar().preferredSize.height / 8), bottom: 10 + (AppBar().preferredSize.height / 8)),
                                                  alignment: Alignment.center,
                                                  // color: Colors.green,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Container(
                                                        /* decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            border: Border.all(color: Colors.white)
                                          ),*/
                                                        child: Row(
                                                          children: [
                                                            GestureDetector(
                                                              onTap: () {
                                                                // locator<HomeTabBloc>().homeTabEventSink.add(0);
                                                                pullToRefreshData(context,0);
                                                              },
                                                              child: Container(
                                                                alignment: Alignment.center,
                                                                width: tabWidth,
                                                                decoration: BoxDecoration(
                                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(tabMenuRadius), bottomLeft: Radius.circular(tabMenuRadius)),
                                                                  border: Border.all(color: Colors.white),
                                                                  color: cTab == 0 ? Colors.white : MyColors.tabbarColor,
                                                                ),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  children: [
                                                                    Text(AppStrings.today, style: TextStyle(color: cTab == 0 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                                    StreamBuilder<int>(
                                                                      initialData: countToday,
                                                                      stream: blocTodayTaskCount.todayTaskCountStream,
                                                                      builder: (context, snapshot) {
                                                                        if (snapshot.hasData) {
                                                                          return Row(
                                                                            children: [
                                                                              SizedBox(width: 3),
                                                                              (HomeScreen.currentTab == 0)
                                                                                  ? Container(
                                                                                      height: tabCountHeight,
                                                                                      alignment: Alignment.center,
                                                                                      padding: EdgeInsets.only(
                                                                                        left: 2,
                                                                                        right: 2,
                                                                                      ),
                                                                                      decoration: BoxDecoration(color: MyColors.colorConvert('#ce2e04'), borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                                      child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                                    )
                                                                                  : Container(
                                                                                      height: tabCountHeight,
                                                                                      alignment: Alignment.center,
                                                                                      padding: EdgeInsets.only(
                                                                                        left: 2,
                                                                                        right: 2,
                                                                                      ),
                                                                                      decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.white), color: Colors.transparent, borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                                      child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                                    ),
                                                                            ],
                                                                          );
                                                                          /*if (snapshot.data == 0) {
                                                                        return Container();
                                                                      } else {
                                                                        return Row(
                                                                          children: [
                                                                            SizedBox(width: 3),
                                                                            Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Container(
                                                                                  padding: EdgeInsets.all(3),
                                                                                  decoration: BoxDecoration(color: MyColors.colorConvert('#ce2e04'), borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                                  child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ],
                                                                        );
                                                                      }*/
                                                                        }
                                                                        return Container();
                                                                      },
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            GestureDetector(
                                                              onTap: () {
                                                                // locator<HomeTabBloc>().homeTabEventSink.add(1);
                                                                pullToRefreshData(context,1);
                                                              },
                                                              child: Container(
                                                                alignment: Alignment.center,
                                                                width: tabWidth,
                                                                decoration: BoxDecoration(
                                                                  // borderRadius: BorderRadius.only(topLeft: Radius.circular(tabMenuRadius),bottomLeft: Radius.circular(tabMenuRadius)),
                                                                  border: Border(
                                                                    top: BorderSide(color: Colors.white),
                                                                    bottom: BorderSide(color: Colors.white),
                                                                    right: BorderSide(color: Colors.white),
                                                                  ),
                                                                  color: cTab == 1 ? Colors.white : MyColors.tabbarColor,
                                                                ),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  children: [
                                                                    Text(AppStrings.tooLate, style: TextStyle(color: cTab == 1 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                                    StreamBuilder<int>(
                                                                      initialData: countTooLate,
                                                                      stream: blocLateTaskCount.lateTaskCountStream,
                                                                      builder: (context, snapshot) {
                                                                        if (snapshot.hasData) {
                                                                          return Row(
                                                                            children: [
                                                                              SizedBox(width: 3),
                                                                              (HomeScreen.currentTab == 1)
                                                                                  ? Container(
                                                                                      alignment: Alignment.center,
                                                                                      height: tabCountHeight,
                                                                                      padding: EdgeInsets.only(
                                                                                        left: 2,
                                                                                        right: 2,
                                                                                      ),
                                                                                      decoration: BoxDecoration(color: MyColors.colorConvert('#ce2e04'), borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                                      child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                                    )
                                                                                  : Container(
                                                                                      height: tabCountHeight,
                                                                                      alignment: Alignment.center,
                                                                                      padding: EdgeInsets.only(
                                                                                        left: 2,
                                                                                        right: 2,
                                                                                      ),
                                                                                      decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.white), color: Colors.transparent, borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                                      child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                                    ),
                                                                            ],
                                                                          );
                                                                        }
                                                                        return Container();
                                                                      },
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            GestureDetector(
                                                              onTap: () {
                                                                // locator<HomeTabBloc>().homeTabEventSink.add(2);
                                                                pullToRefreshData(context,2);
                                                              },

                                                              child: Container(
                                                                alignment: Alignment.center,
                                                                width: tabWidth,
                                                                decoration: BoxDecoration(
                                                                  // borderRadius: BorderRadius.only(topLeft: Radius.circular(tabMenuRadius),bottomLeft: Radius.circular(tabMenuRadius)),
                                                                  border: Border(
                                                                    top: BorderSide(color: Colors.white),
                                                                    bottom: BorderSide(color: Colors.white),
                                                                    // right: BorderSide(color: Colors.white),
                                                                  ),
                                                                  color: cTab == 2 ? Colors.white : MyColors.tabbarColor,
                                                                ),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  children: [
                                                                    Text(AppStrings.planned, style: TextStyle(color: cTab == 2 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                                    /*StreamBuilder<int>(
                                                                  initialData: countPlanned,
                                                                  stream: blocPlannedTaskCount.plannedTaskCountStream,
                                                                  builder: (context, snapshot) {
                                                                    if (snapshot.hasData) {
                                                                      return Row(
                                                                        children: [
                                                                          SizedBox(width: 3),
                                                                          (HomeScreen.currentTab==2)?Container(
                                                                            height: tabCountHeight,
                                                                            alignment: Alignment.center,
                                                                            padding: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                                                            decoration: BoxDecoration(color: MyColors.colorConvert('#ce2e04'), borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                            child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                          ):Container(
                                                                            height: tabCountHeight,
                                                                            alignment: Alignment.center,
                                                                            padding: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                                                            decoration: BoxDecoration(
                                                                                border: Border.all(width: 1,color: Colors.white),
                                                                                color: Colors.transparent, borderRadius: BorderRadius.all(Radius.circular(3))
                                                                            ),
                                                                            child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                          ),
                                                                        ],
                                                                      );
                                                                    }
                                                                    return Container();
                                                                  },
                                                                )*/
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            GestureDetector(
                                                              onTap: () {
                                                                // locator<HomeTabBloc>().homeTabEventSink.add(3);
                                                                pullToRefreshData(context,3);
                                                              },
                                                              child: Container(
                                                                alignment: Alignment.center,
                                                                width: tabWidth,
                                                                decoration: BoxDecoration(
                                                                  borderRadius: BorderRadius.only(topRight: Radius.circular(tabMenuRadius), bottomRight: Radius.circular(tabMenuRadius)),
                                                                  border: Border.all(color: Colors.white),
                                                                  color: cTab == 3 ? Colors.white : MyColors.tabbarColor,
                                                                ),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  children: [
                                                                    Text(AppStrings.rounded, style: TextStyle(color: cTab == 3 ? MyColors.tabbarColor : MyColors.tabbarTextColor, fontSize: 16)),
                                                                    /*StreamBuilder<int>(
                                                                  initialData: countComplete,
                                                                  stream: blocCompleteTaskCount.completeTaskCountStream,
                                                                  builder: (context, snapshot) {
                                                                    if (snapshot.hasData) {
                                                                      if (snapshot.data == 0) {
                                                                        return Container();
                                                                      } else {
                                                                        return Row(
                                                                          children: [
                                                                            SizedBox(width: 3),
                                                                            (HomeScreen.currentTab==3)?Container(
                                                                              height: tabCountHeight,
                                                                              alignment: Alignment.center,
                                                                              padding: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                                                              decoration: BoxDecoration(color: MyColors.colorConvert('#ce2e04'), borderRadius: BorderRadius.all(Radius.circular(3))),
                                                                              child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                            ):Container(
                                                                              height: tabCountHeight,
                                                                              alignment: Alignment.center,
                                                                              padding: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                                                              decoration: BoxDecoration(
                                                                                  border: Border.all(width: 1,color: Colors.white),
                                                                                  color: Colors.transparent, borderRadius: BorderRadius.all(Radius.circular(3))
                                                                              ),
                                                                              child: Text('${snapshot.data}', style: TextStyle(color: Colors.white, fontSize: 10)),
                                                                            ),
                                                                          ],
                                                                        );
                                                                      }
                                                                    }
                                                                    return Container();
                                                                  },
                                                                )*/
                                                                  ],
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      IconButton(
                                                          padding: EdgeInsets.zero,
                                                          onPressed: () {
                                                            blocShowSearch.showSearchEventSink.add(!showSearch);
                                                          },
                                                          iconSize: 35,
                                                          icon: Icon(
                                                            Icons.search,
                                                            color: Colors.white,
                                                          ))
                                                    ],
                                                  ),
                                                );
                                              }
                                              return Container();
                                            }),
                                      ),
                                      GestureDetector(
                                        onTap: () async {
                                          /*cancellableOperation = CancelableOperation.fromFuture(
                                        ApiRequest.getTaskTable(),
                                        onCancel: () => {debugPrint('onCancel')},

                                      );
                                      cancellableOperation.asStream().listen(
                                            (value) => { debugPrint('value: $value') },
                                        onDone: () => { debugPrint('onDone') },
                                      );*/

                                          /*var client = http.Client();
                                      Map<String, String> headers = Map();
                                      headers['Authorization'] = 'Bearer ${appUserModel.accessToken}';
                                      headers['Accept'] = 'application/json';
                                      headers['Content-type'] = 'application/json';
                                      String strAPI = API.GET_TABLE_TASK;//+'?page=$page&limit=$limit';
                                      print("API : postTaskTable : ${strAPI}");
                                      print("API : headers : ${headers.toString()}");
                                      try {
                                        subscription = await client.get(Uri.parse(strAPI), headers: headers).asStream().listen((event) {
                                          debugPrint("result-mayur-postTaskTable : due to long response we are not logging it");
                                          debugPrint("status : ${event.statusCode}");
                                        });

                                      } catch (e) {

                                      }
                                      return;*/
                                          showDialog(
                                            context: context,
                                            builder: (context) => DialogAddTask(),
                                          );
                                        },
                                        child: Container(
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                AppStrings.addTask,
                                                style: TextStyle(fontSize: 16, color: MyColors.tabbarTextColor),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Icon(
                                                Icons.add_circle_outline_outlined,
                                                color: MyColors.tabbarTextColor,
                                                size: 30,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                // Search bar
                                StreamBuilder<bool>(
                                    initialData: false,
                                    stream: blocShowSearch.showSearchStream,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return snapshot.data
                                            ? Container(
                                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                                width: MediaQuery.of(context).size.width,
                                                height: AppBar().preferredSize.height,
                                                color: MyColors.tabbarColor,
                                                child: Row(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      width: centerTabsWidth,
                                                      // color: Colors.red,
                                                      child: TextField(
                                                        controller: searchController,
                                                        decoration: new InputDecoration(
                                                            isDense: true,
                                                            // contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                                            contentPadding: EdgeInsets.all(10.0),
                                                            // labelText: "",
                                                            filled: true,
                                                            fillColor: MyColors.colorConvert('#f4f4f4'),
                                                            border: textFieldOutlineInputBorder,
                                                            focusedBorder: textFieldOutlineInputBorder,
                                                            disabledBorder: textFieldOutlineInputBorder,
                                                            enabledBorder: textFieldOutlineInputBorder,
                                                            errorBorder: textFieldOutlineInputBorder,
                                                            focusedErrorBorder: textFieldOutlineInputBorder,
                                                            hintText: AppStrings.searchByTitle,
                                                            hintStyle: TextStyle(color: MyColors.colorConvert('#c6c6c6'), fontSize: 16)

                                                            //fillColor: Colors.green
                                                            ),
                                                        keyboardType: TextInputType.emailAddress,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : Container();
                                      }
                                      return Container();
                                    }),
                              ],
                            ),
                          ),
                          // Padding
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 20,
                            color: MyColors.colorConvert('#f4f4f4'),
                          ),
                          // List header
                          Container(
                            color: MyColors.colorConvert('#f4f4f4'),
                            child: Container(
                              margin: EdgeInsets.only(left: 20, right: 20),
                              padding: EdgeInsets.only(left: 10, right: 10),
                              width: MediaQuery.of(context).size.width,
                              height: AppBar().preferredSize.height + 10,
                              color: MyColors.taskListHeaderBackColor,
                              child: Row(
                                children: [
                                  //Deadline
                                  StreamBuilder<SortType>(
                                      initialData: sortType,
                                      stream: blocUpDownSort.upDownSortStream,
                                      builder: (context, snapshot) {
                                        return Expanded(
                                            flex: 3,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (isUpClick(snapshot.data)) {
                                                  onClickDownStartTime(null,null);
                                                } else if (isDownClick(snapshot.data)) {
                                                  onClickUpStartTime(null,null);
                                                }
                                              },
                                              child: Container(
                                                color: Colors.transparent,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    // AppStrings.deadline
                                                    Text(appCurrentTab == 3?AppStrings.executed:AppStrings.deadline, style: styleHeader),
                                                    Container(
                                                      margin: EdgeInsets.only(right: 10),
                                                      padding: paddingTopBottomArrow,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          snapshot.data == SortType.UP_START_TIME ? emptyContainer() : arrowUpContainer(),
                                                          // SizedBox(height: 5,),
                                                          snapshot.data == SortType.DOWN_START_TIME ? emptyContainer() : arrowDownContainer()
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ));
                                      }),
                                  //title
                                  StreamBuilder<SortType>(
                                      initialData: sortType,
                                      stream: blocUpDownSort.upDownSortStream,
                                      builder: (context, snapshot) {
                                        return Expanded(
                                            flex: 8,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (isUpClick(snapshot.data)) {
                                                  onClickDownTitle();
                                                } else if (isDownClick(snapshot.data)) {
                                                  onClickUpTitle();
                                                }
                                              },
                                              child: Container(
                                                color: Colors.transparent,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text(AppStrings.title, style: styleHeader),
                                                    Container(
                                                      margin: EdgeInsets.only(right: 10),
                                                      padding: paddingTopBottomArrow,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          snapshot.data == SortType.UP_TITLE ? emptyContainer() : arrowUpContainer(),
                                                          // SizedBox(height: 5,),
                                                          snapshot.data == SortType.DOWN_TITLE ? emptyContainer() : arrowDownContainer()
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ));
                                      }),
                                  //type
                                  StreamBuilder<SortType>(
                                      initialData: sortType,
                                      stream: blocUpDownSort.upDownSortStream,
                                      builder: (context, snapshot) {
                                        return Expanded(
                                            flex: 3,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (isUpClick(snapshot.data)) {
                                                  onClickDownType();
                                                } else if (isDownClick(snapshot.data)) {
                                                  onClickUpType();
                                                }
                                              },
                                              child: Container(
                                                color: Colors.transparent,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text(AppStrings.type, style: styleHeader),
                                                    Container(
                                                      margin: EdgeInsets.only(right: 10),
                                                      padding: paddingTopBottomArrow,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          snapshot.data == SortType.UP_TYPE ? emptyContainer() : arrowUpContainer(),
                                                          // SizedBox(height: 5,),
                                                          snapshot.data == SortType.DOWN_TYPE ? emptyContainer() : arrowDownContainer()
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ));
                                      }),
                                  //location
                                  StreamBuilder<SortType>(
                                      initialData: sortType,
                                      stream: blocUpDownSort.upDownSortStream,
                                      builder: (context, snapshot) {
                                        return Expanded(
                                            flex: 3,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (isUpClick(snapshot.data)) {
                                                  onClickDownLocation();
                                                } else if (isDownClick(snapshot.data)) {
                                                  onClickUpLocation();
                                                }
                                              },
                                              child: Container(
                                                color: Colors.transparent,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text(AppStrings.location, style: styleHeader),
                                                    Container(
                                                      margin: EdgeInsets.only(right: 10),
                                                      padding: paddingTopBottomArrow,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          snapshot.data == SortType.UP_LOCATION ? emptyContainer() : arrowUpContainer(),
                                                          // SizedBox(height: 5,),
                                                          snapshot.data == SortType.DOWN_LOCATION ? emptyContainer() : arrowDownContainer()
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ));
                                      }),
                                  //Frequency
                                  StreamBuilder<SortType>(
                                      initialData: sortType,
                                      stream: blocUpDownSort.upDownSortStream,
                                      builder: (context, snapshot) {
                                        return Expanded(
                                            flex: 4,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (isUpClick(snapshot.data)) {
                                                  onClickDownFrequency();
                                                } else if (isDownClick(snapshot.data)) {
                                                  onClickUpFrequency();
                                                }
                                              },
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Text(AppStrings.frequency, style: styleHeader),
                                                  Container(
                                                    margin: EdgeInsets.only(right: 10),
                                                    padding: paddingTopBottomArrow,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        snapshot.data == SortType.UP_FREQUENCY ? emptyContainer() : arrowUpContainer(),
                                                        // SizedBox(height: 5,),
                                                        snapshot.data == SortType.DOWN_FREQUENCY ? emptyContainer() : arrowDownContainer()
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ));
                                      }),
                                  Expanded(flex: 2, child: Text(AppStrings.status, style: styleHeader)),
                                ],
                              ),
                            ),
                          ),
                          // Padding
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 20,
                            color: MyColors.colorConvert('#f4f4f4'),
                          ),
                          // List
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: MyColors.colorConvert('#f4f4f4'),
                              child: Container(
                                color: MyColors.colorConvert('#f4f4f4'),
                                margin: EdgeInsets.only(left: 20, right: 20),
                                width: MediaQuery.of(context).size.width,
                                child: StreamBuilder<List>(
                                    initialData: [],
                                    stream: locator<TaskListBloc>().taskListStream,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        List<dynamic> listTask = snapshot.data;
                                        if (listTask.length == 0) {
                                          return ListView(
                                            children: [
                                              Container(
                                                height: (MediaQuery.of(context).size.height / 5) * 3,
                                                alignment: Alignment.center,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Image.asset('assets/icon_empty.png'),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Text(
                                                      HomeScreen.currentTab == 0
                                                          ? noMsgToday
                                                          : HomeScreen.currentTab == 1
                                                              ? noMsgPast
                                                              : HomeScreen.currentTab == 2
                                                                  ? noMsgFuture
                                                                  : HomeScreen.currentTab == 3
                                                                      ? noMsgCompleted
                                                                      : '',
                                                      style: TextStyle(color: MyColors.colorConvert('#cccccc'), fontSize: 20),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          );
                                        } else {
                                          return Container(
                                            padding:EdgeInsets.only(bottom: 10),
                                            child: ListView.separated(
                                              shrinkWrap: true,
                                              controller: _controller,
                                              separatorBuilder: (context, index) {
                                                return SizedBox(
                                                  height: 15,
                                                );
                                              },
                                              itemCount: present,//(present + perPage)> originalItems.length?listTask.length:present,
                                              itemBuilder: (context, index) {
                                                Map<String, dynamic> mapTask = listTask[index];
                                                TaskModel taskModel = TaskModel.fromMap(map: mapTask);
                                                taskModel.synced = mapTask['synced'];

                                                String rowColor = '';
                                                if (lstUsrGId.contains(taskModel.group_id)) {
                                                  rowColor = '#777777';
                                                } else {
                                                  rowColor = '#BBBBBB';
                                                }
                                                // debugPrint("query result UsrG -- ${taskModel.group_id}");

                                                return GestureDetector(
                                                  onTap: () async {
                                                    locator<TaskModelBloc>().taskModelEventSink.add(taskModel);
                                                    locator<TaskIndexBloc>().taskIndexEventSink.add(index);
                                                    locator<RefreshHomeScreenBloc>().refreshHomeEventSink.add(false);
                                                    dynamic result = await Navigator.of(context).pushNamed(DetailScreen.routeName);
                                                    debugPrint("backResult : $result");
                                                    if(result == "refresh"){
                                                      pullToRefreshData(context,appCurrentTab);
                                                    }
                                                  },
                                                  child: ItemTask(
                                                    index: index,
                                                    rowColor: rowColor,
                                                    currentTab: HomeScreen.currentTab,
                                                    taskModel: taskModel,
                                                  ),
                                                );
                                              },
                                            ),
                                          );
                                        }
                                      }
                                      return Container();
                                    }),
                              ),
                            ),
                          ),
                          StreamBuilder(
                            stream: blocLoadMore.loadMoreStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData && snapshot.data) {
                                return Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 50,
                                    color: Colors.white.withOpacity(0.7),
                                    child: Center(
                                      child: SizedBox(
                                        width: 40,
                                        height: 40,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 1.5,
                                          valueColor: AlwaysStoppedAnimation<Color>(MyColors.appAccentColor),
                                        ),
                                      ),
                                    ));
                              }
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                height: 20,
                                color: MyColors.colorConvert('#f4f4f4'),
                              );
                            },
                          ),

                        ],
                      ),
                    ),
                    LoadingWidget(),
                    StreamBuilder<bool>(
                        initialData: false,
                        stream: mRefreshingBloc.loadingStream,
                        builder: (context, snapshot) {
                          if (snapshot.hasData && snapshot.data) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              color: Colors.white.withOpacity(0.7),
                                child: Center(
                                  child: SizedBox(
                                    width: 40,
                                    height: 40,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 1.5,
                                      valueColor: AlwaysStoppedAnimation<Color>(MyColors.appAccentColor),
                                    ),
                                  ),
                                )
                            );
                          }
                          return Container();
                        }),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: MyColors.colorConvert('#e5e5e5')),
  );
  TextStyle styleHeader = TextStyle(fontSize: 16, color: MyColors.colorConvert('#e0e2e3'));

  @override
  void dispose() {
    _controller.removeListener(_loadMore);
    super.dispose();
  }

  SortType sortType = SortType.DOWN_START_TIME;
  UpDownSortBloc blocUpDownSort = UpDownSortBloc();

  void onClickUpStartTime(finalList,mOriginalItems) async {
    blocUpDownSort.upDownSortEventSink.add(SortType.UP_START_TIME);
    String strOrderBy=appCurrentTab==3?'finish_date':'start_date';

    List<dynamic> listCopy = List.of(finalList != null ? finalList : mTaskList);
    listCopy.sort((a, b) => b[strOrderBy].compareTo(a[strOrderBy]));

    if(finalList != null) {
      List<dynamic> mTFList = [];
      mTFList.addAll(listCopy.getRange(0, present));
      listCopy = mTFList;
    }
    // debugPrint("originalItems onLoad : listCopy : ${listCopy.length}");

    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    List<dynamic> listCopyOriginal = List.of(mOriginalItems != null ? mOriginalItems : mTaskList);
    listCopyOriginal.sort((a, b) => b[strOrderBy].compareTo(a[strOrderBy]));
    originalItems = List.from(listCopyOriginal);
    List<String> mLog = [];
    originalItems.forEach((element) {
      mLog.add(element['title']);
    });
    // debugPrint("----loadmore-test : 3 : onClickUpStartTime : $mLog");
  }
// - load more chale chhe
// - sort by start time up/down kare tyare 15 record j aave chhe
  void onClickDownStartTime(finalList,mOriginalItems) async {
    blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_START_TIME);
    String strOrderBy=appCurrentTab==3?'finish_date':'start_date';

    List<dynamic> listCopy = List.of(finalList != null ? finalList : mTaskList);
    listCopy.sort((a, b) => a[strOrderBy].compareTo(b[strOrderBy]));

    if(finalList != null) {
      List<dynamic> mTFList = [];
      mTFList.addAll(listCopy.getRange(0, present));
      listCopy = mTFList;
    }
    // debugPrint("originalItems onLoad : listCopy : ${listCopy.length}");

    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    List<dynamic> listCopyOriginal = List.of(mOriginalItems != null ? mOriginalItems : mTaskList);
    listCopyOriginal.sort((a, b) => a[strOrderBy].compareTo(b[strOrderBy]));
    originalItems = List.from(listCopyOriginal);
    List<String> mLog = [];
    originalItems.forEach((element) {
      mLog.add(element['title']);
    });
    // debugPrint("----loadmore-test : 4 : onClickDownStartTime : $mLog");
  }

  void onClickUpTitle() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.UP_TITLE);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => b['title'].toLowerCase().compareTo(a['title'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);
  }

  void onClickDownTitle() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_TITLE);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => a['title'].toLowerCase().compareTo(b['title'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  void onClickUpType() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.UP_TYPE);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => b['tasktype_name'].toLowerCase().compareTo(a['tasktype_name'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  void onClickDownType() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_TYPE);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => a['tasktype_name'].toLowerCase().compareTo(b['tasktype_name'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  void onClickUpLocation() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.UP_LOCATION);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => b['location_name'].toLowerCase().compareTo(a['location_name'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  void onClickDownLocation() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_LOCATION);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => a['location_name'].toLowerCase().compareTo(b['location_name'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  void onClickUpFrequency() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.UP_FREQUENCY);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => b['frequency_name'].toLowerCase().compareTo(a['frequency_name'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  void onClickDownFrequency() async {
    blocUpDownSort.upDownSortEventSink.add(SortType.DOWN_FREQUENCY);

    List<dynamic> listCopy = List.of(mTaskList);
    listCopy.sort((a, b) => a['frequency_name'].toLowerCase().compareTo(b['frequency_name'].toLowerCase()));
    locator<TaskListBloc>().taskListEventSink.add(listCopy);

    originalItems = List.from(listCopy);

  }

  EdgeInsets paddingTopBottomArrow = EdgeInsets.only(top: 15, bottom: 15);

  Widget emptyContainer() {
    return Container(
      width: 18,
      height: 7,
    );
  }

  double widthUpDownArrow = 18;
  double heightUpDownArrow = 2;

  Widget arrowUpContainer() {
    return Container(
      width: 18,
      // height: 7,
      child: Image.asset(
        'assets/icon/arrow_up.png',
        width: widthUpDownArrow,
      ),
    );
  }

  Widget arrowDownContainer() {
    return Container(
      width: 18,
      // height: 7,
      child: Image.asset('assets/icon/arrow_down.png', width: widthUpDownArrow),
    );
  }

  bool isUpClick(SortType mType) {
    if (mType == SortType.UP_TITLE || mType == SortType.UP_START_TIME || mType == SortType.UP_FREQUENCY || mType == SortType.UP_LOCATION || mType == SortType.UP_TYPE) {
      return true;
    }
    return false;
  }

  bool isDownClick(SortType mType) {
    if (mType == SortType.DOWN_TITLE || mType == SortType.DOWN_START_TIME || mType == SortType.DOWN_FREQUENCY || mType == SortType.DOWN_LOCATION || mType == SortType.DOWN_TYPE) {
      return true;
    }
    return false;
  }
}
