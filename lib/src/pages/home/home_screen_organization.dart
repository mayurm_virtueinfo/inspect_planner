import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/complete_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/late_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/planned_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/show_checklist_tab.dart';
import 'package:inspect_planner/src/bloc/show_hide_filter_clear_bloc.dart';
import 'package:inspect_planner/src/bloc/show_search_bloc.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_list_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/bloc/today_task_count_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_add_task.dart';
import 'package:inspect_planner/src/dialog/dialog_filter.dart';
import 'package:inspect_planner/src/dialog/dialog_messages.dart';
import 'package:inspect_planner/src/dialog/dialog_newsposts_popup.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/task_list_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/pages/home/detail_screen.dart';
import 'package:inspect_planner/src/service/navigation_service.dart';
import 'package:inspect_planner/src/utils/app_preferences.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/item_customer.dart';
import 'package:inspect_planner/src/widget/item_task.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../../app_router.dart';

class HomeScreenOrganication extends StatefulWidget {

  static const String routeName = '/HomeScreenOrganication';

  @override
  _HomeScreenOrganicationState createState() => _HomeScreenOrganicationState();
}

class _HomeScreenOrganicationState extends State<HomeScreenOrganication> {

  List<CustomerModel> listCustomerOriginal = [];
  List<CustomerModel> listCustomer = [];
  @override
  void initState() {
    searchController.addListener(searchListener);
    getCustomerList();
    super.initState();

  }
  void searchListener() {
    print("controller change : ${searchController.text}");
    stopTime();
    startTime();
  }

  Timer mainTimer;

  startTime() async {
    mainTimer = Timer(Duration(seconds: 1), requestTimeOut);
    return mainTimer;
  }

  stopTime() {
    if (mainTimer != null) {
      mainTimer.cancel();
    }
  }

  void requestTimeOut() {
    if (searchController.text.length > 0) {
    //   debugPrint("requested");
      searchTaskList();
    }else{
      getCustomerList();
    }
  }

  void searchTaskList() async {
    try {

      String sqlCustList = "select * from ${AppTables.CUSTOMER} where name like ?";
      List<dynamic> sqlval = ['%${searchController.text}%'];
      debugPrint("sqlCustList : $sqlCustList");
      List<Map<String,dynamic>> lCustomer = await appDatabase.rawQuery(sqlCustList,sqlval);
      listCustomer.clear();
      lCustomer.forEach((element) {
        CustomerModel customerModel = CustomerModel.fromMap(map: element);
        listCustomer.add(customerModel);
      });
      setState(() {});
    } catch (e) {
      debugPrint('searchTaskList query error :${e.toString()}');
    }
  }
  void getCustomerList()async{
    String sqlCustList = "select * from ${AppTables.CUSTOMER}";
    debugPrint("sqlCustList : $sqlCustList");
    List<Map<String,dynamic>> lCustomer = await appDatabase.rawQuery(sqlCustList);
    listCustomer.clear();
    lCustomer.forEach((element) {
      CustomerModel customerModel = CustomerModel.fromMap(map: element);
      listCustomer.add(customerModel);
      listCustomerOriginal.add(customerModel);
    });

    setState((){});

  }
  TextEditingController searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    double tabCountHeight = 22;
    double verticalGap = 10;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 7) * 3;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    debugPrint("devicePixelRatio : $devicePixelRatio");
    double centerTabsWidth = (((MediaQuery.of(context).size.width / 5) * 3) / 5) * 4;
    double tabWidth = centerTabsWidth / 4;
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        backgroundColor: Colors.white,
        // resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          leadingWidth: MediaQuery.of(context).size.width / 3,
          backgroundColor: MyColors.colorConvert('#5BB0EC'),
          centerTitle: true,
          leading: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.account_circle_outlined,
                  color: MyColors.appBarTitleColor,
                ),
                SizedBox(
                  width: 5,
                ),
                StreamBuilder<UserModel>(
                    initialData: appUserModel,
                    stream: locator<UserModelBloc>().userModelStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Text(
                          '${AppStrings.welcome} ${snapshot.data.name}',
                          style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                        );
                      }
                      return Container();
                    })
              ],
            ),
          ),
          title: Text(
            AppStrings.customers,
            style: TextStyle(color: MyColors.appBarTitleColor, fontSize: 18),
          ),
          actions: [
            SizedBox(width: 10,),
            GestureDetector(
              onTap: () async {
                try {


                  var connectivityResult = await (Connectivity().checkConnectivity());
                  if (connectivityResult == ConnectivityResult.none) {
                    // I am connected to a mobile network.
                    MyToast.showToast(AppStrings.itIsNotPossibleToLogout, context);
                    return null;
                  }
                  DialogMgr.showProgressDialog(context);
                  Map<String, dynamic> apiResult = await ApiRequest.logout();
                  // check status
                  bool status = apiResult['success'];
                  // message
                  String message = apiResult['message'];
                  // status condition
                  if (status) {
                    await Utility.logoutData();
                    Navigator.of(locator<IPKeys>().keyDialogProgress.currentContext,).pop();
                    AppRouter.navigatorPage(context);
                  } else {
                    Navigator.of(locator<IPKeys>().keyDialogProgress.currentContext,).pop();
                    MyToast.showToast(message, context);
                  }
                } catch (e) {
                  MyToast.showToast(e.toString(), context);
                }
              },
              child: Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      AppStrings.logout,
                      style: TextStyle(fontSize: 16, color: MyColors.appBarTitleColor),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      Icons.logout,
                      color: MyColors.appBarTitleColor,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              ),
            ),


          ],
        ),
        body: Container(
          child: SafeArea(
            child: Column(
              children: [

                SizedBox(height: 20),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppStrings.listOfCustomers,style: TextStyle(
                          fontSize: 20,
                          color: MyColors.colorConvert('#5BB0EC')
                      ),),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        width: 250,
                        height: 45,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: MyColors.colorConvert('#E4E6EF')),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            /*Text('Zoeken...',style: TextStyle(
                              color: MyColors.colorConvert('#B6B6C4'),
                              fontSize: 16
                            ),),*/
                            Container(
                              width: 190,
                              child: TextField(
                                controller: searchController,
                                decoration: new InputDecoration(
                                    isDense: true,
                                    // contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                    contentPadding: EdgeInsets.all(10.0),
                                    // labelText: "",
                                    filled: true,
                                    fillColor: Colors.white,
                                    border: textFieldOutlineInputBorder,
                                    focusedBorder: textFieldOutlineInputBorder,
                                    disabledBorder: textFieldOutlineInputBorder,
                                    enabledBorder: textFieldOutlineInputBorder,
                                    errorBorder: textFieldOutlineInputBorder,
                                    focusedErrorBorder: textFieldOutlineInputBorder,
                                    hintText: AppStrings.toSearch,
                                    hintStyle: TextStyle(color: MyColors.colorConvert('#c6c6c6'), fontSize: 16)

                                  //fillColor: Colors.green
                                ),
                                keyboardType: TextInputType.emailAddress,
                              ),
                            ),
                            Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: MyColors.colorConvert('#E4E6EF'),
                                borderRadius: BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)),
                              ),
                              child: Icon(Icons.search,color: MyColors.colorConvert('#777777'),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  padding: EdgeInsets.only(left: 10, right: 10),
                  width: MediaQuery.of(context).size.width,
                  height: AppBar().preferredSize.height,
                  color: MyColors.taskListHeaderBackColor,
                  child: Row(
                    children: [
                      //Deadline
                      Expanded(flex: 8, child: Text(AppStrings.customer, style: styleHeader.copyWith(fontWeight: FontWeight.bold))),
                      Expanded(
                          flex: 1,
                          child: Container(

                              alignment: Alignment.centerRight,
                              child: Container(
                                  padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                                 alignment: Alignment.center,
                                  child: Text(AppStrings.action, style: styleHeader.copyWith(fontWeight: FontWeight.bold)))
                          )
                      )
                      /*Expanded(
                          flex: 1,
                          child: Container(
                              alignment: Alignment.centerRight,
                              child: Text('Actie', style: styleHeader)
                          )
                      ),*/
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                    controller: ScrollController(),
                    child: Container(
                      constraints: BoxConstraints(
                        minWidth: MediaQuery.of(context).size.width,
                        minHeight: MediaQuery.of(context).size.height,
                      ),
                      margin: EdgeInsets.only(left: 20, right: 20),
                      width: MediaQuery.of(context).size.width,
                      child: ListView.separated(
                        shrinkWrap: true,
                        controller: ScrollController(),
                        separatorBuilder: (context, index) {
                          return Container(
                            height: 1,
                            color: MyColors.colorConvert('#DDDDDD'),
                          );
                        },
                        itemCount: listCustomer.length,
                        itemBuilder: (context, index) {
                          CustomerModel customerModel = listCustomer[index];


                          return GestureDetector(
                            onTap: () async{

                            },
                            child: ItemCustomer(
                              customerModel: customerModel,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }

  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: Colors.white),
  );
  TextStyle styleHeader = TextStyle(fontSize: 16, color: MyColors.colorConvert('#e0e2e3'));

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

}
