import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_router.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/source.dart';

class ImageViewerScreen extends StatefulWidget {
  static const String routeName = "/ImageViewerScreen";
  Source source;
  String url;
  ImageViewerScreen({this.source,this.url});
  // Map<String,dynamic> mapArg;
  // ImageViewerScreen({this.mapArg});
  @override
  _ImageViewerScreenState createState() => _ImageViewerScreenState();
}

// Component UI
class _ImageViewerScreenState extends State<ImageViewerScreen> {
  // SharedPreferences prefs;

  /// Declare startTime to InitState
  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Code Create UI Splash Screen
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leadingWidth: MediaQuery.of(context).size.width / 3,
        backgroundColor: MyColors.colorConvert('#5BB0EC'),
        centerTitle: true,
        leading: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 10,
              ),
              IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: MyColors.appBarTitleColor,
                ),
              )
            ],
          ),
        ),
        actions: [],
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: /*PhotoView(
            // imageProvider: AssetImage("assets/large-image.jpg"),
            imageProvider: widget.source==Source.WEB?
            NetworkImage(widget.url):Image.file(File(widget.url)),
          )*/widget.source == Source.LOCAL?Image.file(File(widget.url),fit: BoxFit.cover,):FancyShimmerImage(
            imageUrl: widget.url,
            boxFit: BoxFit.contain,
          )
      ),
    );
  }
}
