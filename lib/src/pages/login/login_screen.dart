import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/newsposts_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_enter_otp.dart';
import 'package:inspect_planner/src/dialog/dialog_newsposts_popup.dart';
import 'package:inspect_planner/src/models/attachment_model.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/message_view_log_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_frequency_list_model.dart';
import 'package:inspect_planner/src/models/task_frequency_model.dart';
import 'package:inspect_planner/src/models/task_list_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/models/users_groups_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/pages/home/home_screen_organization.dart';
import 'package:inspect_planner/src/service/navigation_service.dart';
import 'package:inspect_planner/src/utils/app_preferences.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_style.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/app_user_role.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/utils/width_sizes.dart';
import 'package:inspect_planner/src/widget/header_widget.dart';
import 'package:inspect_planner/src/widget/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show FilteringTextInputFormatter, PlatformException, SystemNavigator, TextInputFormatter, rootBundle;
import 'package:inspect_planner/src/widget/widget_sync.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = '/LoginScreen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState

    emailController.text = kDebugMode?"smit.laravel+124@gmail.com":'';
    passwordController.text = kDebugMode?"smit.laravel+124@gmail.com":'';

    // emailController.text = '';
    // passwordController.text = '';


    /*ROLE_SUBADMIN*/
    // emailController.text = "cloud9@test.nl";
    // passwordController.text = "Welkom01";

    // emailController.text = "inspectplanner@test.nl";
    // passwordController.text = "smit1234";

    // emailController.text = "normaluser@gmail.com";
    // passwordController.text = "normaluser@gmail.com";

    // without 2fa
    // emailController.text = "cloud9@test.nl";
    // passwordController.text = "Welkom01";

    // with 2fa
    // emailController.text = "VCLIENT.mobile";
    // passwordController.text = "VCLIENT.mobile";
    /*ROLE_SUPER_ADMIN*/
    /*
    emailController.text = "smit@gmail.com";
    passwordController.text = "smit@gmail.com";
    */
    /*ROLE_ORGANISATION*/
    /*emailController.text = "cloud9@admin.nl";
    passwordController.text = "Welkom01";*/



    getPackgeInfo();
    setState(() {});
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  bool invalidCredential = false;
  bool isEmailValid = true;
  bool isPasswordValid = true;
  String emailError = '';
  String passError = '';
  String msgInvalidCredential = '';

  TextStyle loginScreenTextStyle = TextStyle(color: MyColors.colorConvert('#9e9e9e'), fontSize: 14);
  TextStyle loginScreenErrorTextStyle = TextStyle(color: Colors.red, fontSize: 10);

  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: MyColors.colorConvert('#e5e5e5')),
  );

  String version = '';
  String buildNumber = '';

  void getPackgeInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double verticalGap = 10;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    debugPrint("devicePixelRatio : $devicePixelRatio");
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        // resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        body: OrientationBuilder(
          builder: (context, orientation) {
            bool isPortrait = (orientation == Orientation.portrait);
            double loginBoxHeight = (screenHeight / 6) * (isPortrait?3:4);
            return Container(
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    // color: MyColors.appAccentColor,
                    decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/login-regester-background.jpg'), fit: BoxFit.fill)),
                  ),
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              /*IconButton(
                                onPressed: () async {
                                  try {
                                    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                    Map<String, dynamic> apiResult = await ApiRequest.getSupportText();
                                    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                    bool status = apiResult['status'];
                                    if (status) {
                                      String result = await DialogMgr.showSupportDialog(context, apiResult);
                                    }
                                  } catch (e) {
                                    MyToast.showToast(e.toString(), context);
                                  }
                                },
                                icon: Icon(
                                  Icons.info_outlined,
                                  color: MyColors.infoIconColor,
                                  size: 35,
                                ),
                              )*/
                            ],
                          ),
                          Expanded(
                            flex: 1,
                            child: Center(
                              child: Container(
                                width: (screenWidth / 5) * 3,
                                height: loginBoxHeight,
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(10))),
                                child: Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(left: loginBoxWidth / 10, right: loginBoxWidth / 10),
                                      width: double.infinity,
                                      height: loginBoxHeight / 5,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                                        color: MyColors.colorConvert('#5BB0EC'),
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Image.asset(
                                            'assets/app_logo.png',
                                            // width: loginBoxWidth / 1.75,
                                          )
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        padding: EdgeInsets.only(left: loginBoxWidth / 10, right: loginBoxWidth / 10),
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                                        ),
                                        child: SingleChildScrollView(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                height: verticalGap * 2,
                                              ),
                                              invalidCredential
                                                  ? Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                        msgInvalidCredential,
                                                        style: loginScreenErrorTextStyle,
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: verticalGap,
                                                  ),
                                                ],
                                              )
                                                  : Container(),
                                              Row(
                                                children: [
                                                  Text(
                                                    '${AppStrings.emailAddress} / ${AppStrings.userNameString}',
                                                    style: loginScreenTextStyle,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: verticalGap,
                                              ),
                                              Container(
                                                height: loginBoxHeight / 8,
                                                child: TextField(
                                                  controller: emailController,
                                                  decoration: new InputDecoration(
                                                    isDense: true,
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                                                    labelText: "",
                                                    filled: true,
                                                    fillColor: MyColors.colorConvert('#f4f4f4'),
                                                    border: textFieldOutlineInputBorder,
                                                    focusedBorder: textFieldOutlineInputBorder,
                                                    disabledBorder: textFieldOutlineInputBorder,
                                                    enabledBorder: textFieldOutlineInputBorder,
                                                    errorBorder: textFieldOutlineInputBorder,
                                                    focusedErrorBorder: textFieldOutlineInputBorder,

                                                    //fillColor: Colors.green
                                                  ),
                                                  textInputAction: TextInputAction.next,
                                                  keyboardType: TextInputType.emailAddress,
                                                ),
                                              ),
                                              isEmailValid
                                                  ? Container()
                                                  : Column(
                                                children: [
                                                  SizedBox(
                                                    height: verticalGap / 2,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        emailError,
                                                        style: loginScreenErrorTextStyle,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: verticalGap,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    AppStrings.password,
                                                    style: loginScreenTextStyle,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: verticalGap,
                                              ),
                                              Container(
                                                height: loginBoxHeight / 8,
                                                child: TextField(
                                                  controller: passwordController,
                                                  decoration: new InputDecoration(
                                                    labelText: "",
                                                    filled: true,
                                                    // isDense: true,
                                                    // contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),

                                                    fillColor: MyColors.colorConvert('#f4f4f4'),
                                                    border: textFieldOutlineInputBorder,
                                                    focusedBorder: textFieldOutlineInputBorder,
                                                    disabledBorder: textFieldOutlineInputBorder,
                                                    enabledBorder: textFieldOutlineInputBorder,
                                                    errorBorder: textFieldOutlineInputBorder,
                                                    focusedErrorBorder: textFieldOutlineInputBorder,

                                                    //fillColor: Colors.green
                                                  ),

                                                  // keyboardType: TextInputType.pas,
                                                  obscureText: true,
                                                  obscuringCharacter: '*',
                                                ),
                                              ),
                                              isPasswordValid
                                                  ? Container()
                                                  : Column(
                                                children: [
                                                  SizedBox(
                                                    height: verticalGap / 2,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        passError,
                                                        style: loginScreenErrorTextStyle,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: verticalGap * 2,
                                              ),
                                              Container(
                                                height: loginBoxHeight / 8,
                                                width: double.infinity,
                                                child: ElevatedButton(
                                                  style: ElevatedButton.styleFrom(
                                                    padding: EdgeInsets.all(0),
                                                    elevation: 0,
                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                                    primary: MyColors.loginButtonColor,
                                                  ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                                    if (states.contains(MaterialState.pressed)) {
                                                      return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                                    }
                                                    return null;
                                                  })),
                                                  onPressed: () async {
                                                    // onPressedLogin
                                                    /*DialogMgr.showSyncDialog();
                                                    return;*/
                                                    // Map<String, dynamic> otpResult = await showDialog(context: context, builder: (context) => DialogEnterOTP(),);
                                                    // debugPrint('otpResult : $otpResult');
                                                    // return;

                                                    var connectivityResult = await (Connectivity().checkConnectivity());
                                                    if (connectivityResult == ConnectivityResult.none) {
                                                      // I am not connected to an any network.
                                                      MyToast.showToast(AppStrings.internetIsRequiredToLogin, context);
                                                      return null;
                                                    }

                                                    debugPrint("yes");
                                                    String txtEmail = emailController.text;
                                                    String txtPass = passwordController.text;
                                                    if (txtEmail.length == 0) {
                                                      emailError = AppStrings.emailIsRequired;
                                                      isEmailValid = false;
                                                      setState(() {});
                                                      return null;
                                                    }
                                                    // else if (!Utility.isEmailValid(txtEmail)) {
                                                    //   emailError = AppStrings.invalidEmail;
                                                    //   isEmailValid = false;
                                                    //   setState(() {});
                                                    //   return null;
                                                    // }
                                                    else {
                                                      emailError = "";
                                                      isEmailValid = true;
                                                      setState(() {});
                                                    }
                                                    if (txtPass.length == 0) {
                                                      passError = AppStrings.passwordIsRequired;
                                                      isPasswordValid = false;
                                                      setState(() {});
                                                      return null;
                                                    } else {
                                                      passError = "";
                                                      isPasswordValid = true;
                                                      setState(() {});
                                                    }
                                                    FocusScope.of(context).requestFocus(FocusNode());
                                                    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                                    try {
                                                      debugPrint("trying---logging--");
                                                      Map<String, dynamic> apiResult = await ApiRequest.postLogin(username: txtEmail, password: txtPass);
                                                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

                                                      debugPrint("trying---logging---result-- $apiResult");
                                                      // return;
                                                      if (apiResult == null) {
                                                        MyToast.showToast(AppStrings.errWhileLogin, context);
                                                        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                        return;
                                                      } else {
                                                        debugPrint("trying---1--");
                                                        bool status = apiResult['status'];
                                                        if (status != null && status) {
                                                          debugPrint("trying---2--");
                                                          try {
                                                            Map<String, dynamic> userDetail = apiResult['user_detail'];
                                                            int twoFA = userDetail['twofa'];
                                                            int qrShowStatus = userDetail['qr_show_status'];
                                                            String qrImage = userDetail['qr_image'];

                                                            String userId = '${userDetail['id']}';
                                                            int twoFAVerified = userDetail['twofa_verified'];
                                                            debugPrint("trying---2-- twoFA : $twoFA");
                                                            debugPrint("trying---2-- userId : $userId");
                                                            debugPrint("trying---2-- twoFAVerified $twoFAVerified");

                                                            debugPrint("trying---2-- userDetail.containsKey('twofa') ${userDetail.containsKey('twofa')}");
                                                            debugPrint("trying---2-- userDetail.containsKey('twofa_verified') ${userDetail.containsKey('twofa_verified')}");

                                                            if(userDetail.containsKey('twofa') && userDetail.containsKey('twofa_verified')){
                                                              debugPrint("trying---3--");
                                                              int twoFA = userDetail['twofa'];
                                                              String userId = '${userDetail['id']}';
                                                              int twoFAVerified = userDetail['twofa_verified'];
                                                              debugPrint("twoFA : $twoFA");
                                                              if(twoFA == 1 && twoFAVerified == 0){
                                                                debugPrint("trying---4--");
                                                                Map<String, dynamic> otpResult = await showDialog(context: context, builder: (context) => DialogEnterOTP(
                                                                  userId: userId,
                                                                  loginBoxHeight:loginBoxHeight,
                                                                  loginBoxWidth:(screenWidth / 5) * 3,
                                                                  qrShowStatus:qrShowStatus,
                                                                  qrImage:qrImage),
                                                                );
                                                                debugPrint('otpResult : $otpResult');
                                                                if(otpResult != null && otpResult.containsKey('action')){
                                                                  debugPrint("trying---5--");
                                                                  String action = otpResult['action'];
                                                                  if(action == 'otp'){
                                                                    debugPrint("trying---6--");
                                                                    String verified = otpResult['verified'];
                                                                    if(verified == '1'){
                                                                      await Utility.processLogin(mContext: context, apiResult: apiResult);
                                                                    }
                                                                  }
                                                                }
                                                                // debugPrint('result : $result');
                                                              }else if(twoFA == 0){
                                                                debugPrint("trying---7--");
                                                                await Utility.processLogin(mContext: context, apiResult: apiResult);
                                                              }
                                                            }

                                                            debugPrint("trying---8-userDetail-- $userDetail");
                                                            // await Utility.processLogin(mContext: context, apiResult: apiResult);
                                                          }catch (e) {
                                                            locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                            debugPrint("trying---9-process cancelled");
                                                          }
                                                        } else if (apiResult['message'] != null) {
                                                          debugPrint('---10');
                                                          invalidCredential = true;
                                                          msgInvalidCredential = apiResult['message'];
                                                          setState(() {});
                                                        }
                                                        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                      }
                                                    } catch (e) {
                                                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                                    }
                                                  },
                                                  child: Text(AppStrings.login, style: loginScreenTextStyle.copyWith(color: Colors.white)),
                                                ),
                                              ),
                                              SizedBox(
                                                height: verticalGap,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(alignment: Alignment.bottomCenter, child: Text('${AppStrings.version} $version ${AppStrings.build} $buildNumber', style: loginScreenTextStyle.copyWith(fontSize: 14))),
                                    SizedBox(
                                      height: verticalGap,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  LoadingWidget(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
