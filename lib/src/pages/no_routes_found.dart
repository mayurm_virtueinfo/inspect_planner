import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';

class NoRoutesFound extends StatefulWidget {
  static const String routeName = "/";
  @override
  _NoRoutesFoundState createState() => _NoRoutesFoundState();
}

// Component UI
class _NoRoutesFoundState extends State<NoRoutesFound> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Code Create UI Splash Screen
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(AppStrings.noRouteFound,style: TextStyle(
          fontSize: 20
        ),),
      ),
    );
  }
}
