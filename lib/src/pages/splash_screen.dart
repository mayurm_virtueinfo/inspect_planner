import 'dart:async';
import 'dart:ui';

import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_router.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = "/";

  // Map<String,dynamic> mapArg;
  // SplashScreen({this.mapArg});
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

// Component UI
class _SplashScreenState extends State<SplashScreen> {
  // SharedPreferences prefs;

  /// Declare startTime to InitState
  @override
  void initState() {
    /*if(widget.mapArg != null){
      if(widget.mapArg['screen'] == MyOrdersScreen.routeName){
        initialHomeScreen=MyOrdersScreen();
      }
    }*/
    initSplashScreen();
    super.initState();
  }

  startTime(BuildContext context) async {
    return new Timer(Duration(milliseconds: 3000), () {
      // AppRouter.navigateToIntroScreen(context);
      // AppRouter.navigateToLoginScreen(context);
      // Navigator.of(context).pushReplacementNamed(SelectCityScreen.routeName);
      AppRouter.navigatorPage(context);
    });
  }

  /// To navigate layout change

  void initSplashScreen() async {
    AppLog.debugApp('splash screen : 1');
    // prefs = await SharedPreferences.getInstance();
    AppLog.debugApp('splash screen : 2');
    startTime(context);

  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Code Create UI Splash Screen
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height, minWidth: MediaQuery.of(context).size.width),
              color: MyColors.colorConvert('#5BB0EC'),
            ),
            Container(
                constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height, minWidth: MediaQuery.of(context).size.width),
                child: Center(
                  child: Image.asset(
                    "assets/app_logo.png",
                    fit: BoxFit.fill,
                    width: (MediaQuery.of(context).size.width/5)*2.5,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
