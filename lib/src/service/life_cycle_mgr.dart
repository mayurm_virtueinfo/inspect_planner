
import 'package:inspect_planner/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:inspect_planner/src/bloc/life_cycle_bloc.dart';

class LifeCycleMgr extends StatefulWidget {
  final Widget child;
  LifeCycleMgr({Key key, this.child}) : super(key: key);

  _LifeCycleMgrState createState() => _LifeCycleMgrState();
}

class _LifeCycleMgrState extends State<LifeCycleMgr>
    with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    locator<LifeCycleBloc>().lifeCycleEventSink.add(state.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
    );
  }
}