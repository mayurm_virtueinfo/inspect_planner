import 'package:inspect_planner/src/pages/splash_screen.dart';
import 'package:flutter/material.dart';

class NavigationService {
  GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false);
  }
  Future<dynamic> navigateToLogin(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false);
  }
  Future<dynamic> navigateToSplash(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName,(route)=>false);
  }
}
/*
{
  data:{
    "user_detail":{},
    "access_token":""
    "task_table":[],
    "task_type_table":[],
    "function_table":[],
    "location_table":[]
}
}*/
