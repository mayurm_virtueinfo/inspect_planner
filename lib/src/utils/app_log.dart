import 'dart:convert';

import 'package:flutter/material.dart';

class AppLog{
  static JsonDecoder decoder = JsonDecoder();
  static JsonEncoder encoder = JsonEncoder.withIndent('  ');
  static void debugApp(String message){
    debugPrint('App-Log --> $message');
  }
  static void debugApi(String message){
    debugPrint('Api-Log --> $message');
  }
  static void prettyPrintJson(Map<String, dynamic> input) {
    // Map<String, dynamic> object = decoder.convert(input);
    // var prettyString = encoder.convert(object);
    // prettyString.split('\n').forEach((element) => print(element));
  }
}