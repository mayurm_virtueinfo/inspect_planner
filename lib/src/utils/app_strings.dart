class AppStrings{
  static String notitie_toevoegen = "Notitie toevoegen";
  static String notitie = "Notitie";
  static String annuleren = "Annuleren";
  static String cancel = "Cancel";
  static String submit = "Submit";
  static String annuleer = "Annuleer";
  static String submitL = "Annuleer";
  static String cancelS = "Annuleren";
  static String cancelL = "Verstuur";

  static String bewaar = "Bewaar";
  static String opslaan = "Opslaan";
  static String verantwoordelijk = "Verantwoordelijk";
  static String forceTaskCompletion = "Taak geforceerd afronden";
  static String btnForceComplete = "Afronden";
  static String forceTaskCompletionMsg = "U kunt deze taak afronden zonder een checklist te voltooien door op 'Taak geforceerd afronden' te klikken.";
  static String forceTaskCompletionDialogMsg = "Wil je de taak geforceerd afronden?";
  static String forceTaskCompletionDialogTitle = "Taak geforceerd afronden?";



  static String cancelProcess= "Weet je het zeker dat je het synchronisatieproces wilt afbreken?";

  static String nee="Nee";
  static String ja="Ja";

  static String syncMessage="De taken worden gesynchroniseerd, dat kan 5 tot 30 seconden duren.";

  static String emailIsRequired="E-mailadres is verplicht om in te vullen";

  static String otpIsRequired="Invullen Google Authenticator code";

  static String invalidEmail="E-mailadres heeft geen geldig formaat.";

  static String scanQRCode="Scan de QR-code met de Google Authenticator App.";

  static String passwordIsRequired="Wachtwoord is verplicht om in te vullen.";

  static String version="Versie";

  static String build="Build";

  static String completeTask="Taak afronden";

  static String payAttension = 'Let op';

  static String confirmDeleteAttachment = 'Verwijderen';

  static String melding = 'Melding';

  static String linkedTo="Gekoppeld aan";

  static String digitRangeMsg = "Digit range should be";

  static String digitFromLabel = "from";

  static String digitToLabel = "to";

  static String lastSync="laatste sync:";

  static String commentNotLinked="Opmerking is niet gerelateerd aan een gebruiker";

  static String linkedToUser="Relateer aan gebruiker";

  static String err_error_inserting_task="Fout bij het toevoegen van de taak";

  static String addTask="Taak toevoegen";

  static String title="Titel";

  static String pleaseEnterTitle="Titel is een verplicht veld";

  static String taskType='Type taak';

  static String pleseSelectTaskType='Selecteer een type taak';

  static String pleseSelectFrequency='Selecteer de frequentie';

  static String taskFunction='Verantwoordelijke functie';

  static String pleaseSelectFunction='Selecteer de functie';

  static String taskLocation='Locatie';

  static String pleaseSelectLocation='Selecteer de locatie';

  static String deadlineDate='Datum deadline';

  static String errInsertingAttachment='Fout bij het toevoegen van een bijlage';

  static String errAddingTaskAttachment="Fout bij het toevoegen van een bijlage";

  static String executed = "Uitgevoerd";

  static String save='Bewaar';

  static String description='Omschrijving';

  static String pleaseEnterDescription='Omschrijving is een verplicht veld';

  static String pleaseAttachFile='Voeg een bijlage toe';

  static String deleteNote='Verwijder notitie';

  static String required2FA='Invullen Google Authenticator code';

  static String doYouReallyWantToDeleteNote='Wil je de notitie echt verwijderen?';

  static String errDeleteFromLocal="Error delete from local";

  static String remove='Verwijderen';

  static String errUpdatingNote="Fout bij het bijwerken van de notitie";

  static String errUpdatingTask='Fout bij het bijwerken van de taak';

  static String all='Alles';

  static String taskFilter='Taken filteren';

  static String function='Functie';

  static String location='Locatie';

  static String startDate='Begindatum';

  static String endDate='Einddatum';

  static String applyFilter='Filter toepassen';

  // static String taskFiles='Taakbestanden';

  static String fotos='Foto\'s';

  static String foto_uploaded='Foto uploaden';

  static String foto_upload_foto_take = 'Wilt u een bestaande foto uploaded of een foto maken?';

  static String camera = 'Camera';

  static String library = 'Bibliotheek';

  static String errAddingAttachment='Fout bij het toevoegen van de bijlage';

  static String errAddingChecklistAttachment="Fout bij het toevoegen de bijlage";

  static String bestand='Bestand';

  static String errDeletingAttachmentInChecklist='Fout bij het verwijderen van de bijlage';

  static String errDeletingAttachmentInTask='Fout bij het verwijderen van de bijlage';

  static String messages='Berichten';
  static String titleDialogUser='Gebruikersmeldingen';

  static String errAddingMsgViewLogInLocal="Error adding message view log in local";

  static String message='Bericht';

  static String enterNote='Voer notitie in';

  static String close='Sluit';

  static String next_message='Volgend bericht';

  static String id='ID';

  static String questing='Vraag';

  static String answer='Antwoord';

  static String photos='Foto\'s';

  static String log='Logboek';

  static String status='Status';

  static String errCompletingAnswer='Fout bij het verwerken van het antwoord';

  static String errSyncingCompleteTask="Fout bij het synchroniseren van de taak";

  static String noTaskUpdated='Er zijn geen taken bijgewerkt';

  static String errCompletingTask="Fout bij het verwerken van de taak";

  static String rounded='Afgerond';

  static String partlyImplemented='Deels uitgevoerd';

  static String planned='Gepland';

  static String tooLate='Te laat';

  static String toChecklist='Naar checklist';

  static String type="Type";

  static String deadline="Deadline";

  static String repeat="Herhaling";

  static String nextInspectionDate="Volgende Inspectie";

  static String madeBy="Aangemaakt door";

  static String lastUpdatedBy="laatst bijgewerkt door";

  static String info='Info';

  static String checklist='Checklist';

  static String note='Notities';

  static String previousTask='Vorige taak';

  static String nextTask='Volgende taak';

  static String addNote="Notitie toevoegen";

  static String noMsgToday="Er zijn geen taken vandaag";

  static String noMsgPast="Er zijn geen taken te laat";

  static String noMsgNotes="Er zijn geen notities voor deze taak";

  static String select="Selecteer";

  static String noMsgFuture="Er zijn geen taken gepland";

  static String noMsgCompleted="Er zijn geen taken afgerond";

  static String welcome="Welkom";

  static String taskOverview='Taken overzicht';


  static String logout='Uitloggen';

  static String today='Vandaag';

  static String searchByTitle='Zoeken op title';

  static String frequency='Frequentie';

  static String customers='Klanten';

  static String itIsNotPossibleToLogout="Het is niet mogelijk om uit te loggen";

  static String listOfCustomers='Lijst met klanten';

  static String toSearch='Zoeken...';

  static String customer='Klant';

  static String action='Actie';

  static String errWhileLogin="Fout bij het inloggen";

  static String errWhile2FA="Error While checking One Time Password";

  static String otpVerified="One Time Password Verified";

  static String otpVerificationFailed="OTP Verification Failed";

  static String login='Inloggen';

  static String emailAddress='E-mailadres';

  static String enterOTP='Vul de code in die in de Google Authenticator staat.';

  static String userNameString='gebruikersnaam';



  static String password='Wachtwoord';

  static String internetIsRequiredToLogin="Er is geen internet en daarom is het niet mogelijk om in te loggen. Zet Wifi aan en probeer het opnieuw.";

  static String noRouteFound='No routes found';

  static String inspectPlannerApp='Inspectieplanner 3.0';

  static String errGettingSupportText='Error getting support text';

  static String itIsNotPossibleToSync="Fout bij het synchroniseren";

  static String doYouReallyWantToFinishTask='Wil je de taak echt afronden?';

  static String completeChecklistBeforeComplete ='De taak heeft een checklist die niet is voltooid. Wil je deze checklist geforceerd afronden?';

  static String doYouWantToDeleteAttachment = 'Weet u zeker dat u dit item wilt verwijderen?';

  static String no='Nee';

  static String forcedRound='Geforceerd afronden';

  static String yes='Ja';

  static String comments='Opmerkingen';

  static String alles = 'Alles';

  static String no_task_data_found = 'Geen taken gevonden';

  static String sluit = 'Sluit';

  static String sluiten = 'Sluiten';

  static String task_synced = 'Taken zijn gesynchroniseerd';

  static String atchmnt_synced='Bijlagen zijn gesynchroniseerd';

  static String chklst_synced='Checklists zijn gesynchroniseerd';

  static String notes_synced='Notities zijn gesynchroniseerd';

  static String every_how_many_weeks='Om de hoeveel weken?';

  static String pleaseEnterValidWeek='Om de hoeveel weken is verplicht om in te vullen';

  static String every_how_many_months='Om de hoeveel maanden?';

  static String dayparts ='Dagdelen';
  static String days_of_the_week ='Dagen van de week';
  static String morning ='Ochtend';
  static String afternoon ='Middag';
  static String evening ='Avond';
  static String monday ='Maandag';
  static String tuesday ='Dinsdag';
  static String wednesday ='Woensdag';
  static String thursday ='Donderdag';
  static String friday ='Vrijdag';
  static String saturday ='Zaterdag';
  static String sunday ='Zondag';

  static String pleaseSelectDayparts = 'Selecteer een of meerdere dagdelen';

  static String pleaseSelectDaysOfWeek='Selecteer de dagen in de week';

  static String pleaseEnterNumberOfWeeks='Selecteer het aantal weken';

  static String pleaseEnterNumberOfMonth='Selecteer het aantal maanden';

}