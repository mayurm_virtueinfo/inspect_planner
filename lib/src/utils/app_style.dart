import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppStyles{
  static TextStyle errorStyle = TextStyle(
    color: MyColors.backgroundColor
  );
  static OutlineInputBorder focusBorder = OutlineInputBorder(
    borderSide: BorderSide(color: MyColors
        .backgroundColor),
  );
  static TextStyle drawerMenuTextStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: MyColors.backgroundColor);

  static TextStyle textFieldLabelStyle = TextStyle(
    fontSize: 14,
    color: MyColors.loginHintColor
  );
}