class AppTables{
  static const String TASK = 'task';
  static const String FUNCTIONS = 'functions';
  static const String TASK_TYPE = 'tasktype';
  static const String LOCATION = 'location';
  static const String TASK_FREQUENCY = 'taskfrequency';
  static const String CHECKLISTITEM = 'checklistitem';
  static const String NOTE = 'note';
  static const String ATTACHMENT='attachment';
  static const String USERS_GROUPS='users_groups';
  static const String USER='user';
  static const String NEWSPOSTS='newsposts';
  static const String MESSAGE_VIEW_LOG='message_view_log';
  static const String CUSTOMER='customer';
}