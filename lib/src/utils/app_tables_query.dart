import 'package:inspect_planner/src/utils/app_tables.dart';

class AppTablesQuery {
  static const String DELETE_FROM_TABLE_TASK = "DELETE FROM ${AppTables.TASK}";
  static const String DELETE_FROM_TABLE_TASK_TYPE = "DELETE FROM ${AppTables.TASK_TYPE}";
  static const String DELETE_FROM_TABLE_FREQUENCY = "DELETE FROM ${AppTables.TASK_FREQUENCY}";
  static const String DELETE_FROM_TABLE_LOCATION = "DELETE FROM ${AppTables.LOCATION}";
  static const String DELETE_FROM_TABLE_FUNCTIONS = "DELETE FROM ${AppTables.FUNCTIONS}";
  static const String DELETE_FROM_TABLE_ATTACHMENT = "DELETE FROM ${AppTables.ATTACHMENT}";
  static const String DELETE_FROM_TABLE_CHECKLISTITEM = "DELETE FROM ${AppTables.CHECKLISTITEM}";
  static const String DELETE_FROM_TABLE_NOTE = "DELETE FROM ${AppTables.NOTE}";
  static const String DELETE_FROM_TABLE_USER = "DELETE FROM ${AppTables.USER}";
  static const String DELETE_FROM_TABLE_USERS_GROUPS = "DELETE FROM ${AppTables.USERS_GROUPS}";
  static const String DELETE_FROM_TABLE_NEWSPOSTS = "DELETE FROM ${AppTables.NEWSPOSTS}";
  static const String DELETE_FROM_TABLE_MESSAGE_VIEW_LOG = "DELETE FROM ${AppTables.MESSAGE_VIEW_LOG}";
  static const String DELETE_FROM_TABLE_CUSTOMER = "DELETE FROM ${AppTables.CUSTOMER}";

  static const String CREATE_TABLE_TASK = "CREATE TABLE  ${AppTables.TASK} (" +
      "id INTEGER NOT NULL," +
      "customer_id INTEGER DEFAULT NULL," +
      "location_id INTEGER DEFAULT NULL," +
      "group_id INTEGER DEFAULT NULL," +
      "user_id INTEGER DEFAULT NULL," +
      "clone_task_id INTEGER DEFAULT NULL," +
      "title TEXT," +
      "description TEXT," +
      "create_date TEXT," +
      "removed INTEGER NOT NULL," +
      "stamitem INTEGER NOT NULL," +
      "customer_stamitem INTEGER NOT NULL," +
      "start_date TEXT DEFAULT NULL," +
      "tempate_id INTEGER DEFAULT NULL," +
      "type_id INTEGER DEFAULT NULL," +
      "frequency_id INTEGER DEFAULT NULL," +
      "last_edit_user_id INTEGER DEFAULT NULL," +
      "status_id INTEGER DEFAULT NULL," +
      "createdby_user_id INTEGER DEFAULT NULL," +
      "finish_date TEXT DEFAULT NULL," +
      "deadline_notification_count INTEGER NOT NULL DEFAULT '0'," +
      "frequency_daily_part_of_day INTEGER DEFAULT NULL," +
      "part_of_day_this_task INTEGER DEFAULT NULL," +
      "frequency_step_number INTEGER DEFAULT NULL," +
      "days_active INTEGER NOT NULL DEFAULT '0'," +
      "active_daypart INTEGER DEFAULT NULL," +
      "currunt_active_daypart INTEGER DEFAULT NULL," +
      "tasktype_name TEXT DEFAULT NULL," +
      "frequency_name TEXT DEFAULT NULL," +
      "location_name TEXT DEFAULT NULL," +
      "function_name TEXT DEFAULT NULL," +
      "createdby_user_name TEXT DEFAULT NULL," +
      "synced INTEGER NOT NULL," +
      "force_completed INTEGER DEFAULT 0" +
      ")";

  static const String INSERT_TABLE_TASK =
      "INSERT INTO ${AppTables.TASK} (id,  customer_id,  location_id,  group_id,  user_id,  clone_task_id,  title,  description,  create_date,  removed,  stamitem,  customer_stamitem,  start_date,  tempate_id,  type_id,  frequency_id,  last_edit_user_id,  status_id,  createdby_user_id,  finish_date,  deadline_notification_count,  frequency_daily_part_of_day,  part_of_day_this_task,  frequency_step_number, days_active, active_daypart, currunt_active_daypart, tasktype_name,  frequency_name,  "
      "location_name, function_name, createdby_user_name, synced, force_completed) VALUES";

  static const String CREATE_TABLE_FUNCTIONS = "CREATE TABLE  ${AppTables.FUNCTIONS} (" +
      "id INTEGER NOT NULL," +
      "customer_id INTEGER DEFAULT NULL," +
      "name TEXT NOT NULL," +
      "removed INTEGER NOT NULL," +
      "create_date TEXT NOT NULL," +
      "stamitem INTEGER NOT NULL," +
      "clone_usergroup_id INTEGER DEFAULT NULL," +
      "is_main_usergroup INTEGER DEFAULT NULL" +
    ")";

  static const String INSERT_TABLE_FUNCTIONS =
      "INSERT INTO ${AppTables.FUNCTIONS} (id,  customer_id,  name,  removed,  create_date,  stamitem, clone_usergroup_id, is_main_usergroup) VALUES";

  static const String CREATE_TABLE_TASK_TYPE = "CREATE TABLE  ${AppTables.TASK_TYPE} (" +
      "id INTEGER NOT NULL," +
      "name TEXT NOT NULL" +
    ")";
  static const String INSERT_TABLE_TASK_TYPE =
      "INSERT INTO ${AppTables.TASK_TYPE} (id,  name) VALUES";

  static const String CREATE_TABLE_LOCATION = "CREATE TABLE  ${AppTables.LOCATION} (" +
      "id INTEGER NOT NULL," +
      "customer_id INTEGER DEFAULT NULL," +
      "parent_id INTEGER DEFAULT NULL," +
      "name TEXT NOT NULL," +
      "removed INTEGER NOT NULL," +
      "create_date TEXT NOT NULL," +
      "stamitem INTEGER NOT NULL," +
      "clone_location_id INTEGER DEFAULT NULL" +
      ")";

  static const String INSERT_TABLE_LOCATION =
      "INSERT INTO ${AppTables.LOCATION} (id,  customer_id, parent_id, name, removed, create_date, stamitem, clone_location_id) VALUES";

  static const String CREATE_TABLE_TASK_FREQUENCY = "CREATE TABLE  ${AppTables.TASK_FREQUENCY} (" +
      "id INTEGER NOT NULL," +
      "name TEXT NOT NULL," +
      "date_modify TEXT NOT NULL," +
      "sort_order INTEGER NOT NULL" +
      ")";

  static const String INSERT_TABLE_TASK_FREQUENCY =
      "INSERT INTO ${AppTables.TASK_FREQUENCY} (id,  name, date_modify, sort_order) VALUES";


  static const String CREATE_TABLE_CHECKLISTITEM = "CREATE TABLE  ${AppTables.CHECKLISTITEM} (" +
      "id INTEGER NOT NULL," +
      "task_id INTEGER DEFAULT NULL," +
      "question TEXT NOT NULL," +
      "type INTEGER NOT NULL," +
      "result TEXT," +
      "removed INTEGER NOT NULL," +
      "description TEXT," +
      "order_number INTEGER DEFAULT NULL," +
      "unique_hash TEXT DEFAULT NULL," +
      "clone_checklist_id INTEGER DEFAULT NULL," +
      "synced INTEGER NOT NULL," +
      "digit_range TEXT DEFAULT NULL,"+
      "right_answer TEXT DEFAULT NULL,"+
      "wrong_answer TEXT DEFAULT NULL,"+
      "dropdown_answers TEXT DEFAULT NULL,"+
      "is_multiple INTEGER DEFAULT NULL"
      ")";

  static const String INSERT_TABLE_CHECKLISTITEM =
      "INSERT INTO ${AppTables.CHECKLISTITEM} (id, task_id, question, type, result, removed, description, order_number, unique_hash, clone_checklist_id, synced, digit_range, right_answer, wrong_answer, dropdown_answers, is_multiple ) VALUES";

  static const String CREATE_TABLE_NOTE = "CREATE TABLE  ${AppTables.NOTE} (" +
      "id INTEGER NOT NULL," +
      "user_id INTEGER DEFAULT NULL," +
      "task_id INTEGER DEFAULT NULL," +
      "note TEXT NOT NULL," +
      "create_date TEXT NOT NULL," +
      "assignedto_user_id INTEGER DEFAULT NULL," +
      "synced INTEGER NOT NULL," +
      "removed INTEGER NOT NULL" +
    ")";

  static const String INSERT_TABLE_NOTE =
      "INSERT INTO ${AppTables.NOTE} (id, user_id,  task_id,  note,  create_date,  assignedto_user_id, synced, removed ) VALUES";


  static const String CREATE_TABLE_ATTACHMENT = "CREATE TABLE  ${AppTables.ATTACHMENT} (" +
      "id INTEGER NOT NULL," +
      "attachments_id INTEGER DEFAULT NULL," +
      "createDate TEXT DEFAULT NULL," +
      "image_name TEXT DEFAULT NULL," +
      "removed INTEGER NOT NULL," +
      "Checklistitem_id INTEGER DEFAULT NULL," +
      "task_id INTEGER DEFAULT NULL," +
      "synced INTEGER NOT NULL" +
      ")";

  static const String INSERT_TABLE_ATTACHMENT =
      "INSERT INTO ${AppTables.ATTACHMENT} (id, attachments_id,  createDate,  image_name,  removed,  Checklistitem_id, task_id, synced ) VALUES";


  static const String CREATE_TABLE_USER = "CREATE TABLE  ${AppTables.USER} (" +
      "id INTEGER NOT NULL," +
      "customer_id INTEGER DEFAULT NULL," +
      "username TEXT NOT NULL," +
      "password TEXT DEFAULT NULL," +
      "name TEXT NOT NULL," +
      "phone_number TEXT NOT NULL," +
      "role TEXT DEFAULT NULL," +
      "is_active INTEGER NOT NULL," +
      "removed INTEGER NOT NULL," +
      "create_date TEXT NOT NULL," +
      "apiHash TEXT DEFAULT NULL," +
      "apiHash_endDate TEXT DEFAULT NULL," +
      "passwordResetHash TEXT DEFAULT NULL," +
      "is_manager INTEGER NOT NULL," +
      "skip_update_email INTEGER DEFAULT NULL" +
      ")";

  static const String INSERT_TABLE_USER =
      "INSERT INTO ${AppTables.USER} (id, customer_id, username, password, name, phone_number, role, is_active, removed, create_date, apiHash, apiHash_endDate, passwordResetHash, is_manager, skip_update_email) VALUES";


  static const String CREATE_TABLE_USERS_GROUPS = "CREATE TABLE  ${AppTables.USERS_GROUPS} (" +
      "user_id INTEGER DEFAULT NULL," +
      "usergroup_id INTEGER DEFAULT NULL," +
      "id INTEGER NOT NULL," +
      "is_main_usergroup INTEGER NOT NULL" +
      ")";

  static const String INSERT_TABLE_USERS_GROUPS =
      "INSERT INTO ${AppTables.USERS_GROUPS} (user_id, usergroup_id, id, is_main_usergroup) VALUES";

  static const String CREATE_TABLE_NEWSPOSTS = "CREATE TABLE  ${AppTables.NEWSPOSTS} (" +
      "id INTEGER NOT NULL," +
      "title TEXT NOT NULL," +
      "publication_date TEXT NOT NULL," +
      "due_date TEXT NOT NULL," +
      "status TEXT NOT NULL," +
      "created_at TEXT DEFAULT NULL," +
      "updated_at TEXT DEFAULT NULL," +
      "removed INTEGER NOT NULL DEFAULT 0," +
      "content TEXT" +
      ")";

  static const String INSERT_TABLE_NEWSPOSTS =
      "INSERT INTO ${AppTables.NEWSPOSTS} (id, title, publication_date, due_date, status, created_at, updated_at, removed, content) VALUES";

  static const String CREATE_TABLE_MESSAGE_VIEW_LOG = "CREATE TABLE  ${AppTables.MESSAGE_VIEW_LOG} (" +
      "id INTEGER NOT NULL," +
      "user_id int NOT NULL," +
      "message_id int NOT NULL," +
      "date TEXT NOT NULL," +
      "created_at TEXT DEFAULT NULL," +
      "updated_at TEXT DEFAULT NULL," +
      "synced INTEGER NOT NULL" +
    ")";

  static const String INSERT_TABLE_MESSAGE_VIEW_LOG =
      "INSERT INTO ${AppTables.MESSAGE_VIEW_LOG} (id, user_id, message_id, date, created_at, updated_at, synced) VALUES";

  static const String CREATE_TABLE_CUSTOMER = "CREATE TABLE  ${AppTables.CUSTOMER} (" +
      "id INTEGER NOT NULL," +
      "name TEXT NOT NULL," +
      "phone_number TEXT NOT NULL," +
      "is_active INTEGER NOT NULL," +
      "create_date TEXT NOT NULL," +
      "removed INTEGER NOT NULL," +
      "ipad_access INTEGER NOT NULL," +
      "dashboard_access INTEGER NOT NULL" +
      ")";

  static const String INSERT_TABLE_CUSTOMER =
      "INSERT INTO ${AppTables.CUSTOMER} (id, name, phone_number, is_active, create_date, removed, ipad_access, dashboard_access) VALUES";

}
