class Const{
  static String defaultProfileUrl =
      "https://firebasestorage.googleapis.com/v0/b/viecommerse.appspot.com/o/icon%2Favatar.png?alt=media&token=48325d36-29d1-4045-b94a-65a2cd0423fd";
  static String defaultCoverUrl =
      "https://firebasestorage.googleapis.com/v0/b/viecommerse.appspot.com/o/img%2FheaderProfile.png?alt=media&token=727ba335-4a99-4634-9ae6-9b08be93b9d8";

  static String defaultEventUrl = "https://www.artconnect.com/assets/default/default_event_list-16bafe1eff586f39e858135e25bc4922654e681a08e4229ea0a0e247bc4b84ba.png";

static const int CHKLIST_TYPE_1  = 1;
static const int CHKLIST_TYPE_2  = 2;
static const int CHKLIST_TYPE_3  = 3;
static const int CHKLIST_TYPE_4  = 4;
static const int CHKLIST_TYPE_5  = 5;
static const int CHKLIST_TYPE_6  = 6;

static Pattern regexChklistInputTextPattern = r'^[0-9,-]*';
static RegExp regexChklistInputText = RegExp(regexChklistInputTextPattern);
}
