import 'package:flutter_html/flutter_html.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/dialog/dialog_validation.dart';
import 'package:inspect_planner/src/service/navigation_service.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/widget_sync.dart';
import 'package:url_launcher/url_launcher.dart';

class DialogMgr {
  static Future<String> showSupportDialog(BuildContext mContext, Map<String, dynamic> mapSupport) async {
    dynamic data = mapSupport['data'];
    String supportText = data['support_text'];
    double screenWidth = MediaQuery.of(mContext).size.width;
    double screenHeight = MediaQuery.of(mContext).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(mContext);
    return showDialog<String>(
      context: mContext,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: Container(
              width: loginBoxWidth,
              height: loginBoxHeight,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: loginBoxWidth / 10, right: loginBoxWidth / 20),
                    // width: double.infinity,
                    height: loginBoxHeight / 8,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                      color: MyColors.colorConvert('#5BB0EC'),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
                        Text(AppStrings.inspectPlannerApp, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white)),
                        IconButton(
                          onPressed: () => Navigator.of(context).pop("cancel"),
                          icon: Icon(Icons.clear, color: MyColors.colorConvert('#365e77'), size: 30),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Html(
                      data: supportText,
                      style: {
                        "html": Style(
                          color: MyColors.colorConvert('#8d8d8d'),
                          lineHeight: LineHeight.number(1.5)
                        )
                      },
                      onLinkTap: (url, context, attributes, element) async{
                        debugPrint("url :$url");
                        // bool flagLaunch = await canLaunch(url);
                        // if(flagLaunch){
                          await launch(url);
                        // }else{
                        //   MyToast.showToast('Could not launch $url', mContext);
                        // }
                      //
                      },
                    ),
                  ),
                ],
              )),
        );
      },
    );
  }
  static Future<String> showDescriptionDialog(BuildContext mContext, String description) async {
    double screenWidth = MediaQuery.of(mContext).size.width;
    double screenHeight = MediaQuery.of(mContext).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(mContext);
    return showDialog<String>(
      context: mContext,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: Container(
              width: loginBoxWidth,
              height: loginBoxHeight,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: loginBoxWidth / 10, right: loginBoxWidth / 20),
                    // width: double.infinity,
                    height: loginBoxHeight / 8,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                      color: MyColors.colorConvert('#5BB0EC'),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
                        Text(AppStrings.description, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white)),
                        IconButton(
                          onPressed: () => Navigator.of(context).pop("cancel"),
                          icon: Icon(Icons.clear, color: MyColors.colorConvert('#365e77'), size: 30),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Html(
                      data: description,
                      style: {
                        "html": Style(
                            color: MyColors.colorConvert('#8d8d8d'),
                            lineHeight: LineHeight.number(1.5)
                        )
                      },
                      onLinkTap: (url, context, attributes, element) async{
                        debugPrint("url :$url");
                        // bool flagLaunch = await canLaunch(url);
                        // if(flagLaunch){
                        await launch(url);
                        // }else{
                        //   MyToast.showToast('Could not launch $url', mContext);
                        // }
                        //
                      },
                    ),
                  ),
                ],
              )),
        );
      },
    );
  }
  static Future<String> showProgressDialog(BuildContext mContext) async {
    return showDialog<String>(
      context: mContext,
      barrierDismissible: true, // user must tap button!
      barrierColor: Colors.white.withOpacity(0.5),
      builder: (BuildContext context) {
        return Dialog(
          key: locator<IPKeys>().keyDialogProgress,
          insetPadding: EdgeInsets.all(20),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Center(
            child: SizedBox(
              width: 40,
              height: 40,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                valueColor: AlwaysStoppedAnimation<Color>(MyColors.appAccentColor),
              ),
            ),
          ),
        );
      },
    );
  }
  static Future<String> showValidationDialog(BuildContext mContext) async {
    return showDialog(
      context: mContext,
      builder: (context) => DialogValidation(
        message: 'drMessage',
      ),
    );;
  }
  static void showSyncDialog({bool showCancelButton=false}){
    debugPrint("showSyncDialog");
    showDialog(
      barrierDismissible: false,
      context: locator<NavigationService>().navigatorKey.currentContext,
      builder: (context) => WidgetSync(showCancelButton: showCancelButton,),
    );
  }
  static void hideSyncDialog(){
    Navigator.of(locator<IPKeys>().keyAppSync.currentContext).pop();
  }
}
