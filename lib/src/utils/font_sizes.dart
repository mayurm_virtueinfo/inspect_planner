class FontSizes{
  static const double contact_font_size = 13.0;
  static const double junagadh_splash_screen_font_size = 20.0;
  static const double group_registration_font_size = 17.0;
  static const double admin_title_font_size = 20.0;
  static const double sponser_junagadh_font_size = 18.0;
}