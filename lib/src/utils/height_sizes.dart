class HeightSizes{
  static const double sponser_screen_top_bottom_padding = 25.0;
  static const double home_screen_admin_appbar_height = 35.0;
  static const double sponser_screen_img_app_sponcer_height = 110.0;
  static const double application_drawer_height = 150.0;
  static const double drawer_memnu_menu_height = 30.0;
}