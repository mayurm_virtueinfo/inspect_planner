import 'package:flutter/material.dart';

class MyColors{
  static Color get taskListHeaderBackColor => MyColors.colorConvert('#2a4c67');

  static Color get appBackgroundColor => MyColors.colorConvert('#f4f4f4');

  static Color get tabbarColor => MyColors.colorConvert('#4289c6');

  static Color get infoIconColor => MyColors.colorConvert('#e6fdff');

  static Color get viewAllColor => MyColors.colorConvert('#3fa1ff');

  static Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    if (color.length == 6) {
      return Color(int.parse("0xFF"+color));
    } else if (color.length == 8) {
      return Color(int.parse("0x"+color));
    }
  }

//  static Color get backgroundColor => MyColors.colorConvert("#ededed");
  static Color get backgroundColor => MyColors.colorConvert("#B1383D");
  static Color get white => Colors.white;
  static Color get borderColor => MyColors.colorConvert("#baaa9d");
  static Color get hintColor => MyColors.colorConvert("#c2c2c2");
  static Color get splashBackgroundColor => MyColors.colorConvert("#4B4C50");
  static Color get sponsorGradientColor3 => MyColors.colorConvert("#8F3036");
  static Color get sponsorGradientColor2 => MyColors.colorConvert("#A62D34");
  static Color get sponsorGradientColor1 => MyColors.colorConvert("#DC2128");
  static Color get sponsorButtonBackground => MyColors.colorConvert("#FDFEFF");
  static Color get sponsorButtonTextColor => MyColors.colorConvert("#2E328B");
  static Color get bottomNavigationBarColor => Colors.white;
  static Color get bottomNavigationBarBGColor => Colors.grey;
  static Color get appAccentColor => MyColors.colorConvert('#3FA1FF');
  static Color get loginHintColor => MyColors.colorConvert('#dbe1e6');
  static Color get noRecordFound => MyColors.colorConvert('#f85c5c');
  static Color get red => MyColors.colorConvert('#db1111');
  static Color get green => MyColors.colorConvert('#4db91c');
  static Color get orange => MyColors.colorConvert('#ef7d0b');
  static Color get skyBlue => MyColors.colorConvert('#3fa1ff');
  static Color get appBodyColor => MyColors.colorConvert('#f5f8fa');
  static Color get appDividerColor => MyColors.colorConvert('#E5EAED');
  static Color get appStopDelivery => MyColors.colorConvert('#DB1111');
  static Color get appStartDelivery => MyColors.colorConvert('#4DB91C');
  static Color get loginButtonColor => MyColors.colorConvert('#3A627D');
  static Color get loginButtonReveleColor => MyColors.colorConvert('#E8F0FE');
  static Color get appBarTitleColor => MyColors.colorConvert('#e3f1fb');
  static Color get tabbarTextColor => MyColors.colorConvert('#d8e7f3');
  static Color get cameraButtonColor => MyColors.colorConvert('#007AFF');
}