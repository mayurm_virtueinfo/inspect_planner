import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
class MyToast{
  /*static showToast(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: MyColors.appAccentColor,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }*/
  static showToast(String message, BuildContext context,{int length}){
    if(length!=null) {
      Toast.show(message, context, duration: length, gravity: Toast.BOTTOM, backgroundColor: MyColors.colorConvert('#5BB0EC'), textColor: Colors.white,);
    }else{
      Toast.show(message, context, duration: 2, gravity: Toast.BOTTOM, backgroundColor: MyColors.colorConvert('#5BB0EC'), textColor: Colors.white,);
    }
  }

}