import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/customer_bloc.dart';
import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/home_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/last_sync_date_time_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc_event.dart';
import 'package:inspect_planner/src/bloc/next_task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/prev_task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/show_checklist_tab.dart';
import 'package:inspect_planner/src/bloc/task_index_bloc.dart';
import 'package:inspect_planner/src/bloc/task_model_bloc.dart';
import 'package:inspect_planner/src/bloc/user_model_bloc.dart';
import 'package:inspect_planner/src/models/attachment_model.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/functions_list_model.dart';
import 'package:inspect_planner/src/models/functions_model.dart';
import 'package:inspect_planner/src/models/location_list_model.dart';
import 'package:inspect_planner/src/models/location_model.dart';
import 'package:inspect_planner/src/models/message_view_log_model.dart';
import 'package:inspect_planner/src/models/newsposts_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_frequency_list_model.dart';
import 'package:inspect_planner/src/models/task_frequency_model.dart';
import 'package:inspect_planner/src/models/task_list_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/task_type_list_model.dart';
import 'package:inspect_planner/src/models/task_type_model.dart';
import 'package:inspect_planner/src/models/user_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/models/users_groups_model.dart';
import 'package:inspect_planner/src/pages/home/home_screen.dart';
import 'package:inspect_planner/src/pages/home/home_screen_organization.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:inspect_planner/src/utils/app_preferences.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/app_tables_query.dart';
import 'package:inspect_planner/src/utils/app_user_role.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';

// import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:flutter_sms/flutter_sms.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import 'const.dart';

class Utility {
  static int taskSyncCount = 0;
  static int checklistSyncCount = 0;
  static int attachmentSyncCount = 0;
  static int noteSyncCount = 0;
  static bool isSyncingToWeb =false;

  static void clearSyncCount() {
    taskSyncCount = 0;
    checklistSyncCount = 0;
    attachmentSyncCount = 0;
    noteSyncCount = 0;
  }

  static int miniumCoupon = 5;

  static bool isEmailValid(String em) {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  static Future<String> getDeviceUUID() async {
    String deviceName;
    String deviceVersion;
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.toString();
        identifier = build.androidId; //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor; //UUID for iOS
      }
    } on PlatformException {
      throw Exception('Failed to get platform version');
    }

//if (!mounted) return;
    return identifier;
  }

  static double getDevicePixelRatio(BuildContext context) {
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    debugPrint("devicePixelRatio - original : $devicePixelRatio");
    if (devicePixelRatio >= 2) {
      devicePixelRatio = 1.75;
    }
    return devicePixelRatio;
  }

  static String getFormatedScheduledDate(DateTime selDate) {
    var formattedDate = DateFormat("E, MMM dd yyyy").format(selDate);
    //Mon, Feb 01 2021
    return formattedDate;
  }

  static String formatTimestamp(timestamp) {
    debugPrint("debug timestamp : ${timestamp}");
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch);
    var formattedDate = DateFormat("dd-MM-yyyy").format(date);
    return formattedDate;
  }

  static String formatMessageTimestamp(timestamp) {
    debugPrint("debug timestamp : ${timestamp}");
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch);
    var formattedDate = DateFormat("dd-MM-yyyy HH:mm:ss").format(date);
    return formattedDate;
  }

  static String formatEventTimestamp(timestamp) {
    debugPrint("debug timestamp : ${timestamp}");
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch);
    var formattedDate = DateFormat("dd-MM-yyyy hh:mm aa").format(date);
    return formattedDate;
  }

  static DateTime formatStringToDate({String date}) {
    DateTime newDate = DateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
    return newDate;
  }

  static String formatCurrentTime() {
    DateTime date = DateTime.now();
    var formattedDate = DateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    return formattedDate;
  }

  static String formatfromDateTime(DateTime dateTime) {
    var formattedDate = DateFormat("yyyy-MM-dd HH:mm:ss").format(dateTime);
    return formattedDate;
  }

  /* static void sendMessage(String message) async {
    String _result = await sendSMS(message: message, recipients: recipents)
        .catchError((onError) {
      print(onError);
    });
    print(_result);
  }*/
  static String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;
  }

  static Color getStatusColor(String status) {
    Color dtStatusColor = Colors.yellow;
    if (status == '0') {
      dtStatusColor = Colors.green;
    } else if (status == '1') {
      dtStatusColor = MyColors.colorConvert('#F39B44');
    } else if (status == '3') {
      dtStatusColor = Colors.red;
    }
    return dtStatusColor;
  }

  static Future<int> getAttachmentCount({TaskModel taskModel}) async {
    String sqlAttachCount = "select count(*) from ${AppTables.ATTACHMENT} where task_id = ? and removed != ?";
    List<dynamic> sqlValues = [taskModel.id, 1];
    List<Map<String, dynamic>> listStatus = await appDatabase.rawQuery(sqlAttachCount, sqlValues);
    int qCount = Sqflite.firstIntValue(listStatus);
    // debugPrint("count attachment : ${qCount}");
    // blocAttachmentIcon.attachmentIconEventSink.add(qCount);
    return Future.value(qCount);
  }

  static Future<List<Map<String, dynamic>>> getAttachment({TaskModel taskModel}) async {
    String sqlAttachCount = "select * from ${AppTables.ATTACHMENT} where task_id = ? and removed != ?";
    List<dynamic> sqlValues = [taskModel.id, 1];
    List<Map<String, dynamic>> listAttachments = await appDatabase.rawQuery(sqlAttachCount, sqlValues);
    // blocAttachmentIcon.attachmentIconEventSink.add(qCount);
    return Future.value(listAttachments);
  }

  static Future<int> getCheckListAttachmentCount({CheckListItemModel chkLstItmModel}) async {
    String sqlAttachCount = "select count(*) from ${AppTables.ATTACHMENT} where Checklistitem_id = ? and removed != ?";
    List<dynamic> sqlValues = [chkLstItmModel.id, 1];
    List<Map<String, dynamic>> listStatus = await appDatabase.rawQuery(sqlAttachCount, sqlValues);
    int qCount = Sqflite.firstIntValue(listStatus);
    // debugPrint("count attachment : ${qCount}");
    // blocAttachmentIcon.attachmentIconEventSink.add(qCount);
    return Future.value(qCount);
  }

  static Future<List<Map<String, dynamic>>> getCheckListAttachment({CheckListItemModel chkLstItmModel}) async {
    String sqlAttachCount = "select * from ${AppTables.ATTACHMENT} where Checklistitem_id = ? and removed != ?";
    List<dynamic> sqlValues = [chkLstItmModel.id, 1];
    List<Map<String, dynamic>> listAttachments = await appDatabase.rawQuery(sqlAttachCount, sqlValues);
    // blocAttachmentIcon.attachmentIconEventSink.add(qCount);
    return Future.value(listAttachments);
  }

  static bool isSameDate(DateTime firstDate, DateTime secondDate) {
    return firstDate.year == secondDate.year && firstDate.month == secondDate.month && firstDate.day == secondDate.day;
  }

  static Future<bool> logoutData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    await clearDatabase();
    return Future.value(true);
  }

  static Future<bool> clearDatabase() async {
    Batch batchDropTable = appDatabase.batch();
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_TASK);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_TASK_TYPE);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_FREQUENCY);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_LOCATION);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_FUNCTIONS);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_ATTACHMENT);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_CHECKLISTITEM);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_NOTE);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_USER);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_USERS_GROUPS);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_NEWSPOSTS);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_MESSAGE_VIEW_LOG);
    batchDropTable.execute(AppTablesQuery.DELETE_FROM_TABLE_CUSTOMER);
    List<dynamic> res = await batchDropTable.commit(continueOnError: true);
    debugPrint("Drop table response : ${res.toString()}");
    return Future.value(true);
  }

  static void printJson(String log, dynamic json) {
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(json);
    debugPrint("-------------------$log-------------------");
    debugPrint(prettyprint);
    debugPrint("-------------------$log-end-------------------");
  }

  static String getFrequencyName({TaskModel taskModel}) {
    // debugPrint("--frequency : getFrequencyName");
    String frequencyName = '';
    if (taskModel.frequency_id != null && taskModel.frequency_id == 2) {
      // debugPrint("--frequency : 1");
      int frequency_step_number = taskModel.frequency_step_number;
      // debugPrint("---ma-frequency_step_number ${taskModel.title} $frequency_step_number");
      if (frequency_step_number != null && frequency_step_number == 0) {
        // debugPrint("--frequency : 2");
        frequencyName = 'Elke Dag';
      } else if (frequency_step_number != null && frequency_step_number == 127) {
        // debugPrint("--frequency : 3");
        frequencyName = 'Elke Dag';
      } else {
        // debugPrint("--frequency : 4");
        if (frequency_step_number == null) {
          // debugPrint("--frequency : 5");
          frequencyName = 'Elke Dag';
        } else {
          // debugPrint("--frequency : 6");
          List<int> dayAray = GetFrequencyStepNumber(frequency_step_number);
          // debugPrint("----frequency_step_number : ${taskModel.title} : ${dayAray}");
          frequencyName = GetFrequencyStepNumberNameDisplay(dayAray);
        }
      }
    } else {
      // debugPrint("--frequency : 7");
      frequencyName = taskModel.frequency_name;
    }
    return frequencyName;
  }

  static List<int> GetFrequencyStepNumber(int frequency_step_number) {
    // debugPrint("frequency_step_number : $frequency_step_number ");
    List<int> dayArray = [];
    if (frequency_step_number >= 64) {
      // $day_array[] = 7;
      dayArray.add(7);
      frequency_step_number = frequency_step_number - 64;
    }

    if (frequency_step_number >= 32) {
      dayArray.add(6);
      frequency_step_number = frequency_step_number - 32;
    }

    if (frequency_step_number >= 16) {
      dayArray.add(5);
      frequency_step_number = frequency_step_number - 16;
    }

    if (frequency_step_number >= 8) {
      dayArray.add(4);
      frequency_step_number = frequency_step_number - 8;
    }

    if (frequency_step_number >= 4) {
      dayArray.add(3);
      frequency_step_number = frequency_step_number - 4;
    }

    if (frequency_step_number >= 2) {
      dayArray.add(2);
      frequency_step_number = frequency_step_number - 2;
    }

    if (frequency_step_number >= 1) {
      dayArray.add(1);
      frequency_step_number = frequency_step_number - 1;
    }

    return dayArray;
  }

  static String GetFrequencyStepNumberNameDisplay(List<int> dayArray) {
    List<String> finalArray = [];

    if (dayArray.contains(1)) {
      finalArray.add('Ma');
    }
    if (dayArray.contains(2)) {
      finalArray.add('Di');
    }
    if (dayArray.contains(3)) {
      finalArray.add('Wo');
    }
    if (dayArray.contains(4)) {
      finalArray.add('Do');
    }
    if (dayArray.contains(5)) {
      finalArray.add('Vr');
    }
    if (dayArray.contains(6)) {
      finalArray.add('Za');
    }
    if (dayArray.contains(7)) {
      finalArray.add('Zo');
    }
    return finalArray.join(', ');
  }

  static Future<void> checkForChecklistTab({TaskModel taskModel}) async {
    String sql = "select * from ${AppTables.CHECKLISTITEM} where task_id = ${taskModel.id} and removed = 0 order by order_number asc";
    List<Map<String, dynamic>> currentChecklist = await appDatabase.rawQuery(sql);
    debugPrint('sql detal checklist - length :${currentChecklist.length} : ${sql}');

    if (currentChecklist.length > 0) {
      locator<ShowHideChecklistTabBloc>().showHideChecklistTabEventSink.add(true);
    } else {
      locator<ShowHideChecklistTabBloc>().showHideChecklistTabEventSink.add(false);
    }
  }

  static Future<void> processSyncWebToApp({BuildContext mContext, Map<String, dynamic> apiResult}) async {
    debugPrint("test------7");
    if (appUserModel.role == AppUserRole.ROLE_ORGANISATION) {
      debugPrint("test------8");
      Map<String, dynamic> customerResult = await ApiRequest.getOrganizationClientList(accessToken: appUserModel.accessToken);
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      if (!customerResult['status']) {
        MyToast.showToast(customerResult['message'], mContext);
        return;
      }

      debugPrint('-----------------------------------------');
      debugPrint("trying---customer--");
      // Adding tasktype in database
      List<dynamic> customerData = customerResult['data'];
      List<CustomerModel> listCustomer = [];
      customerData.forEach((element) {
        CustomerModel customerModel = CustomerModel.fromMap(map: element);
        listCustomer.add(customerModel);
      });

      Batch batchCustomer = appDatabase.batch();
      listCustomer.forEach((CustomerModel tskModel) async {
        String sql = AppTablesQuery.INSERT_TABLE_CUSTOMER + '(?,?,?,?,?,?,?,?)';
        batchCustomer.rawInsert(sql, tskModel.getDataList(tskModel));
      });
      List<dynamic> customerCommitResult = await batchCustomer.commit(continueOnError: true);
      Utility.printJson('customerCommitResult', customerCommitResult);
      // Navigator.of(mContext).pushReplacementNamed(HomeScreenOrganication.routeName);
      return;
    }
    // debugPrint("test------9");
    // Map<String, dynamic> customerDetail = apiResult['customer'];
    // CustomerModel customerModel = CustomerModel.fromMap(map: customerDetail);
    // locator<CustomerBloc>().customerEventSink.add(customerModel);
    // await prefs.setString(AppPreferences.pref_customer, json.encode(customerDetail));
    // debugPrint("test------10");
    debugPrint('-----------------------------------------');
    debugPrint("trying---tasktype--");
    // Adding tasktype in database
    List<dynamic> taskTypeData = apiResult['tasktype'];
    TaskTypeListModel taskTypeListModel = TaskTypeListModel();
    List<TaskTypeModel> listTask = await taskTypeListModel.getTaskList(taskTypeData);

    Batch batchTaskType = appDatabase.batch();
    listTask.forEach((TaskTypeModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_TASK_TYPE + '(?,?)';
      batchTaskType.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---functions--");
    // Adding functions in database
    Batch batchFunctions = appDatabase.batch();
    List<dynamic> functionsData = apiResult['functions'];
    FunctionsListModel functionListModel = FunctionsListModel();
    List<FunctionsModel> listFunctions = await functionListModel.getTaskList(functionsData);
    listFunctions.forEach((FunctionsModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_FUNCTIONS + '(?,?,?,?,?,?,?,?)';
      batchFunctions.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---location--");
    // Adding location in database
    Batch batchLocation = appDatabase.batch();
    List<dynamic> locationData = apiResult['location'];
    LocationListModel locationListModel = LocationListModel();
    List<LocationModel> listLocations = await locationListModel.getTaskList(locationData);
    listLocations.forEach((LocationModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_LOCATION + '(?,?,?,?,?,?,?,?)';
      batchLocation.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---taskfrequency--");
    // Adding frequency in database
    Batch batchFrequency = appDatabase.batch();
    List<dynamic> taskFrequencyData = apiResult['taskfrequency'];
    TaskFrequencyListModel taskFrequencyListModel = TaskFrequencyListModel();
    List<TaskFrequencyModel> listTaskFrequency = await taskFrequencyListModel.getTaskList(taskFrequencyData);

    listTaskFrequency.forEach((TaskFrequencyModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_TASK_FREQUENCY + '(?,?,?,?)';
      batchFrequency.rawInsert(sql, tskModel.getDataList(tskModel));
    });
    //

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---user--");
    // Adding user in database
    Batch batchUser = appDatabase.batch();
    List<dynamic> userData = apiResult['user'];
    debugPrint("trying---user-- ${userData}");
    debugPrint("trying---user--from utility - processSyncWebToApp : ${userData}");
    List<UserTableModel> listUser = [];
    userData.forEach((element) {
      UserTableModel userTableModel = UserTableModel.fromMap(map: element);
      listUser.add(userTableModel);
    });

    listUser.forEach((UserTableModel usrTableModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_USER + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      batchUser.rawInsert(sql, usrTableModel.getDataList(usrTableModel));
    });
    //

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---users_groups--");
    // Adding usersgroups in database
    Batch batchUsersGroups = appDatabase.batch();
    List<dynamic> usersGroupsData = apiResult['users_groups'];
    List<UsersGroupsModel> listUsersGroups = [];
    usersGroupsData.forEach((element) {
      UsersGroupsModel userTableModel = UsersGroupsModel.fromMap(map: element);
      listUsersGroups.add(userTableModel);
    });

    listUsersGroups.forEach((UsersGroupsModel usrTableModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_USERS_GROUPS + '(?,?,?,?)';
      List<dynamic> values = usrTableModel.getDataList(usrTableModel);
      batchUsersGroups.rawInsert(sql, values);
    });
    //

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---messages ( newsposts ) --");
    // Adding functions in database
    Batch batchNewsposts = appDatabase.batch();
    List<dynamic> newspostsData = apiResult['messages'];
    List<NewspostsModel> listNewsposts = [];
    newspostsData.forEach((element) {
      NewspostsModel newpostsModel = NewspostsModel.fromMap(map: element);
      listNewsposts.add(newpostsModel);
    });
    listNewsposts.forEach((NewspostsModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_NEWSPOSTS + '(?,?,?,?,?,?,?,?,?)';
      batchNewsposts.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    debugPrint('-----------------------------------------');
    debugPrint("trying---message_view_log ( newsposts ) --");
    // Adding functions in database
    Batch batchMessageViewLog = appDatabase.batch();
    List<dynamic> messageViewLogData = apiResult['message_view_log'];
    List<MessageViewLogModel> listMessageViewLog = [];
    messageViewLogData.forEach((element) {
      MessageViewLogModel msgViewLogModel = MessageViewLogModel.fromMap(map: element);
      msgViewLogModel.synced = 1;
      listMessageViewLog.add(msgViewLogModel);
    });
    listMessageViewLog.forEach((MessageViewLogModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_MESSAGE_VIEW_LOG + '(?,?,?,?,?,?,?)';
      batchMessageViewLog.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    // Syncing Task

    try {
      Map<String, dynamic> apiResult = await ApiRequest.getTaskTable();

      // check status
      bool status = apiResult['status'];
      // message
      List<dynamic> data = apiResult['data'];
      // status condition
      if (status) {
        TaskListModel taskListModel = TaskListModel();
        List<TaskModel> listTask = await taskListModel.getTaskList(data);

        Batch batchTaskList = appDatabase.batch();
        Batch batchCheckListItem = appDatabase.batch();
        Batch batchNoteListItem = appDatabase.batch();
        Batch batchAttachmentListItem = appDatabase.batch();
        listTask.forEach((TaskModel tskModel) async {
          // Adding checklistitem in database
          List<CheckListItemModel> chkListItmMdl = tskModel.checklistitem;
          if (chkListItmMdl.length > 0) {
            chkListItmMdl.forEach((CheckListItemModel element) {
              element.synced = 1;
              String sqlChkLstItmMdl = AppTablesQuery.INSERT_TABLE_CHECKLISTITEM + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
              batchCheckListItem.rawInsert(sqlChkLstItmMdl, element.getChkListDataList(element));

              element.checklist_attachment.forEach((eleChkLstAtchmnt) {
                eleChkLstAtchmnt.synced = 1;
                String sqlAtchmntLstItmMdl = AppTablesQuery.INSERT_TABLE_ATTACHMENT + '(?,?,?,?,?,?,?,?)';
                batchAttachmentListItem.rawInsert(sqlAtchmntLstItmMdl, eleChkLstAtchmnt.getDataList(eleChkLstAtchmnt));
              });
            });
          }

          // Adding task_note in database
          List<NoteModel> noteList = tskModel.task_note;
          if (noteList.length > 0) {
            noteList.forEach((NoteModel element) {
              element.synced = 1;
              element.removed = 0;
              String sqlNoteLstItmMdl = AppTablesQuery.INSERT_TABLE_NOTE + '(?,?,?,?,?,?,?,?)';
              batchNoteListItem.rawInsert(sqlNoteLstItmMdl, element.getDataList(element));
            });
          }

          // Adding attachment in database
          List<AttachmentModel> attachmentList = tskModel.task_attachment;
          if (attachmentList.length > 0) {
            attachmentList.forEach((AttachmentModel element) {
              element.synced = 1;
              String sqlAtchmntLstItmMdl = AppTablesQuery.INSERT_TABLE_ATTACHMENT + '(?,?,?,?,?,?,?,?)';
              batchAttachmentListItem.rawInsert(sqlAtchmntLstItmMdl, element.getDataList(element));
            });
          }

          tskModel.synced = 1;
          String sql = AppTablesQuery.INSERT_TABLE_TASK + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
          batchTaskList.rawInsert(sql, tskModel.getDataList(tskModel));
        });
        try {
          DateTime syncStartTime = DateTime.now();
          List responses = await Future.wait([
            batchTaskType.commit(continueOnError: true),
            batchFunctions.commit(continueOnError: true),
            batchLocation.commit(continueOnError: true),
            batchFrequency.commit(continueOnError: true),
            batchTaskList.commit(continueOnError: true),
            batchCheckListItem.commit(continueOnError: true),
            batchNoteListItem.commit(continueOnError: true),
            batchAttachmentListItem.commit(continueOnError: true),
            batchUser.commit(continueOnError: true),
            batchUsersGroups.commit(continueOnError: true),
            batchNewsposts.commit(continueOnError: true),
            batchMessageViewLog.commit(continueOnError: true)
          ]);
          Utility.addLastSyncTime();
          DateTime syncEndTime = DateTime.now();
          Duration dur = syncEndTime.difference(syncStartTime);
          debugPrint("Sync time : ${dur.inSeconds}");
          debugPrint("Sync success : ${responses.toString()}");
        } catch (e) {
          debugPrint("Error syncing data : ${e.toString()}");
        }

        /*locator<NewspostsBloc>().newspostsEventSink.add(listNewsposts);*/

        // Navigator.of(mContext).pushReplacementNamed(HomeScreen.routeName);
      } else {
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        MyToast.showToast(AppStrings.no_task_data_found, mContext);
      }
    } catch (e) {
      MyToast.showToast(e.toString(), mContext);
    }
  }

  static Future<void> processLogin({BuildContext mContext, Map<String, dynamic> apiResult}) async {
    debugPrint("trying---user_detail--");
    debugPrint("test------1");
    Map<String, dynamic> userDetail = apiResult['user_detail'];
    debugPrint("test------2");
    userDetail['access_token'] = apiResult['access_token'];
    debugPrint("test------3");
    UserModel userModel = UserModel.fromMap(map: userDetail);
    debugPrint("test------4");
    locator<UserModelBloc>().userModelEventSink.add(userModel);
    debugPrint("test------5");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    debugPrint("test------6");
    await prefs.setString(AppPreferences.pref_user, json.encode(userDetail));
    debugPrint("test------7");
    if (userModel.role == AppUserRole.ROLE_ORGANISATION) {
      debugPrint("test------8");
      Map<String, dynamic> customerResult = await ApiRequest.getOrganizationClientList(accessToken: userModel.accessToken);
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      if (!customerResult['status']) {
        MyToast.showToast(customerResult['message'], mContext);
        return;
      }

      debugPrint('-----------------------------------------');
      debugPrint("trying---customer--");
      // Adding tasktype in database
      List<dynamic> customerData = customerResult['data'];
      List<CustomerModel> listCustomer = [];
      customerData.forEach((element) {
        CustomerModel customerModel = CustomerModel.fromMap(map: element);
        listCustomer.add(customerModel);
      });

      Batch batchCustomer = appDatabase.batch();
      listCustomer.forEach((CustomerModel tskModel) async {
        String sql = AppTablesQuery.INSERT_TABLE_CUSTOMER + '(?,?,?,?,?,?,?,?)';
        batchCustomer.rawInsert(sql, tskModel.getDataList(tskModel));
      });
      List<dynamic> customerCommitResult = await batchCustomer.commit(continueOnError: true);
      Utility.printJson('customerCommitResult', customerCommitResult);
      Navigator.of(mContext).pushReplacementNamed(HomeScreenOrganication.routeName);
      return;
    }
    debugPrint("test------9");
    Map<String, dynamic> customerDetail = apiResult['customer'];
    CustomerModel customerModel = CustomerModel.fromMap(map: customerDetail);
    locator<CustomerBloc>().customerEventSink.add(customerModel);
    await prefs.setString(AppPreferences.pref_customer, json.encode(customerDetail));
    debugPrint("test------10");
    debugPrint('-----------------------------------------');
    debugPrint("trying---tasktype--");
    // Adding tasktype in database
    List<dynamic> taskTypeData = apiResult['tasktype'];
    TaskTypeListModel taskTypeListModel = TaskTypeListModel();
    List<TaskTypeModel> listTask = await taskTypeListModel.getTaskList(taskTypeData);

    Batch batchTaskType = appDatabase.batch();
    listTask.forEach((TaskTypeModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_TASK_TYPE + '(?,?)';
      batchTaskType.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---functions--");
    // Adding functions in database
    Batch batchFunctions = appDatabase.batch();
    List<dynamic> functionsData = apiResult['functions'];
    FunctionsListModel functionListModel = FunctionsListModel();
    List<FunctionsModel> listFunctions = await functionListModel.getTaskList(functionsData);
    listFunctions.forEach((FunctionsModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_FUNCTIONS + '(?,?,?,?,?,?,?,?)';
      batchFunctions.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---location--");
    // Adding location in database
    Batch batchLocation = appDatabase.batch();
    List<dynamic> locationData = apiResult['location'];
    LocationListModel locationListModel = LocationListModel();
    List<LocationModel> listLocations = await locationListModel.getTaskList(locationData);
    listLocations.forEach((LocationModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_LOCATION + '(?,?,?,?,?,?,?,?)';
      batchLocation.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---taskfrequency--");
    // Adding frequency in database
    Batch batchFrequency = appDatabase.batch();
    List<dynamic> taskFrequencyData = apiResult['taskfrequency'];
    TaskFrequencyListModel taskFrequencyListModel = TaskFrequencyListModel();
    List<TaskFrequencyModel> listTaskFrequency = await taskFrequencyListModel.getTaskList(taskFrequencyData);

    listTaskFrequency.forEach((TaskFrequencyModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_TASK_FREQUENCY + '(?,?,?,?)';
      batchFrequency.rawInsert(sql, tskModel.getDataList(tskModel));
    });
    //

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---user--");
    // Adding user in database
    Batch batchUser = appDatabase.batch();
    List<dynamic> userData = apiResult['user'];

    List<UserTableModel> listUser = [];
    userData.forEach((element) {
      UserTableModel userTableModel = UserTableModel.fromMap(map: element);
      listUser.add(userTableModel);
    });

    listUser.forEach((UserTableModel usrTableModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_USER + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      batchUser.rawInsert(sql, usrTableModel.getDataList(usrTableModel));
    });
    //

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---users_groups--");
    // Adding usersgroups in database
    Batch batchUsersGroups = appDatabase.batch();
    List<dynamic> usersGroupsData = apiResult['users_groups'];
    List<UsersGroupsModel> listUsersGroups = [];
    usersGroupsData.forEach((element) {
      UsersGroupsModel userTableModel = UsersGroupsModel.fromMap(map: element);
      listUsersGroups.add(userTableModel);
    });

    listUsersGroups.forEach((UsersGroupsModel usrTableModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_USERS_GROUPS + '(?,?,?,?)';
      List<dynamic> values = usrTableModel.getDataList(usrTableModel);
      batchUsersGroups.rawInsert(sql, values);
    });
    //

    //
    debugPrint('-----------------------------------------');
    debugPrint("trying---messages ( newsposts ) --");
    // Adding functions in database
    Batch batchNewsposts = appDatabase.batch();
    List<dynamic> newspostsData = apiResult['messages'];
    List<NewspostsModel> listNewsposts = [];
    newspostsData.forEach((element) {
      NewspostsModel newpostsModel = NewspostsModel.fromMap(map: element);
      listNewsposts.add(newpostsModel);
    });
    listNewsposts.forEach((NewspostsModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_NEWSPOSTS + '(?,?,?,?,?,?,?,?,?)';
      batchNewsposts.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    debugPrint('-----------------------------------------');
    debugPrint("trying---message_view_log ( newsposts ) --");
    // Adding functions in database
    Batch batchMessageViewLog = appDatabase.batch();
    List<dynamic> messageViewLogData = apiResult['message_view_log'];
    List<MessageViewLogModel> listMessageViewLog = [];
    messageViewLogData.forEach((element) {
      MessageViewLogModel msgViewLogModel = MessageViewLogModel.fromMap(map: element);
      msgViewLogModel.synced = 1;
      listMessageViewLog.add(msgViewLogModel);
    });
    listMessageViewLog.forEach((MessageViewLogModel tskModel) async {
      String sql = AppTablesQuery.INSERT_TABLE_MESSAGE_VIEW_LOG + '(?,?,?,?,?,?,?)';
      batchMessageViewLog.rawInsert(sql, tskModel.getDataList(tskModel));
    });

    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
    // Syncing Task

    try {
      DialogMgr.showSyncDialog(showCancelButton: false);

      Map<String, dynamic> apiResult = await ApiRequest.getTaskTable();
      // debugPrint('getTaskTable : $apiResult');
      // check status
      bool status = apiResult['status'];
      // message
      List<dynamic> data = apiResult['data'];
      // status condition
      if (status) {
        TaskListModel taskListModel = TaskListModel();
        List<TaskModel> listTask = await taskListModel.getTaskList(data);

        Batch batchTaskList = appDatabase.batch();
        Batch batchCheckListItem = appDatabase.batch();
        Batch batchNoteListItem = appDatabase.batch();
        Batch batchAttachmentListItem = appDatabase.batch();
        listTask.forEach((TaskModel tskModel) async {
          // Adding checklistitem in database
          List<CheckListItemModel> chkListItmMdl = tskModel.checklistitem;
          if (chkListItmMdl.length > 0) {
            chkListItmMdl.forEach((CheckListItemModel element) {
              element.synced = 1;
              String sqlChkLstItmMdl = AppTablesQuery.INSERT_TABLE_CHECKLISTITEM + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
              batchCheckListItem.rawInsert(sqlChkLstItmMdl, element.getChkListDataList(element));

              element.checklist_attachment.forEach((eleChkLstAtchmnt) {
                eleChkLstAtchmnt.synced = 1;
                String sqlAtchmntLstItmMdl = AppTablesQuery.INSERT_TABLE_ATTACHMENT + '(?,?,?,?,?,?,?,?)';
                batchAttachmentListItem.rawInsert(sqlAtchmntLstItmMdl, eleChkLstAtchmnt.getDataList(eleChkLstAtchmnt));
              });
            });
          }

          // Adding task_note in database
          List<NoteModel> noteList = tskModel.task_note;
          if (noteList.length > 0) {
            noteList.forEach((NoteModel element) {
              element.synced = 1;
              element.removed = 0;
              String sqlNoteLstItmMdl = AppTablesQuery.INSERT_TABLE_NOTE + '(?,?,?,?,?,?,?,?)';
              batchNoteListItem.rawInsert(sqlNoteLstItmMdl, element.getDataList(element));
            });
          }

          // Adding attachment in database
          List<AttachmentModel> attachmentList = tskModel.task_attachment;
          if (attachmentList.length > 0) {
            attachmentList.forEach((AttachmentModel element) {
              element.synced = 1;
              String sqlAtchmntLstItmMdl = AppTablesQuery.INSERT_TABLE_ATTACHMENT + '(?,?,?,?,?,?,?,?)';
              batchAttachmentListItem.rawInsert(sqlAtchmntLstItmMdl, element.getDataList(element));
            });
          }

          tskModel.synced = 1;
          String sql = AppTablesQuery.INSERT_TABLE_TASK + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
          batchTaskList.rawInsert(sql, tskModel.getDataList(tskModel));
        });
        try {
          DateTime syncStartTime = DateTime.now();
          List responses = await Future.wait([
            batchTaskType.commit(continueOnError: true),
            batchFunctions.commit(continueOnError: true),
            batchLocation.commit(continueOnError: true),
            batchFrequency.commit(continueOnError: true),
            batchTaskList.commit(continueOnError: true),
            batchCheckListItem.commit(continueOnError: true),
            batchNoteListItem.commit(continueOnError: true),
            batchAttachmentListItem.commit(continueOnError: true),
            batchUser.commit(continueOnError: true),
            batchUsersGroups.commit(continueOnError: true),
            batchNewsposts.commit(continueOnError: true),
            batchMessageViewLog.commit(continueOnError: true)
          ]);
          Utility.addLastSyncTime();
          DateTime syncEndTime = DateTime.now();
          Duration dur = syncEndTime.difference(syncStartTime);
          debugPrint("Sync time : ${dur.inSeconds}");
          debugPrint("Sync success : ${responses.toString()}");
        } catch (e) {
          debugPrint("Error syncing data : ${e.toString()}");
          throw Exception(e);
        }

        /*locator<NewspostsBloc>().newspostsEventSink.add(listNewsposts);*/
        DialogMgr.hideSyncDialog();

        Navigator.of(mContext).pushReplacementNamed(HomeScreen.routeName);
      } else {
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        MyToast.showToast(AppStrings.no_task_data_found, mContext);
      }
    } catch (e) {
      debugPrint("Error --123-- : ${e.toString()}");
      // MyToast.showToast(e.toString(), mContext);
      throw Exception(e);
    }
  }

  static String getTitle(TaskModel tmpTaskModel) {
    String title = '';
    if (tmpTaskModel.frequency_id == 2) {

      if(tmpTaskModel.frequency_daily_part_of_day != 0){
        if (tmpTaskModel.currunt_active_daypart == 1) {
          title = AppStrings.morning;
        }
        if (tmpTaskModel.currunt_active_daypart == 2) {
          title = AppStrings.afternoon;
        }
        if (tmpTaskModel.currunt_active_daypart == 4) {
          title = AppStrings.evening;
        }
      }
    }

    if (title.length == 0) {
      return '';
    } else {
      return ' ( $title ) ';
    }
  }

  static String getFrequencyString(TaskModel tmpTaskModel) {
    int part_of_day_this_task = tmpTaskModel.frequency_daily_part_of_day;
    String day_part_message = '';
    if (tmpTaskModel.frequency_id == 2) {
      if (part_of_day_this_task == 1) {
        day_part_message = "( Ochtend )";
      }

      if (part_of_day_this_task == 2) {
        day_part_message = "( Middag )";
      }

      if (part_of_day_this_task == 4) {
        day_part_message = "( Avond )";
      }

      if (part_of_day_this_task == 3) {
        day_part_message = "( Ochtend , Middag )";
      }

      if (part_of_day_this_task == 5) {
        day_part_message = "( Ochtend , Avond )";
      }

      if (part_of_day_this_task == 6) {
        day_part_message = "( Middag , Avond )";
      }

      if (part_of_day_this_task == 7) {
        day_part_message = "( Ochtend , Middag , Avond )";
      }
    }
    if (day_part_message.length == 0) {
      return '';
    } else {
      return day_part_message;
    }
  }

  static dynamic GetTaskNextDeadline(day_of_week, reverse_array) {
    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    if (!reverse_array.asMap().containsValue(day_of_week)) {
      if (day_of_week != 7) {
        day_of_week = day_of_week + 1;
      } else {
        day_of_week = 1;
      }
    } else {
      return day_of_week;
    }

    return 'x';
  }

  static Map<String, dynamic> getFrequencyStepNumber({int frequency_step_number}) {
    List<int> dayArray = [];
    dayArray.add(8);
    if (frequency_step_number >= 64) {
      // $day_array[] = 7;
      dayArray.add(7);
      frequency_step_number = frequency_step_number - 64;
    }

    if (frequency_step_number >= 32) {
      // $day_array[] = 6;
      dayArray.add(6);
      frequency_step_number = frequency_step_number - 32;
    }

    if (frequency_step_number >= 16) {
      // $day_array[] = 5;
      dayArray.add(5);
      frequency_step_number = frequency_step_number - 16;
    }

    if (frequency_step_number >= 8) {
      // $day_array[] = 4;
      dayArray.add(4);
      frequency_step_number = frequency_step_number - 8;
    }

    if (frequency_step_number >= 4) {
      // $day_array[] = 3;
      dayArray.add(3);
      frequency_step_number = frequency_step_number - 4;
    }

    if (frequency_step_number >= 2) {
      // $day_array[] = 2;
      dayArray.add(2);
      frequency_step_number = frequency_step_number - 2;
    }

    if (frequency_step_number >= 1) {
      // $day_array[] = 1;
      dayArray.add(1);
      frequency_step_number = frequency_step_number - 1;
    }
    Map<String, dynamic> mapData = Map();
    mapData['array'] = dayArray;
    mapData['frequency'] = frequency_step_number;
    return mapData;
  }

  static void addLastSyncTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String lastSync = prefs.getString(AppPreferences.pref_last_sync_date_time);
    // if(lastSync == null){
    DateTime dtLastSync = DateTime.now();
    String strLastSync = DateFormat('dd-MM-yyyy hh:mm').format(dtLastSync);
    await prefs.setString(AppPreferences.pref_last_sync_date_time, strLastSync);
    locator<LastSyncDateTimeBloc>().lastSyncDateTimeEventSink.add(strLastSync);
    // }
  }

  static String getPrettyJSONString(jsonObject) {
    var encoder = new JsonEncoder.withIndent("     ");
    return encoder.convert(jsonObject);
  }
  static String dtFyMdHms = 'yyyy-MM-dd HH:mm:ss';
  static completeTask({TaskModel mAppTaskModel, BuildContext context, List<Map<String, dynamic>> currentChecklist,int currentTab, bool forceComplete=false}) async {

    // return;
    // String result = await showDialog(context: context, builder: (context) => DialogConfirmCompleteTask(),);
    // if(result == 'no'){
    //   return;
    // }
    // update task set status_id = 3, finish_date = '$todayDate' where task_id = 'taskID'

    // 1st : Create clone task when user complete task

    Map<String, dynamic> mapCloneTask = mAppTaskModel.toJSON();
    debugPrint("New Clone Task of CurrentTask : $mapCloneTask");
    debugPrint("New Clone Task of CurrentTask : start_date : ${mapCloneTask['start_date']}");
    int maxTaskId = new DateTime.now().millisecondsSinceEpoch;
    debugPrint("new task id : $maxTaskId");

    Map<String, dynamic> mapNewTask = Map();
    mapNewTask['id'] = maxTaskId ?? '';
    mapNewTask['customer_id'] = mapCloneTask['customer_id'];
    mapNewTask['location_id'] = mapCloneTask['location_id'];
    mapNewTask['group_id'] = mapCloneTask['group_id'];
    mapNewTask['user_id'] = appUserModel.id; // need to ask
    mapNewTask['clone_task_id'] = mapCloneTask['clone_task_id'];
    mapNewTask['title'] = '${mapCloneTask['title']}';

    String tmpDescription = '';
    if(mapCloneTask['description'] != null && mapCloneTask['description'].toString().trim() != ''){
      mapNewTask['description'] = mapCloneTask['description'];
    }
    // debugPrint(Utility.getPrettyJSONString(mapNewTask));
    // return;
    mapNewTask['create_date'] = DateFormat(dtFyMdHms).format(DateTime.now());
    mapNewTask['removed'] = 0;
    mapNewTask['stamitem'] = 0;
    mapNewTask['customer_stamitem'] = 0;

    int active_daypart = 0;
    int currunt_active_daypart = 0;

    String frequencyId = '${mapCloneTask['frequency_id']}';
    debugPrint('----1168 : frequencyId : $frequencyId');

    if(frequencyId !=null){
      int intFrequencyId = int.parse(frequencyId);

      if(intFrequencyId > 1){
        if (frequencyId == "2") {
          String sqlQ = "select * from ${AppTables.TASK_FREQUENCY} where id = ?";
          List<dynamic> sqlV = [frequencyId];
          List<Map<String, Object>> listNewData = await appDatabase.rawQuery(sqlQ, sqlV);

          if (listNewData.length > 0) {
            Map<String, dynamic> mapFrequencyData = listNewData[0];
            String mStartDate = mapCloneTask['start_date'];
            debugPrint("IAAPP-test-mStartDate-String : $mStartDate");
            DateTime dtStartDate = DateFormat(dtFyMdHms).parse(mStartDate);
            // DateTime newAddedDT = mDTStartDate.add(Duration(days: 1));
            int last_daypart = mapCloneTask['active_daypart']??0;
            int part_of_day_this_task = mapCloneTask['frequency_daily_part_of_day'];
            active_daypart = mapCloneTask['active_daypart'];
            currunt_active_daypart = 1;
            String new_expire_date = "";

            if (part_of_day_this_task == 0) {
              active_daypart = 4;
              currunt_active_daypart = 4;
            }

            if (part_of_day_this_task == 1) {
              active_daypart = 1;
              currunt_active_daypart = 1;
            }

            if (part_of_day_this_task == 2) {
              active_daypart = 2;
              currunt_active_daypart = 2;
            }

            if (part_of_day_this_task == 4) {
              active_daypart = 4;
              currunt_active_daypart = 4;
            }

            if (part_of_day_this_task == 3) {
              if (active_daypart == 3) {
                active_daypart = 1;
                currunt_active_daypart = 1;
              } else {
                new_expire_date = mStartDate;
                active_daypart = 2;
                currunt_active_daypart = 2;
              }
            }

            if (part_of_day_this_task == 5) {
              if (active_daypart == 5) {
                active_daypart = 1;
                currunt_active_daypart = 1;
              } else {
                new_expire_date = mStartDate;
                active_daypart = 4;
                currunt_active_daypart = 4;
              }
            }

            if (part_of_day_this_task == 6) {
              if (active_daypart == 6) {
                active_daypart = 2;
                currunt_active_daypart = 2;
              } else {
                new_expire_date = mStartDate;
                active_daypart = 4;
                currunt_active_daypart = 4;
                currunt_active_daypart = 4;
              }
            }

            if (part_of_day_this_task == 7) {
              if (active_daypart == 1) {
                active_daypart = 2;
                currunt_active_daypart = 2;
                new_expire_date = mStartDate;
              }
              if (active_daypart == 3) {
                active_daypart = 4;
                currunt_active_daypart = 4;
                new_expire_date = mStartDate;
              }
              if (active_daypart == 7) {
                currunt_active_daypart = 1;
                active_daypart = 1;
              }
            }
            debugPrint("last_daypart : $last_daypart");
            debugPrint("active_daypart : $active_daypart");
            if (last_daypart == part_of_day_this_task) {
              active_daypart = active_daypart;
            } else {
              active_daypart = last_daypart + active_daypart;
            }

            if (new_expire_date.length != 0) {
              debugPrint("IAAPP-test-0 : $mStartDate");
              mapNewTask['start_date'] = mStartDate;
            } else {
              debugPrint("IAAPP-test : new expire date else: $new_expire_date");
              if (mAppTaskModel.frequency_step_number == 127) {
                // DateTime dtToday = DateTime.now();
                dtStartDate = dtStartDate.add(Duration(days: 1));
                String strStartDate = DateFormat(dtFyMdHms).format(dtStartDate);
                debugPrint("IAAPP-test-1 : $strStartDate");
                mapNewTask['start_date'] = strStartDate;
              } else {
                int mFrequencyStemNumber = mAppTaskModel.frequency_step_number;
                Map<String, dynamic> mapData = Utility.getFrequencyStepNumber(frequency_step_number: mFrequencyStemNumber);
                int nFrequencyStepNumber = mapData['frequency'];
                List<int> dayArray = mapData['array'];
                debugPrint('dayArray A-Z : ${dayArray} ${dayArray.length}');
                dayArray = dayArray.reversed.toList();
                debugPrint('dayArray Z-A : ${dayArray} ${dayArray.length}');
                dayArray.removeLast();
                debugPrint('dayArray Removed Last : ${dayArray} ${dayArray.length}');
                int mWeekday = dtStartDate.weekday;
                debugPrint('day of week : ${mWeekday}');
                debugPrint('task_id : ${mAppTaskModel.id}');
                String day = '';

                if (!dayArray.asMap().containsValue(mWeekday)) {
                  debugPrint("--test--if");
                  int day_of_week = int.parse('${Utility.GetTaskNextDeadline(mWeekday, dayArray)}');
                  if (day_of_week == 1) {
                    day = AppStrings.monday;
                    debugPrint("--test--if-1");
                  }
                  if (day_of_week == 2) {
                    debugPrint("--test--if-2");
                    day = AppStrings.tuesday;
                  }
                  if (day_of_week == 3) {
                    debugPrint("--test--if-3");
                    day = AppStrings.wednesday;
                  }
                  if (day_of_week == 4) {
                    debugPrint("--test--if-4");
                    day = AppStrings.thursday;
                  }
                  if (day_of_week == 5) {
                    debugPrint("--test--if-5");
                    day = AppStrings.friday;
                  }
                  if (day_of_week == 6) {
                    debugPrint("--test--if-6");
                    day = AppStrings.saturday;
                  }
                  if (day_of_week == 7) {
                    debugPrint("--test--if-7");
                    day = AppStrings.sunday;
                  }
                  int tmpD = 0;
                  debugPrint("--test--if-7.1 : $day_of_week");
                  if (dtStartDate.weekday > day_of_week) {
                    debugPrint("--test--if-8");
                    tmpD = (7 - dtStartDate.weekday) + day_of_week;
                  } else {
                    debugPrint("--test--if-9");
                    tmpD = (day_of_week - dtStartDate.weekday);
                  }

                  DateTime nextWeekdayDate = dtStartDate.add(Duration(days: tmpD));
                  debugPrint('nextWeekdayDate : ${nextWeekdayDate}');
                  String strNextWeedDayDate = DateFormat(dtFyMdHms).format(nextWeekdayDate);
                  debugPrint("IAAPP-test-2 : $strNextWeedDayDate");
                  mapNewTask['start_date'] = strNextWeedDayDate;
                } else {
                  debugPrint("--test--else");
                  dayArray.asMap().forEach((key, value) {
                    debugPrint('key : ${key}');
                    debugPrint('value : ${value}');
                    int nextKey = key + 1;
                    if (mWeekday == value) {
                      if (dayArray.asMap().containsKey(nextKey)) {
                        var dayOfWeek = 0;
                        if (dayArray[nextKey] == 1) {
                          day = AppStrings.monday;
                          dayOfWeek = 1;
                        }
                        if (dayArray[nextKey] == 2) {
                          day = AppStrings.tuesday;
                          dayOfWeek = 2;
                        }
                        if (dayArray[nextKey] == 3) {
                          day = AppStrings.wednesday;
                          dayOfWeek = 3;
                        }
                        if (dayArray[nextKey] == 4) {
                          day = AppStrings.thursday;
                          dayOfWeek = 4;
                        }
                        if (dayArray[nextKey] == 5) {
                          day = AppStrings.friday;
                          dayOfWeek = 5;
                        }
                        if (dayArray[nextKey] == 6) {
                          day = AppStrings.saturday;
                          dayOfWeek = 6;
                        }
                        if (dayArray[nextKey] == 7) {
                          day = AppStrings.sunday;
                          dayOfWeek = 7;
                        }
                        debugPrint("----startDate if :${dtStartDate}");
                        debugPrint("----startDate if day :$day");
                        debugPrint("${dtStartDate.weekday}");
                        int tmpD = 0;

                        if (dtStartDate.weekday > dayOfWeek) {
                          tmpD = (7 - dtStartDate.weekday) + dayOfWeek;
                        } else {
                          tmpD = (dayOfWeek - dtStartDate.weekday);
                        }

                        DateTime nextWeekdayDate = dtStartDate.add(Duration(days: tmpD));
                        debugPrint('nextWeekdayDate : ${nextWeekdayDate}');
                        String strNextWeedDayDate = DateFormat(dtFyMdHms).format(nextWeekdayDate);
                        debugPrint("IAAPP-test-3 : $strNextWeedDayDate");
                        mapNewTask['start_date'] = strNextWeedDayDate;
                      } else {
                        var dayOfWeek = 0;
                        if (dayArray[0] == 1) {
                          day = AppStrings.monday;
                          dayOfWeek = 1;
                        }
                        if (dayArray[0] == 2) {
                          day = AppStrings.tuesday;
                          dayOfWeek = 2;
                        }
                        if (dayArray[0] == 3) {
                          day = AppStrings.wednesday;
                          dayOfWeek = 3;
                        }
                        if (dayArray[0] == 4) {
                          day = AppStrings.thursday;
                          dayOfWeek = 4;
                        }
                        if (dayArray[0] == 5) {
                          day = AppStrings.friday;
                          dayOfWeek = 5;
                        }
                        if (dayArray[0] == 6) {
                          day = AppStrings.saturday;
                          dayOfWeek = 6;
                        }
                        if (dayArray[0] == 7) {
                          day = AppStrings.sunday;
                          dayOfWeek = 7;
                        }
                        debugPrint("----startDate else :${dtStartDate}");
                        debugPrint("----startDate else day :$day");
                        debugPrint("${dtStartDate.weekday}");
                        int tmpD = 0;
                        if (dtStartDate.weekday > dayOfWeek) {
                          tmpD = (7 - dtStartDate.weekday) + dayOfWeek;
                        } else {
                          tmpD = (dayOfWeek - dtStartDate.weekday);
                        }

                        DateTime nextWeekdayDate = dtStartDate.add(Duration(days: tmpD));
                        debugPrint('nextWeekdayDate : ${nextWeekdayDate}');
                        String strNextWeedDayDate = DateFormat(dtFyMdHms).format(nextWeekdayDate);
                        debugPrint("IAAPP-test-3 : $strNextWeedDayDate");
                        mapNewTask['start_date'] = strNextWeedDayDate;
                      }
                    }
                  });
                }
              }
            }
          }
        }
        else {
          String sqlQ = "select * from ${AppTables.TASK_FREQUENCY} where id = ?";
          List<dynamic> sqlV = [frequencyId];
          List<Map<String, Object>> listNewData = await appDatabase.rawQuery(sqlQ, sqlV);
          if (listNewData.length > 0) {
            Map<String, dynamic> mapFrequencyData = listNewData[0];
            String dateModify = '${mapFrequencyData['date_modify']}';

            String frequencyStepNumber = '${mapCloneTask['frequency_step_number']}';
            if(frequencyStepNumber is String){
              debugPrint("frequencyStepNumber is String");
            }


            if (frequencyStepNumber != 'null' && frequencyStepNumber != "1" && frequencyStepNumber != "0") {
              // steps for the login
              //frequencyStepNumber & dateModify ( Ex. 2 * +1 week = 14 days)
              //get current date ( YYYY-MM-DD HH:MM:SS ) ( Ex. 2022-03-04 19:07:00 )
              // Add days ( 14 days )
              // Result store in start_date ( Ex. mapNewTask['start_date'] = result date )

              String mDateModify = dateModify;
              List<String> mResult = mDateModify.split(" ");
              debugPrint("splite result : ${mResult}");
              String digit = mResult[0].trim();
              String calType = mResult[1].trim();
              digit = digit.replaceAll("+", "");
              int intDigit = int.parse(digit);
              int mDays = 0;
              if (calType == "months") {
                mDays = 30;
              } else if (calType == "year") {
                mDays = 365;
              } else if (calType == "week") {
                mDays = 7;
              } else if (calType == "day") {
                mDays = 1;
              }
              // debugPrint("----1444 : $frequencyStepNumber");
              // if(frequencyStepNumber == 'null'){
              //   frequencyStepNumber='0';
              // }
              debugPrint("----1445 : $frequencyStepNumber");
              DateTime mDateTime = DateTime.now();
              int mFSN = int.parse(frequencyStepNumber);
              int mTotalDaysToAdd = (mDays * intDigit * mFSN);
              DateTime mStartDate = mDateTime.add(Duration(days: mTotalDaysToAdd));
              debugPrint("Total Days : $mTotalDaysToAdd");
              String mStrStartDate = DateFormat(dtFyMdHms).format(mStartDate);
              debugPrint("Next date : ${mStrStartDate}");

              mapNewTask['start_date'] = mStrStartDate;
            } 
            else {
              String mDateModify = dateModify;
              List<String> mResult = mDateModify.split(" ");
              debugPrint("splite result: else : $mResult");
              String digit = mResult[0].trim();
              String calType = mResult[1].trim();
              digit = digit.replaceAll("+", "");
              int intDigit = int.parse(digit);
              int mDays = 0;
              if (calType == "months") {
                mDays = 30;
              } else if (calType == "year") {
                mDays = 365;
              } else if (calType == "week") {
                mDays = 7;
              } else if (calType == "day") {
                mDays = 1;
              }

              String strStartDate = mapCloneTask['start_date'];
              DateTime dtStartDate =DateFormat(dtFyMdHms).parse(strStartDate);
              DateTime mDateTime = dtStartDate;//DateTime.now();
              int mTotalDaysToAdd = (mDays * intDigit);
              DateTime mStartDate = mDateTime.add(Duration(days: mTotalDaysToAdd));
              debugPrint("Total Days : $mTotalDaysToAdd");
              String mStrStartDate = DateFormat(dtFyMdHms).format(mStartDate);
              debugPrint("Next date : $mStrStartDate");
              mapNewTask['start_date'] = mStrStartDate;
            }
          }
        }

        mapNewTask['tempate_id'] = mapCloneTask['tempate_id'];
        mapNewTask['type_id'] = mapCloneTask['type_id'];
        mapNewTask['frequency_id'] = mapCloneTask['frequency_id'];
        mapNewTask['last_edit_user_id'] = mapCloneTask['last_edit_user_id'];
        mapNewTask['status_id'] = 1;
        mapNewTask['createdby_user_id'] = mapCloneTask['createdby_user_id'];
        mapNewTask['createdby_user_name'] = mapCloneTask['createdby_user_name']; // discuss
        mapNewTask['finish_date'] = mapCloneTask['finish_date'];
        mapNewTask['deadline_notification_count'] = mapCloneTask['deadline_notification_count'];
        mapNewTask['frequency_daily_part_of_day'] = mapCloneTask['frequency_daily_part_of_day'];
        mapNewTask['part_of_day_this_task'] = mapCloneTask['part_of_day_this_task'];
        mapNewTask['frequency_step_number'] = mapCloneTask['frequency_step_number'];
        mapNewTask['days_active'] = mapCloneTask['days_active'];
        mapNewTask['tasktype_name'] = mapCloneTask['tasktype_name'];
        mapNewTask['frequency_name'] = mapCloneTask['frequency_name'];
        mapNewTask['location_name'] = mapCloneTask['location_name'];
        mapNewTask['function_name'] = mapCloneTask['function_name'];
        mapNewTask['active_daypart'] = active_daypart; // new
        mapNewTask['currunt_active_daypart'] = currunt_active_daypart; // new
        debugPrint("add task request : $mapNewTask");

        TaskModel taskModel = TaskModel.fromMap(map: mapNewTask);
        taskModel.synced = 0;
        debugPrint("---Task---oldID : ${taskModel.id}");
        String sql = AppTablesQuery.INSERT_TABLE_TASK + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        int insertResult = await appDatabase.rawInsert(sql, taskModel.getDataList(taskModel));
        if (insertResult == 0) {
          debugPrint("Error inserting task");
          MyToast.showToast('Error inserting task', context);
          return;
        }
        //If network availabe, Newaly created task will be synced to web
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult != ConnectivityResult.none) {

          maxTaskId = await AppSync.addTask(context: context, mapNewTask: mapNewTask);
          if (maxTaskId == syncErrorValue) {
            return;
          }
          debugPrint("AddTask sync success");
        }
        debugPrint("task added --compete task : $insertResult");

        // return;
        // 2nd Clone checklist

        debugPrint("Current Task Checklist : ${currentChecklist}");
        // Insert checklist in local first
        Batch batchCheckListItem = appDatabase.batch();
        if (currentChecklist.length > 0) {
          locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
          List<Map<String, dynamic>> lstUploadChkLst = [];
          List<int> chkLstIds = [];
          for (Map<String, dynamic> element in currentChecklist) {


            if(element['question'] == null || element['question'] == ''){

            }else{
              Map<String, dynamic> mNewChkList = Map();

              await Future.delayed(const Duration(milliseconds: 100));

              int millis = new DateTime.now().millisecondsSinceEpoch;
              // int micros = new DateTime.now().microsecond;

              int maxCheckListId = millis;
              debugPrint("new created checklist id : $maxCheckListId");
              mNewChkList['id'] = maxCheckListId;
              mNewChkList['task_id'] = maxTaskId;

              String question = '${element['question']}';
              mNewChkList['question'] = question;

              mNewChkList['type'] = element['type'];
              mNewChkList['result'] = null;//element['result'];
              mNewChkList['removed'] = element['removed'];
              mNewChkList['description'] = element['description'];
              mNewChkList['order_number'] = element['order_number'];
              mNewChkList['unique_hash'] = element['unique_hash'];
              mNewChkList['clone_checklist_id'] = element['clone_checklist_id'];
              mNewChkList['synced'] = 0;
              mNewChkList['digit_range'] = element['digit_range'];
              mNewChkList['right_answer'] = element['right_answer'];
              mNewChkList['wrong_answer'] = element['wrong_answer'];
              mNewChkList['dropdown_answers'] = element['dropdown_answers'];
              mNewChkList['is_multiple'] = element['is_multiple'];

              String sqlChkLstItmMdl = AppTablesQuery.INSERT_TABLE_CHECKLISTITEM + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
              CheckListItemModel chItmMdl = CheckListItemModel.fromMap(map: mNewChkList);
              batchCheckListItem.rawInsert(sqlChkLstItmMdl, chItmMdl.getChkListDataList(chItmMdl));

              String strDigitRange = element['digit_range'];
              if (strDigitRange != null) {
                Map<String, dynamic> mapDigitRange = json.decode(strDigitRange);
                mNewChkList['digit_range'] = mapDigitRange;
              } else {
                mNewChkList['digit_range'] = strDigitRange;
              }

              chkLstIds.add(maxCheckListId);
              lstUploadChkLst.add(mNewChkList);
            }
          }
          // await currentChecklist.forEach((Map<String, dynamic> element) async {});
          List responses = await batchCheckListItem.commit(continueOnError: true);
          debugPrint("Checklist insert result : ${responses}");

          //If network availabe, Newaly created checklist will be synced to web
          var connectivityResult = await (Connectivity().checkConnectivity());
          if (connectivityResult != ConnectivityResult.none && mAppTaskModel.synced == 1) {
            Map<String, dynamic> mapRequest = Map();
            mapRequest['sync_add_checklist'] = lstUploadChkLst;
            debugPrint("Checklist sync request : $mapRequest");
            Map<String, dynamic> syncChklstResult = await AppSync.funSyncAddChecklistAnswer(context: context, mapRequest: mapRequest, chklstIds: chkLstIds);
            debugPrint("Checklist sync result : $syncChklstResult");
          }
        }
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      }


      //3rd Update current task to complete and sync
      try {
        DateTime dtFinishDate = DateTime.now();
        String strFinishDate = DateFormat(dtFyMdHms).format(dtFinishDate);
        int force_completed=0;
        if(forceComplete){
          force_completed=1;
        }
        String sqlTaskComplete = "update ${AppTables.TASK} set status_id = ?, finish_date = ?, synced = ?, force_completed = ? where id = ?";
        List<dynamic> sqlValues = [3, strFinishDate, 0, force_completed, mAppTaskModel.id];
        debugPrint("3rd Step : sqlTaskComplete : $sqlTaskComplete");
        debugPrint("3rd Step : sqlValues : $sqlValues");

        int updateResult = await appDatabase.rawUpdate(sqlTaskComplete, sqlValues);
        debugPrint("3rd Step : updateResult : $updateResult");
        if (updateResult == 0) {
          debugPrint("Error completing answer");
          MyToast.showToast(AppStrings.errCompletingAnswer, context);
          return;
        }
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult != ConnectivityResult.none && mAppTaskModel.synced == 1) {
          int updateCompleteTaskId = await AppSync.updateCompleteTask(context: context, id: mAppTaskModel.id, status_id: 3, finish_date: strFinishDate,force_completed: force_completed);
          if (updateCompleteTaskId == syncErrorValue) {
            MyToast.showToast(AppStrings.errSyncingCompleteTask, context);
            return;
          }
        }
        if (updateResult != 0) {
          String sqlQ = "select * from ${AppTables.TASK} where id = ?";
          List<dynamic> sqlV = [mAppTaskModel.id];
          debugPrint("3rd Step : updateResult : sqlQ : $sqlQ");
          debugPrint("3rd Step : updateResult : sqlV : $sqlV");
          List<Map<String, Object>> listNewData = await appDatabase.rawQuery(sqlQ, sqlV);
          debugPrint("3rd Step : updateResult : sqlV : $sqlV");
          debugPrint("3rd Step : updateResult : listNewData : $listNewData");
          if (listNewData.length > 0) {
            Map<String, dynamic> mapNewData = listNewData[0];
            debugPrint("3rd Step : updateResult : mapNewData : ${mapNewData}");
            TaskModel taskM = TaskModel.fromMap(map: mapNewData);
            debugPrint("3rd Step : updateResult : taskM : ${taskM.toJSON()}");
            debugPrint("-------testing--appindex : $appTaskIndex");
            int newAppIndex = appTaskIndex - 1;
            locator<TaskIndexBloc>().taskIndexEventSink.add(newAppIndex);
            locator<TaskModelBloc>().taskModelEventSink.add(taskM);
            locator<DetailTabBloc>().detailTabEventSink.add(currentTab);
            locator<HomeTabBloc>().homeTabEventSink.add(HomeScreen.currentTab);
          }
          debugPrint("update task result : $updateResult");
        } else {
          MyToast.showToast(AppStrings.noTaskUpdated, context);
        }
      } catch (e) {
        debugPrint("Error completing task : ${e.toString()}");
        MyToast.showToast(AppStrings.errCompletingTask, context);
      }
    }

    Utility.checkNextPrevTask();
  }

  static void checkNextPrevTask() async {
    debugPrint("checkNextPrevTask : appTaskModel : id : ${appTaskModel.id}");
    if (appTaskModel.clone_task_id == null) {
      // keep code same as else part
      locator<NextTaskIndexBloc>().nextTaskIndexEventSink.add(0);
      locator<PrevTaskIndexBloc>().prevTaskIndexEventSink.add(0);
      return;
    }

    // get previous task
    String queryPrevTask = 'select * from ${AppTables.TASK} where removed = ? and clone_task_id = ? and id < ? order by id desc limit 1';
    List<dynamic> qValuesPrevTask = [0, appTaskModel.clone_task_id, appTaskModel.id];
    debugPrint("queryPrevTask : $queryPrevTask");
    debugPrint("query values qValuesPrevTask : $qValuesPrevTask");
    List<Map> listPrevTask = await appDatabase.rawQuery(queryPrevTask, qValuesPrevTask);
    debugPrint('appTaskModel prev : list : ${listPrevTask}');
    if (listPrevTask.length > 0) {
      locator<PrevTaskIndexBloc>().prevTaskIndexEventSink.add(1);
      debugPrint('isTaskAvail : Prev : true');
    } else {
      locator<PrevTaskIndexBloc>().prevTaskIndexEventSink.add(0);
      debugPrint('isTaskAvail : Prev : false');
    }

    // get next task
    String queryNextTask = 'select * from ${AppTables.TASK} where customer_id = ? and removed = ? and clone_task_id = ? and id > ? order by id asc limit 1';
    List<dynamic> qValuesNextTask = [appUserModel.customer_id, 0, appTaskModel.clone_task_id, appTaskModel.id];
    debugPrint("queryNextTask : $queryNextTask");
    debugPrint("query values qValuesNextTask : $qValuesNextTask");
    List<Map> listNextTask = await appDatabase.rawQuery(queryNextTask, qValuesNextTask);
    debugPrint('appTaskModel next : list : ${listNextTask}');
    if (listNextTask.length > 0) {
      locator<NextTaskIndexBloc>().nextTaskIndexEventSink.add(1);
      debugPrint('isTaskAvail : Next : true');
    } else {
      locator<NextTaskIndexBloc>().nextTaskIndexEventSink.add(0);
      debugPrint('isTaskAvail : Next : false');
    }
  }

  static bool isValidString({String value}) {
    bool isValid = false;
    List<String> searchKeywords = List<String>.generate(value.length, (index) => value[index]); // ['J','o','h','n',' ','D','o','e']
    // debugPrint('onChangeText : searchKeywords : $searchKeywords');
    int minusCount = 0;
    int commaCount = 0;
    searchKeywords.forEach((item) {
      // debugPrint('onChangeText : item : $item');
      if (item == '-') {
        minusCount++;
      } else if (item == ',') {
        commaCount++;
      }
    });
    debugPrint('onChangeText : minusCount : $minusCount');
    debugPrint('onChangeText : commaCount : $commaCount');
    debugPrint('onChangeText : searchKeywords : $searchKeywords');
    if (minusCount > 1 || commaCount > 1 || value.length == 0) {
      isValid = false;
      debugPrint('onChangeText : isValid : if : $isValid');
    } else {
      debugPrint('onChangeText : isValid : else ');
      if (searchKeywords.length > 0 && (minusCount != 0 && commaCount != 0)) {
        debugPrint('onChangeText : isValid : if - if ');
        isValid = true;
      }else if (searchKeywords.length > 0 && (minusCount != 0 && searchKeywords[0] != '-')) {
        debugPrint('onChangeText : isValid : if - if ');
        isValid = false;
      } else {
        debugPrint('onChangeText : isValid : if - else ');
        isValid = true;
      }
    }
    return isValid;
  }
  static bool isValidType5String({CheckListItemModel checkListItemModel}) {
    // blocChecklistResult.checklistResultEventSink.add(null);

    String value = checkListItemModel.result;
    debugPrint("update Result : ---7 ");
    debugPrint("update Result : ---digitRange : ${checkListItemModel.digit_range}");

    Map<String, dynamic> digitRange = json.decode(checkListItemModel?.digit_range);
    String drDecimal = digitRange['decimal'];
    String drFromRange = digitRange['from_range'];
    String drToRange = digitRange['to_range'];
    String drMessage = digitRange['message'];



    debugPrint('onChangeText : value : $value');

    bool isValid = false;
    if(value == null){
      isValid = false;
      return isValid;
    }
    if (Utility.isValidString(value: value)) {
      debugPrint('onChangeText : searchKeywords : Valid');

      // return;

      if (value == '') {
        debugPrint('onChangeText : value 1 : $value');
        // widget.onSubmitted(value, drMessage, isValid);
        isValid = false;
        return isValid;
      } else if (value == '-') {
        debugPrint('onChangeText : value 2 : $value');
        // widget.onSubmitted(value, drMessage, isValid);
        isValid = false;
        return isValid;
      }
      String nValue = value;
      debugPrint('onChangeText : nValue--- : $nValue');
      List<String> sArray = nValue.split(',');
      debugPrint('onChangeText : sArray--- : $sArray');
      String first = sArray.first;
      String second = sArray.last;
      debugPrint('onChangeText : first--- : $first');
      debugPrint('onChangeText : second--- : $second');
      String mS = '';
      if(second.contains('-')){
        mS=second.replaceAll('-', '');
        mS = '-'+first+','+mS;
      }
      else if(first.contains('-')){
        mS=first.replaceAll('-', '');
        mS = '-'+mS+','+second;
      }else{
        mS = nValue;
      }
      debugPrint('onChangeText : mS--- : $mS');
      String actualValue = mS.replaceAll(',', '.');
      String actualFrom = drFromRange.replaceAll(',', '.');
      String actualTo = drToRange.replaceAll(',', '.');
      double doubleFrom = double.parse(actualFrom);
      double doubleTo = double.parse(actualTo);
      double doubleActual = double.parse(actualValue);

      AppLog.debugApp('onSubmitted : actualValue : $actualValue');
      AppLog.debugApp('onSubmitted : actualFrom : $actualFrom');
      AppLog.debugApp('onSubmitted : actualTo : $actualTo');

      AppLog.debugApp('onSubmitted : doubleActual : $doubleActual');
      AppLog.debugApp('onSubmitted : doubleFrom : $doubleFrom');
      AppLog.debugApp('onSubmitted : doubleTo : $doubleTo');

      int intFrom = doubleFrom.toInt();
      int intTo = doubleTo.toInt();
      int intActual = doubleActual.toInt();

      AppLog.debugApp('onSubmitted : intActual : $intActual');
      AppLog.debugApp('onSubmitted : intFrom : $intFrom');
      AppLog.debugApp('onSubmitted : intTo : $intTo');

      if (doubleActual >= doubleFrom && doubleActual <= doubleTo) {
        isValid = true;
      }else{
        isValid = false;

      }
      return isValid;

    } else {
      if (value == '') {
        // widget.onSubmitted(value, drMessage, isValid);
        return isValid;
      }
    }
    return isValid;

  }
  static Future<bool> isAllAnswerCompleted()async{
    String sql = "select * from ${AppTables.CHECKLISTITEM} where task_id = ${appTaskModel.id} and removed = 0 order by order_number asc";
    debugPrint('sql detal checklist : ${sql}');
    List<Map<String,dynamic>> mCheckListItems= await appDatabase.rawQuery(sql);
    List<Map<String,dynamic>> resultList = mCheckListItems.where((i) => i['type'] == Const.CHKLIST_TYPE_1 || i['type'] == Const.CHKLIST_TYPE_2 || i['type'] == Const.CHKLIST_TYPE_3 || i['type'] == Const.CHKLIST_TYPE_5 || i['type'] == Const.CHKLIST_TYPE_6).toList();

    bool anyEmptyResult = resultList.any((element) => element['result'] == '' || element['result'] == null);
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(resultList);
    debugPrint('-------------Result Checklist----------------');
    debugPrint('-------------anyEmptyResult----------------${anyEmptyResult}');
    return Future.value(anyEmptyResult?false:true);

  }
}
