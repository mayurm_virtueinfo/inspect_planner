class WidthSizes{
  static const double login_signup_button_width = 110.0;
  static const double splash_screen_img_bapasitaram_width = 160.0;
  static const double drawer_img_bapasitaram_width = 80.0;
  static const double drawer_img_savealife_width = 110.0;
  static const double splash_screen_img_savealife_width = 130.0;
  static const double sponser_screen_img_app_sponcer_width = 180.0;
  static const double admin_img_btn_login_circle_width = 60.0;
  static const double member_img_btn_login_circle_wide_width = 120.0;
  static const double home_screen_admin_menu_width = 20.0;
  static const double home_screen_admin_menu_padding_left_width = 15.0;
}