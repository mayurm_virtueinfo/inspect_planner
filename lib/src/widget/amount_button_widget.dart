import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:flutter/material.dart';

class AmountButtonWidget extends StatefulWidget {
  Function onClickButton;
  int amount;
  int highlightData;
  AmountButtonWidget({this.onClickButton,this.amount,this.highlightData});

  @override
  _AmountButtonWidgetState createState() => _AmountButtonWidgetState();
}

class _AmountButtonWidgetState extends State<AmountButtonWidget> {
  int tmpHighlightedData;
  @override
  void initState() {
    // TODO: implement initState
    tmpHighlightedData = widget.highlightData;
    setState(() {

    });
    super.initState();
  }
  @override
  void didUpdateWidget(AmountButtonWidget oldWidget) {



    super.didUpdateWidget(oldWidget);
    setState(() {
      tmpHighlightedData= widget.highlightData;
      // widgetCounter = widget.initialCounter;
      // oldCounter = widget.previousValue;
    });
  }
  @override
  Widget build(BuildContext context) {
    // debugPrint("refresh amount-button-widget : $tmpHighlightedData} ${widget.highlightData}]");
    return Material(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100.0),
      ),
      clipBehavior: Clip.hardEdge,
      color: Colors.transparent,
      child: InkWell(
        onTap: this.widget.onClickButton,
        child: Card(
          shape: CircleBorder(),
          color: this.tmpHighlightedData==widget.amount?MyColors.skyBlue:Colors.white,
          child: Container(
              alignment: Alignment.center,
              width: 35,
              height: 35,
              child: Text('${this.widget.amount}',style: TextStyle(
                  color: this.tmpHighlightedData==widget.amount?Colors.white:Colors.grey
              ),)
          ),
        ),
      ),
    );
  }
}
