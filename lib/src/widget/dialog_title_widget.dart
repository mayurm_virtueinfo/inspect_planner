import 'package:flutter/material.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';

class DialogTitleWidget extends StatelessWidget {
  String title;
  bool hideCloseIcon;
  DialogTitleWidget({this.title,this.hideCloseIcon=false});

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 7;
    return Container(
      padding: EdgeInsets.only(left: 30, right: 20),
      // width: double.infinity,
      height: loginBoxHeight / 7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        color: MyColors.colorConvert('#5BB0EC'),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // Image.asset('assets/app_logo.png',width: loginBoxWidth/1.75,)
          Text('${this.title}', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
          hideCloseIcon?Container():IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.clear, color: MyColors.colorConvert('#365e77'), size: 30),
          )
        ],
      ),
    );
  }
}
