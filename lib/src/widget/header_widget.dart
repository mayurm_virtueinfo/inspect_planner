import 'package:inspect_planner/src/utils/utility.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeaderWidget extends StatelessWidget {
  String title;
  IconData icon;
  Function onClickCancel;
  HeaderWidget({this.title,this.onClickCancel,this.icon});
  @override
  Widget build(BuildContext context) {
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    return Container(
        margin: EdgeInsets.only(left: 15,right: 15),
        alignment: Alignment.topLeft,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            IconButton(
              icon: Icon(this.icon, color: Colors.black,size: 30,),
              onPressed:this.onClickCancel,
            ),
            Expanded(
              flex:1,
                child: Container(
                  alignment: Alignment.center,
                    child: Text(this.title??'',style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),))
            ),
            Container(
              height: 35,
              width: 35,
            )
          ],
        ));
  }
}
