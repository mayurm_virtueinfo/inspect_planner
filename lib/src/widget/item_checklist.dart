import 'dart:async';
import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/app_sync.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/checklist_result_bloc.dart';

import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_img_upload_checklist.dart';
import 'package:inspect_planner/src/dialog/dialog_img_upload_task.dart';
import 'package:inspect_planner/src/dialog/dialog_validation.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/const.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:inspect_planner/src/widget/type_five_widget.dart';
import 'package:inspect_planner/src/widget/type_six_widget_multiple.dart';
import 'package:inspect_planner/src/widget/type_six_widget_single.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class ItemCheckList extends StatefulWidget {
  CheckListItemModel checkListItemModel;
  int currentTab;
  int index;
  TaskModel taskModel;
  ItemCheckList({this.taskModel,this.checkListItemModel,this.currentTab,this.index,Key key});

  @override
  _ItemCheckListState createState() => _ItemCheckListState();
}

class _ItemCheckListState extends State<ItemCheckList> {

  bool isYes = false;
  bool isNo = false;
  bool isFocusText = false;
  // FocusNode ansCtrlFocusNode = FocusNode();
  StreamSubscription<bool> keyboardSubscription;
  final focusNode = FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    debugPrint('------------flow-initdata');
    initItemData();
    super.initState();
  }
  void initItemData() async{

    getCloneTask();
    checkForTheAnswer();
    getCheckListAttachment();
  }
  AttachmentIconBloc blocAttachmentIcon = AttachmentIconBloc();
  void getCheckListAttachment()async{
    int qCount = await Utility.getCheckListAttachmentCount(chkLstItmModel:widget.checkListItemModel);
    blocAttachmentIcon.attachmentIconEventSink.add(qCount);
  }
  void updateChecklist({String value,BuildContext mContext,bool isValid})async{

    String sqlChkLstUpdate = "update ${AppTables.CHECKLISTITEM} set result = ?, synced = ? where id = ?";
    List<dynamic> sqlValues = [value,0,widget.checkListItemModel.id];
    debugPrint('updateChecklist --- sql ---> query : $sqlChkLstUpdate');
    debugPrint('updateChecklist --- sql ---> values : $sqlValues');
    int updateResult = await appDatabase.rawUpdate(sqlChkLstUpdate,sqlValues);
    debugPrint("updateChecklist --- Result : $updateResult");
    // if(updateResult == 0){
    //   debugPrint("Error updating checklist answer");
    //   MyToast.showToast('Error deleting attachment in checklist', mContext);
    //   return;
    // }
    // var connectivityResult = await (Connectivity().checkConnectivity());
    // if (connectivityResult != ConnectivityResult.none && appTaskModel.synced == 1 && widget.checkListItemModel.synced == 1 ) {
    //   int updateChklstid = await AppSync.updateChecklistAnswer(context:mContext,id: widget.checkListItemModel.id,result: value);
    //
    //   if(updateChklstid == syncErrorValue){
    //     MyToast.showToast("Error syncing checlist answer", mContext);
    //     return;
    //   }
    // }
    // locator<DetailTabBloc>().detailTabEventSink.add(1);
    blocChecklistResult.checklistResultEventSink.add(value==''?null:value);

    debugPrint("update Result : ---");
    if(widget.checkListItemModel.type == Const.CHKLIST_TYPE_5){
      debugPrint("update Result : ---1");
      if(isValid){
        debugPrint("update Result : ---2 $isValid");
        blocChecklistResult.checklistResultEventSink.add(value);
      }else{
        debugPrint("update Result : ---3 $isValid");
        blocChecklistResult.checklistResultEventSink.add(null);
      }
    }else{
      debugPrint("update Result : ---4 $isValid");
      blocChecklistResult.checklistResultEventSink.add(value==''?null:value);
    }

  }

  void checkForTheAnswer(){
    // type 1 = yes/no
    // type 2 = good/wrong
    // type 3 = textbox
    if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_1 && (widget.checkListItemModel.result==null || widget.checkListItemModel.result=='')){
      isYes = false;
      isNo = false;
      blocChecklistResult.checklistResultEventSink.add(null);
    }
    else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_1 && widget.checkListItemModel.result=='0'){
      isYes = false;
      isNo = true;
      blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
    }else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_1 && widget.checkListItemModel.result=='1'){
      isYes = true;
      isNo = false;
      blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
    }
    else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_2 && (widget.checkListItemModel.result==null || widget.checkListItemModel.result=='')) {
      isGood = false;
      isWrong = false;
      blocChecklistResult.checklistResultEventSink.add(null);
    }
    else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_2 && widget.checkListItemModel.result=='0'){
      isGood = false;
      isWrong = true;
      blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
    }else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_2 && widget.checkListItemModel.result=='1'){
      isGood = true;
      isWrong = false;
      blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
    }else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_3 && (widget.checkListItemModel.result==null || widget.checkListItemModel.result=='')){
      debugPrint('M-type-3 --- 1 : ${widget.checkListItemModel.result}');
      answerControllerType3.text = '';
      blocChecklistResult.checklistResultEventSink.add(null);
    }
    else if(widget.checkListItemModel.type==Const.CHKLIST_TYPE_3){
      debugPrint('M-type-3 --- 2 : ${widget.checkListItemModel.result}');
      answerControllerType3.text = widget.checkListItemModel.result;
      answerControllerType3.selection = TextSelection.fromPosition(TextPosition(offset: answerControllerType3.text.length));
      blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
    }
    else if(widget.checkListItemModel.type == Const.CHKLIST_TYPE_5) {
      debugPrint('M-type-5 --- 1 : ${widget.checkListItemModel.result}');
      if (Utility.isValidType5String(checkListItemModel: widget.checkListItemModel)) {
        debugPrint('M-type-5 --- 2 ');
        blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
      } else {
        debugPrint('M-type-5 --- 3 ');
        blocChecklistResult.checklistResultEventSink.add(null);
      }
    }else if(widget.checkListItemModel.type == Const.CHKLIST_TYPE_6){

      if(widget.checkListItemModel.result == '' || widget.checkListItemModel.result == null){
        blocChecklistResult.checklistResultEventSink.add(null);
      }
      else{
        blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
      }

    }
    else if(widget.checkListItemModel.result == '' || widget.checkListItemModel.result == null){
      blocChecklistResult.checklistResultEventSink.add(null);
    }
    else{
      blocChecklistResult.checklistResultEventSink.add(widget.checkListItemModel.result);
    }

    setState(() {});

  }
  List<Map<String,dynamic>> listLog = [];
  void getCloneTask()async{
    String sqlTask =  'select * from ${AppTables.TASK} where clone_task_id = ${widget.taskModel.clone_task_id} and clone_task_id is not null and finish_date is not null and id != ${widget.taskModel.id} order by finish_date desc';
    List<Map<String,dynamic>> listCloneTask = await appDatabase.rawQuery(sqlTask);
    Map<String,dynamic> mapLog=Map();
    mapLog['finish_date'] = 'Datum:';
    mapLog['answer'] = 'Waarde:';
    List<Map<String,dynamic>> tmpListLog = [];
    tmpListLog.add(mapLog);
    await Future.forEach(listCloneTask, (element) async{
      Map<String,dynamic> mapLog=Map();
      TaskModel taskModel = TaskModel.fromMap(map: element);
      mapLog['finish_date'] = taskModel.finish_date;
      String sqlChecklist = 'select * from ${AppTables.CHECKLISTITEM} where task_id = ? and question = ?';
      List<dynamic> sqlValues = [taskModel.id,widget.checkListItemModel.question];
      List<Map<String,dynamic>> listResultLog = await appDatabase.rawQuery(sqlChecklist,sqlValues);
      if(listResultLog.length>0){
        CheckListItemModel chkLstItmMdl = CheckListItemModel.fromMap(map: listResultLog[0]);
        chkLstItmMdl.synced = element['synced'];

        // type 1 = yes/no
        // type 2 = good/wrong
        // type 3 = textbox
        if(chkLstItmMdl.type==Const.CHKLIST_TYPE_1){
          if(chkLstItmMdl.result == '1'){
            mapLog['answer'] = 'Yes';
          }else{
            mapLog['answer'] = 'No';
          }
        }else if(chkLstItmMdl.type==Const.CHKLIST_TYPE_2){
          if(chkLstItmMdl.result == '1'){
            mapLog['answer'] = 'Good';
          }else{
            mapLog['answer'] = 'Wrong';
          }
        }else if(chkLstItmMdl.type==Const.CHKLIST_TYPE_3){
          mapLog['answer'] = chkLstItmMdl.result;
        }
        tmpListLog.add(mapLog);
      }
    });
    listLog.clear();
    listLog.addAll(tmpListLog);
    setState(() {});
  }
  TextEditingController answerControllerType3 = TextEditingController();

  ChecklistResultBloc blocChecklistResult = ChecklistResultBloc();
  @override
  void didUpdateWidget(ItemCheckList oldWidget) {

    super.didUpdateWidget(oldWidget);
    initItemData();
  }
  bool showLog = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10,bottom:10,left: 10,right: 10),
      decoration: BoxDecoration(
        color: MyColors.colorConvert('#ffffff'),
        border: Border.all(color: MyColors.colorConvert('#e0e0e0'))
      ),
      child: Column(
        children: [
          Row(
            children: [
              //Deadline
              Expanded(flex: 2, child: Text(widget.checkListItemModel.type>Const.CHKLIST_TYPE_6?'':'#${widget.index+1} ${kDebugMode?'t-${widget.checkListItemModel.type}':''}', style: styleHeader.copyWith(fontWeight: FontWeight.bold))),
              // Expanded(flex: 2, child: Text(widget.checkListItemModel.type>Const.CHKLIST_TYPE_6?'':'#${widget.index+1}', style: styleHeader.copyWith(fontWeight: FontWeight.bold))),
              Expanded(flex: 8, child: Container(
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(child: Text('${widget.checkListItemModel.question}',style: TextStyle(
                      fontWeight: widget.checkListItemModel.type>Const.CHKLIST_TYPE_3?FontWeight.bold:FontWeight.normal
                    ),)),
                    SizedBox(width: 5,),
                    (widget.checkListItemModel.description!=null && widget.checkListItemModel.description!='')?GestureDetector(
                      onTap: ()async{
                        dynamic result = await DialogMgr.showDescriptionDialog(context, widget.checkListItemModel.description);
                        debugPrint("Dialog result : ${result}");
                      },
                      child: Icon(Icons.info,color: MyColors.colorConvert('#57a5e8'),size: 20,)
                    ):Container(),
                    SizedBox(width: 10,),
                  ],
                ),
              )),

              // type 3 = textbox
              Expanded(flex: 5, child: widget.checkListItemModel.type==Const.CHKLIST_TYPE_3?Container(
                padding: EdgeInsets.only(top: 5,bottom: 5),
                height: 45,
                child: Stack(
                  children: [
                    FocusScope(
                      child: Focus(
                        onFocusChange: (value) {
                          debugPrint('onFocusChange Type 3 : $value');
                          if(!value){
                            updateChecklist(value: answerControllerType3.text,mContext: context);
                          }
                        },
                        child: TextField(
                          textInputAction: TextInputAction.done,
                          onSubmitted: (value) {

                            updateChecklist(value: value,mContext: context);

                          },
                          controller: answerControllerType3,
                          decoration: new InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                            labelText: "",
                            filled: true,
                            fillColor: MyColors.colorConvert('#f4f4f4'),
                            border: textFieldOutlineInputBorder,
                            focusedBorder: textFieldOutlineInputBorder,
                            disabledBorder: textFieldOutlineInputBorder,
                            enabledBorder: textFieldOutlineInputBorder,
                            errorBorder: textFieldOutlineInputBorder,
                            focusedErrorBorder: textFieldOutlineInputBorder,

                            //fillColor: Colors.green
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                    ),
                    widget.taskModel.status_id==3?Container(
                      color: MyColors.colorConvert('#D1D1D1').withOpacity(0.5)
                    ):Container() 
                  ],
                ),
              ):
              // type 1 = yes/no
              widget.checkListItemModel.type==Const.CHKLIST_TYPE_1?
                  yesNoWidget():
              // type 2 = good/wrong
                widget.checkListItemModel.type==Const.CHKLIST_TYPE_2?
                  goodWrongWidget():
                widget.checkListItemModel.type == Const.CHKLIST_TYPE_5 ?
                TypeFiveWidget(
                  // answerControllerType5: answerControllerType5,
                  taskModel: widget.taskModel,
                  checkListItemModel: widget.checkListItemModel,
                  currentTab: widget.currentTab,
                  index: widget.index,
                  onSubmitted: (value,errorMessage,isValid) {

                    updateChecklist(value: value,mContext: context,isValid: isValid);

                    //
                  },

                ):
                (widget.checkListItemModel?.type == Const.CHKLIST_TYPE_6 && widget.checkListItemModel?.is_multiple == 0) ?
                TypeSixWidgetSingle(
                  taskModel: widget.taskModel,
                  checkListItemModel: widget.checkListItemModel,
                  currentTab: widget.currentTab,
                  index: widget.index,
                  onChange: (value) {
                    if(value==''){
                      return;
                    }
                    updateChecklist(value: value,mContext: context);
                  },
                ):
                (widget.checkListItemModel?.type == Const.CHKLIST_TYPE_6 && widget.checkListItemModel?.is_multiple == 1) ?
                TypeSixWidgetMultiple(
                  taskModel: widget.taskModel,
                  checkListItemModel: widget.checkListItemModel,
                  currentTab: widget.currentTab,
                  index: widget.index,
                  onChange: (value) {
                    updateChecklist(value: value,mContext: context);
                  },
                ):
              Container()
              ),
              Expanded(flex: 2, child: (widget.checkListItemModel.type!=Const.CHKLIST_TYPE_4)?GestureDetector(
                onTap: () async{
                  Map<String,dynamic> result = await showDialog(
                    context: context,
                    builder: (_) => DialogImgUploadChecklist(checkListItemModel: widget.checkListItemModel,),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  child: StreamBuilder<int>(
                    initialData: 0,
                    stream: blocAttachmentIcon.attachmentIconStream,
                    builder: (context, snapshot) {
                      if(snapshot.hasData){
                        return Stack(
                          alignment: Alignment.topRight,
                          children: [

                            Transform.rotate(
                                angle: 45 * math.pi / 180,
                                child: Icon(Icons.attach_file_outlined, size: 40, color: MyColors.colorConvert('#57a5e8'),)
                            ),
                            Container(
                              height: 25,width: 25,
                              padding: EdgeInsets.all(2),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(100)),
                                  border: Border.all(width: 1,color: Colors.white),
                                  color: MyColors.colorConvert('#57a5e8')
                              ),
                              child: Text('${snapshot.data}',
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ],
                        );
                      }
                      return Container();
                    }
                  ),
                ),
              ):Container(),),
              // Show Log
              Expanded(flex: 2, child: (widget.checkListItemModel.type!=Const.CHKLIST_TYPE_4)?GestureDetector(
                onTap: () {
                  setState(() {
                    showLog= (!showLog);
                  });
                },
                child: Icon(Icons.history, size: 40, color: MyColors.colorConvert('#57a5e8'),)):Container()
              ),
              // Tick Icon
              Expanded(flex: 2, child: (widget.checkListItemModel.type!=Const.CHKLIST_TYPE_4)?Align(
                alignment: Alignment.centerLeft,
                  child: StreamBuilder<String>(
                    initialData: null,
                    stream: blocChecklistResult.checklistResultStream,
                    builder: (context, snapshot) {
                      return Icon(Icons.check, size: 40, color: snapshot.data==null?MyColors.colorConvert('#f6f6f6'):MyColors.colorConvert('#57a5e8'),);
                    }
                  )):Container()),
            ],
          ),
          showLog?Column(
            children: [
              SizedBox(height: 5,),
              Container(
                height: 60,
                width: double.infinity,

                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  border: Border.all(color: MyColors.colorConvert('#DDDDDD')),
                  color: MyColors.colorConvert('#EEEEEE'),
                ),
                child: ListView.separated(
                  separatorBuilder: (context, index) {
                    return SizedBox(width: 20,);
                  },
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: listLog.length,
                  itemBuilder: (context, index) {
                    Map<String,dynamic> mapData = listLog[index];

                  return Container(
                    padding: EdgeInsets.only(left: 3,right: 3),
                    alignment: Alignment.center,
                    child: index==0?Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('${mapData['finish_date']}'),
                        Text('${mapData['answer']}'),
                      ],
                    ):Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('${DateFormat('dd-MM-yyyy').format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(mapData['finish_date']))}'),
                        Text('${mapData['answer']}'),
                      ],
                    ),
                  );
                },),
              )
            ],
          ):Container()
        ],
      ),
    );
  }
  TextStyle styleHeader = TextStyle(fontSize: 16, color: MyColors.colorConvert('#8b8b8b'));
  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: MyColors.colorConvert('#e5e5e5')),
  );

  Widget yesNoWidget({BuildContext mContext}){
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Row(
            children: [
              GestureDetector(
                onTap: () async{
                  if(widget.taskModel.status_id==3){
                    return;
                  }
                  if(isYes){
                    return;
                  }
                  isYes=(!isYes);
                  if(isYes){
                    /*{
                      id: 979725,
                      task_id: 107485,
                      question: Quetion3,
                      type: 3,
                      result: null,
                      removed: 0,
                      description: Quetion3,
                      order_number: 1500,
                      unique_hash: a84f61ce597f28612e2099fb82b4740d,
                      clone_checklist_id: 979713
                    }*/
                    // update checklist table here
                    isNo=false;
                    updateChecklist(value: '1',mContext: mContext);
                  }else{
                    isNo=true;
                  }
                  setState(() {});
                },
                child: Container(
                  padding: isYes?EdgeInsets.all(5):EdgeInsets.zero,
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      color: MyColors.colorConvert('#f6f6f6'),
                      border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                  ),
                  child: isYes?Container(

                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: widget.taskModel.status_id==3?MyColors.colorConvert('#D1D1D1'):MyColors.colorConvert('#57a5e8'),
                        border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                    ),
                  ):Container(),
                ),
              ),
              SizedBox(width: 10,),
              Text('Ja',style: TextStyle(fontSize: answerFontSize,color: MyColors.colorConvert('#909090')),)
            ],
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          flex: 1,
          child: Row(
            children: [
              GestureDetector(
                onTap: () async{
                  if(widget.taskModel.status_id==3){
                    return;
                  }
                  if(isNo){
                    return;
                  }
                  isNo=(!isNo);
                  if(isNo){
                    isYes=false;
                    /*{
                      id: 979725,
                      task_id: 107485,
                      question: Quetion3,
                      type: 3,
                      result: null,
                      removed: 0,
                      description: Quetion3,
                      order_number: 1500,
                      unique_hash: a84f61ce597f28612e2099fb82b4740d,
                      clone_checklist_id: 979713
                    }*/
                    // update checklist table here
                    updateChecklist(value: '0',mContext: mContext);

                  }else{
                    isYes=true;
                  }
                  setState(() {});
                },
                child: Container(
                  padding: isNo?EdgeInsets.all(5):EdgeInsets.zero,
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      color: MyColors.colorConvert('#f6f6f6'),
                      border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                  ),
                  child: isNo?Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: widget.taskModel.status_id==3?MyColors.colorConvert('#D1D1D1'):MyColors.colorConvert('#57a5e8'),
                        border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                    ),
                  ):Container(),
                ),
              ),
              SizedBox(width: 10,),
              Text('Nee',style: TextStyle(fontSize: answerFontSize,color: MyColors.colorConvert('#909090')),)
            ],
          ),
        )
      ],);
  }
  bool isGood = false;
  bool isWrong = false;
  Widget goodWrongWidget({BuildContext mContext}){
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Row(
            children: [
              GestureDetector(
                onTap: () async{
                  if(widget.taskModel.status_id==3){
                    return;
                  }
                  if(isGood){
                    return;
                  }
                  isGood=(!isGood);
                  if(isGood){
                    isWrong=false;
                    updateChecklist(value: '1',mContext: mContext);
                  }else{
                    isWrong=true;
                  }
                  setState(() {});
                },
                child: Container(
                  padding: isGood?EdgeInsets.all(5):EdgeInsets.zero,
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      color: MyColors.colorConvert('#f6f6f6'),
                      border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                  ),
                  child: isGood?Container(

                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: widget.taskModel.status_id==3?MyColors.colorConvert('#D1D1D1'):MyColors.colorConvert('#57a5e8'),
                        border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                    ),
                  ):Container(),
                ),
              ),
              SizedBox(width: 10,),
              Text('Goed',style: TextStyle(fontSize: answerFontSize,color: MyColors.colorConvert('#909090')),)
            ],
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          flex: 1,
          child: Row(
            children: [
              GestureDetector(
                onTap: () async{
                  if(widget.taskModel.status_id==3){
                    return;
                  }
                  if(isWrong){
                    return;
                  }
                  isWrong=(!isWrong);
                  if(isWrong){
                    isGood=false;
                    updateChecklist(value: '0',mContext: mContext);
                  }else{
                    isGood=true;
                  }
                  setState(() {});
                },
                child: Container(
                  padding: isWrong?EdgeInsets.all(5):EdgeInsets.zero,
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      color: MyColors.colorConvert('#f6f6f6'),
                      border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                  ),
                  child: isWrong?Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: widget.taskModel.status_id==3?MyColors.colorConvert('#D1D1D1'):MyColors.colorConvert('#57a5e8'),
                        border: Border.all(color: MyColors.colorConvert('#eaeaea'))
                    ),
                  ):Container(),
                ),
              ),
              SizedBox(width: 10,),
              Text('Fout',style: TextStyle(fontSize: answerFontSize,color: MyColors.colorConvert('#909090')),)
            ],
          ),
        )
      ],);
  }
  double answerFontSize = 14;
}