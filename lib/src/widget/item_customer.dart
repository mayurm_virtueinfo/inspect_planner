import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/api/api_request.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/models/customer_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/my_toast.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class ItemCustomer extends StatefulWidget {
  CustomerModel customerModel;
  ItemCustomer({this.customerModel});

  @override
  _ItemCustomerState createState() => _ItemCustomerState();
}

class _ItemCustomerState extends State<ItemCustomer> {

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10,bottom:10,left: 10,right: 10),
     /* decoration: BoxDecoration(
        color: MyColors.colorConvert('#ffffff'),
        border: Border.all(color: MyColors.colorConvert('#e0e0e0'))
      ),*/
      child: Row(
        children: [
          Expanded(flex: 8, child: Text(widget.customerModel.name, style: TextStyle(
            color: MyColors.colorConvert('#787878'),
            fontSize: 16
          ))),

          Expanded(
              flex: 1,
              child: Container(

                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    onTap: () async{
                      DialogMgr.showProgressDialog(context);
                      Map<String,dynamic> mapClientLoginResult = await ApiRequest.getClientLogin(customerModel: widget.customerModel);
                      Navigator.of(locator<IPKeys>().keyDialogProgress.currentContext,).pop();
                      if(!mapClientLoginResult['status']){
                        MyToast.showToast('Error while login', context);
                        return;
                      }
                      await Utility.processLogin(mContext: context,apiResult: mapClientLoginResult);


                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                        decoration: BoxDecoration(
                            color: MyColors.colorConvert('#F4F4F4'),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            border: Border.all(color: MyColors.colorConvert('#CCCCCC'))
                        ),
                        child: Text('Inloggen', style: TextStyle(fontSize: 16, color: Colors.black))),
                  )
              )
          ),
        ],
      ),
    );
  }
  TextStyle styleHeader = TextStyle(fontSize: 16, color: Colors.black);
}