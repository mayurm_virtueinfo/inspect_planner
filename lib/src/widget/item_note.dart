import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';

import 'package:inspect_planner/src/bloc/detail_tab_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_delete_note.dart';
import 'package:inspect_planner/src/dialog/dialog_edit_note.dart';
import 'package:inspect_planner/src/dialog/dialog_img_upload_task.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/note_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/models/user_table_model.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class ItemNote extends StatefulWidget {
  TaskModel taskModel;
  NoteModel noteModel;
  int index;
  ItemNote({this.taskModel,this.noteModel,this.index});

  @override
  _ItemNoteState createState() => _ItemNoteState();
}

class _ItemNoteState extends State<ItemNote> {


  @override
  void initState() {
    // TODO: implement initState
    initItemData();
    super.initState();
  }

  String noteUserName='';
  String functionValues='';
  String linkedToUser='';
  void initItemData() async{
    getNoteUserName();
    getAssignToUserValue();
    getLinkedToUser();
  }
  void getLinkedToUser() async{
    debugPrint("linked to user : ${widget.noteModel.assignedto_user_id}");
    if(widget.noteModel.assignedto_user_id != null){
      String sql = 'select * from ${AppTables.USER} where customer_id = ? and removed = ? and id = ?';
      List<dynamic> sqlValues = [appUserModel.customer_id,0,widget.noteModel.assignedto_user_id];
      List<Map<String,dynamic>> listUser = await appDatabase.rawQuery(sql,sqlValues);
      if(listUser.length>0){
        Map<String,dynamic> mapUser = listUser.first;
        linkedToUser = mapUser['name'];
      }
      setState(() {});
    }
  }
  void getNoteUserName() async{
    if(widget.noteModel.user_id != null) {
      String sql = 'select * from ${AppTables.USER} where id = ?';
      List<dynamic> values = [widget.noteModel.user_id];
      List<dynamic> userResult = await appDatabase.rawQuery(sql, values);
      debugPrint('userResult ---1: ${userResult}');
      if (userResult.length > 0) {
        UserTableModel userTableModel = UserTableModel.fromMap(
            map: userResult[0]);
        noteUserName = userTableModel.name;
        setState(() {});
      }
    }
  }

  void getFunctionValues()async{
    if(widget.noteModel.user_id != null) {
      String sql = 'select usergroup_id from ${AppTables.USERS_GROUPS} where user_id = ?';
      List<dynamic> values = [widget.noteModel.user_id];
      List<dynamic> userResult = await appDatabase.rawQuery(sql, values);
      debugPrint('users groups ---1: ${userResult}');
      List<int> lstUserGroupId = [];

      userResult.forEach((element) {
        lstUserGroupId.add(element['usergroup_id']);
      });
      String strUserGroupId = lstUserGroupId.join(",");
      debugPrint("strUserGroupId : ${strUserGroupId}");

      String sqlFunction = 'select name from ${AppTables.FUNCTIONS} where id IN ($strUserGroupId)';
      List<dynamic> functionResult = await appDatabase.rawQuery(sqlFunction);
      debugPrint('function result : ${functionResult}');
      List<String> functionNames = [];
      functionResult.forEach((element) {
        functionNames.add('${element['name']}');
      });
      functionValues = functionNames.join(', ');
      setState(() {});
    }
  }
  void getAssignToUserValue(){
    if(widget.noteModel.assignedto_user_id == null){
      functionValues = 'Admin';
    }else{
      getFunctionValues();
    }
    setState(() {});
  }

  @override
  void didUpdateWidget(covariant ItemNote oldWidget) {
    // TODO: implement didUpdateWidget
    initItemData();
    super.didUpdateWidget(oldWidget);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: MyColors.colorConvert('#ffffff'),
        border: Border.all(color: MyColors.colorConvert('#e0e0e0'))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
      /*'${widget.noteModel.note}'*/
                      Text('${noteUserName}',style: TextStyle(
                          fontSize: 20,
                          color: MyColors.colorConvert('#54a3e8')
                        ),
                      ),
                      Text('${widget.noteModel.note}',style: TextStyle(
                          color: MyColors.colorConvert('#797979'),
                          fontSize: 14
                      ),),
                    ],
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text('#${widget.index}',style: TextStyle(
                    fontSize: 20,
                    color: MyColors.colorConvert('#797979')
                  ),),
                  Text('${DateFormat('dd-MM-yyyy').format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(widget.noteModel.create_date))} at ${DateFormat('HH:mm').format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(widget.noteModel.create_date))}',style: TextStyle(
                    fontSize: 16,
                    color: MyColors.colorConvert('#c3c3c3')
                  ),),
                  linkedToUser==''?Container():Text('${AppStrings.linkedTo}: $linkedToUser',style: TextStyle(
                      fontSize: 16,
                      color: MyColors.colorConvert('#c3c3c3')
                  ),)
                ],
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 15,bottom: 15),
            width: double.infinity,
            height: 1,
            color: MyColors.colorConvert('#f9f9f9'),
          ),

          /*SizedBox(height: 10,),
          functionValues.length==0?Container():Column(
            children: [
              Text('$functionValues',style: TextStyle(
                  fontSize: 16,
                  color: MyColors.colorConvert('#c3c3c3')
              ),),
              SizedBox(height: 10,),
            ],
          ),*/

          appUserModel.id==widget.noteModel.user_id?Row(
            children: [
              GestureDetector(
                onTap: () {
                  showDialog(context: context, builder: (context) => DialogDeleteNote(noteModel: widget.noteModel,),);
                },
                child: Row(
                  children: [
                    Icon(Icons.clear_rounded,size:20,color: MyColors.colorConvert('#d0021b'),),
                    SizedBox(width: 5,),
                    Text('Verwijderen',style: TextStyle(
                      color: MyColors.colorConvert('#f63a36'),
                      fontSize: 14
                    ),)
                  ],
                ),
              ),
              SizedBox(width: 15,),
              GestureDetector(
                onTap: () {
                  showDialog(context: context, builder: (context) => DialogEditNote(noteModel: widget.noteModel,),);
                },
                child: Row(
                  children: [
                    Icon(Icons.edit,size:20,color: MyColors.colorConvert('#5bb0ec'),),
                    SizedBox(width: 5,),
                    Text('Aanpassen',style: TextStyle(
                        color: MyColors.colorConvert('#6eace2'),
                        fontSize: 14
                    ),)
                  ],
                ),
              )
            ],
          ):Container()

        ],
      ),
    );
  }

}