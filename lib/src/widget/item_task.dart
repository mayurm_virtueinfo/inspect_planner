import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class ItemTask extends StatefulWidget {
  int index;
  String rowColor;
  TaskModel taskModel;
  int currentTab;
  ItemTask({this.index,this.rowColor,this.taskModel,this.currentTab});

  @override
  _ItemTaskState createState() => _ItemTaskState();
}

class _ItemTaskState extends State<ItemTask> {

  StatusIconBloc blocStatusIcon = StatusIconBloc();
  NoteIconBloc blocNoteIcon = NoteIconBloc();
  AttachmentIconBloc blocAttachmentIcon = AttachmentIconBloc();
  String frequencyName = '';

  @override
  void initState() {
    // TODO: implement initState
    initItemData();
    initFrequencyName();
    setState(() {});
    super.initState();
  }
  void initFrequencyName(){
    frequencyName = Utility.getFrequencyName(taskModel: widget.taskModel);
    setState(() {});
  }

  void initItemData() async{
    try {
      strStartDate = widget.taskModel.start_date == null ? '' : DateFormat('dd-MM-yyyy').format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(appCurrentTab==3?widget.taskModel.finish_date:widget.taskModel.start_date)) ?? '';
    }catch(e){
      strStartDate = "err : ${widget.taskModel.start_date}";
    }
    getStatusCount();
    getNoteCount();
    int qCount = await Utility.getAttachmentCount(taskModel: widget.taskModel);
    blocAttachmentIcon.attachmentIconEventSink.add(qCount);
  }
  void getStatusCount()async{
    String iconPath = '';
    // debugPrint("called item task");

    List<Map<String,dynamic>> listStatus = await appDatabase.rawQuery("select count(*) from ${AppTables.CHECKLISTITEM} where task_id = ${widget.taskModel.id} and result is not null");
    int qCount = Sqflite.firstIntValue(listStatus);
    // debugPrint("called item task listStatus : ${qCount}");
    int sCount = 0;
    if(qCount == 0){
      sCount=0;
    }
    else{
      sCount=1;
    }

    if(widget.currentTab == 0){
      if(sCount==1){
        iconPath = 'assets/status/reload-orange.png';
      }else{
        iconPath = 'assets/status/calendar-dark.png';
      }
    }
    else if(widget.currentTab == 1){
      if(sCount==1){
        iconPath = 'assets/status/reload-red.png';
      }else{
        iconPath = 'assets/status/calendar-red.png';
      }
    }
    else if(widget.currentTab == 2){
      if(sCount==1){
        iconPath = 'assets/status/reload-orange.png';
      }else{
        iconPath = 'assets/status/calendar-dark.png';
      }
    }else if(widget.currentTab == 3){
        iconPath = 'assets/status/check-blue.png';
    }
    blocStatusIcon.statusIconEventSink.add(iconPath);
  }
  void getNoteCount()async{

    List<Map<String,dynamic>> listStatus = await appDatabase.rawQuery("select count(*) from ${AppTables.NOTE} where task_id = ${widget.taskModel.id} and removed = 0");
    int qCount = Sqflite.firstIntValue(listStatus);
    // debugPrint("count note : ${qCount}");
    blocNoteIcon.noteIconEventSink.add(qCount);
  }

  String strStartDate = "";
  @override
  void didUpdateWidget(ItemTask oldWidget) {

    super.didUpdateWidget(oldWidget);
    initItemData();
    initFrequencyName();
  }
  @override
  Widget build(BuildContext context) {
    TextStyle styleHeader = TextStyle(fontSize: 16, color: MyColors.colorConvert(widget.rowColor));//'#8b8b8b'
    return Container(
      padding: EdgeInsets.only(top: 10,bottom:10,left: 10,right: 10),
      decoration: BoxDecoration(
        color: MyColors.colorConvert('#ffffff'),
        border: Border.all(color: MyColors.colorConvert('#e0e0e0'))
      ),
      child: Row(
        children: [
          //Deadline
          // ${widget.index}-
          Expanded(flex: 3, child: Text('${strStartDate}', style: styleHeader)),
          Expanded(flex: 8, child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Flexible(child: Column(
              //   crossAxisAlignment: CrossAxisAlignment.start,
              //   children: [
              //     Text('${widget.taskModel.title??''}', style: styleHeader),
              //     Text('${Utility.getTitle(widget.taskModel)}', style: styleHeader),
              //     (widget.taskModel.status_id == 3 && widget.taskModel.force_completed == 1)?Padding(
              //         padding: const EdgeInsets.only(left: 10),
              //         child: Text('!',
              //           style: TextStyle(
              //               fontSize: 20,
              //               color: Colors.red,
              //               fontWeight: FontWeight.bold
              //           ),
              //         )
              //     ):Container(),
              //   ],
              // )),
              Flexible(
                child: RichText(text: TextSpan(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextSpan(
                      text: '${widget.taskModel.title??''}',
                      style: styleHeader,
                      // recognizer: TapGestureRecognizer()..onTap = () {
                      //   // Single tapped.
                      // },
                    ),
                    TextSpan(
                        text: '${Utility.getTitle(widget.taskModel)}',
                        style: styleHeader,
                        // recognizer:  DoubleTapGestureRecognizer()..onDoubleTap = () {
                        //   // Double tapped.
                        // }
                    ),
                    TextSpan(
                      text: (widget.taskModel.status_id == 3 && widget.taskModel.force_completed == 1)?'  !':'',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.red,
                          fontWeight: FontWeight.bold
                      )
                    ),
                    WidgetSpan(child: StreamBuilder<int>(
                        initialData: 0,
                        stream: blocNoteIcon.noteIconStream,
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return snapshot.data==0?Container():Padding(
                                padding: const EdgeInsets.only(left: 10,top: 5),
                                child: Image.asset('assets/icon/icon-chat-dark-grey.png',width: 20,)
                            );
                          }
                          return Container();
                        }
                    ),),
                    WidgetSpan(child: StreamBuilder<int>(
                        initialData: 0,
                        stream: blocAttachmentIcon.attachmentIconStream,
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return snapshot.data==0?Container():Padding(
                              padding: const EdgeInsets.only(left: 10,top: 5),
                              child: /*Icon(Icons.attach_file_outlined, size: 25, color: MyColors.loginButtonColor,)*/
                              Stack(
                                alignment: Alignment.topRight,
                                children: [

                                  Transform.rotate(
                                      angle: 45 * math.pi / 180,
                                      child: Icon(Icons.attach_file_outlined, size: 40, color: MyColors.colorConvert('#57a5e8'),)
                                  ),
                                  Container(
                                    height: 25,width: 25,
                                    padding: EdgeInsets.all(2),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(100)),
                                        border: Border.all(width: 1,color: Colors.white),
                                        color: MyColors.colorConvert('#57a5e8')
                                    ),
                                    child: Text('${snapshot.data}',
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }
                          return Container();
                        }
                    ),)
                  ],
                )),
              ),
            ],
          )),
          Expanded(flex: 3, child: Text(widget.taskModel.tasktype_name??'', style: styleHeader)),
          Expanded(flex: 3, child: Text(widget.taskModel.location_name??'', style: styleHeader)),
          Expanded(flex: 4, child: Text(frequencyName??'', style: styleHeader)),
          Expanded(flex: 2, child: Container(
            // color: Colors.grey,
            child: StreamBuilder<String>(
              initialData: '',
              stream: blocStatusIcon.statusIconStream,
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  if(snapshot.data==''){
                    return Container();
                  }else {
                    return Image.asset(snapshot.data,height: 50,width: 50,);
                  }
                }
                return Container();
              }
            ),
          )/*Text('${widget.taskModel.status_id??''}', style: styleHeader)*/),
        ],
      ),
    );
  }

}