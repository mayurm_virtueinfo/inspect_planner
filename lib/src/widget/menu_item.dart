import 'package:inspect_planner/src/utils/height_sizes.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  Function onTap;
  String text;
  MenuItem({@required this.onTap,@required this.text});
  @override
  Widget build(BuildContext context) {
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    final drawer_memnu_menu_height =
        HeightSizes.drawer_memnu_menu_height * devicePixelRatio;
    return GestureDetector(
      onTap: this.onTap,
      /*onTap: () {
        if (_scaffoldKey.currentState.isDrawerOpen) {
          Navigator.pop(context);
          Navigator.of(context)
              .pushNamed(MemberProfileScreen.routeName);
        }
      },*/
      child: Container(
        height: drawer_memnu_menu_height,
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 20),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.account_circle,
              color: MyColors.backgroundColor,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              /*"Profile"*/this.text,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: MyColors.backgroundColor),
            ),
          ],
        ),
      ),
    );
  }
}
