import 'package:flutter/material.dart';

class TitleWidget extends StatelessWidget {
  String title;
  TitleWidget({this.title});
  @override
  Widget build(BuildContext context) {
    return Text(this.title,style: TextStyle(
        fontWeight: FontWeight.bold
    ),);
  }
}
