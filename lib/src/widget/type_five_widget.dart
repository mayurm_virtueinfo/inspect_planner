import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/alert_bloc.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/checklist_result_bloc.dart';
import 'package:inspect_planner/src/bloc/loading/loading_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_validation.dart';
import 'package:inspect_planner/src/models/alert_model.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/const.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class TypeFiveWidget extends StatefulWidget {
  Function onSubmitted;
  Function onChanged;
  CheckListItemModel checkListItemModel;
  int index;
  String rowColor;
  TaskModel taskModel;
  int currentTab;
  TypeFiveWidget({this.onChanged, this.onSubmitted, this.index, this.rowColor, this.taskModel, this.currentTab, this.checkListItemModel});

  @override
  _ItemTaskState createState() => _ItemTaskState();
}

class _ItemTaskState extends State<TypeFiveWidget> {

  // RegExp regexFormat = RegExp(r'-?[0-9]*\.?[0-9]+');
  TextEditingController answerControllerType5 = TextEditingController();
  String maskNewFormat = '';

  String strStartDate = "";
  int totalInputTextMaxLength = 1;

  var maskFormatter;
  bool isDialogOpen = false;
  final focusNode = FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    maskFormatter = new MaskTextInputFormatter(mask: maskNewFormat, filter: {maskCharacter: Const.regexChklistInputText});
    prefillData();
    buildMaskFormat();

    focusNode.addListener(() {

      if(!focusNode.hasFocus){
        // FocusScope.of(context).requestFocus(FocusNode());
        print("Has focus: ${focusNode.hasFocus}");
        print("Has focus: isDialogOpen : ${isDialogOpen}");
        onChangeText(answerControllerType5.text);
      }
    });
    super.initState();

  }

  @override
  void didUpdateWidget(TypeFiveWidget oldWidget) {
      prefillData();
      buildMaskFormat();
    super.didUpdateWidget(oldWidget);
  }
  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }
  String drMessage = '';
  String drFromRange = '';
  String drToRange = '';
  String drDecimal = '';
  String maskCharacter = '0';

  void buildMaskFormat() {
    Map<String, dynamic> digitRange = json.decode(widget?.checkListItemModel?.digit_range);

    debugPrint('digitRange : $digitRange');

      drDecimal = digitRange['decimal'];
      drFromRange = digitRange['from_range'];
      drToRange = digitRange['to_range'];
      drMessage = digitRange['message'];

    List<String> drFRL = drFromRange.split(',');
    int cFromRange = int.parse(drFRL.length > 0 ? drFRL[0] : 0);

    List<String> drTRL = drToRange.split(',');
    int cToRange = int.parse(drTRL.length > 0 ? drTRL[0] : 0);

    int bigValue = (cFromRange > cToRange) ? cFromRange : cToRange;
    debugPrint('bigValue : $bigValue');

    int minusCharacterLength = 1;
    int bigValueLength = bigValue.toString().length;
    int decimalValueLength = int.parse(drDecimal);
    int commaValueLength = 1;

    int mTotalInputTextMaxLength = (minusCharacterLength + bigValueLength + decimalValueLength + commaValueLength);

    debugPrint('bigValue length: ${bigValue.toString().length}');

    debugPrint('drDecimal : $drDecimal');
    debugPrint('drFromRange : $drFromRange');
    debugPrint('drToRange : $drToRange');
    debugPrint('drMessage : $drMessage');

    debugPrint('mTotalInputTextMaxLength : $mTotalInputTextMaxLength');
    totalInputTextMaxLength = mTotalInputTextMaxLength;
    String strNFormat = '';
    for (int i = 0; i < (minusCharacterLength + bigValueLength); i++) {
      strNFormat += maskCharacter;
    }
    strNFormat += ',';
    for (int i = 0; i < decimalValueLength; i++) {
      strNFormat += maskCharacter;
    }
    debugPrint('strNFormat : $strNFormat');

      maskNewFormat = strNFormat;
      maskFormatter.updateMask(mask: maskNewFormat, filter: {maskCharacter: Const.regexChklistInputText});
    setState(() {
    });
  }

  void prefillData() {
    setState(() {
      String changeValue = widget.checkListItemModel.result;

      if (changeValue == null || changeValue == '') {
        // answerControllerType5.text = '';
        answerControllerType5 = TextEditingController(text: '');
        // answerControllerType5.selection = TextSelection.fromPosition(TextPosition(offset: answerControllerType5.text.length));

      } else {
        answerControllerType5 = TextEditingController(text: changeValue);
        // answerControllerType5.selection = TextSelection.fromPosition(TextPosition(offset: answerControllerType5.text.length));
      }
      answerControllerType5.selection = TextSelection.fromPosition(TextPosition(offset: answerControllerType5.text.length));
      debugPrint('answerControllerType5 : ${answerControllerType5.text}');
      // maskFormatter.updateMask(newValue: answerControllerType5.value,mask: maskNewFormat, filter: { "#": RegExp('-?[0-9]*') });

    });
  }

  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: MyColors.colorConvert('#e5e5e5')),
  );



  void onChangeText(String value) async{
    debugPrint('onChangeText : value : $value');

    bool isValid = false;
    if (Utility.isValidString(value: value)) {
      debugPrint('onChangeText : searchKeywords : Valid');

      // return;

      if (value == '') {
        debugPrint('onChangeText : value 1 : $value');
        widget.onSubmitted(value, drMessage, isValid);
        return;
      } else if (value == '-') {
        debugPrint('onChangeText : value 2 : $value');
        widget.onSubmitted(value, drMessage, isValid);

        return;
      }
      String nValue = value;
      debugPrint('onChangeText : nValue--- : $nValue');
      List<String> sArray = nValue.split(',');
      String first = sArray.first;
      String second = sArray.last;
      debugPrint('onChangeText : first--- : $first');
      debugPrint('onChangeText : second--- : $second');
      String mS = '';
      if(second.contains('-')){
        mS=second.replaceAll('-', '');
        mS = '-'+first+','+mS;
      }
      else if(first.contains('-')){
        mS=first.replaceAll('-', '');
        mS = '-'+mS+','+second;
      }else{
        mS = nValue;
      }
      debugPrint('onChangeText : mS--- : $mS');
      String actualValue = mS.replaceAll(',', '.');
      String actualFrom = drFromRange.replaceAll(',', '.');
      String actualTo = drToRange.replaceAll(',', '.');
      double doubleFrom = double.parse(actualFrom);
      double doubleTo = double.parse(actualTo);
      double doubleActual = double.parse(actualValue);

      AppLog.debugApp('onSubmitted : actualValue : $actualValue');
      AppLog.debugApp('onSubmitted : actualFrom : $actualFrom');
      AppLog.debugApp('onSubmitted : actualTo : $actualTo');

      AppLog.debugApp('onSubmitted : doubleActual : $doubleActual');
      AppLog.debugApp('onSubmitted : doubleFrom : $doubleFrom');
      AppLog.debugApp('onSubmitted : doubleTo : $doubleTo');

      int intFrom = doubleFrom.toInt();
      int intTo = doubleTo.toInt();
      int intActual = doubleActual.toInt();

      AppLog.debugApp('onSubmitted : intActual : $intActual');
      AppLog.debugApp('onSubmitted : intFrom : $intFrom');
      AppLog.debugApp('onSubmitted : intTo : $intTo');

      if (doubleActual >= doubleFrom && doubleActual <= doubleTo) {
        isValid = true;
      }
      debugPrint('onSubmitted : nValue : $nValue');
      if (!isValid) {
        debugPrint('onSubmitted : showDialog ');
        showValidationDialog(drMessage);

        widget.onSubmitted(value, drMessage, isValid);
      }else{
        // locator<AlertBloc>().alertEventSink.add(null);
        widget.onSubmitted(value, drMessage, isValid);
      }

    } else {
      if (value == '') {
        widget.onSubmitted(value, drMessage, isValid);
      }
      showValidationDialog(drMessage);
    }
  }
  void showValidationDialog(String message) async{
    debugPrint('showValidationDialog : isDialogOpen : $isDialogOpen');
    // await Future.delayed(const Duration(milliseconds: 500));
    // DialogMgr.showValidationDialog(context);
    locator<AlertBloc>().alertEventSink.add(AlertModel(
      show: true,
      message: message
    ));
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      height: 45,
      child: Stack(
        children: [
          TextField(
            focusNode: focusNode,
            textInputAction: TextInputAction.done,
            // maxLength: maskNewFormat.length,

            onChanged: (value) {
              debugPrint('onChanged : text : key : Type 5 : $value');
              String maskkedValue = value;
              String mNFormat = maskNewFormat;
              // String paddedValue = value.replaceAll(',', '');;
              String hString = value.replaceAll(',', '');
              String mString = hString.replaceAll(new RegExp(r'^0+(?=.)'), '');

              debugPrint('onChanged : text : mString : ${mString}');
              debugPrint('onChanged : text : mString : length : ${mString.length}');
              debugPrint('onChanged : text : maskNewFormat : length : ${maskNewFormat.length}');
              int padLeft  = 0;

              debugPrint('onChanged : text : padLeft : length : ${padLeft}');
              String paddedValueDemo = mString;
              String paddedValue = '';

              debugPrint('onChanged : text : padded : value : Type 5 : ${paddedValue}');

              if(mString.contains('-')){
                List<String> arrSplite = maskkedValue.split(',');
                String first = arrSplite[0];
                String second = arrSplite[1];
                if(first.contains('-')){
                  mNFormat  = '0'+maskNewFormat;
                }else
                if(second.contains('-')){
                  mNFormat  = maskNewFormat+'0';
                }else{
                  mNFormat  = maskNewFormat;
                }
              }
              else{
                mNFormat = maskNewFormat;
              }

              if(mString.contains('-')){
                padLeft  = mNFormat.length-2;
              }else{
                padLeft  = mNFormat.length-1;
              }
              if(paddedValueDemo.length<mNFormat.length){
                paddedValue = paddedValueDemo.padLeft(padLeft,'0');
                debugPrint('onChanged : text : check : if');
              }else if(paddedValueDemo.length==mNFormat.length){
                paddedValue = paddedValueDemo;
                debugPrint('onChanged : text : check : else if');
              }else{
                debugPrint('onChanged : text : check : else');
                paddedValue = paddedValueDemo.substring(1).padRight(padLeft,'0');
              }

              MaskTextInputFormatter textMask = new MaskTextInputFormatter(
                  mask: mNFormat,
                  filter: {maskCharacter: Const.regexChklistInputText},
                  initialText: paddedValue
              );
              String gMskText = textMask.getMaskedText();
              debugPrint('onChanged : text : mNFormat : ${mNFormat}');
              debugPrint('onChanged : text : padded : masked value : Type 5 : ${gMskText}');

              setState(() {
                answerControllerType5 = TextEditingController(
                  text: gMskText
                );
                answerControllerType5.selection = TextSelection.fromPosition(TextPosition(offset: answerControllerType5.text.length));
              });


              MaskTextInputFormatter mayur = new MaskTextInputFormatter(
                  mask: mNFormat,
                  filter: {maskCharacter: Const.regexChklistInputText},
                  initialText: maskkedValue
              );

              debugPrint('onChanged : text : testingMask : ${mayur.getMaskedText()}');
            },
            onSubmitted: (value) async{
              //enter-five
              onChangeText(value);
            },
            controller: answerControllerType5,
            textAlign: TextAlign.right,

            decoration: new InputDecoration(
              counterText: "",
              hintText: maskNewFormat,
              isDense: true,
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              labelText: "",
              filled: true,
              fillColor: MyColors.colorConvert('#f4f4f4'),
              border: textFieldOutlineInputBorder,
              focusedBorder: textFieldOutlineInputBorder,
              disabledBorder: textFieldOutlineInputBorder,
              enabledBorder: textFieldOutlineInputBorder,
              errorBorder: textFieldOutlineInputBorder,
              focusedErrorBorder: textFieldOutlineInputBorder,

              //fillColor: Colors.green
            ),
            keyboardType: TextInputType.numberWithOptions(decimal: true),
            inputFormatters: <TextInputFormatter>[
              // for below version 2 use this
              FilteringTextInputFormatter.allow(Const.regexChklistInputText),
// for version 2 and greater youcan also use this
//               maskFormatter,
              LengthLimitingTextInputFormatter(maskNewFormat.length+1)
            ],
          ),
          widget.taskModel.status_id == 3 ? Container(color: MyColors.colorConvert('#D1D1D1').withOpacity(0.5)) : Container()
        ],
      ),
    );
  }
}
