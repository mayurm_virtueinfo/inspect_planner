import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;
import 'package:multiselect/multiselect.dart';

class TypeSixWidgetMultiple extends StatefulWidget {
  Function onChange;
  CheckListItemModel checkListItemModel;
  int index;
  String rowColor;
  TaskModel taskModel;
  int currentTab;
  TypeSixWidgetMultiple({this.onChange,this.index,this.rowColor,this.taskModel,this.currentTab,this.checkListItemModel});

  @override
  _ItemTaskState createState() => _ItemTaskState();
}

class _ItemTaskState extends State<TypeSixWidgetMultiple> {
  // List<String> dropdownList;
  // String selectedValue;
  TextEditingController typeFiveController = TextEditingController();


  List<String> selectedValue= [];
  List<String> listDropdown = [];

  @override
  void initState() {
    // TODO: implement initState
    
    initChanges();
    super.initState();
  }

  void initChanges(){
    setState(() {
      listDropdown = widget?.checkListItemModel?.dropdown_answers?.split(',');
      if(widget?.checkListItemModel?.result != null){
        selectedValue = widget?.checkListItemModel?.result?.split(',');
      }else{
        selectedValue = [];
      }
    });

    debugPrint('widget checklist Multiple Dropdown Result : ${widget?.checkListItemModel?.result}');
    debugPrint('widget checklist Multiple Dropdown : ${widget.checkListItemModel.toJSON()}');
  }

  @override
  void didUpdateWidget(TypeSixWidgetMultiple oldWidget) {
    initChanges();
    super.didUpdateWidget(oldWidget);
  }

  String strStartDate = "";

  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(0),
    // borderSide: null,
  );
  BorderRadiusGeometry borderRadius = BorderRadius.all(Radius.circular(5));
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            // width: (dialogWidth/5)*3,
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                borderRadius: borderRadius,
                color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
            ),
            height: 50,
            child: DropDownMultiSelect(
              // childBuilder: (option) => Icon(Icons.info,color: MyColors.colorConvert('#57a5e8'),size: 20,),
              // menuItembuilder: (option) => Icon(Icons.info,color: MyColors.colorConvert('#57a5e8'),size: 20,),
              decoration: new InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                labelText: "",
                filled: false,
                fillColor: MyColors.colorConvert('#FF0000'),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                focusedErrorBorder: InputBorder.none,

                //fillColor: Colors.green
              ),
              onChanged: (List<String> x) {
                setState(() {
                  List<String> newList = new List<String>.from(x);
                  debugPrint('onChanged list : $newList');
                  String newString = newList.join(',');//.trim().replaceFirst(',', '');
                  debugPrint('onChanged string : $newString');
                  List<String> newFilteredList = new List<String>.from(newString.split(','));
                  debugPrint('onChanged newFilteredList : $newFilteredList');
                  debugPrint('onChanged join : ${newFilteredList.join(',')}');


                  debugPrint('onChanged x length: ${x.length}');
                  List<String> newX = newList.where((i) => i != '').toList();
                  for (final candidate in x) {
                    debugPrint('loop x : ${candidate}');
                  }
                  debugPrint('onChanged newX : ${newX}');
                  debugPrint('onChanged newX length : ${newX.length}');
                  debugPrint('onChanged x : ${x}');
                  debugPrint('onChanged x join : ${x.join(',')}');
                  selectedValue =newX;
                  widget.onChange(newX.join(','));
                });
              },
              options: listDropdown,
              selectedValues: selectedValue,
              whenEmpty: '',
            ),
          ),
          widget.taskModel.status_id==3?Container(
              color: MyColors.colorConvert('#D1D1D1').withOpacity(0.5),
              width: double.infinity,
              height: 50,
          ):Container()
        ],
      ),
    );
  }

}