import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/attachment_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/note_icon_bloc.dart';
import 'package:inspect_planner/src/bloc/status_icon_bloc.dart';
import 'package:inspect_planner/src/models/check_list_item_model.dart';
import 'package:inspect_planner/src/models/task_model.dart';
import 'package:inspect_planner/src/utils/app_log.dart';
import 'package:inspect_planner/src/utils/app_tables.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class TypeSixWidgetSingle extends StatefulWidget {
  Function onChange;
  CheckListItemModel checkListItemModel;
  int index;
  String rowColor;
  TaskModel taskModel;
  int currentTab;
  TypeSixWidgetSingle({this.onChange,this.index,this.rowColor,this.taskModel,this.currentTab,this.checkListItemModel});

  @override
  _TypeSixWidgetSingle createState() => _TypeSixWidgetSingle();
}

class _TypeSixWidgetSingle extends State<TypeSixWidgetSingle> {
  // List<String> dropdownList;
  // String selectedValue;
  TextEditingController typeFiveController = TextEditingController();


  List<DropdownMenuItem<String>> _dropdownList = [];
  String selectedValue;
  List<String> listDropdown = [];

  @override
  void initState() {
    // TODO: implement initState
    initChanges();
    super.initState();
  }
  void initChanges(){

    setState(() {
      listDropdown = widget?.checkListItemModel?.dropdown_answers?.split(',');

      debugPrint("-mm-drop-down-mm ${selectedValue = widget?.checkListItemModel?.result}");
      if(widget?.checkListItemModel?.result != null){
        bool isAvail = listDropdown.any((element) => element == widget?.checkListItemModel?.result);
        if(isAvail){
          selectedValue = widget?.checkListItemModel?.result;
        }else{
          selectedValue = null;
        }

      }else{

        selectedValue = null;
      }

      if(listDropdown != null ){
        _dropdownList = buildDropDownMenuItemsTaskType(listDropdown);
      }else{
        _dropdownList = [];
      }
    });

    AppLog.debugApp('initChanges checklist Single Dropdown : ${widget.checkListItemModel.toJSON()}');
  }
  @override
  void didUpdateWidget(TypeSixWidgetSingle oldWidget) {
    initChanges();
    super.didUpdateWidget(oldWidget);
  }

  List<DropdownMenuItem<String>> buildDropDownMenuItemsTaskType(List listItems) {
    List<DropdownMenuItem<String>> items = [];
    for (String listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: Text(
                listItem,
                style: TextStyle(fontStyle: FontStyle.normal, color: MyColors.colorConvert('#8c8c8c'), fontSize: 17, decoration: TextDecoration.none),
              ),
            ),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }


  String strStartDate = "";

  OutlineInputBorder textFieldOutlineInputBorder = OutlineInputBorder(
    borderRadius: new BorderRadius.circular(5.0),
    borderSide: new BorderSide(color: MyColors.colorConvert('#e5e5e5')),
  );
  BorderRadiusGeometry borderRadius = BorderRadius.all(Radius.circular(5));
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                border: Border.all(width: 1.0, color: MyColors.colorConvert('#e5e5e5')),
                borderRadius: borderRadius,
                color: MyColors.colorConvert('#f4f4f4') /*MyColors.dropdownBackgroudColor*/
            ),
            height: 50,
            child: DropdownButtonHideUnderline(

              child: DropdownButton(
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: MyColors.colorConvert('#d8d8d8'),
                    size: 25,
                  ),
                  value: selectedValue,
                  items: _dropdownList,
                  onChanged: (value) async {
                    FocusScope.of(context).requestFocus(FocusNode());
                    setState((){
                      selectedValue = value;
                    });
                    widget.onChange(value);
                  }),
            ),
          ),
          widget.taskModel.status_id==3?Container(
            color: MyColors.colorConvert('#D1D1D1').withOpacity(0.5),
            width: double.infinity,
            height: 50,
          ):Container()
        ] ,
      ),
    );
  }

}