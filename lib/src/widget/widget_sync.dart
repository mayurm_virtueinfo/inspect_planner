import 'package:flutter/material.dart';
import 'package:inspect_planner/main.dart';
import 'package:inspect_planner/src/bloc/attachment_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/cancel_sync_bloc.dart';
import 'package:inspect_planner/src/bloc/checklist_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/note_sync_count_bloc.dart';
import 'package:inspect_planner/src/bloc/show_hide_sync_progress_bloc.dart';
import 'package:inspect_planner/src/bloc/task_sync_count_bloc.dart';
import 'package:inspect_planner/src/dialog/dialog_cancel_sync_process.dart';
import 'package:inspect_planner/src/utils/app_strings.dart';
import 'package:inspect_planner/src/utils/dialog_mgr.dart';
import 'package:inspect_planner/src/utils/ip_keys.dart';
import 'package:inspect_planner/src/utils/my_colors.dart';
import 'package:inspect_planner/src/utils/utility.dart';

class WidgetSync extends StatelessWidget {
  bool showCancelButton;
  WidgetSync({this.showCancelButton});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double loginBoxHeight = (screenHeight / 10) * 4;
    double loginBoxWidth = (screenWidth / 5) * 3;
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double dialogWidth = (MediaQuery.of(context).size.width/3)*2;
    double dialogHeight = (MediaQuery.of(context).size.height/3)*2;
    return OrientationBuilder(
      builder: (context, orientation){
        bool isPortrait = (orientation == Orientation.portrait);
        double loginBoxHeight = (screenHeight / 6) * (isPortrait?3:4);
        return Material(
          key: locator<IPKeys>().keyAppSync,
          type: MaterialType.transparency,
          child: Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Container(

              padding: EdgeInsets.all(20),
              width: (screenWidth/5)*3,
              height: loginBoxHeight/1.75,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: MyColors.colorConvert('#5BB0EC')
              ),
              child: Column(
                children: [
                  StreamBuilder<bool>(
                      initialData: true,
                      stream: locator<ShowHideSyncProgressBloc>().showHideSyncProgressStream,
                      builder: (context, snapshot) {
                        if(snapshot.hasData && snapshot.data){
                          return GestureDetector(
                            onTap: ()async{
                              String result = await showDialog(context: context, builder: (context) => DialogCancelSyncProcess(),);
                              debugPrint('dialog result : $result');
                              if(result == 'yes'){
                                locator<CancelSyncBloc>().cancelSyncEventSink.add(true);
                              }
                            },
                            child: Container(
                              // padding: EdgeInsets.only(right:AppBar().preferredSize.height),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  (this.showCancelButton==true)?Icon(Icons.clear,color: Colors.white,size: 35,):Container()
                                ],
                              ),
                            ),
                          );
                        }
                        return Container();
                      }
                  ),

                  Expanded(
                    flex: 1,

                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(
                              flex: 1,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    /*GestureDetector(
                                      onTap: () {
                                        locator<TaskSyncCountBloc>().taskSyncCountEventSink.add(1);
                                        locator<AttachmentSyncCountBloc>().attachmentSyncCountEventSink.add(2);
                                        locator<ChecklistSyncCountEventSink>().checklistSyncCountEventSink.add(3);
                                        locator<NoteSyncCountBloc>().noteSyncCountEventSink.add(4);
                                        locator<ShowHideSyncProgressBloc>().showHideSyncProgressEventSink.add(false);
                                      },
                                      child: Text("test")
                                    ),*/
                                    StreamBuilder<bool>(
                                        initialData: true,
                                        stream: locator<ShowHideSyncProgressBloc>().showHideSyncProgressStream,
                                        builder: (context, snapshot) {
                                          if(snapshot.hasData && snapshot.data){
                                            return Column(
                                              children: [
                                                SizedBox(height: 20,),
                                                Text(AppStrings.syncMessage,style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16
                                                ),textAlign: TextAlign.center,),
                                                SizedBox(height: 20,),
                                                SizedBox(
                                                  width: 40,
                                                  height: 40,
                                                  child: CircularProgressIndicator(
                                                    strokeWidth: 1.5,
                                                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                                                  ),
                                                ),
                                              ],
                                            );
                                          }
                                          return Container();
                                        }
                                    ),
                                    SizedBox(height: 20,),
                                    StreamBuilder<int>(
                                        stream: locator<TaskSyncCountBloc>().taskSyncCountStream,
                                        builder: (context, snapshot) {
                                          if(snapshot.hasData && snapshot.data>0){
                                            return Text("${snapshot.data} ${AppStrings.task_synced}",style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white
                                            ),);
                                          }
                                          return Container();
                                        }
                                    ),
                                    StreamBuilder<int>(
                                        stream: locator<AttachmentSyncCountBloc>().attachmentSyncCountStream,
                                        builder: (context, snapshot) {
                                          if(snapshot.hasData && snapshot.data>0){
                                            return Text("${snapshot.data} ${AppStrings.atchmnt_synced}",style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white
                                            ),);
                                          }
                                          return Container();
                                        }
                                    ),
                                    StreamBuilder<int>(
                                        stream: locator<ChecklistSyncCount>().checklistSyncCountStream,
                                        builder: (context, snapshot) {
                                          if(snapshot.hasData && snapshot.data>0){
                                            return Text("${snapshot.data} ${AppStrings.chklst_synced}",style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white
                                            ),);
                                          }
                                          return Container();
                                        }
                                    ),
                                    StreamBuilder<int>(
                                        stream: locator<NoteSyncCountBloc>().noteSyncCountStream,
                                        builder: (context, snapshot) {
                                          if(snapshot.hasData && snapshot.data>0){
                                            return Text("${snapshot.data} ${AppStrings.notes_synced}",style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white
                                            ),);
                                          }
                                          return Container();
                                        }
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            StreamBuilder<bool>(
                                initialData: true,
                                stream: locator<ShowHideSyncProgressBloc>().showHideSyncProgressStream,
                                builder: (context, snapshot) {
                                  if(snapshot.hasData && !snapshot.data){
                                    return Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          width: 80,
                                          height: 50,
                                          child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              padding: EdgeInsets.all(0),
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),

                                              primary: Colors.white,
                                            ).copyWith(overlayColor: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
                                              if (states.contains(MaterialState.pressed)) {
                                                return MyColors.loginButtonReveleColor.withOpacity(0.2);
                                              }
                                              return null;
                                            })),
                                            onPressed: () async {
                                              debugPrint('hide Sync Dialog');
                                              DialogMgr.hideSyncDialog();
                                              // DialogMgr.hideSyncDialog();
                                            },
                                            child: Text(AppStrings.sluit, style: TextStyle(
                                                fontSize: 17,color: MyColors.colorConvert('#5BB0EC')
                                            )),
                                          ),
                                        ),
                                      ],
                                    );
                                  }
                                  return Container();
                                }
                            ),

                          ],
                        ),
                      )),


                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
